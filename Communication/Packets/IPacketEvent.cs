﻿using Pylenor.Communication.Packets.Incoming;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets
{
    internal interface IPacketEvent
    {
        void Parse(Session session, ClientPacket packet);
    }
}