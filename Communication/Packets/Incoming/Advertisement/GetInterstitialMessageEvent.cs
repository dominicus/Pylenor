﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Advertisement
{
    internal class GetInterstitialMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var ad = PylenorEnvironment.GetGame().GetAdvertisementManager().GetRandomRoomAdvertisement();

            var serverPacket = new ServerPacket(258);

            if (ad == null)
            {
                serverPacket.WriteString("");
                serverPacket.WriteString("");
            }
            else
            {
                serverPacket.WriteString(ad.AdImage);
                serverPacket.WriteString(ad.AdLink);

                ad.OnView();
            }

            session.SendPacket(serverPacket);
        }
    }
}