﻿using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Advertisement
{
    internal class GetRoomAdMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().LoadingRoom <= 0 || !session.GetPlayer().LoadingChecksPassed)
            {
                return;
            }

            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().LoadingRoom);

            if (room == null)
            {
                return;
            }

            session.ClearRoom();

            var serverPacket = new ServerPacket(30);

            if (room.Model.StaticFurniMap != "")
            {
                serverPacket.WriteString(room.Model.StaticFurniMap);
            }
            else
            {
                serverPacket.WriteInteger(0);
            }

            session.SendPacket(serverPacket);

            if (room.Type == "private")
            {
                var floorItems = room.FloorItems;
                var wallItems = room.WallItems;

                serverPacket = new ServerPacket(32);
                serverPacket.WriteInteger(floorItems.Count);

                foreach (var item in floorItems)
                {
                    item.Serialize(serverPacket);
                }

                session.SendPacket(serverPacket);

                serverPacket = new ServerPacket(45);
                serverPacket.WriteInteger(wallItems.Count);

                foreach (var item in wallItems)
                {
                    item.Serialize(serverPacket);
                }

                session.SendPacket(serverPacket);
            }

            room.AddUserToRoom(session, session.GetPlayer().SpectatorMode);

            var usersToDisplay = room.UserList.Where(user => !user.IsSpectator).ToList();

            serverPacket = new ServerPacket(28);
            serverPacket.WriteInteger(usersToDisplay.Count);

            foreach (var user in usersToDisplay)
            {
                user.Serialize(serverPacket);
            }

            session.SendPacket(serverPacket);

            //GXI
            serverPacket = new ServerPacket(472);
            serverPacket.WriteBoolean(room.Hidewall);
            session.SendPacket(serverPacket);

            if (room.Type == "public")
            {
                serverPacket = new ServerPacket(471);
                serverPacket.WriteBoolean(false);
                serverPacket.WriteString(room.ModelName);
                serverPacket.WriteBoolean(false);
                session.SendPacket(serverPacket);
            }
            else if (room.Type == "private")
            {
                serverPacket = new ServerPacket(471);
                serverPacket.WriteBoolean(true);
                serverPacket.WriteUInt(room.RoomId);

                serverPacket.WriteBoolean(room.CheckRights(session, true));

                session.SendPacket(serverPacket);

                serverPacket = new ServerPacket(454);
                serverPacket.WriteInteger(1);
                serverPacket.WriteUInt(room.RoomId);
                serverPacket.WriteInteger(0);
                serverPacket.WriteString(room.Name);
                serverPacket.WriteString(room.Owner);
                serverPacket.WriteInteger(room.State);
                serverPacket.WriteInteger(0);
                serverPacket.WriteInteger(25);
                serverPacket.WriteString(room.Description);
                serverPacket.WriteInteger(0);
                serverPacket.WriteInteger(1);
                serverPacket.WriteInteger(8228);
                serverPacket.WriteInteger(room.Category);
                serverPacket.WriteString("");
                serverPacket.WriteInteger(room.TagCount);

                foreach (var tag in room.Tags)
                {
                    serverPacket.WriteString(tag);
                }

                room.Icon.Serialize(serverPacket);
                serverPacket.WriteBoolean(false);
                session.SendPacket(serverPacket);
            }

            var updates = room.SerializeStatusUpdates(true);

            if (updates != null)
            {
                session.SendPacket(updates);
            }

            foreach (var user in room.UserList.Where(user => !user.IsSpectator))
            {
                if (user.IsDancing)
                {
                    serverPacket = new ServerPacket(480);
                    serverPacket.WriteInteger(user.VirtualId);
                    serverPacket.WriteInteger(user.DanceId);
                    session.SendPacket(serverPacket);
                }

                if (user.IsAsleep)
                {
                    serverPacket = new ServerPacket(486);
                    serverPacket.WriteInteger(user.VirtualId);
                    serverPacket.WriteBoolean(true);
                    session.SendPacket(serverPacket);
                }

                if (user.CarryItemId > 0 && user.CarryTimer > 0)
                {
                    serverPacket = new ServerPacket(482);
                    serverPacket.WriteInteger(user.VirtualId);
                    serverPacket.WriteInteger(user.CarryTimer);
                    session.SendPacket(serverPacket);
                }

                if (user.IsBot) continue;
                if (user.GetClient().GetPlayer() == null ||
                    user.GetClient().GetPlayer().GetAvatarEffectsInventoryComponent() == null ||
                    user.GetClient().GetPlayer().GetAvatarEffectsInventoryComponent().CurrentEffect < 1)
                    continue;
                serverPacket = new ServerPacket(485);
                serverPacket.WriteInteger(user.VirtualId);
                serverPacket
                    .WriteInteger(
                        user.GetClient().GetPlayer().GetAvatarEffectsInventoryComponent().CurrentEffect);
                session.SendPacket(serverPacket);
            }
        }
    }
}