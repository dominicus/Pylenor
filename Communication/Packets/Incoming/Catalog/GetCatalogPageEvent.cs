﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Catalog
{
    internal class GetCatalogPageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var page = PylenorEnvironment.GetGame().GetCatalog().GetPage(packet.PopWiredInt32());

            if (page == null || !page.Enabled || !page.Visible ||
                page.MinRank > session.GetPlayer().Rank)
            {
                return;
            }

            if (page.ClubOnly && !session.GetPlayer().GetSubscriptionManager().HasSubscription("habbo_club"))
            {
                session.SendNotif("This page is for Club members only!");
                return;
            }

            session.SendPacket(PylenorEnvironment.GetGame().GetCatalog().SerializePage(page));

            if (page.Layout != "recycler") return;
            var serverPacket = new ServerPacket(ServerPacketHeader.RecyclerStatusMessageComposer);
            serverPacket.WriteBoolean(true);
            serverPacket.WriteBoolean(false);
            session.SendPacket(serverPacket);
        }
    }
}