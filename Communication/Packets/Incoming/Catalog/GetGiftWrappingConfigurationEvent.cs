﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Catalog
{
    internal class GetGiftWrappingConfigurationEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(620);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(10);
            for (var i = 187; i < 191; i++)
            {
                serverPacket.WriteInteger(i);
            }
            serverPacket.WriteInteger(7);
            serverPacket.WriteInteger(0);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(2);
            serverPacket.WriteInteger(3);
            serverPacket.WriteInteger(4);
            serverPacket.WriteInteger(5);
            serverPacket.WriteInteger(6);
            serverPacket.WriteInteger(11);
            serverPacket.WriteInteger(0);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(2);
            serverPacket.WriteInteger(3);
            serverPacket.WriteInteger(4);
            serverPacket.WriteInteger(5);
            serverPacket.WriteInteger(6);
            serverPacket.WriteInteger(7);
            serverPacket.WriteInteger(8);
            serverPacket.WriteInteger(9);
            serverPacket.WriteInteger(10);
            serverPacket.WriteInteger(1);
            session.SendPacket(serverPacket);
        }
    }
}