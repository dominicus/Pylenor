﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Catalog
{
    internal class GetIsOfferGiftableEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var id = packet.PopWiredUInt();

            var item = PylenorEnvironment.GetGame().GetCatalog().GetItem(id);

            if (item == null)
            {
                return;
            }

            var serverPacket = new ServerPacket(622);
            serverPacket.WriteUInt(item.Id);
            serverPacket.WriteBoolean(item.GetBaseItem().AllowGift);
            session.SendPacket(serverPacket);
        }
    }
}