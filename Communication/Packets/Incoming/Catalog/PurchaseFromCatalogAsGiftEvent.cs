﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Catalog
{
    internal class PurchaseFromCatalogAsGiftEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var pageId = packet.PopWiredInt32();
            var itemId = packet.PopWiredUInt();
            var extraData = packet.PopString();
            var giftUser = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var giftMessage = PylenorEnvironment.FilterInjectionChars(packet.PopString());

            PylenorEnvironment.GetGame()
                .GetCatalog()
                .HandlePurchase(session, pageId, itemId, extraData, true, giftUser, giftMessage);
        }
    }
}