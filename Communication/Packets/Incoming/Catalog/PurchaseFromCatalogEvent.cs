﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Catalog
{
    internal class PurchaseFromCatalogEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var pageId = packet.PopWiredInt32();
            var itemId = packet.PopWiredUInt();
            var extraData = packet.PopString();

            PylenorEnvironment.GetGame().GetCatalog().HandlePurchase(session, pageId, itemId, extraData, false, "", "");
        }
    }
}