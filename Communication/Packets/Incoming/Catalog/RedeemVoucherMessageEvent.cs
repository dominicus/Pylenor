﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Catalog
{
    internal class RedeemVoucherMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            PylenorEnvironment.GetGame()
                .GetCatalog()
                .GetVoucherHandler()
                .TryRedeemVoucher(session, packet.PopString());
        }
    }
}