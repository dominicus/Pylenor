﻿using System;
using System.Text;
using Pylenor.Util;

namespace Pylenor.Communication.Packets.Incoming
{
    public class ClientPacket
    {
        private readonly byte[] _body;
        private int _pointer;

        public ClientPacket(uint messageId, byte[] body)
        {
            if (body == null)
            {
                body = new byte[0];
            }

            Id = messageId;
            _body = body;

            _pointer = 0;
        }

        public uint Id { get; }

        public int Length => _body.Length;

        public int RemainingLength => _body.Length - _pointer;

        public string Header => Encoding.Default.GetString(Base64Encoding.EncodeUInt32(Id, 2));

        public override string ToString()
        {
            return Header + PylenorEnvironment.GetDefaultEncoding().GetString(_body);
        }

        public void ResetPointer()
        {
            _pointer = 0;
        }

        public void AdvancePointer(int i)
        {
            _pointer += i;
        }

        public string GetBody()
        {
            return Encoding.Default.GetString(_body);
        }

        public byte[] ReadBytes(int bytes)
        {
            if (bytes > RemainingLength)
            {
                bytes = RemainingLength;
            }

            var data = new byte[bytes];

            for (var i = 0; i < bytes; i++)
            {
                data[i] = _body[_pointer++];
            }

            return data;
        }

        public byte[] PlainReadBytes(int bytes)
        {
            if (bytes > RemainingLength)
            {
                bytes = RemainingLength;
            }

            var data = new byte[bytes];

            for (int x = 0, y = _pointer; x < bytes; x++, y++)
            {
                data[x] = _body[y];
            }

            return data;
        }

        public byte[] ReadFixedValue()
        {
            var len = Base64Encoding.DecodeInt32(ReadBytes(2));
            return ReadBytes(len);
        }

        public bool PopBase64Boolean()
        {
            return RemainingLength > 0 && _body[_pointer++] == Base64Encoding.Positive;
        }

        public int PopInt32()
        {
            return Base64Encoding.DecodeInt32(ReadBytes(2));
        }

        public uint PopUInt32()
        {
            return (uint) PopInt32();
        }

        public string PopString()
        {
            return PopString(PylenorEnvironment.GetDefaultEncoding());
        }

        public string PopString(Encoding encoding)
        {
            return encoding.GetString(ReadFixedValue()).Replace(Convert.ToChar(1), ' ');
        }

        public int PopFixedInt32()
        {
            int i;

            var s = PopString(Encoding.ASCII);

            int.TryParse(s, out i);

            return i;
        }

        public uint PopFixedUInt32()
        {
            return (uint) PopFixedInt32();
        }

        public bool PopWiredBoolean()
        {
            return RemainingLength > 0 && _body[_pointer++] == WireEncoding.Positive;
        }

        public int PopWiredInt32()
        {
            if (RemainingLength < 1)
            {
                return 0;
            }

            var data = PlainReadBytes(WireEncoding.MaxIntegerByteAmount);

            int totalBytes;
            var i = WireEncoding.DecodeInt32(data, out totalBytes);

            _pointer += totalBytes;

            return i;
        }

        public uint PopWiredUInt()
        {
            return (uint) PopWiredInt32();
        }
    }
}