﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class AcceptBuddyMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetMessenger() == null)
            {
                return;
            }

            var amount = packet.PopWiredInt32();

            for (var i = 0; i < amount; i++)
            {
                var requestId = packet.PopWiredUInt();

                var messRequest = session.GetPlayer().GetMessenger().GetRequest(requestId);

                if (messRequest == null)
                {
                    continue;
                }

                if (messRequest.To != session.GetPlayer().Id)
                {
                    return;
                }

                if (!session.GetPlayer().GetMessenger().FriendshipExists(messRequest.To, messRequest.From))
                {
                    session.GetPlayer().GetMessenger().CreateFriendship(messRequest.From);
                }

                session.GetPlayer().GetMessenger().HandleRequest(requestId);
            }
        }
    }
}