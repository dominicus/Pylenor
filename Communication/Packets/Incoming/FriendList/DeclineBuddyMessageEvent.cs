﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class DeclineBuddyMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetMessenger() == null)
            {
                return;
            }
            var mode = packet.PopWiredInt32();
            var amount = packet.PopWiredInt32();

            if (mode == 0 && amount == 1)
            {
                var requestId = packet.PopWiredUInt();

                session.GetPlayer().GetMessenger().HandleRequest(requestId);
            }
            else if (mode == 1)
            {
                session.GetPlayer().GetMessenger().HandleAllRequests();
            }
        }
    }
}