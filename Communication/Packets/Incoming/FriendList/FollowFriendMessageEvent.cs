﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class FollowFriendMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var buddyId = packet.PopWiredUInt();

            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(buddyId);

            if (client?.GetPlayer() == null || !client.GetPlayer().InRoom)
            {
                return;
            }

            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(client.GetPlayer().CurrentRoomId);

            if (room == null)
            {
                return;
            }
            var serverPacket = new ServerPacket(286);
            serverPacket.WriteBoolean(room.IsPublic);
            serverPacket.WriteUInt(client.GetPlayer().CurrentRoomId);
            session.SendPacket(serverPacket);

            if (!room.IsPublic)
            {
                session.PrepareRoom(room.RoomId, "");
            }
        }
    }
}