﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class HabboSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetMessenger() == null)
            {
                return;
            }

            session.SendPacket(session.GetPlayer().GetMessenger().PerformSearch(packet.PopString()));
        }
    }
}