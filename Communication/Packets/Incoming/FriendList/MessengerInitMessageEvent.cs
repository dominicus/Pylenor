﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class MessengerInitMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.GetPlayer().InitMessenger();
        }
    }
}