﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class RemoveBuddyMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetMessenger() == null)
            {
                return;
            }

            var requests = packet.PopWiredInt32();

            for (var i = 0; i < requests; i++)
            {
                session.GetPlayer().GetMessenger().DestroyFriendship(packet.PopWiredUInt());
            }
        }
    }
}