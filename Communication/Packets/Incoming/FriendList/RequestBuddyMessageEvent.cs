﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class RequestBuddyMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetMessenger() == null)
            {
                return;
            }

            session.GetPlayer().GetMessenger().RequestBuddy(packet.PopString());
        }
    }
}