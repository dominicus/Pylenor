﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class SendMsgMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var userId = packet.PopWiredUInt();
            var message = PylenorEnvironment.FilterInjectionChars(packet.PopString());

            if (session.GetPlayer().GetMessenger() == null)
            {
                return;
            }

            session.GetPlayer().GetMessenger().SendInstantMessage(userId, message);
        }
    }
}