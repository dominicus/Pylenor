﻿using System.Collections.Generic;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.FriendList
{
    internal class SendRoomInviteMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var count = packet.PopWiredInt32();

            var userIds = new List<uint>();

            for (var i = 0; i < count; i++)
            {
                userIds.Add(packet.PopWiredUInt());
            }

            var message = PylenorEnvironment.FilterInjectionChars(packet.PopString(), true);

            var p = new ServerPacket(135);
            p.WriteUInt(session.GetPlayer().Id);
            p.WriteString(message);

            foreach (var client in from id in userIds
                where session.GetPlayer().GetMessenger().FriendshipExists(session.GetPlayer().Id, id)
                select PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(id))
            {
                if (client == null)
                {
                    return;
                }

                client.SendPacket(p);
            }
        }
    }
}