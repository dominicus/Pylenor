﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Handshake
{
    internal class InfoRetrieveMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var userInfoComposer = new ServerPacket(5);
            userInfoComposer.WriteString(session.GetPlayer().Id.ToString());
            userInfoComposer.WriteString(session.GetPlayer().Username);
            userInfoComposer.WriteString(session.GetPlayer().Look);
            userInfoComposer.WriteString(session.GetPlayer().Gender.ToUpper());
            userInfoComposer.WriteString(session.GetPlayer().Motto);
            userInfoComposer.WriteString(session.GetPlayer().RealName);
            userInfoComposer.WriteInteger(0);
            userInfoComposer.WriteString("");
            userInfoComposer.WriteInteger(0);
            userInfoComposer.WriteInteger(0);
            userInfoComposer.WriteInteger(session.GetPlayer().Respect);
            userInfoComposer.WriteInteger(session.GetPlayer().DailyRespectPoints);
            userInfoComposer.WriteInteger(session.GetPlayer().DailyPetRespectPoints);
            session.SendPacket(userInfoComposer);
        }
    }
}