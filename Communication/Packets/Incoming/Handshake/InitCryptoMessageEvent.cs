﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Handshake
{
    internal class InitCryptoMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var sessionParams = new ServerPacket(257);
            sessionParams.WriteString(string.Empty);
            sessionParams.WriteInteger(0);
            session.SendPacket(sessionParams);
        }
    }
}