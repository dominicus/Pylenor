﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Handshake
{
    internal class SsoTicketMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer() == null)
            {
                session.Login(packet.PopString());
            }
            else
            {
                session.SendNotif("You are already logged in!");
            }
        }
    }
}