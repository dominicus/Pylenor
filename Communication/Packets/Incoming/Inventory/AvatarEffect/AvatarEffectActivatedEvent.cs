﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.AvatarEffect
{
    internal class AvatarEffectActivatedEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.GetPlayer().GetAvatarEffectsInventoryComponent().EnableEffect(packet.PopWiredInt32());
        }
    }
}