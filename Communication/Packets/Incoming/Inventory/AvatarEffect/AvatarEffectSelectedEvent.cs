﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.AvatarEffect
{
    internal class AvatarEffectSelectedEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.GetPlayer().GetAvatarEffectsInventoryComponent().ApplyEffect(packet.PopWiredInt32());
        }
    }
}