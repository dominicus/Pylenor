﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Badges
{
    internal class GetBadgesEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(session.GetPlayer().GetBadgeComponent().Serialize());
        }
    }
}