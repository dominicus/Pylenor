﻿using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Badges
{
    internal class SetActivatedBadgesEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.GetPlayer().GetBadgeComponent().ResetSlots();

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE user_badges SET badge_slot = '0' WHERE user_id = '" +
                                      session.GetPlayer().Id + "'");
            }

            while (packet.RemainingLength > 0)
            {
                var slot = packet.PopWiredInt32();
                var badge = packet.PopString();

                if (badge.Length == 0)
                {
                    continue;
                }

                if (!session.GetPlayer().GetBadgeComponent().HasBadge(badge) || slot < 1 || slot > 5)
                {
                    // zomg haxx0r
                    return;
                }

                session.GetPlayer().GetBadgeComponent().GetBadge(badge).Slot = slot;

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("slotid", slot);
                    dbClient.AddParamWithValue("badge", badge);
                    dbClient.AddParamWithValue("userid", session.GetPlayer().Id);
                    dbClient.ExecuteQuery(
                        "UPDATE user_badges SET badge_slot = @slotid WHERE badge_id = @badge AND user_id = @userid LIMIT 1");
                }
            }

            var message = new ServerPacket(228);
            message.WriteUInt(session.GetPlayer().Id);
            message.WriteInteger(session.GetPlayer().GetBadgeComponent().EquippedCount);

            foreach (var badge in session.GetPlayer().GetBadgeComponent().BadgeList.Where(badge => badge.Slot > 0))
            {
                message.WriteInteger(badge.Slot);
                message.WriteString(badge.Code);
            }

            if (session.GetPlayer().InRoom &&
                PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId) != null)
            {
                PylenorEnvironment.GetGame()
                    .GetRoomManager()
                    .GetRoom(session.GetPlayer().CurrentRoomId)
                    .SendMessage(message);
            }
            else
            {
                session.SendPacket(message);
            }
        }
    }
}