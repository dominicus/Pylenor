﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Pets
{
    internal class GetPetInventoryEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetInventoryComponent() == null)
            {
                return;
            }

            session.SendPacket(session.GetPlayer().GetInventoryComponent().SerializePetInventory());
        }
    }
}