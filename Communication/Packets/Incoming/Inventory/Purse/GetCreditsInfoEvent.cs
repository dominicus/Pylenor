﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Purse
{
    internal class GetCreditsInfoEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.GetPlayer().UpdateCreditsBalance(false);
            session.GetPlayer().UpdateActivityPointsBalance(false);
        }
    }
}