﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Trading
{
    internal class AcceptTradingEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CanTradeInRoom)
            {
                return;
            }

            var trade = room.GetUserTrade(session.GetPlayer().Id);

            trade?.Accept(session.GetPlayer().Id);
        }
    }
}