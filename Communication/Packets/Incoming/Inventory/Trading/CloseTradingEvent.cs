﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Trading
{
    internal class CloseTradingEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CanTradeInRoom)
            {
                return;
            }

            room.TryStopTrade(session.GetPlayer().Id);
        }
    }
}