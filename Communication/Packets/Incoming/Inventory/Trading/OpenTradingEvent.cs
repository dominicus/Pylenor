﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Trading
{
    internal class OpenTradingEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CanTradeInRoom)
            {
                return;
            }

            var user = room.GetRoomUserByHabbo(session.GetPlayer().Id);
            var user2 = room.GetRoomUserByVirtualId(packet.PopWiredInt32());

            room.TryStartTrade(user, user2);
        }
    }
}