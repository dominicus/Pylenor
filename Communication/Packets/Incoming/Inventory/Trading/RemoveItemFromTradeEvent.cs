﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Inventory.Trading
{
    internal class RemoveItemFromTradeEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CanTradeInRoom)
            {
                return;
            }

            var trade = room.GetUserTrade(session.GetPlayer().Id);
            var item = session.GetPlayer().GetInventoryComponent().GetItem(packet.PopWiredUInt());

            if (trade == null || item == null)
            {
                return;
            }

            trade.TakeBackItem(session.GetPlayer().Id, item);
        }
    }
}