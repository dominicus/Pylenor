﻿using System.Data;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class BuyOfferMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var itemId = packet.PopWiredUInt();
            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                row =
                    dbClient.ReadDataRow("SELECT * FROM catalog_marketplace_offers WHERE offer_id = '" + itemId +
                                         "' LIMIT 1");
            }

            if (row == null || (string) row["state"] != "1" ||
                (double) row["timestamp"] <=
                PylenorEnvironment.GetGame().GetCatalog().GetMarketplace().FormatTimestamp())
            {
                session.SendNotif("Sorry, this offer has expired.");
                return;
            }

            var item = PylenorEnvironment.GetGame().GetItemManager().GetItem((uint) row["item_id"]);

            if (item == null)
            {
                return;
            }

            if ((int) row["total_price"] >= 1)
            {
                session.GetPlayer().Credits -= (int) row["total_price"];
                session.GetPlayer().UpdateCreditsBalance(true);
            }

            PylenorEnvironment.GetGame().GetCatalog().DeliverItems(session, item, 1, (string) row["extra_data"]);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE catalog_marketplace_offers SET state = '2' WHERE offer_id = '" + itemId +
                                      "' LIMIT 1");
            }

            var serverPacket = new ServerPacket(67);
            serverPacket.WriteUInt(item.ItemId);
            serverPacket.WriteString(item.Name);
            serverPacket.WriteInteger(0);
            serverPacket.WriteInteger(0);
            serverPacket.WriteInteger(1);
            serverPacket.WriteString(item.Type.ToLower());
            serverPacket.WriteInteger(item.Sprite);
            serverPacket.WriteString("");
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(-1);
            serverPacket.WriteString("");
            session.SendPacket(serverPacket);

            session.SendPacket(PylenorEnvironment.GetGame()
                .GetCatalog()
                .GetMarketplace()
                .SerializeOffers(-1, -1, "", 1));
        }
    }
}