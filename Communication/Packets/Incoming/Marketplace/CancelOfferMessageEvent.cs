﻿using System.Data;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class CancelOfferMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var itemId = packet.PopWiredUInt();
            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                row =
                    dbClient.ReadDataRow("SELECT * FROM catalog_marketplace_offers WHERE offer_id = '" + itemId +
                                         "' LIMIT 1");
            }

            if (row == null || (uint) row["user_id"] != session.GetPlayer().Id || (string) row["state"] != "1")
            {
                return;
            }

            var item = PylenorEnvironment.GetGame().GetItemManager().GetItem((uint) row["item_id"]);

            if (item == null)
            {
                return;
            }

            PylenorEnvironment.GetGame().GetCatalog().DeliverItems(session, item, 1, (string) row["extra_data"]);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM catalog_marketplace_offers WHERE offer_id = '" + itemId + "' LIMIT 1");
            }

            var serverPacket = new ServerPacket(614);
            serverPacket.WriteUInt((uint) row["offer_id"]);
            serverPacket.WriteBoolean(true);
            session.SendPacket(serverPacket);
        }
    }
}