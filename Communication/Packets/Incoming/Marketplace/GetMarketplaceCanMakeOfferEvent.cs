﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class GetMarketplaceCanMakeOfferEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(611);
            serverPacket.WriteBoolean(true);
            serverPacket.WriteInteger(2);
            session.SendPacket(serverPacket);
        }
    }
}