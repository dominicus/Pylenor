﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class GetMarketplaceConfigurationMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(612);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(5);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(10000);
            serverPacket.WriteInteger(48);
            serverPacket.WriteInteger(7);
            session.SendPacket(serverPacket);
        }
    }
}