﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class GetOffersMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var minPrice = packet.PopWiredInt32();
            var maxPrice = packet.PopWiredInt32();
            var searchQuery = packet.PopString();
            var filterMode = packet.PopWiredInt32();

            session.SendPacket(PylenorEnvironment.GetGame()
                .GetCatalog()
                .GetMarketplace()
                .SerializeOffers(minPrice, maxPrice, searchQuery, filterMode));
        }
    }
}