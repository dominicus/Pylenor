﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class GetOwnOffersMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(
                PylenorEnvironment.GetGame().GetCatalog().GetMarketplace().SerializeOwnOffers(session.GetPlayer().Id));
        }
    }
}