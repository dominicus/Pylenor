﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class MakeOfferMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().GetInventoryComponent() == null)
            {
                return;
            }

            var sellingPrice = packet.PopWiredInt32();
            packet.PopWiredInt32();
            var itemId = packet.PopWiredUInt();

            var item = session.GetPlayer().GetInventoryComponent().GetItem(itemId);

            if (item == null || !item.GetBaseItem().AllowTrade)
            {
                return;
            }

            PylenorEnvironment.GetGame().GetCatalog().GetMarketplace().SellItem(session, item.Id, sellingPrice);
        }
    }
}