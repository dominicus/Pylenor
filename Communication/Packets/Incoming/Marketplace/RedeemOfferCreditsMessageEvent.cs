﻿using System.Data;
using System.Linq;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Marketplace
{
    internal class RedeemOfferCreditsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            DataTable results;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                results =
                    dbClient.ReadDataTable("SELECT asking_price FROM catalog_marketplace_offers WHERE user_id = '" +
                                           session.GetPlayer().Id + "' AND state = '2';");
            }

            if (results == null)
            {
                return;
            }

            var profit = results.Rows.Cast<DataRow>().Sum(row => (int) row["asking_price"]);

            if (profit >= 1)
            {
                session.GetPlayer().Credits += profit;
                session.GetPlayer().UpdateCreditsBalance(true);
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM catalog_marketplace_offers WHERE user_id = '" +
                                      session.GetPlayer().Id +
                                      "' AND state = '2';");
            }
        }
    }
}