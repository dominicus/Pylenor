﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class AddFavouriteRoomMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var id = packet.PopWiredUInt();

            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(id);

            if (data == null || session.GetPlayer().FavoriteRooms.Count >= 30 ||
                session.GetPlayer().FavoriteRooms.Contains(id) || data.Type == "public")
            {
                var genericErrorComposer = new ServerPacket(ServerPacketHeader.GenericErrorComposer);
                genericErrorComposer.WriteInteger(-9001);
                session.SendPacket(genericErrorComposer);

                return;
            }
            var favouriteChangedComposer = new ServerPacket(ServerPacketHeader.FavouriteChangedComposer);
            favouriteChangedComposer.WriteUInt(id);
            favouriteChangedComposer.WriteBoolean(true);
            session.SendPacket(favouriteChangedComposer);

            session.GetPlayer().FavoriteRooms.Add(id);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("INSERT INTO user_favorites (user_id,room_id) VALUES ('" + session.GetPlayer().Id +
                                      "','" + id + "')");
            }
        }
    }
}