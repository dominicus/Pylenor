﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Core;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class CanCreateRoomMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(512);
            if (session.GetPlayer().OwnedRooms.Count > ServerConfiguration.RoomUserLimit)
            {
                serverPacket.WriteBoolean(true); // true = show error with number below
                serverPacket.WriteInteger(ServerConfiguration.RoomUserLimit);
            }
            else
            {
                serverPacket.WriteBoolean(false);
            }
            session.SendPacket(serverPacket);
        }
    }
}