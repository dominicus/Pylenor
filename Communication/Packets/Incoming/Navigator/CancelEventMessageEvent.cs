﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class CancelEventMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true) || room.Event == null)
            {
                return;
            }

            room.Event = null;

            var message = new ServerPacket(370);
            message.WriteString("-1");
            room.SendMessage(message);
        }
    }
}