﻿using System.Collections.Generic;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class CreateEventMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true) || room.Event != null || room.State != 0)
            {
                return;
            }

            var category = packet.PopWiredInt32();
            var name = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var descr = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var tagCount = packet.PopWiredInt32();

            room.Event = new RoomEvent(room.RoomId, name, descr, category, null) {Tags = new List<string>()};

            for (var i = 0; i < tagCount; i++)
            {
                room.Event.Tags.Add(PylenorEnvironment.FilterInjectionChars(packet.PopString()));
            }

            room.SendMessage(room.Event.Serialize(session));
        }
    }
}