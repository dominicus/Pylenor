﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class CreateFlatMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var roomName = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var modelName = packet.PopString();
            packet.PopString();
            // unused, room open by default on creation. may be added in later build of Player?

            var newRoom = PylenorEnvironment.GetGame().GetRoomManager().CreateRoom(session, roomName, modelName);

            if (newRoom == null) return;
            var serverPacket = new ServerPacket(59);
            serverPacket.WriteUInt(newRoom.Id);
            serverPacket.WriteString(newRoom.Name);
            session.SendPacket(serverPacket);
        }
    }
}