﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class DeleteFavouriteRoomMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var id = packet.PopWiredUInt();

            session.GetPlayer().FavoriteRooms.Remove(id);

            var favouriteChangedComposer = new ServerPacket(ServerPacketHeader.FavouriteChangedComposer);
            favouriteChangedComposer.WriteUInt(id);
            favouriteChangedComposer.WriteBoolean(false);
            session.SendPacket(favouriteChangedComposer);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM user_favorites WHERE user_id = '" + session.GetPlayer().Id +
                                      "' AND room_id = '" + id + "' LIMIT 1");
            }
        }
    }
}