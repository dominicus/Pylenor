﻿using System.Collections.Generic;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class EditEventMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true) || room.Event == null)
            {
                return;
            }

            var category = packet.PopWiredInt32();
            var name = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var descr = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var tagCount = packet.PopWiredInt32();

            room.Event.Category = category;
            room.Event.Name = name;
            room.Event.Description = descr;
            room.Event.Tags = new List<string>();

            for (var i = 0; i < tagCount; i++)
            {
                room.Event.Tags.Add(PylenorEnvironment.FilterInjectionChars(packet.PopString()));
            }

            room.SendMessage(room.Event.Serialize(session));
        }
    }
}