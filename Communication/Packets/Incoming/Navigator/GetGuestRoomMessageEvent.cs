﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class GetGuestRoomMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var roomId = packet.PopWiredUInt();
            var loadingState = packet.PopWiredBoolean();
            var following = packet.PopWiredBoolean();

            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(roomId);

            if (data == null)
            {
                return;
            }
            var getGuestRoomResultComposer = new ServerPacket(ServerPacketHeader.GetGuestRoomResultComposer);
            getGuestRoomResultComposer.WriteBoolean(loadingState);
            data.Serialize(getGuestRoomResultComposer, false);
            getGuestRoomResultComposer.WriteBoolean(following);
            getGuestRoomResultComposer.WriteBoolean(loadingState);
            session.SendPacket(getGuestRoomResultComposer);
        }
    }
}