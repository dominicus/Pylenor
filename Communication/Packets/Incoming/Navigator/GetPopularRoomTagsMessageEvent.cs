﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class GetPopularRoomTagsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(PylenorEnvironment.GetGame().GetNavigator().SerializePopularRoomTags());
        }
    }
}