﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class GetPublicSpaceCastLibsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var id = packet.PopWiredUInt();

            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(id);

            if (data == null || data.Type != "public")
            {
                return;
            }

            var serverPacket = new ServerPacket(453);
            serverPacket.WriteUInt(data.Id);
            serverPacket.WriteString(data.CCTs);
            serverPacket.WriteUInt(data.Id);
            session.SendPacket(serverPacket);
        }
    }
}