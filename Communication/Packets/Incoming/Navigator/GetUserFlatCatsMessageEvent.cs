﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class GetUserFlatCatsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(PylenorEnvironment.GetGame().GetNavigator().SerializeFlatCategories());
        }
    }
}