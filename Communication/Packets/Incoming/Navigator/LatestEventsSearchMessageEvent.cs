﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class LatestEventsSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var category = int.Parse(packet.PopString());

            session.SendPacket(PylenorEnvironment.GetGame().GetNavigator().SerializeEventListing(session, category));
        }
    }
}