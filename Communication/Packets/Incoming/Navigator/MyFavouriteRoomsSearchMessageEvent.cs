﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class MyFavouriteRoomsSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(PylenorEnvironment.GetGame().GetNavigator().SerializeFavoriteRooms(session));
        }
    }
}