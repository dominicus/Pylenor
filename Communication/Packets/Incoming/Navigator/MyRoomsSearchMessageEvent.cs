﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class MyRoomsSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(PylenorEnvironment.GetGame().GetNavigator().SerializeRoomListing(session, -3));
        }
    }
}