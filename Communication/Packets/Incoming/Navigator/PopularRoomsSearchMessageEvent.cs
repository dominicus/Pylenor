﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class PopularRoomsSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(PylenorEnvironment.GetGame()
                .GetNavigator()
                .SerializeRoomListing(session, int.Parse(packet.PopString())));
        }
    }
}