﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class RateFlatMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || session.GetPlayer().RatedRooms.Contains(room.RoomId) || room.CheckRights(session, true))
            {
                return;
            }

            var rating = packet.PopWiredInt32();

            switch (rating)
            {
                case -1:

                    room.Score--;
                    break;

                case 1:

                    room.Score++;
                    break;

                default:

                    return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE rooms SET score = '" + room.Score + "' WHERE id = '" + room.RoomId +
                                      "' LIMIT 1");
            }

            session.GetPlayer().RatedRooms.Add(room.RoomId);

            var serverPacket = new ServerPacket(345);
            serverPacket.WriteInteger(room.Score);
            session.SendPacket(serverPacket);
        }
    }
}