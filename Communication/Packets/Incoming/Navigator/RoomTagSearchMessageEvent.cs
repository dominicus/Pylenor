﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class RoomTagSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            packet.PopWiredInt32();
            session.SendPacket(
                PylenorEnvironment.GetGame().GetNavigator().SerializeSearchResults(packet.PopString()));
        }
    }
}