﻿using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class RoomTextSearchMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            session.SendPacket(
                PylenorEnvironment.GetGame().GetNavigator().SerializeSearchResults(packet.PopString()));
        }
    }
}