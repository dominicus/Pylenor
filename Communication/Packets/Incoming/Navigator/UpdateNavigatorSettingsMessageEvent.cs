﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class UpdateNavigatorSettingsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var roomId = packet.PopWiredUInt();
            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(roomId);

            if (roomId != 0)
            {
                if (data == null || data.Owner.ToLower() != session.GetPlayer().Username.ToLower())
                {
                    return;
                }
            }

            session.GetPlayer().HomeRoom = roomId;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET home_room = '" + roomId + "' WHERE id = '" +
                                      session.GetPlayer().Id + "' LIMIT 1");
            }

            var serverPacket = new ServerPacket(455);
            serverPacket.WriteUInt(roomId);
            session.SendPacket(serverPacket);
        }
    }
}