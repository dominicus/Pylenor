﻿using System.Collections.Generic;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.Communication.Packets.Incoming.Navigator
{
    internal class UpdateRoomThumbnailMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            packet.PopWiredInt32();

            var items = new Dictionary<int, int>();

            var background = packet.PopWiredInt32();
            var topLayer = packet.PopWiredInt32();
            var amountOfItems = packet.PopWiredInt32();

            for (var i = 0; i < amountOfItems; i++)
            {
                var pos = packet.PopWiredInt32();
                var item = packet.PopWiredInt32();

                if (pos < 0 || pos > 10)
                {
                    return;
                }

                if (item < 1 || item > 27)
                {
                    return;
                }

                if (items.ContainsKey(pos))
                {
                    return;
                }

                items.Add(pos, item);
            }

            if (background < 1 || background > 24)
            {
                return;
            }

            if (topLayer < 0 || topLayer > 11)
            {
                return;
            }

            var formattedItems = new StringBuilder();
            var j = 0;

            foreach (var item in items)
            {
                if (j > 0)
                {
                    formattedItems.Append("|");
                }

                formattedItems.Append(item.Key + "," + item.Value);

                j++;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE rooms SET icon_bg = '" + background + "', icon_fg = '" + topLayer +
                                      "', icon_items = '" + formattedItems + "' WHERE id = '" + room.RoomId +
                                      "' LIMIT 1");
            }

            room.Icon = new RoomIcon(background, topLayer, items);

            var serverPacket = new ServerPacket(457);
            serverPacket.WriteUInt(room.RoomId);
            serverPacket.WriteBoolean(true);
            session.SendPacket(serverPacket);

            serverPacket.Init(456);
            serverPacket.WriteUInt(room.RoomId);
            session.SendPacket(serverPacket);

            var data = new RoomData();
            data.Fill(room);

            serverPacket.Init(454);
            serverPacket.WriteBoolean(false);
            data.Serialize(serverPacket, false);
            session.SendPacket(serverPacket);
        }
    }
}