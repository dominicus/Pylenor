﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Recycler
{
    internal class GetRecyclerPrizesMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(506);
            serverPacket.WriteInteger(5);

            for (uint i = 5; i >= 1; i--)
            {
                serverPacket.WriteUInt(i);

                if (i <= 1)
                {
                    serverPacket.WriteInteger(0);
                }
                else
                    switch (i)
                    {
                        case 2:
                            serverPacket.WriteInteger(4);
                            break;
                        case 3:
                            serverPacket.WriteInteger(40);
                            break;
                        case 4:
                            serverPacket.WriteInteger(200);
                            break;
                        default:
                            if (i >= 5)
                            {
                                serverPacket.WriteInteger(2000);
                            }
                            break;
                    }

                var rewards = PylenorEnvironment.GetGame().GetCatalog().GetEcotronRewardsForLevel(i);

                serverPacket.WriteInteger(rewards.Count);

                foreach (var reward in rewards)
                {
                    serverPacket.WriteString(reward.GetBaseItem().Type.ToLower());
                    serverPacket.WriteUInt(reward.DisplayId);
                }
            }

            session.SendPacket(serverPacket);
        }
    }
}