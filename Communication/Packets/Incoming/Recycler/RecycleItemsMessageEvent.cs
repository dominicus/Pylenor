﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Recycler
{
    internal class RecycleItemsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (!session.GetPlayer().InRoom)
            {
                return;
            }

            var itemCount = packet.PopWiredInt32();

            if (itemCount != 5)
            {
                return;
            }

            for (var i = 0; i < itemCount; i++)
            {
                var item = session.GetPlayer().GetInventoryComponent().GetItem(packet.PopWiredUInt());

                if (item != null && item.GetBaseItem().AllowRecycle)
                {
                    session.GetPlayer().GetInventoryComponent().RemoveItem(item.Id);
                }
                else
                {
                    return;
                }
            }

            var newItemId = PylenorEnvironment.GetGame().GetCatalog().GenerateItemId();
            var reward = PylenorEnvironment.GetGame().GetCatalog().GetRandomEcotronReward();

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("INSERT INTO user_items (id,user_id,base_item,extra_data) VALUES ('" + newItemId +
                                      "','" + session.GetPlayer().Id + "','1478','" + DateTime.Now.ToLongDateString() +
                                      "')");
                dbClient.ExecuteQuery("INSERT INTO user_presents (item_id,base_id,amount,extra_data) VALUES ('" +
                                      newItemId + "','" + reward.BaseId + "','1','')");
            }

            session.GetPlayer().GetInventoryComponent().UpdateItems(true);

            var serverPacket = new ServerPacket(508);
            serverPacket.WriteBoolean(true);
            serverPacket.WriteUInt(newItemId);
            session.SendPacket(serverPacket);
        }
    }
}