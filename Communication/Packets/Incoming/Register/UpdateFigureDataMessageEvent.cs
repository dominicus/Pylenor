﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Misc;

namespace Pylenor.Communication.Packets.Incoming.Register
{
    internal class UpdateFigureDataMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            if (session.GetPlayer().MutantPenalty)
            {
                session.SendNotif(
                    "Because of a penalty or restriction on your account, you are not allowed to change your look.");
                return;
            }

            var gender = packet.PopString().ToUpper();
            var look = PylenorEnvironment.FilterInjectionChars(packet.PopString());

            if (!AntiMutant.ValidateLook(look, gender))
            {
                return;
            }

            session.GetPlayer().Look = look;
            session.GetPlayer().Gender = gender.ToLower();

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("look", look);
                dbClient.AddParamWithValue("gender", gender);
                dbClient.ExecuteQuery("UPDATE users SET look = @look, gender = @gender WHERE id = '" +
                                      session.GetPlayer().Id + "' LIMIT 1");
            }

            PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(session, 1, 1);

            var avatarUpdate = new ServerPacket(266);
            avatarUpdate.WriteInteger(-1);
            avatarUpdate.WriteString(session.GetPlayer().Look);
            avatarUpdate.WriteString(session.GetPlayer().Gender.ToLower());
            avatarUpdate.WriteString(session.GetPlayer().Motto);
            session.SendPacket(avatarUpdate);

            if (!session.GetPlayer().InRoom) return;
            var room = session.GetPlayer().CurrentRoom;

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            var roomUpdate = new ServerPacket(266);
            roomUpdate.WriteInteger(user.VirtualId);
            roomUpdate.WriteString(session.GetPlayer().Look);
            roomUpdate.WriteString(session.GetPlayer().Gender.ToLower());
            roomUpdate.WriteString(session.GetPlayer().Motto);
            room.SendMessage(roomUpdate);
        }
    }
}