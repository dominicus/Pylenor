﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class ApplyEffectMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var item = session.GetPlayer().GetInventoryComponent().GetItem(packet.PopWiredUInt());

            if (item == null)
            {
                return;
            }

            var type = "floor";

            if (item.GetBaseItem().Name.ToLower().Contains("wallpaper"))
            {
                type = "wallpaper";
            }
            else if (item.GetBaseItem().Name.ToLower().Contains("landscape"))
            {
                type = "landscape";
            }

            switch (type)
            {
                case "floor":
                    room.Floor = item.ExtraData;
                    break;

                case "wallpaper":
                    room.Wallpaper = item.ExtraData;
                    break;

                case "landscape":
                    room.Landscape = item.ExtraData;
                    break;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE rooms SET " + type + " = '" + item.ExtraData + "' WHERE id = '" +
                                      room.RoomId + "' LIMIT 1");
            }

            session.GetPlayer().GetInventoryComponent().RemoveItem(item.Id);

            var message = new ServerPacket(46);
            message.WriteString(type);
            message.WriteString(item.ExtraData);
            room.SendMessage(message);
        }
    }
}