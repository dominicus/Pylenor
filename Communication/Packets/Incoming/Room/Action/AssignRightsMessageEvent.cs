﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class AssignRightsMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var userId = packet.PopWiredUInt();
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);
            var roomUser = room?.GetRoomUserByHabbo(userId);
            if (room == null || !room.CheckRights(session, true) || roomUser == null || roomUser.IsBot ||
                room.UsersWithRights.Contains(userId))
                return;
            room.UsersWithRights.Add(userId);
            using (var client = PylenorEnvironment.GetDatabase().GetClient())
            {
                client.ExecuteQuery(string.Concat("INSERT INTO room_rights (room_id,user_id) VALUES (",
                    room.RoomId, ",", userId, ")"));
            }
            var serverPacket = new ServerPacket(510);
            serverPacket.WriteUInt(room.RoomId);
            serverPacket.WriteUInt(userId);
            serverPacket.WriteString(roomUser.GetClient().GetPlayer().Username);
            session.SendPacket(serverPacket);
            roomUser.AddStatus("flatctrl", "");
            roomUser.UpdateNeeded = true;
            roomUser.GetClient().SendPacket(new ServerPacket(42));
        }
    }
}