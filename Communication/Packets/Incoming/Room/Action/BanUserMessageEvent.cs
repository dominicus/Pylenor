﻿namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class BanUserMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return; // insufficient permissions
            }

            var userId = packet.PopWiredUInt();
            var user = room.GetRoomUserByHabbo(userId);

            if (user == null || user.IsBot)
            {
                return;
            }

            if (user.GetClient().GetPlayer().HasFuse("fuse_mod"))
            {
                return;
            }

            room.AddBan(userId);
            room.RemoveUserFromRoom(user.GetClient(), true, true);
        }
    }
}