﻿namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class KickBotMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var bot = room.GetRoomUserByVirtualId(packet.PopWiredInt32());

            if (bot == null || !bot.IsBot)
            {
                return;
            }

            room.RemoveBot(bot.VirtualId, true);
        }
    }
}