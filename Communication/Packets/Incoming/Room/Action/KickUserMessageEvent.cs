﻿namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class KickUserMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null)
            {
                return;
            }

            if (!room.CheckRights(session))
            {
                return; // insufficient permissions
            }
            var user = room.GetRoomUserByHabbo(packet.PopWiredUInt());

            if (user == null || user.IsBot)
            {
                return;
            }

            if (room.CheckRights(user.GetClient(), true) || user.GetClient().GetPlayer().HasFuse("fuse_mod"))
            {
                return; // can't kick room owner or mods!
            }

            room.RemoveUserFromRoom(user.GetClient(), true, true);
        }
    }
}