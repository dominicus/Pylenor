﻿using System;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class LetUserInMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session))
            {
                return;
            }

            var name = packet.PopString();
            var result = packet.ReadBytes(1);

            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(name);

            if (client == null)
            {
                return;
            }

            if (result[0] == Convert.ToByte(65))
            {
                client.GetPlayer().LoadingChecksPassed = true;

                session.SendPacket(new ServerPacket(41));
            }
            else
            {
                session.SendPacket(new ServerPacket(131));
            }
        }
    }
}