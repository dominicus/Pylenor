﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class RemoveAllRightsMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            foreach (var userId in room.UsersWithRights)
            {
                var user = room.GetRoomUserByHabbo(userId);

                if (user != null && !user.IsBot)
                {
                    user.GetClient().SendPacket(new ServerPacket(43));
                }

                var serverPacket = new ServerPacket(511);
                serverPacket.WriteUInt(room.RoomId);
                serverPacket.WriteUInt(userId);
                session.SendPacket(serverPacket);
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM room_rights WHERE room_id = '" + room.RoomId + "'");
            }

            room.UsersWithRights.Clear();
        }
    }
}