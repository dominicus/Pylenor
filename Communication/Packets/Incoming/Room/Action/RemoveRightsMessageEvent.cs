﻿using System.Text;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Action
{
    internal class RemoveRightsMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);
            if (room == null || !room.CheckRights(session, true)) return;
            var builder = new StringBuilder();
            var length = packet.PopWiredInt32();
            for (var i = 0; i < length; i++)
            {
                if (i > 0)
                {
                    builder.Append(" OR ");
                }
                var userId = packet.PopWiredUInt();
                room.UsersWithRights.Remove(userId);
                builder.Append(string.Concat("room_id = '", room.RoomId, "' AND user_id = '", userId, "'"));
                var roomUser = room.GetRoomUserByHabbo(userId);
                if (roomUser != null && !roomUser.IsBot)
                {
                    roomUser.GetClient().SendPacket(new ServerPacket(43));
                    roomUser.RemoveStatus("flatctrl");
                    roomUser.UpdateNeeded = true;
                }
                var serverPacket = new ServerPacket(511);
                serverPacket.WriteUInt(room.RoomId);
                serverPacket.WriteUInt(userId);
                session.SendPacket(serverPacket);
            }
            using (var databaseClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                databaseClient.ExecuteQuery("DELETE FROM room_rights WHERE " + builder);
            }
        }
    }
}