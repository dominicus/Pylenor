﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Avatar
{
    internal class ChangeMottoMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var motto = PylenorEnvironment.FilterInjectionChars(packet.PopString());

            if (motto == session.GetPlayer().Motto) // Prevents spam?
            {
                return;
            }

            session.GetPlayer().Motto = motto;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("motto", motto);
                dbClient.ExecuteQuery("UPDATE users SET motto = @motto WHERE id = '" + session.GetPlayer().Id +
                                      "' LIMIT 1");
            }

            var mottoUpdate = new ServerPacket(484);
            mottoUpdate.WriteInteger(-1);
            mottoUpdate.WriteString(session.GetPlayer().Motto);
            session.SendPacket(mottoUpdate);
            if (!session.GetPlayer().InRoom) return;
            var room = session.GetPlayer().CurrentRoom;

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            var roomUpdate = new ServerPacket(266);
            roomUpdate.WriteInteger(user.VirtualId);
            roomUpdate.WriteString(session.GetPlayer().Look);
            roomUpdate.WriteString(session.GetPlayer().Gender.ToLower());
            roomUpdate.WriteString(session.GetPlayer().Motto);
            room.SendMessage(roomUpdate);
        }
    }
}