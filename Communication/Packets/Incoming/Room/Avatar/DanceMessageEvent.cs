﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Avatar
{
    internal class DanceMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            user.Unidle();

            var danceId = packet.PopWiredInt32();

            if (danceId < 0 || danceId > 4 || (!session.GetPlayer().HasFuse("fuse_use_club_dance") && danceId > 1))
            {
                danceId = 0;
            }

            if (danceId > 0 && user.CarryItemId > 0)
            {
                user.CarryItem(0);
            }

            user.DanceId = danceId;

            var danceMessage = new ServerPacket(480);
            danceMessage.WriteInteger(user.VirtualId);
            danceMessage.WriteInteger(danceId);
            room.SendMessage(danceMessage);
        }
    }
}