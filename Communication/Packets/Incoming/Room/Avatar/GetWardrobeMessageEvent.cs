﻿using System.Data;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Avatar
{
    internal class GetWardrobeMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var wardrobeMessageComposer = new ServerPacket(ServerPacketHeader.WardrobeMessageComposer);
            wardrobeMessageComposer.WriteBoolean(session.GetPlayer().HasFuse("fuse_use_wardrobe"));

            if (session.GetPlayer().HasFuse("fuse_use_wardrobe"))
            {
                DataTable wardrobeData;

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("userid", session.GetPlayer().Id);
                    wardrobeData = dbClient.ReadDataTable("SELECT * FROM user_wardrobe WHERE user_id = @userid;");
                }

                if (wardrobeData == null)
                {
                    wardrobeMessageComposer.WriteInteger(0);
                }
                else
                {
                    wardrobeMessageComposer.WriteInteger(wardrobeData.Rows.Count);

                    foreach (DataRow row in wardrobeData.Rows)
                    {
                        wardrobeMessageComposer.WriteUInt((uint) row["slot_id"]);
                        wardrobeMessageComposer.WriteString((string) row["look"]);
                        wardrobeMessageComposer.WriteString((string) row["gender"]);
                    }
                }
            }

            session.SendPacket(wardrobeMessageComposer);
        }
    }
}