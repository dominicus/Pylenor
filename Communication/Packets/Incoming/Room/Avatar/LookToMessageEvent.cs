﻿using Pylenor.HabboHotel.Pathfinding;

namespace Pylenor.Communication.Packets.Incoming.Room.Avatar
{
    internal class LookToMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            user.Unidle();

            var x = packet.PopWiredInt32();
            var y = packet.PopWiredInt32();

            if (x == user.X && y == user.Y)
            {
                return;
            }

            var rot = Rotation.Calculate(user.X, user.Y, x, y);

            user.SetRot(rot);
        }
    }
}