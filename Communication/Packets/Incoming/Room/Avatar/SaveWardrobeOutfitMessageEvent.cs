﻿using Pylenor.HabboHotel.Misc;

namespace Pylenor.Communication.Packets.Incoming.Room.Avatar
{
    internal class SaveWardrobeOutfitMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var slotId = packet.PopWiredUInt();

            var look = packet.PopString();
            var gender = packet.PopString();

            if (!AntiMutant.ValidateLook(look, gender))
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", session.GetPlayer().Id);
                dbClient.AddParamWithValue("slotid", slotId);
                dbClient.AddParamWithValue("look", look);
                dbClient.AddParamWithValue("gender", gender.ToUpper());

                dbClient.ExecuteQuery(
                    dbClient.ReadDataRow(
                        "SELECT null FROM user_wardrobe WHERE user_id = @userid AND slot_id = @slotid LIMIT 1") != null
                        ? "UPDATE user_wardrobe SET look = @look, gender = @gender WHERE user_id = @userid AND slot_id = @slotid LIMIT 1"
                        : "INSERT INTO user_wardrobe (user_id,slot_id,look,gender) VALUES (@userid,@slotid,@look,@gender)");
            }
        }
    }
}