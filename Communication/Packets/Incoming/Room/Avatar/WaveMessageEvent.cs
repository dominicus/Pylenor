﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Avatar
{
    internal class WaveMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            user.Unidle();

            user.DanceId = 0;

            var message = new ServerPacket(481);
            message.WriteInteger(user.VirtualId);
            room.SendMessage(message);
        }
    }
}