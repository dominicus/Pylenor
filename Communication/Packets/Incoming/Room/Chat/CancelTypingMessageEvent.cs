﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Chat
{
    internal class CancelTypingMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            var message = new ServerPacket(361);
            message.WriteInteger(user.VirtualId);
            message.WriteBoolean(false);
            room.SendMessage(message);
        }
    }
}