﻿namespace Pylenor.Communication.Packets.Incoming.Room.Chat
{
    internal class ChatMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            user?.Chat(session, PylenorEnvironment.FilterInjectionChars(packet.PopString()), false);
        }
    }
}