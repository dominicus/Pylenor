﻿using System;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Chat
{
    internal class WhisperMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null)
            {
                return;
            }

            if (session.GetPlayer().Muted)
            {
                session.SendNotif("You are muted.");
                return;
            }

            var Params = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var toUser = Params.Split(' ')[0];
            var message = Params.Substring(toUser.Length + 1);

            var user = room.GetRoomUserByHabbo(session.GetPlayer().Id);
            var user2 = room.GetRoomUserByHabbo(toUser);

            var tellMsg = new ServerPacket(25);
            tellMsg.WriteInteger(user.VirtualId);
            tellMsg.WriteString(message);
            tellMsg.WriteBoolean(false);

            if (!user.IsBot)
            {
                user.GetClient().SendPacket(tellMsg);
            }

            user.Unidle();

            if (user2 == null || user2.IsBot) return;
            if (!user2.GetClient().GetPlayer().MutedUsers.Contains(session.GetPlayer().Id))
            {
                user2.GetClient().SendPacket(tellMsg);
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("message",
                    "<Whisper to " + user2.GetClient().GetPlayer().Username + ">: " + message);
                dbClient.ExecuteQuery(
                    "INSERT INTO chatlogs (user_id,room_id,hour,minute,timestamp,message,user_name,full_date) VALUES ('" +
                    session.GetPlayer().Id + "','" + room.RoomId + "','" + DateTime.Now.Hour + "','" +
                    DateTime.Now.Minute + "','" + PylenorEnvironment.GetUnixTimestamp() + "',@message,'" +
                    session.GetPlayer().Username + "','" + DateTime.Now.ToLongDateString() + "')");
            }
        }
    }
}