﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class GetFurnitureAliasesMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            if (session.GetPlayer().LoadingRoom <= 0)
            {
                return;
            }
            var serverPacket = new ServerPacket(297);
            serverPacket.WriteInteger(0);
            session.SendPacket(serverPacket);
        }
    }
}