﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class GetItemDataMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var item = room?.GetItem(packet.PopWiredUInt());

            if (item == null || item.GetBaseItem().InteractionType.ToLower() != "postit")
            {
                return;
            }

            var serverPacket = new ServerPacket(48);
            serverPacket.WriteString(item.Id.ToString());
            serverPacket.WriteString(item.ExtraData);
            session.SendPacket(serverPacket);
        }
    }
}