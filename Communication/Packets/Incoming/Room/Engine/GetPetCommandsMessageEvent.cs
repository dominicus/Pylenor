﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class GetPetCommandsMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var petId = packet.PopWiredUInt();
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);
            var petUser = room.GetPet(petId);
            var serverPacket = new ServerPacket(605);
            serverPacket.WriteUInt(petId);
            var level = petUser.PetData.Level;
            serverPacket.WriteInteger(level);
            for (var i = 0; level > i;)
            {
                i++;
                serverPacket.WriteInteger(i - 1);
            }
            session.SendPacket(serverPacket);
        }
    }
}