﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class GetRoomEntryDataMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            if (session.GetPlayer().LoadingRoom <= 0)
            {
                return;
            }

            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(session.GetPlayer().LoadingRoom);

            if (data == null)
            {
                return;
            }

            if (data.Model == null)
            {
                session.SendNotif("Sorry, model data is missing from this room and therefore cannot be loaded.");
                session.SendPacket(new ServerPacket(18));
                session.ClearRoom();
                return;
            }

            session.SendPacket(data.Model.SerializeHeightmap());
            session.SendPacket(data.Model.SerializeRelativeHeightmap());
        }
    }
}