﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class MoveAvatarMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null || !user.CanWalk)
            {
                return;
            }

            var moveX = packet.PopWiredInt32();
            var moveY = packet.PopWiredInt32();

            if (moveX == user.X && moveY == user.Y)
            {
                return;
            }

            user.MoveTo(moveX, moveY);
        }
    }
}