﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class MoveObjectMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session))
            {
                return;
            }

            var item = room.GetItem(packet.PopWiredUInt());

            if (item == null)
            {
                return;
            }

            var x = packet.PopWiredInt32();
            var y = packet.PopWiredInt32();
            var rotation = packet.PopWiredInt32();
            packet.PopWiredInt32();

            room.SetFloorItem(session, item, x, y, rotation, false);
        }
    }
}