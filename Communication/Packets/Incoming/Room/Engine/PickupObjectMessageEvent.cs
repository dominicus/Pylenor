﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class PickupObjectMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            packet.PopWiredInt32();

            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var item = room.GetItem(packet.PopWiredUInt());

            if (item == null)
            {
                return;
            }

            if (item.GetBaseItem().InteractionType.ToLower() == "postit")
            {
                return; // not allowed to pick up post.its
            }

            room.RemoveFurniture(session, item.Id);
            session.GetPlayer().GetInventoryComponent().AddItem(item.Id, item.BaseItem, item.ExtraData);
            session.GetPlayer().GetInventoryComponent().UpdateItems(false);
        }
    }
}