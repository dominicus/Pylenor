﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.HabboHotel.Items;

namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class PlaceObjectMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session))
            {
                return;
            }

            var placementData = packet.PopString();
            var dataBits = placementData.Split(' ');

            dataBits[0] = dataBits[0].Contains("-") ? dataBits[0].Replace("-", "") : dataBits[0];

            var itemId = uint.Parse(dataBits[0]);

            var item = session.GetPlayer().GetInventoryComponent().GetItem(itemId);

            if (item == null)
            {
                return;
            }

            if (item.GetBaseItem().InteractionType.ToLower() == "dimmer")
            {
                if (room.ItemCountByType("dimmer") >= 1)
                {
                    session.SendNotif("You can only have one moodlight in a room.");
                    return;
                }
            }

            // Wall Item
            if (dataBits[1].StartsWith(":"))
            {
                var wallPos = room.WallPositionCheck(":" + placementData.Split(':')[1]);

                if (wallPos == null)
                {
                    var serverPacket = new ServerPacket(516);
                    serverPacket.WriteInteger(11);
                    session.SendPacket(serverPacket);

                    return;
                }

                var roomItem = new RoomItem(item.Id, room.RoomId, item.BaseItem, item.ExtraData, 0, 0, 0.0, 0, wallPos);

                if (room.SetWallItem(session, roomItem))
                {
                    session.GetPlayer().GetInventoryComponent().RemoveItem(itemId);
                }
            }
            else
            {
                var x = int.Parse(dataBits[1]);
                var y = int.Parse(dataBits[2]);
                var rot = int.Parse(dataBits[3]);

                var roomItem = new RoomItem(item.Id, room.RoomId, item.BaseItem, item.ExtraData, 0, 0, 0, 0, "");

                if (room.SetFloorItem(session, roomItem, x, y, rot, true))
                {
                    session.GetPlayer().GetInventoryComponent().RemoveItem(itemId);
                }
            }
        }
    }
}