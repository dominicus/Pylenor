﻿using Pylenor.HabboHotel.RoomBots;

namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class PlacePetMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || (!room.AllowPets && !room.CheckRights(session, true)))
            {
                return;
            }

            var petId = packet.PopWiredUInt();

            var pet = session.GetPlayer().GetInventoryComponent().GetPet(petId);

            if (pet == null || pet.PlacedInRoom)
            {
                return;
            }

            var x = packet.PopWiredInt32();
            var y = packet.PopWiredInt32();

            if (!room.CanWalk(x, y, 0, true))
            {
                return;
            }

            if (room.PetCount >= PylenorEnvironment.GetGame().GetRoomManager().MaxPetsPerRoom)
            {
                session.SendNotif("There are too many pets in this room. A room may only contain up to " +
                                  PylenorEnvironment.GetGame().GetRoomManager().MaxPetsPerRoom + " pets.");
                return;
            }

            pet.PlacedInRoom = true;
            pet.RoomId = room.RoomId;

            room.DeployBot(
                new RoomBot(pet.PetId, pet.RoomId, "pet", "freeroam", pet.Name, "", pet.Look, x, y, 0, 0, 0, 0, 0, 0),
                pet);

            if (room.CheckRights(session, true))
            {
                session.GetPlayer().GetInventoryComponent().MovePetToRoom(pet.PetId, room.RoomId);
            }
        }
    }
}