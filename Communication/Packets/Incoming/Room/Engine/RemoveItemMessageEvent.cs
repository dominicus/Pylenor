﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class RemoveItemMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var item = room.GetItem(packet.PopWiredUInt());

            if (item == null || item.GetBaseItem().InteractionType.ToLower() != "postit")
            {
                return;
            }

            room.RemoveFurniture(session, item.Id);
        }
    }
}