﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class RemovePetFromFlatMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || room.IsPublic || (!room.AllowPets && !room.CheckRights(session, true)))
            {
                return;
            }

            var petId = packet.PopWiredUInt();
            var petUser = room.GetPet(petId);

            if (petUser?.PetData == null || petUser.PetData.OwnerId != session.GetPlayer().Id)
            {
                return;
            }

            session.GetPlayer().GetInventoryComponent().AddPet(petUser.PetData);
            room.RemoveBot(petUser.VirtualId, false);
        }
    }
}