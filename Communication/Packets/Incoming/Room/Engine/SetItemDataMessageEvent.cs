﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class SetItemDataMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var item = room?.GetItem(packet.PopWiredUInt());

            if (item == null || item.GetBaseItem().InteractionType.ToLower() != "postit")
            {
                return;
            }

            var data = packet.PopString();
            var color = data.Split(' ')[0];
            var text = PylenorEnvironment.FilterInjectionChars(data.Substring(color.Length + 1), true);

            if (!room.CheckRights(session))
            {
                if (!data.StartsWith(item.ExtraData))
                {
                    return; // we can only ADD stuff! older stuff changed, this is not allowed
                }
            }

            switch (color)
            {
                case "FFFF33":
                case "FF9CFF":
                case "9CCEFF":
                case "9CFF9C":

                    break;

                default:

                    return; // invalid color
            }

            item.ExtraData = color + " " + text;
            item.UpdateState(true, true);
        }
    }
}