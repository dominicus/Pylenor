﻿namespace Pylenor.Communication.Packets.Incoming.Room.Engine
{
    internal class UseFurnitureMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var item = room?.GetItem(packet.PopWiredUInt());

            if (item == null)
            {
                return;
            }

            var hasRights = room.CheckRights(session);

            item.Interactor.OnTrigger(session, item, packet.PopWiredInt32(), hasRights);
        }
    }
}