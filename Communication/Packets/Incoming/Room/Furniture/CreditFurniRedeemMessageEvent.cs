﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Furniture
{
    internal class CreditFurniRedeemMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var exchange = room.GetItem(packet.PopWiredUInt());

            if (exchange == null)
            {
                return;
            }

            if (!exchange.GetBaseItem().Name.StartsWith("CF_") && !exchange.GetBaseItem().Name.StartsWith("CFC_"))
            {
                return;
            }

            var split = exchange.GetBaseItem().Name.Split('_');
            var value = int.Parse(split[1]);

            if (value > 0)
            {
                session.GetPlayer().Credits += value;
                session.GetPlayer().UpdateCreditsBalance(true);
            }

            room.RemoveFurniture(null, exchange.Id);

            session.SendPacket(new ServerPacket(219));
        }
    }
}