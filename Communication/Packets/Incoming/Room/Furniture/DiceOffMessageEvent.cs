﻿namespace Pylenor.Communication.Packets.Incoming.Room.Furniture
{
    internal class DiceOffMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var item = room?.GetItem(packet.PopWiredUInt());

            if (item == null)
            {
                return;
            }

            var hasRights = room.CheckRights(session);

            item.Interactor.OnTrigger(session, item, -1, hasRights);
        }
    }
}