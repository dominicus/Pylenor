﻿using System.Data;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Furniture
{
    internal class PresentOpenMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var present = room.GetItem(packet.PopWiredUInt());

            if (present == null)
            {
                return;
            }

            DataRow data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data =
                    dbClient.ReadDataRow("SELECT base_id,amount,extra_data FROM user_presents WHERE item_id = '" +
                                         present.Id + "' LIMIT 1");
            }

            if (data == null)
            {
                return;
            }

            var baseItem = PylenorEnvironment.GetGame().GetItemManager().GetItem((uint) data["base_id"]);

            if (baseItem == null)
            {
                return;
            }

            room.RemoveFurniture(session, present.Id);

            var serverPacket = new ServerPacket(219);
            serverPacket.WriteUInt(present.Id);
            session.SendPacket(serverPacket);

            serverPacket = new ServerPacket(129);
            serverPacket.WriteString(baseItem.Type);
            serverPacket.WriteInteger(baseItem.Sprite);
            serverPacket.WriteString(baseItem.Name);
            session.SendPacket(serverPacket);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM user_presents WHERE item_id = '" + present.Id + "' LIMIT 1");
            }

            PylenorEnvironment.GetGame()
                .GetCatalog()
                .DeliverItems(session, baseItem, (int) data["amount"], (string) data["extra_data"]);
        }
    }
}