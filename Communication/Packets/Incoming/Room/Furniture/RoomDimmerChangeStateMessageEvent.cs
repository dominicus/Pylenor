﻿using System.Linq;

namespace Pylenor.Communication.Packets.Incoming.Room.Furniture
{
    internal class RoomDimmerChangeStateMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true) || room.MoodlightData == null)
            {
                return;
            }

            var item = room.Items.FirstOrDefault(I => I.GetBaseItem().InteractionType.ToLower() == "dimmer");

            if (item == null)
            {
                return;
            }

            if (room.MoodlightData.Enabled)
            {
                room.MoodlightData.Disable();
            }
            else
            {
                room.MoodlightData.Enable();
            }

            item.ExtraData = room.MoodlightData.GenerateExtraData();
            item.UpdateState();
        }
    }
}