﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.Furniture
{
    internal class RoomDimmerGetPresetsMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true) || room.MoodlightData == null)
            {
                return;
            }

            var serverPacket = new ServerPacket(365);
            serverPacket.WriteInteger(room.MoodlightData.Presets.Count);
            serverPacket.WriteInteger(room.MoodlightData.CurrentPreset);

            var i = 0;

            foreach (var preset in room.MoodlightData.Presets)
            {
                i++;

                serverPacket.WriteInteger(i);
                serverPacket.WriteInteger(int.Parse(PylenorEnvironment.BoolToEnum(preset.BackgroundOnly)) + 1);
                serverPacket.WriteString(preset.ColorCode);
                serverPacket.WriteInteger(preset.ColorIntensity);
            }

            session.SendPacket(serverPacket);
        }
    }
}