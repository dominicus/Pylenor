﻿using System.Linq;

namespace Pylenor.Communication.Packets.Incoming.Room.Furniture
{
    internal class RoomDimmerSavePresetMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true) || room.MoodlightData == null)
            {
                return;
            }

            var item = room.Items.FirstOrDefault(I => I.GetBaseItem().InteractionType.ToLower() == "dimmer");

            if (item == null)
            {
                return;
            }

            var preset = packet.PopWiredInt32();
            var backgroundMode = packet.PopWiredInt32();
            var colorCode = packet.PopString();
            var intensity = packet.PopWiredInt32();

            var backgroundOnly = backgroundMode >= 2;

            room.MoodlightData.Enabled = true;
            room.MoodlightData.CurrentPreset = preset;
            room.MoodlightData.UpdatePreset(preset, colorCode, intensity, backgroundOnly);

            item.ExtraData = room.MoodlightData.GenerateExtraData();
            item.UpdateState();
        }
    }
}