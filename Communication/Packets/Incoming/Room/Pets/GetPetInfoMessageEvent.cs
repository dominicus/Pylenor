﻿using System.Data;

namespace Pylenor.Communication.Packets.Incoming.Room.Pets
{
    internal class GetPetInfoMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var petId = packet.PopWiredUInt();

            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("petid", petId);
                row = dbClient.ReadDataRow("SELECT * FROM user_pets WHERE id = @petid LIMIT 1");
            }

            if (row == null)
            {
                return;
            }

            session.SendPacket(PylenorEnvironment.GetGame().GetCatalog().GeneratePetFromRow(row).SerializeInfo());
        }
    }
}