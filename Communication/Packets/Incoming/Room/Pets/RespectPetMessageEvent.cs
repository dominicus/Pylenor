﻿namespace Pylenor.Communication.Packets.Incoming.Room.Pets
{
    internal class RespectPetMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || room.IsPublic || (!room.AllowPets && !room.CheckRights(session, true)))
            {
                return;
            }

            var petId = packet.PopWiredUInt();
            var petUser = room.GetPet(petId);

            if (petUser?.PetData == null || petUser.PetData.OwnerId != session.GetPlayer().Id)
            {
                return;
            }

            petUser.PetData.OnRespect();
            session.GetPlayer().DailyPetRespectPoints--;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", session.GetPlayer().Id);
                dbClient.ExecuteQuery(
                    "UPDATE users SET daily_pet_respect_points = daily_pet_respect_points - 1 WHERE id = @userid LIMIT 1");
            }
        }
    }
}