﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Communication.Packets.Incoming.Room.PublicRoom
{
    internal class TryBusMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(81);
            serverPacket.WriteString("Infobus is closed..");
            session.SendPacket(serverPacket);
        }
    }
}