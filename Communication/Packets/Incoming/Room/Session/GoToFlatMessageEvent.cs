﻿namespace Pylenor.Communication.Packets.Incoming.Room.Session
{
    internal class GoToFlatMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            session.FinalizeRoom();
        }
    }
}