﻿namespace Pylenor.Communication.Packets.Incoming.Room.Session
{
    internal class OpenConnectionMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            packet.PopWiredInt32();
            var id = packet.PopWiredUInt();
            packet.PopWiredInt32();

            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(id);

            if (data == null || data.Type != "public")
            {
                return;
            }

            session.PrepareRoom(data.Id, "");
        }
    }
}