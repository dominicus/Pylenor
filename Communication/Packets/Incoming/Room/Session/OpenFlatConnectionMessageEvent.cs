﻿namespace Pylenor.Communication.Packets.Incoming.Room.Session
{
    internal class OpenFlatConnectionMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            var id = packet.PopWiredUInt();
            var password = packet.PopString();
            packet.PopWiredInt32();

            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(id);

            if (data == null || data.Type != "private")
            {
                return;
            }
            session.PrepareRoom(id, password);
        }
    }
}