﻿namespace Pylenor.Communication.Packets.Incoming.Room.Session
{
    internal class QuitMessageEvent : IPacketEvent
    {
        public void Parse(Sessions.Session session, ClientPacket packet)
        {
            if (session.GetPlayer().InRoom)
            {
                PylenorEnvironment.GetGame()
                    .GetRoomManager()
                    .GetRoom(session.GetPlayer().CurrentRoomId)
                    .RemoveUserFromRoom(session, true, false);
            }
        }
    }
}