﻿using System;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.RoomSettings
{
    internal class DeleteRoomMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var roomId = packet.PopWiredUInt();
            var data = PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(roomId);

            if (data == null ||
                !string.Equals(data.Owner, session.GetPlayer().Username, StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM rooms WHERE id = '" + data.Id + "' LIMIT 1");
                dbClient.ExecuteQuery("DELETE FROM user_favorites WHERE room_id = '" + data.Id + "'");
                dbClient.ExecuteQuery("DELETE FROM room_items WHERE room_id = '" + data.Id + "'");
                dbClient.ExecuteQuery("DELETE FROM room_rights WHERE room_id = '" + data.Id + "'");
                dbClient.ExecuteQuery("UPDATE users SET home_room = '0' WHERE home_room = '" + data.Id + "'");
                // todo: delete room stuff
            }

            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(data.Id);

            if (room != null)
            {
                foreach (var user in room.UserList.Where(user => !user.IsBot))
                {
                    user.GetClient().SendPacket(new ServerPacket(18));
                    user.GetClient().GetPlayer().OnLeaveRoom();
                }

                PylenorEnvironment.GetGame().GetRoomManager().UnloadRoom(data.Id);
            }

            session.SendPacket(new ServerPacket(101));
            session.SendPacket(PylenorEnvironment.GetGame().GetNavigator().SerializeRoomListing(session, -3));
        }
    }
}