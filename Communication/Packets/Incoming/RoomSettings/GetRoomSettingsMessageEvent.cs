﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.RoomSettings
{
    internal class GetRoomSettingsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            var serverPacket = new ServerPacket(465);
            serverPacket.WriteUInt(room.RoomId);
            serverPacket.WriteString(room.Name);
            serverPacket.WriteString(room.Description);
            serverPacket.WriteInteger(room.State);
            serverPacket.WriteInteger(room.Category);
            serverPacket.WriteInteger(room.UsersMax);
            serverPacket.WriteInteger(25);
            serverPacket.WriteInteger(room.TagCount);

            foreach (var tag in room.Tags)
            {
                serverPacket.WriteString(tag);
            }

            serverPacket.WriteInteger(room.UsersWithRights.Count); // users /w rights count

            foreach (var userId in room.UsersWithRights)
            {
                serverPacket.WriteUInt(userId);
                serverPacket.WriteString(PylenorEnvironment.GetGame().GetClientManager().GetNameById(userId));
            }

            serverPacket.WriteInteger(room.UsersWithRights.Count); // users /w rights count

            serverPacket.WriteBoolean(room.AllowPets); // allows pets in room - pet system lacking, so always off
            serverPacket.WriteBoolean(room.AllowPetsEating);
            // allows pets to eat your food - pet system lacking, so always off
            serverPacket.WriteBoolean(room.AllowWalkthrough);
            serverPacket.WriteBoolean(room.Hidewall);

            session.SendPacket(serverPacket);
        }
    }
}