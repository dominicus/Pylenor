﻿using System.Collections.Generic;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.Communication.Packets.Incoming.RoomSettings
{
    internal class SaveRoomSettingsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || !room.CheckRights(session, true))
            {
                return;
            }

            packet.PopWiredInt32();
            var name = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var description = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var state = packet.PopWiredInt32();
            var password = PylenorEnvironment.FilterInjectionChars(packet.PopString());
            var maxUsers = packet.PopWiredInt32();
            var categoryId = packet.PopWiredInt32();
            var tagCount = packet.PopWiredInt32();

            var tags = new List<string>();
            var formattedTags = new StringBuilder();

            for (var i = 0; i < tagCount; i++)
            {
                if (i > 0)
                {
                    formattedTags.Append(",");
                }

                var tag = PylenorEnvironment.FilterInjectionChars(packet.PopString().ToLower());

                tags.Add(tag);
                formattedTags.Append(tag);
            }

            var petsAllow = 0;
            var petsAllowEat = 0;
            var walkthroughAllow = 0;
            var wallHide = 0;

            var allowPets = packet.PlainReadBytes(1)[0].ToString();
            packet.AdvancePointer(1);

            var allowPetsEat = packet.PlainReadBytes(1)[0].ToString();
            packet.AdvancePointer(1);

            var allowWalkthrough = packet.PlainReadBytes(1)[0].ToString();
            packet.AdvancePointer(1);

            var hidewall = packet.PlainReadBytes(1)[0].ToString();
            packet.AdvancePointer(1);

            if (name.Length < 1)
            {
                return;
            }

            if (state < 0 || state > 2)
            {
                return;
            }

            if (maxUsers != 10 && maxUsers != 15 && maxUsers != 20 && maxUsers != 25)
            {
                return;
            }

            var flatCat = PylenorEnvironment.GetGame().GetNavigator().GetFlatCat(categoryId);

            if (flatCat == null)
            {
                return;
            }

            if (flatCat.MinRank > session.GetPlayer().Rank)
            {
                session.SendNotif(
                    "You are not allowed to use this category. Your room has been moved to no category instead.");
                categoryId = 0;
            }

            if (tagCount > 2)
            {
                return;
            }

            if (state < 0 || state > 2)
            {
                return;
            }

            if (allowPets == "65")
            {
                petsAllow = 1;
                room.AllowPets = true;
            }
            else
            {
                room.AllowPets = false;
            }

            if (allowPetsEat == "65")
            {
                petsAllowEat = 1;
                room.AllowPetsEating = true;
            }
            else
            {
                room.AllowPetsEating = false;
            }

            if (allowWalkthrough == "65")
            {
                walkthroughAllow = 1;
                room.AllowWalkthrough = true;
            }
            else
            {
                room.AllowWalkthrough = false;
            }

            if (hidewall == "65")
            {
                wallHide = 1;
                room.Hidewall = true;
            }
            else
            {
                room.Hidewall = false;
            }

            room.Name = name;
            room.State = state;
            room.Description = description;
            room.Category = categoryId;
            room.Password = password;
            room.Tags = tags;
            room.UsersMax = maxUsers;

            var formattedState = "open";

            if (room.State == 1)
            {
                formattedState = "locked";
            }
            else if (room.State > 1)
            {
                formattedState = "password";
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("caption", room.Name);
                dbClient.AddParamWithValue("description", room.Description);
                dbClient.AddParamWithValue("password", room.Password);
                dbClient.AddParamWithValue("tags", formattedTags.ToString());
                dbClient.ExecuteQuery(
                    "UPDATE rooms SET caption = @caption, description = @description, password = @password, category = '" +
                    categoryId + "', state = '" + formattedState + "', tags = @tags, users_max = '" + maxUsers +
                    "', allow_pets = '" + petsAllow + "', allow_pets_eat = '" + petsAllowEat +
                    "', allow_walkthrough = '" + walkthroughAllow + "', allow_hidewall = '" + wallHide +
                    "' WHERE id = '" + room.RoomId + "' LIMIT 1");
            }

            var serverPacket = new ServerPacket(467);
            serverPacket.WriteUInt(room.RoomId);
            session.SendPacket(serverPacket);

            serverPacket = new ServerPacket(456);
            serverPacket.WriteUInt(room.RoomId);
            session.SendPacket(serverPacket);

            serverPacket = new ServerPacket(472);
            serverPacket.WriteBoolean(room.Hidewall);
            PylenorEnvironment.GetGame()
                .GetRoomManager()
                .GetRoom(session.GetPlayer().CurrentRoomId)
                .SendMessage(serverPacket);

            var data = new RoomData();
            data.Fill(room);

            serverPacket = new ServerPacket(454);
            serverPacket.WriteBoolean(false);
            data.Serialize(serverPacket, false);
            serverPacket.WriteBoolean(false);
            serverPacket.WriteBoolean(true);
            session.SendPacket(serverPacket);
        }
    }
}