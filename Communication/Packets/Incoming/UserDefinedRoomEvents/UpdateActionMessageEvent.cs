﻿using System;
using Pylenor.Communication.Sessions;
using Pylenor.Util;

namespace Pylenor.Communication.Packets.Incoming.UserDefinedRoomEvents
{
    class UpdateActionMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);
            var item = room.GetItem(packet.PopWiredUInt());
            switch (item.GetBaseItem().InteractionType.ToLower())
            {
                case "wf_act_saymsg":
                    packet.PopWiredBoolean();
                    var message = PylenorEnvironment.DoFilter(packet.PopString(), false, true);
                    if (message.Length > 100)
                    {
                        message = message.Substring(0, 100);
                    }
                    item.Extra1 = message;
                    break;
                case "wf_act_kick_user":
                    packet.PopWiredBoolean();
                    var user = PylenorEnvironment.DoFilter(packet.PopString(), false, true);
                    if (user.Length > 200)
                    {
                        user = user.Substring(0, 200);
                    }
                    item.Extra1 = user;
                    break;
                case "wf_trg_furnistate":
                case "wf_trg_onfurni":
                case "wf_trg_offfurni":
                case "wf_act_moveuser":
                case "wf_act_togglefurni":
                    packet.PopWiredBoolean();
                    packet.PopString();
                    item.Extra1 = packet.ToString().Substring(packet.Length - (packet.RemainingLength - 2));
                    item.Extra1 = item.Extra1.Substring(0, item.Extra1.Length - 2);
                    packet.ResetPointer();
                    item = room.GetItem(packet.PopWiredUInt());
                    packet.PopWiredBoolean();
                    packet.PopString();
                    var length = packet.PopWiredInt32();
                    item.Extra2 = "";
                    for (var i = 0; i < length; i++)
                    {
                        item.Extra2 = item.Extra2 + "," + Convert.ToInt32(packet.PopWiredUInt());
                    }
                    if (item.Extra2.Length > 0)
                    {
                        item.Extra2 = item.Extra2.Substring(1);
                    }
                    break;
                case "wf_act_givepoints":
                    packet.PopWiredInt32();
                    item.Extra1 = Convert.ToString(packet.PopWiredInt32());
                    item.Extra2 = Convert.ToString(packet.PopWiredInt32());
                    break;
                case "wf_act_moverotate":
                    packet.PopWiredInt32();
                    item.Extra1 = Convert.ToString(packet.PopWiredInt32());
                    item.Extra2 = Convert.ToString(packet.PopWiredInt32());
                    packet.PopString();
                    var num2 = packet.PopWiredInt32();
                    item.Extra3 = "";
                    item.Extra4 = "";
                    if (num2 > 0)
                    {
                        item.Extra4 = OldEncoding.EncodeVl64(num2);
                        for (var i = 0; i < num2; i++)
                        {
                            var val = packet.PopWiredInt32();
                            item.Extra4 += OldEncoding.EncodeVl64(val);
                            item.Extra3 = item.Extra3 + "," + Convert.ToString(val);
                        }
                        item.Extra3 = item.Extra3.Substring(1);
                    }
                    item.Extra5 = Convert.ToString(packet.PopWiredInt32());
                    break;
                case "wf_act_matchfurni":
                    packet.PopWiredInt32();
                    item.Extra2 = "";
                    item.Extra2 += packet.PopWiredBoolean() ? "I" : "H";
                    item.Extra2 += packet.PopWiredBoolean() ? "I" : "H";
                    item.Extra2 += packet.PopWiredBoolean() ? "I" : "H";
                    packet.PopString();
                    var len = packet.PopWiredInt32();
                    item.Extra1 = "";
                    item.Extra3 = "";
                    item.Extra4 = "";
                    if (len > 0)
                    {
                        item.Extra4 = OldEncoding.EncodeVl64(len);
                        for (var i = 0; i < len; i++)
                        {
                            var id = packet.PopWiredInt32();
                            item.Extra4 += OldEncoding.EncodeVl64(id);
                            item.Extra3 = item.Extra3 + "," + Convert.ToString(id);
                            var secondItem = room.GetItem(Convert.ToUInt32(id));
                            var copy = secondItem.Extra1;
                            item.Extra1 = string.Concat(copy, ";", secondItem.X, ",", secondItem.Y, ",", secondItem.Z,
                                ",", secondItem.Rot, ",", secondItem.ExtraData);
                        }
                        item.Extra3 = item.Extra3.Substring(1);
                        item.Extra1 = item.Extra1.Substring(1);
                    }
                    break;
            }
            Console.WriteLine("(action) {0}", item.Extra1);
            Console.WriteLine("(action) {0}", item.Extra2);
            Console.WriteLine("(action) {0}", item.Extra3);
            Console.WriteLine("(action) {0}", item.Extra4);
            Console.WriteLine("(action) {0}", item.Extra5);
            Console.WriteLine("(action) {0}", item.ExtraData);
            item.UpdateState(true, false);
        }
    }
}
