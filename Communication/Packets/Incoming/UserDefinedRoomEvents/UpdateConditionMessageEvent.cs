﻿using System;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Items;
using Pylenor.Util;

namespace Pylenor.Communication.Packets.Incoming.UserDefinedRoomEvents
{
    class UpdateConditionMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);
            var item = room.GetItem(packet.PopWiredUInt());
            var interaction = item.GetBaseItem().InteractionType.ToLower();
            if ((interaction == "wf_cnd_trggrer_on_frn" || interaction == "wf_cnd_furnis_hv_avtrs" ||
                 interaction == "wf_cnd_has_furni_on"))
            {
                packet.PopWiredBoolean();
                packet.PopString();
                item.Extra1 = packet.ToString().Substring(packet.Length - (packet.RemainingLength - 2));
                item.Extra1 = item.Extra1.Substring(0, item.Extra1.Length - 1);
                packet.ResetPointer();
                item = room.GetItem(packet.PopWiredUInt());
                packet.PopWiredBoolean();
                packet.PopString();
                var length = packet.PopWiredInt32();
                item.Extra2 = "";
                for (var i = 0; i < length; i++)
                {
                    item.Extra2 = item.Extra2 + "," + Convert.ToString(packet.PopWiredUInt());
                }
                if (item.Extra2.Length > 0)
                {
                    item.Extra2 = item.Extra2.Substring(1);
                }
            }
            if (interaction == "wf_cnd_match_snapshot")
            {
                packet.PopWiredInt32();
                item.Extra2 = "";
                item.Extra2 += packet.PopWiredBoolean() ? "I" : "H";
                item.Extra2 += packet.PopWiredBoolean() ? "I" : "H";
                item.Extra2 += packet.PopWiredBoolean() ? "I" : "H";
                packet.PopString();
                var length = packet.PopWiredInt32();
                item.Extra1 = "";
                item.Extra3 = "";
                item.Extra4 = "";
                if (length > 0)
                {
                    item.Extra4 = OldEncoding.EncodeVl64(length);
                    for (var i = 0; i < length; i++)
                    {
                        var id = packet.PopWiredInt32();
                        item.Extra4 += OldEncoding.EncodeVl64(id);
                        item.Extra3 = item.Extra3 + "," + Convert.ToString(id);
                        var secondItem = room.GetItem(Convert.ToUInt32(id));
                        var copy = item.Extra1;
                        item.Extra1 = string.Concat(copy, ";", secondItem.X, ",", secondItem.Y, ",", secondItem.Z, ",",
                            secondItem.Rot, ",", secondItem.ExtraData);
                    }
                    item.Extra3 = item.Extra3.Substring(1);
                    item.Extra1 = item.Extra1.Substring(1);
                }
            }
            Console.WriteLine("(condition) {0}", item.Extra1);
            Console.WriteLine("(condition) {0}", item.Extra2);
            Console.WriteLine("(condition) {0}", item.Extra3);
            Console.WriteLine("(condition) {0}", item.Extra4);
            Console.WriteLine("(condition) {0}", item.Extra5);
            Console.WriteLine("(condition) {0}", item.ExtraData);
            item.UpdateState(true, false);
        }
    }
}
