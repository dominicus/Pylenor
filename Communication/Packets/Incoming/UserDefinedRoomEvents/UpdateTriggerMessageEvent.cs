﻿using System;
using System.Globalization;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.UserDefinedRoomEvents
{
    class UpdateTriggerMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);
            var item = room?.GetItem(packet.PopWiredUInt());
            if (item == null) return;
            var interaction = item.GetBaseItem().InteractionType.ToLower();
            if (interaction == "wf_trg_onsay")
            {
                packet.PopWiredBoolean();
                var value = packet.PopWiredBoolean();
                var text3 = packet.PopString();
                text3 = PylenorEnvironment.DoFilter(text3, false, true);
                if (text3.Length > 100)
                {
                    text3 = text3.Substring(0, 100);
                }
                item.Extra1 = text3;
                item.Extra2 = Convert.ToString(value);
            }
            else if (interaction == "wf_trg_enterroom")
            {
                packet.PopWiredBoolean();
                var text3 = packet.PopString();
                item.Extra1 = text3;
            }
            else if (interaction == "wf_trg_timer")
            {
                packet.PopWiredBoolean();
                var text2 = packet.ToString().Substring(packet.Length - (packet.RemainingLength - 2));
                var array = text2.Split('@');
                item.Extra2 = array[0];
                item.Extra1 =
                    Convert.ToString(Convert.ToString(packet.PopWiredInt32()*0.5, CultureInfo.InvariantCulture));
            }
            else if (interaction == "wf_trg_attime")
            {
                packet.PopWiredBoolean();
                var text2 = packet.ToString().Substring(packet.Length - (packet.RemainingLength - 2));
                var array = text2.Split('@');
                item.Extra2 = array[0];
                item.Extra1 =
                    Convert.ToString(Convert.ToString(packet.PopWiredInt32()*0.5, CultureInfo.InvariantCulture));
            }
            else if (interaction == "wf_trg_atscore")
            {
                packet.PopWiredBoolean();
                var text2 = packet.ToString().Substring(packet.Length - (packet.RemainingLength - 2));
                var array = text2.Split('@');
                item.Extra2 = array[0];
                item.Extra1 = Convert.ToString(packet.PopWiredInt32());
            }

            if ((interaction == "wf_cnd_time_more_than" || interaction == "wf_cnd_time_less_than"))
            {
                packet.PopWiredBoolean();
                var text2 = packet.ToString().Substring(packet.Length - (packet.RemainingLength - 2));
                var array = text2.Split('@');
                item.Extra2 = array[0];
                item.Extra1 = Convert.ToString(Convert.ToString(packet.PopWiredInt32() * 0.5, CultureInfo.InvariantCulture));
            }
            Console.WriteLine("(trigger) {0}", item.Extra1);
            Console.WriteLine("(trigger) {0}", item.Extra2);
            Console.WriteLine("(trigger) {0}", item.Extra3);
            Console.WriteLine("(trigger) {0}", item.Extra4);
            Console.WriteLine("(trigger) {0}", item.Extra5);
            Console.WriteLine("(trigger) {0}", item.ExtraData);
        }
    }
}
