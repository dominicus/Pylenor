﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class ApproveNameMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(36);
            serverPacket.WriteInteger(PylenorEnvironment.GetGame().GetCatalog().CheckPetName(packet.PopString()) ? 0 : 2);
            session.SendPacket(serverPacket);
        }
    }
}