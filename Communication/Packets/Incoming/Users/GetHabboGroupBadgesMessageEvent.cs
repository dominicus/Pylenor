﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class GetHabboGroupBadgesMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var serverPacket = new ServerPacket(309);
            serverPacket.WriteString("IcIrDs43103s19014d5a1dc291574a508bc80a64663e61a00");
            session.SendPacket(serverPacket);
        }
    }
}