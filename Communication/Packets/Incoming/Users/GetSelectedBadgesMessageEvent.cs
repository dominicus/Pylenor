﻿using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class GetSelectedBadgesMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(packet.PopWiredUInt());

            if (user == null || user.IsBot)
            {
                return;
            }

            var serverPacket = new ServerPacket(228);
            serverPacket.WriteUInt(user.GetClient().GetPlayer().Id);
            serverPacket.WriteInteger(user.GetClient().GetPlayer().GetBadgeComponent().EquippedCount);

            foreach (
                var badge in user.GetClient().GetPlayer().GetBadgeComponent().BadgeList.Where(badge => badge.Slot > 0))
            {
                serverPacket.WriteInteger(badge.Slot);
                serverPacket.WriteString(badge.Code);
            }

            session.SendPacket(serverPacket);
        }
    }
}