﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class GetUserTagsMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            var user = room?.GetRoomUserByHabbo(packet.PopWiredUInt());

            if (user == null || user.IsBot)
            {
                return;
            }

            var serverPacket = new ServerPacket(350);
            serverPacket.WriteUInt(user.GetClient().GetPlayer().Id);
            serverPacket.WriteInteger(user.GetClient().GetPlayer().Tags.Count);

            foreach (var tag in user.GetClient().GetPlayer().Tags)
            {
                serverPacket.WriteString(tag);
            }

            session.SendPacket(serverPacket);
        }
    }
}