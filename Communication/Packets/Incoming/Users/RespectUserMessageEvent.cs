﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class RespectUserMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null || session.GetPlayer().DailyRespectPoints <= 0)
            {
                return;
            }

            var user = room.GetRoomUserByHabbo(packet.PopWiredUInt());

            if (user == null || user.GetClient().GetPlayer().Id == session.GetPlayer().Id || user.IsBot)
            {
                return;
            }

            session.GetPlayer().DailyRespectPoints--;
            user.GetClient().GetPlayer().Respect++;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET respect = respect + 1 WHERE id = '" +
                                      user.GetClient().GetPlayer().Id + "' LIMIT 1");
                dbClient.ExecuteQuery("UPDATE users SET daily_respect_points = daily_respect_points - 1 WHERE id = '" +
                                      session.GetPlayer().Id + "' LIMIT 1");
            }

            // FxkqUzYP_
            var message = new ServerPacket(440);
            message.WriteUInt(user.GetClient().GetPlayer().Id);
            message.WriteInteger(user.GetClient().GetPlayer().Respect);
            room.SendMessage(message);
        }
    }
}