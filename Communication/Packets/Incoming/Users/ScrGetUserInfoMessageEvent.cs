﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class ScrGetUserInfoMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var subscriptionId = packet.PopString();

            var scrSendUserInfoComposer = new ServerPacket(ServerPacketHeader.ScrSendUserInfoComposer);
            scrSendUserInfoComposer.WriteString(subscriptionId.ToLower());

            if (session.GetPlayer().GetSubscriptionManager().HasSubscription(subscriptionId))
            {
                double expire = session.GetPlayer().GetSubscriptionManager().GetSubscription(subscriptionId).ExpireTime;
                var timeLeft = expire - PylenorEnvironment.GetUnixTimestamp();
                var totalDaysLeft = (int) Math.Ceiling(timeLeft/86400);
                var monthsLeft = totalDaysLeft/31;

                if (monthsLeft >= 1) monthsLeft--;

                scrSendUserInfoComposer.WriteInteger(totalDaysLeft - (monthsLeft*31));
                scrSendUserInfoComposer.WriteBoolean(true);
                scrSendUserInfoComposer.WriteInteger(monthsLeft);
                scrSendUserInfoComposer.WriteInteger(1);
                scrSendUserInfoComposer.WriteInteger(1);

                scrSendUserInfoComposer.WriteInteger(session.GetPlayer().HasFuse("fuse_use_vip_outfits") ? 2 : 1);
            }
            else
            {
                for (var i = 0; i < 3; i++)
                {
                    scrSendUserInfoComposer.WriteInteger(0);
                }
            }

            session.SendPacket(scrSendUserInfoComposer);
        }
    }
}