﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication.Packets.Incoming.Users
{
    internal class UnignoreUserMessageEvent : IPacketEvent
    {
        public void Parse(Session session, ClientPacket packet)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

            if (room == null)
            {
                return;
            }
            packet.PopWiredUInt();
            var username = packet.PopString();
            var ignoreUser = room.GetRoomUserByHabbo(username);
            if (ignoreUser == null)
                return;
            var id = ignoreUser.GetClient().GetPlayer().Id;

            if (!session.GetPlayer().MutedUsers.Contains(id))
            {
                return;
            }

            session.GetPlayer().MutedUsers.Remove(id);

            var serverPacket = new ServerPacket(419);
            serverPacket.WriteInteger(3);
            session.SendPacket(serverPacket);
        }
    }
}