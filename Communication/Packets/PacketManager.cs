﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Pylenor.Communication.Packets.Incoming;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Util;

namespace Pylenor.Communication.Packets
{
    internal sealed class PacketManager
    {
        /// <summary>
        ///     The maximum time a task can run for before it is considered dead
        ///     (can be used for debugging any locking issues with certain areas of code)
        /// </summary>
        private const int MaximumRunTimeInSec = 300; // 5 minutes

        /// <summary>
        ///     Testing the Task code
        /// </summary>
        private readonly bool _ignoreTasks = false;

        /// <summary>
        ///     The task factory which is used for running Asynchronous tasks, in this case we use it to execute packets.
        /// </summary>
        private readonly TaskFactory _packetHandler;

        /// <summary>
        ///     Currently running tasks to keep track of what the current load is
        /// </summary>
        private readonly ConcurrentDictionary<int, Task> _runningTasks;

        /// <summary>
        ///     The collection that stores all the packets to a packet id as the key.
        /// </summary>
        internal readonly Dictionary<uint, IPacketEvent> Packets;

        /// <summary>
        ///     Storage of names from Info Events.
        /// </summary>
        public Dictionary<string, uint> InfoEvents;

        /// <summary>
        ///     Initializes a new instance of the PacketManager.
        /// </summary>
        public PacketManager()
        {
            Packets = new Dictionary<uint, IPacketEvent>();
            InfoEvents = new Dictionary<string, uint>();
            _packetHandler = new TaskFactory(TaskCreationOptions.PreferFairness, TaskContinuationOptions.None);
            _runningTasks = new ConcurrentDictionary<int, Task>();
        }

        /// <summary>
        ///     Handles the packet provided
        /// </summary>
        /// <param name="session">The session which invokes this packet.</param>
        /// <param name="packet">The client packet received.</param>
        public void ExecutePacket(Session session, ClientPacket packet)
        {
            IPacketEvent pak;

            if (!Packets.TryGetValue(packet.Id, out pak))
            {
                Console.WriteLine("Unhandled packet {0} ({1})", GetName(packet.Id), packet.Id);
                return;
            }

            Console.WriteLine("<Session " + session.Id + "> Executing packet: {0} ({1})", GetName(packet.Id), packet.Id);

            if (!_ignoreTasks)
            {
                //var start = DateTime.Now;

                var cancelSource = new CancellationTokenSource();
                var token = cancelSource.Token;

                var t = _packetHandler.StartNew(() =>
                {
                    pak.Parse(session, packet);
                    token.ThrowIfCancellationRequested();
                }, token);

                _runningTasks.TryAdd(t.Id, t);

                try
                {
                    if (!t.Wait(MaximumRunTimeInSec*1000, token))
                    {
                        cancelSource.Cancel();
                    }
                }
                catch (AggregateException ex)
                {
                    foreach (var e in ex.Flatten().InnerExceptions)
                    {
                        Console.WriteLine("Unhandled Error: " + e.Message + " - " + e.StackTrace);
                        session.Disconnect();
                    }
                }
                catch (OperationCanceledException)
                {
                    session.Disconnect();
                }
                finally
                {
                    Task removedTask;
                    _runningTasks.TryRemove(t.Id, out removedTask);

                    cancelSource.Dispose();

                    //Console.WriteLine("Event took " + (DateTime.Now - start).Milliseconds + "ms to complete.");
                }
            }
            else
            {
                pak.Parse(session, packet);
            }
        }

        public void WaitForAllToComplete()
        {
            foreach (var t in _runningTasks.Values)
            {
                t.Wait();
            }
        }

        public void UnregisterAll()
        {
            Packets.Clear();
        }

        public void Register()
        {
            foreach (var packet in typeof (ClientPacketHeader).GetFields())
            {
                var packetId = (uint) packet.GetValue(0);
                var packetName = packet.Name;

                if (!InfoEvents.ContainsValue(packetId))
                {
                    InfoEvents.Add(packetName, packetId);
                }
            }
            foreach (var type in Assembly.GetCallingAssembly().GetTypes())
            {
                if (type == null)
                {
                    continue;
                }
                if (!type.GetInterfaces().Contains(typeof (IPacketEvent))) continue;
                var constructorInfo = type.GetConstructor(new Type[] {});

                if (constructorInfo == null) continue;
                var constructed = constructorInfo.Invoke(new object[] {}) as IPacketEvent;

                var header = GetHeader(constructed);

                if (!Packets.ContainsKey(header) && header > 0)
                {
                    Packets.Add(header, constructed);
                }
            }
            Console.WriteLine("Loaded {0}/{1} MessageEvent(s), and {2} MessageComposer(s).", Packets.Count,
                InfoEvents.Count, typeof (ServerPacketHeader).GetFields().Length);
        }

        /// <summary>
        ///     Returns an header from an InfoEvent.
        /// </summary>
        /// <param name="Event"></param>
        /// <returns></returns>
        public uint GetHeader(IPacketEvent Event)
        {
            using (var adapter = new DictionaryAdapter<string, uint>(InfoEvents))
            {
                return adapter.TryPopValue(Event.GetType().Name);
            }
        }

        /// <summary>
        ///     Returns an name of an InfoEvent.
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public string GetName(uint header)
        {
            using (var adapter = new DictionaryAdapter<string, uint>(InfoEvents))
            {
                return adapter.TryPopKey(header);
            }
        }
    }
}