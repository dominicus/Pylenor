﻿using System;
using System.Net.Sockets;
using System.Threading;
using Pylenor.Communication.Sessions;

namespace Pylenor.Communication
{
    public sealed class ServerSocket
    {
        /// <summary>
        ///     The buffer manager for allocation a buffer block to a SocketAsyncEventArgs.
        /// </summary>
        private BufferManager _bufferManager;

        /// <summary>
        ///     The socket used for listening for incoming connections.
        /// </summary>
        private Socket _listenSocket;

        /// <summary>
        ///     The semaphore used for controlling the max socket accept operations at a time.
        /// </summary>
        private SemaphoreSlim _maxAcceptOpsEnforcer;

        /// <summary>
        ///     The semaphore used for controlling the max connections to the server.
        /// </summary>
        private SemaphoreSlim _maxConnectionsEnforcer;

        /// <summary>
        ///     The semaphore used for controlling the max SocketAsyncEventArgs to be used for send operations at a time.
        /// </summary>
        private SemaphoreSlim _maxSaeaSendEnforcer;

        /// <summary>
        ///     The pool of re-usable SocketAsyncEventArgs for accept operations.
        /// </summary>
        private SocketAsyncEventArgsPool _poolOfAcceptEventArgs;

        /// <summary>
        ///     The pool of re-usable SocketAsyncEventArgs for receiving data.
        /// </summary>
        private SocketAsyncEventArgsPool _poolOfRecEventArgs;

        /// <summary>
        ///     The pool of re-usable SocketAsyncEventArgs for sending data.
        /// </summary>
        private SocketAsyncEventArgsPool _poolOfSendEventArgs;

        /// <summary>
        ///     The settings to use with this ServerSocket.
        /// </summary>
        private ServerSocketSettings _settings;

        /// <summary>
        ///     Initializes a new instance of the ServerSocket.
        /// </summary>
        /// <param name="settings">The settings to use with this ServerSocket.</param>
        public ServerSocket(ServerSocketSettings settings)
        {
            _settings = settings;

            _bufferManager =
                new BufferManager(
                    (_settings.BufferSize*_settings.NumOfSaeaForRec) + (_settings.BufferSize*_settings.NumOfSaeaForSend),
                    _settings.BufferSize);
            _poolOfAcceptEventArgs = new SocketAsyncEventArgsPool();
            _poolOfRecEventArgs = new SocketAsyncEventArgsPool();
            _poolOfSendEventArgs = new SocketAsyncEventArgsPool();

            _maxConnectionsEnforcer = new SemaphoreSlim(_settings.MaxConnections, _settings.MaxConnections);
            _maxSaeaSendEnforcer = new SemaphoreSlim(_settings.NumOfSaeaForSend, _settings.NumOfSaeaForSend);
            _maxAcceptOpsEnforcer = new SemaphoreSlim(_settings.MaxSimultaneousAcceptOps,
                _settings.MaxSimultaneousAcceptOps);
        }

        public void Init()
        {
            _bufferManager.InitBuffer();

            for (var i = 0; i < _settings.MaxSimultaneousAcceptOps; i++)
            {
                var acceptEventArg = new SocketAsyncEventArgs();
                acceptEventArg.Completed +=
                    AcceptEventArg_Completed;

                _poolOfAcceptEventArgs.Push(acceptEventArg);
            }

            // receive objects
            for (var i = 0; i < _settings.NumOfSaeaForRec; i++)
            {
                var eventArgObjectForPool = new SocketAsyncEventArgs();
                _bufferManager.SetBuffer(eventArgObjectForPool);

                eventArgObjectForPool.Completed +=
                    IO_ReceiveCompleted;
                eventArgObjectForPool.UserToken = new Session((uint) i, this);
                _poolOfRecEventArgs.Push(eventArgObjectForPool);
            }

            // send objects
            for (var i = 0; i < _settings.NumOfSaeaForSend; i++)
            {
                var eventArgObjectForPool = new SocketAsyncEventArgs();
                _bufferManager.SetBuffer(eventArgObjectForPool);

                eventArgObjectForPool.Completed +=
                    IO_SendCompleted;
                eventArgObjectForPool.UserToken = new SendDataToken();
                _poolOfSendEventArgs.Push(eventArgObjectForPool);
            }
        }

        public void StartListen()
        {
            _listenSocket = new Socket(_settings.Endpoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _listenSocket.Bind(_settings.Endpoint);
            _listenSocket.Listen(_settings.Backlog);

            StartAccept();
        }

        private void StartAccept()
        {
            SocketAsyncEventArgs acceptEventArgs;

            _maxAcceptOpsEnforcer.Wait();

            if (!_poolOfAcceptEventArgs.TryPop(out acceptEventArgs)) return;
            _maxConnectionsEnforcer.Wait();
            var willRaiseEvent = _listenSocket.AcceptAsync(acceptEventArgs);

            if (!willRaiseEvent)
            {
                ProcessAccept(acceptEventArgs);
            }
        }

        private void AcceptEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs acceptEventArgs)
        {
            StartAccept();

            if (acceptEventArgs.SocketError != SocketError.Success)
            {
                HandleBadAccept(acceptEventArgs);
                _maxAcceptOpsEnforcer.Release();
                return;
            }

            SocketAsyncEventArgs recEventArgs;

            if (_poolOfRecEventArgs.TryPop(out recEventArgs))
            {
                ((Session) recEventArgs.UserToken).Socket = acceptEventArgs.AcceptSocket;

                acceptEventArgs.AcceptSocket = null;
                _poolOfAcceptEventArgs.Push(acceptEventArgs);
                _maxAcceptOpsEnforcer.Release();
                PylenorEnvironment.GetGame()
                    .GetClientManager()
                    .StartClient(((Session) recEventArgs.UserToken).Id, ((Session) recEventArgs.UserToken));
                Console.WriteLine("<Session " + ((Session) recEventArgs.UserToken).Id + "> is now in use.");

                StartReceive(recEventArgs);
            }
            else
            {
                HandleBadAccept(acceptEventArgs);
                Console.WriteLine("Cannot handle this session, there are no more receive objects available for us.");
            }
        }

        private void IO_SendCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.LastOperation != SocketAsyncOperation.Send)
            {
                throw new InvalidOperationException(
                    "Tried to pass a send operation but the operation expected was not a send.");
            }

            ProcessSend(e);
        }

        private void IO_ReceiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.LastOperation != SocketAsyncOperation.Receive)
            {
                throw new InvalidOperationException(
                    "Tried to pass a receive operation but the operation expected was not a receive.");
            }

            ProcessReceive(e);
        }

        private void StartReceive(SocketAsyncEventArgs receiveEventArgs)
        {
            var token = (Session) receiveEventArgs.UserToken;

            var willRaiseEvent = token.Socket.ReceiveAsync(receiveEventArgs);

            if (!willRaiseEvent)
            {
                ProcessReceive(receiveEventArgs);
            }
        }

        private void ProcessReceive(SocketAsyncEventArgs receiveEventArgs)
        {
            var token = (Session) receiveEventArgs.UserToken;

            if (receiveEventArgs.BytesTransferred > 0 && receiveEventArgs.SocketError == SocketError.Success)
            {
                var dataReceived = new byte[receiveEventArgs.BytesTransferred];
                Buffer.BlockCopy(receiveEventArgs.Buffer, receiveEventArgs.Offset, dataReceived, 0,
                    receiveEventArgs.BytesTransferred);
                token.OnReceiveData(dataReceived);

                StartReceive(receiveEventArgs);
            }
            else
            {
                CloseClientSocket(receiveEventArgs);
                ReturnReceiveSaea(receiveEventArgs);
            }
        }

        public void SendData(Socket socket, byte[] data)
        {
            _maxSaeaSendEnforcer.Wait();
            SocketAsyncEventArgs sendEventArgs;
            _poolOfSendEventArgs.TryPop(out sendEventArgs);

            var token = (SendDataToken) sendEventArgs.UserToken;
            token.DataToSend = data;
            token.SendBytesRemainingCount = data.Length;

            sendEventArgs.AcceptSocket = socket;
            StartSend(sendEventArgs);
        }

        private void StartSend(SocketAsyncEventArgs sendEventArgs)
        {
            var token = (SendDataToken) sendEventArgs.UserToken;

            if (token.SendBytesRemainingCount <= _settings.BufferSize)
            {
                sendEventArgs.SetBuffer(sendEventArgs.Offset, token.SendBytesRemainingCount);
                Buffer.BlockCopy(token.DataToSend, token.BytesSentAlreadyCount, sendEventArgs.Buffer,
                    sendEventArgs.Offset, token.SendBytesRemainingCount);
            }
            else
            {
                sendEventArgs.SetBuffer(sendEventArgs.Offset, _settings.BufferSize);
                Buffer.BlockCopy(token.DataToSend, token.BytesSentAlreadyCount, sendEventArgs.Buffer,
                    sendEventArgs.Offset, _settings.BufferSize);
            }

            var willRaiseEvent = sendEventArgs.AcceptSocket.SendAsync(sendEventArgs);

            if (!willRaiseEvent)
            {
                ProcessSend(sendEventArgs);
            }
        }

        private void ProcessSend(SocketAsyncEventArgs sendEventArgs)
        {
            var token = (SendDataToken) sendEventArgs.UserToken;

            if (sendEventArgs.SocketError == SocketError.Success)
            {
                token.SendBytesRemainingCount = token.SendBytesRemainingCount - sendEventArgs.BytesTransferred;

                if (token.SendBytesRemainingCount == 0)
                {
                    token.Reset();
                    ReturnSendSaea(sendEventArgs);
                }
                else
                {
                    token.BytesSentAlreadyCount += sendEventArgs.BytesTransferred;
                    StartSend(sendEventArgs);
                }
            }
            else
            {
                token.Reset();
                CloseClientSocket(sendEventArgs);
                ReturnSendSaea(sendEventArgs);
            }
        }

        private void CloseClientSocket(SocketAsyncEventArgs args)
        {
            var con = (Session) args.UserToken;

            try
            {
                con.Socket.Shutdown(SocketShutdown.Both);
            }
            catch (SocketException)
            {
            }

            con.Socket.Close();
            con.OnDisconnection();
            PylenorEnvironment.GetGame().GetClientManager().RemoveClient(con.Id);
            Console.WriteLine("<Session " + con.Id + "> is no longer in use.");
        }

        private void ReturnReceiveSaea(SocketAsyncEventArgs args)
        {
            _poolOfRecEventArgs.Push(args);
            _maxConnectionsEnforcer.Release();
        }

        private void ReturnSendSaea(SocketAsyncEventArgs args)
        {
            _poolOfSendEventArgs.Push(args);
            _maxSaeaSendEnforcer.Release();
        }

        private void HandleBadAccept(SocketAsyncEventArgs acceptEventArgs)
        {
            acceptEventArgs.AcceptSocket.Shutdown(SocketShutdown.Both);
            acceptEventArgs.AcceptSocket.Close();
            _poolOfAcceptEventArgs.Push(acceptEventArgs);
        }
    }
}