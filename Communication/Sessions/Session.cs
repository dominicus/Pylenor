﻿using System;
using System.Net.Sockets;
using System.Text;
using Pylenor.Communication.Packets.Incoming;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Core;
using Pylenor.HabboHotel.Misc;
using Pylenor.HabboHotel.Support;
using Pylenor.HabboHotel.Users;
using Pylenor.HabboHotel.Users.Authenticator;
using Pylenor.Util;

namespace Pylenor.Communication.Sessions
{
    public sealed class Session
    {
        private bool _disconnectedCalled;

        /// <summary>
        ///     The game player linked to this active session.
        /// </summary>
        private Player _player;

        /// <summary>
        ///     Initializes a new instance of the Session class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="manager">The manager which created this session.</param>
        public Session(uint id, ServerSocket manager)
        {
            Id = id;
            Manager = manager;
        }

        /// <summary>
        ///     Unique ID which indentifies this Session. (It may only be used for debugging purposes)
        /// </summary>
        public uint Id { get; }

        /// <summary>
        ///     The manager that was used to create this session.
        /// </summary>
        public ServerSocket Manager { get; }

        /// <summary>
        ///     The Socket used for the backend of the communication.
        /// </summary>
        public Socket Socket { get; set; }

        /// <summary>
        ///     Gets the IP Address of this connection session.
        /// </summary>
        public string IpAddress => Socket.RemoteEndPoint.ToString().Split(':')[0];

        public bool PongOk { get; set; }

        /// <summary>
        ///     This method is called when data incoming has been received.
        /// </summary>
        /// <param name="data">The array of data.</param>
        public void OnReceiveData(byte[] data)
        {
            if (data[0] == 64)
            {
                var pos = 0;

                while (pos < data.Length)
                {
                    try
                    {
                        var length = Base64Encoding.DecodeInt32(new[] {data[pos++], data[pos++], data[pos++]});
                        var id = Base64Encoding.DecodeUInt32(new[] {data[pos++], data[pos++]});

                        var content = new byte[length - 2];

                        for (var i = 0; i < content.Length; i++)
                        {
                            content[i] = data[pos++];
                        }

                        var message = new ClientPacket(id, content);
                        PylenorEnvironment._packetManager.ExecutePacket(this, message);
                    }
                    catch (EntryPointNotFoundException e)
                    {
                        PylenorEnvironment.GetLogging().WriteLine("User D/C: " + e.Message, LogLevel.Error);
                        Disconnect();
                    }
                }
            }
            else
            {
                SendData(CrossdomainPolicy.GetXmlPolicy());
            }
        }

        /// <summary>
        ///     This method is called when the Session has disconnected.
        /// </summary>
        public void OnDisconnection()
        {
            _player?.OnDisconnect();

            _player = null; // only cleanup player stuff here, not socket, that is all handled automatically.
        }

        /// <summary>
        ///     Forces this player to be disconnected
        /// </summary>
        public void Disconnect()
        {
            if (_disconnectedCalled) return;
            _disconnectedCalled = true;
            Socket.Disconnect(true); // This needs improving (DisconnectAsync)
        }

        public void SendPacket(ServerPacket packet)
        {
            SendData(packet.GetBytes());
        }

        public void SendData(byte[] data)
        {
            Manager.SendData(Socket, data);
        }

        public void SendData(string data)
        {
            SendData(Encoding.UTF8.GetBytes(data));
        }

        /// <summary>
        ///     Sets the player for this session.
        /// </summary>
        /// <param name="player"></param>
        public void SetPlayer(Player player)
        {
            if (_player != null)
            {
                throw new InvalidOperationException("Cannot set a player that has already been set.");
            }

            _player = player;
        }

        /// <summary>
        ///     Retrieves the active player for this session.
        /// </summary>
        /// <returns></returns>
        public Player GetPlayer()
        {
            return _player;
        }

        public void Login(string authTicket)
        {
            try
            {
                Console.WriteLine(authTicket);
                var player = Authenticator.TryLoginPlayer(authTicket);

                PylenorEnvironment.GetGame().GetClientManager().LogClonesOut(player.Username);

                SetPlayer(player);
                _player.LoadData();
            }
            catch (IncorrectLoginException e)
            {
                SendNotif("Login error: " + e.Message);
                Disconnect();

                return;
            }

            try
            {
                PylenorEnvironment.GetGame().GetBanManager().CheckForBanConflicts(this);
            }
            catch (ModerationBanException e)
            {
                SendBanMessage(e.Message);
                Disconnect();

                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET online = '1', ip_last = '" + IpAddress +
                                      "' WHERE id = '" + GetPlayer().Id + "' LIMIT 1");
                dbClient.ExecuteQuery("UPDATE user_info SET login_timestamp = '" + PylenorEnvironment.GetUnixTimestamp() +
                                      "' WHERE user_id = '" + GetPlayer().Id + "' LIMIT 1");
            }

            var rights = PylenorEnvironment.GetGame().GetRoleManager().GetRightsForHabbo(GetPlayer());
            var serverPacket = new ServerPacket(2);
            serverPacket.WriteInteger(rights.Count);

            foreach (var right in rights)
            {
                serverPacket.WriteString(right);
            }

            SendPacket(serverPacket);

            if (GetPlayer().HasFuse("fuse_mod"))
            {
                SendPacket(PylenorEnvironment.GetGame().GetModerationTool().SerializeTool());
                PylenorEnvironment.GetGame().GetModerationTool().SendOpenTickets(this);
            }

            SendPacket(GetPlayer().GetAvatarEffectsInventoryComponent().Serialize());

            serverPacket = new ServerPacket(290);
            serverPacket.WriteBoolean(true);
            serverPacket.WriteBoolean(false);
            SendPacket(serverPacket);

            serverPacket = new ServerPacket(3);
            SendPacket(serverPacket);

            serverPacket = new ServerPacket(517);
            serverPacket.WriteBoolean(true);
            SendPacket(serverPacket);

            if (PylenorEnvironment.GetGame().GetPixelManager().NeedsUpdate(this))
            {
                PylenorEnvironment.GetGame().GetPixelManager().GivePixels(this);
            }

            serverPacket = new ServerPacket(455);
            serverPacket.WriteUInt(GetPlayer().HomeRoom);
            SendPacket(serverPacket);

            serverPacket = new ServerPacket(458);
            serverPacket.WriteInteger(30);
            serverPacket.WriteInteger(GetPlayer().FavoriteRooms.Count);

            foreach (var id in GetPlayer().FavoriteRooms)
            {
                serverPacket.WriteUInt(id);
            }

            SendPacket(serverPacket);

            WelcomeMessage(ServerConfiguration.MOTD);
            PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(this, 11, 1);
        }

        public void SendBanMessage(string message)
        {
            var banMessage = new ServerPacket(35);
            banMessage.WriteString("A Moderator has kicked you from habbo Hotel:", 13);
            banMessage.WriteString(message);
            SendPacket(banMessage);
        }

        public void SendNotification(string message, string unknown)
        {
            var serverPacket = new ServerPacket(161);
            serverPacket.WriteString(message);
            serverPacket.WriteString(unknown);
            SendPacket(serverPacket);
        }

        public void SendNotif(string message)
        {
            SendNotif(message, false);
        }

        public void SendNotif(string message, bool fromManager)
        {
            var packet = new ServerPacket(fromManager ? 139 : (uint) 161);
            packet.WriteString(message);
            SendPacket(packet);
        }

        public void WelcomeMessage(string message)
        {
            var packet = new ServerPacket(810);
            packet.WriteUInt(1);
            packet.WriteString(message);
            SendPacket(packet);
        }

        public void SendNotif(string message, string url)
        {
            var packet = new ServerPacket(161);
            packet.WriteString(message);
            packet.WriteString(url);
            SendPacket(packet);
        }

        public void ClearRoom()
        {
            GetPlayer().LoadingRoom = 0;
            GetPlayer().LoadingChecksPassed = false;
        }

        public void PrepareRoom(uint id, string password)
        {
            ClearRoom();

            if (PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(id) == null)
            {
                return;
            }

            if (GetPlayer().InRoom)
            {
                var oldRoom = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(GetPlayer().CurrentRoomId);

                oldRoom?.RemoveUserFromRoom(this, false, false);
            }

            if (!PylenorEnvironment.GetGame().GetRoomManager().IsRoomLoaded(id))
            {
                PylenorEnvironment.GetGame().GetRoomManager().LoadRoom(id);
            }

            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(id);

            if (room == null)
            {
                return;
            }

            GetPlayer().LoadingRoom = id;

            if (room.UserIsBanned(GetPlayer().Id))
            {
                if (room.HasBanExpired(GetPlayer().Id))
                {
                    room.RemoveBan(GetPlayer().Id);
                }
                else
                {
                    var packet = new ServerPacket(224);
                    packet.WriteInteger(4);
                    SendPacket(packet);

                    SendPacket(new ServerPacket(18));

                    return;
                }
            }

            if (room.UsersNow >= room.UsersMax)
            {
                if (
                    !PylenorEnvironment.GetGame()
                        .GetRoleManager()
                        .RankHasRight(GetPlayer().Rank, "fuse_enter_full_rooms"))
                {
                    var packet = new ServerPacket(224);
                    packet.WriteInteger(1);
                    SendPacket(packet);

                    SendPacket(new ServerPacket(18));

                    return;
                }
            }

            if (room.Type == "public")
            {
                if (room.State > 0 && !GetPlayer().HasFuse("fuse_mod"))
                {
                    SendNotif("This public room is accessible to staff only.");

                    SendPacket(new ServerPacket(18));

                    return;
                }

                var packet = new ServerPacket(166);
                packet.WriteString("/client/public/" + room.ModelName + "/" + room.RoomId);
                SendPacket(packet);
            }
            else if (room.Type == "private")
            {
                SendPacket(new ServerPacket(19));

                if (!GetPlayer().HasFuse("fuse_enter_any_room") && !room.CheckRights(this, true) &&
                    !GetPlayer().IsTeleporting)
                {
                    if (room.State == 1)
                    {
                        if (room.UserCount == 0)
                        {
                            SendPacket(new ServerPacket(131));
                        }
                        else
                        {
                            var packet = new ServerPacket(91);
                            packet.WriteString(string.Empty);
                            SendPacket(packet);

                            var ringMessage = new ServerPacket(91);
                            ringMessage.WriteString(GetPlayer().Username);
                            room.SendMessageToUsersWithRights(ringMessage);
                        }

                        return;
                    }
                    if (room.State == 2)
                    {
                        if (!string.Equals(password, room.Password, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var packet = new ServerPacket(33);
                            packet.WriteInteger(-100002);
                            SendPacket(packet);

                            SendPacket(new ServerPacket(18));

                            return;
                        }
                    }
                }

                var privateComposer = new ServerPacket(166);
                privateComposer.WriteString("/client/private/" + room.RoomId + "/id");
                SendPacket(privateComposer);
            }

            GetPlayer().LoadingChecksPassed = true;

            FinalizeRoom();
        }

        public void FinalizeRoom()
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(GetPlayer().LoadingRoom);

            if (room == null || !GetPlayer().LoadingChecksPassed)
            {
                return;
            }

            // todo: Room.SerializeGroupBadges()
            var packet = new ServerPacket(309);
            packet.WriteString("IcIrDs43103s19014d5a1dc291574a508bc80a64663e61a00");
            SendPacket(packet);

            packet = new ServerPacket(69);
            packet.WriteString(room.ModelName);
            packet.WriteUInt(room.RoomId);
            SendPacket(packet);

            if (GetPlayer().SpectatorMode)
            {
                SendPacket(new ServerPacket(254));
            }

            if (room.Type != "private") return;
            if (room.Wallpaper != "0.0")
            {
                packet = new ServerPacket(46);
                packet.WriteString("wallpaper");
                packet.WriteString(room.Wallpaper);
                SendPacket(packet);
            }

            if (room.Floor != "0.0")
            {
                packet = new ServerPacket(46);
                packet.WriteString("floor");
                packet.WriteString(room.Floor);
                SendPacket(packet);
            }

            packet = new ServerPacket(46);
            packet.WriteString("landscape");
            packet.WriteString(room.Landscape);
            SendPacket(packet);

            if (room.CheckRights(this, true))
            {
                SendPacket(new ServerPacket(42));
                SendPacket(new ServerPacket(47));
            }
            else if (room.CheckRights(this))
            {
                SendPacket(new ServerPacket(42));
            }

            packet = new ServerPacket(345);

            if (GetPlayer().RatedRooms.Contains(room.RoomId) || room.CheckRights(this, true))
            {
                packet.WriteInteger(room.Score);
            }
            else
            {
                packet.WriteInteger(-1);
            }

            SendPacket(packet);

            if (room.HasOngoingEvent)
            {
                SendPacket(room.Event.Serialize(this));
            }
            else
            {
                packet = new ServerPacket(370);
                packet.WriteString("-1");
                SendPacket(packet);
            }
        }
    }
}