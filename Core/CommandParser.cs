﻿using System.Text;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.Core
{
    internal class CommandParser
    {
        public static void Parse(string input)
        {
            var Params = input.Split(' ');

            switch (Params[0])
            {
                case "reload_models":
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetRoomManager().LoadModels(dbClient);
                    }
                    break;

                case "reload_bans":
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetBanManager().LoadBans(dbClient);
                    }
                    break;

                case "reload_navigator":
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetNavigator().Initialize(dbClient);
                    }
                    PylenorEnvironment.GetLogging().WriteLine("Re-initialized navigator successfully.");

                    break;

                case "reload_items":
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetItemManager().LoadItems(dbClient);
                    }
                    PylenorEnvironment.GetLogging()
                        .WriteLine("Please note that changes may not be reflected immediatly in currently loaded rooms.");

                    break;

                case "reload_help":
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetHelpTool().LoadCategories(dbClient);
                        PylenorEnvironment.GetGame().GetHelpTool().LoadTopics(dbClient);
                    }
                    PylenorEnvironment.GetLogging().WriteLine("Reloaded help categories and topics successfully.");

                    break;

                case "reload_catalog":
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetCatalog().Initialize(dbClient);
                    }
                    PylenorEnvironment.GetGame().GetClientManager().BroadcastMessage(new ServerPacket(441));
                    PylenorEnvironment.GetLogging().WriteLine("Published catalog successfully.");

                    break;

                case "reload_roles":
                    var db = PylenorEnvironment.GetDatabase().GetClient();
                    PylenorEnvironment.GetGame().GetRoleManager().LoadRoles(db);
                    PylenorEnvironment.GetGame().GetRoleManager().LoadRights(db);
                    PylenorEnvironment.GetLogging().WriteLine("Reloaded ranks and rights successfully.");

                    break;

                case "cls":

                    PylenorEnvironment.GetLogging().Clear();
                    break;
                case "about":

                    PylenorEnvironment.GetLogging()
                        .WriteLine("This emulator was created by Method and Nillus, and carried on by PowahAlert");

                    break;

                case "help":

                    PylenorEnvironment.GetLogging()
                        .WriteLine(
                            "Available commands are: about, cls, close, help, reload_catalog, reload_navigator, reload_roles, reload_help, reload_items");

                    break;

                case "close":

                    PylenorEnvironment.Destroy();

                    break;

                default:

                    PylenorEnvironment.GetLogging()
                        .WriteLine(
                            "Unrecognized command or operation: " + input +
                            ". Use 'help' for a list of available commands.", LogLevel.Warning);

                    break;
            }
        }

        public static string MergeParams(string[] Params, int start)
        {
            var mergedParams = new StringBuilder();

            for (var i = 0; i < Params.Length; i++)
            {
                if (i < start)
                {
                    continue;
                }

                if (i > start)
                {
                    mergedParams.Append(" ");
                }

                mergedParams.Append(Params[i]);
            }

            return mergedParams.ToString();
        }
    }
}