﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Pylenor.Core
{
    internal class ConfigurationData
    {
        public Dictionary<string, string> Data;

        public ConfigurationData(string filePath)
        {
            Data = new Dictionary<string, string>();

            if (!File.Exists(filePath))
            {
                throw new Exception("Unable to locate configuration file at '" + filePath + "'.");
            }

            try
            {
                using (var stream = new StreamReader(filePath))
                {
                    string line = null;

                    while ((line = stream.ReadLine()) != null)
                    {
                        if (line.Length < 1 || line.StartsWith("#"))
                        {
                            continue;
                        }

                        var delimiterIndex = line.IndexOf('=');

                        if (delimiterIndex == -1) continue;
                        var key = line.Substring(0, delimiterIndex);
                        var val = line.Substring(delimiterIndex + 1);

                        Data.Add(key, val);
                    }

                    stream.Close();
                }
            }

            catch (Exception e)
            {
                throw new Exception("Could not process configuration file: " + e.Message);
            }
        }
    }
}