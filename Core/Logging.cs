﻿using System;

namespace Pylenor.Core
{
    public enum LogLevel
    {
        Debug = 0,
        Information = 1,
        Warning = 2,
        Error = 3
    }

    public class Logging
    {
        public string LogFileName;
        public LogLevel MinimumLogLevel;

        public void Clear()
        {
            Console.Clear();
        }

        public void WriteLine(string line)
        {
            WriteLine(line, LogLevel.Information);
        }

        public void WriteLine(string line, LogLevel level)
        {
            if (level < MinimumLogLevel)
            {
                return;
            }

            SetLogColor(level);
            Console.WriteLine("[" + DateTime.Now + "]: " + line);
            ResetLogColor();
        }

        public void Write(string line)
        {
            SetLogColor(LogLevel.Information);
            Console.Write("[" + DateTime.Now + "]: " + line);
            ResetLogColor();
        }

        private static void SetLogColor(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                case LogLevel.Information:
                default:

                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;

                case LogLevel.Warning:

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;

                case LogLevel.Error:

                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }
        }

        private static void ResetLogColor()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}