﻿namespace Pylenor.HabboHotel.Achievements
{
    internal class Achievement
    {
        public string BadgeCode;
        public bool DynamicBadgeLevel;
        public uint Id;
        public int Levels;
        public int PixelBase;
        public double PixelMultiplier;

        public Achievement(uint id, int levels, string badgeCode, int pixelBase, double pixelMultiplier,
            bool dynamicBadgeLevel)
        {
            Id = id;
            Levels = levels;
            BadgeCode = badgeCode;
            PixelBase = pixelBase;
            PixelMultiplier = pixelMultiplier;
            DynamicBadgeLevel = dynamicBadgeLevel;
        }
    }
}