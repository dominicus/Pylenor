﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Achievements
{
    internal class AchievementManager
    {
        public Dictionary<uint, Achievement> Achievements;

        public AchievementManager()
        {
            Achievements = new Dictionary<uint, Achievement>();
        }

        public void LoadAchievements(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading achievements");
            Achievements.Clear();
            var data = dbClient.ReadDataTable("SELECT * FROM achievements;");

            if (data == null) return;
            foreach (DataRow row in data.Rows)
            {
                Achievements.Add((uint) row["id"],
                    new Achievement((uint) row["id"], (int) row["levels"], (string) row["badge"],
                        (int) row["pixels_base"], (double) row["pixels_multiplier"],
                        PylenorEnvironment.EnumToBool(row["dynamic_badgelevel"].ToString())));
            }
            Console.WriteLine("...completed!");
        }

        public bool UserHasAchievement(Session session, uint id, int minLevel)
        {
            return session.GetPlayer().Achievements.ContainsKey(id) && session.GetPlayer().Achievements[id] >= minLevel;
        }

        public ServerPacket SerializeAchievementList(Session session)
        {
            var achievementsToList = new List<Achievement>();
            var nextAchievementLevels = new Dictionary<uint, int>();

            foreach (var achievement in Achievements.Values)
            {
                if (!session.GetPlayer().Achievements.ContainsKey(achievement.Id))
                {
                    achievementsToList.Add(achievement);
                    nextAchievementLevels.Add(achievement.Id, 1);
                }
                else
                {
                    if (session.GetPlayer().Achievements[achievement.Id] >= achievement.Levels)
                    {
                        continue;
                    }

                    achievementsToList.Add(achievement);
                    nextAchievementLevels.Add(achievement.Id, session.GetPlayer().Achievements[achievement.Id] + 1);
                }
            }

            var message = new ServerPacket(436);
            message.WriteInteger(achievementsToList.Count);

            foreach (var achievement in achievementsToList)
            {
                var level = nextAchievementLevels[achievement.Id];

                message.WriteUInt(achievement.Id);
                message.WriteInteger(level);
                message.WriteString(FormatBadgeCode(achievement.BadgeCode, level,
                    achievement.DynamicBadgeLevel));
            }

            return message;
        }

        public void UnlockAchievement(Session session, uint achievementId, int level)
        {
            // Get the achievement
            var achievement = Achievements[achievementId];

            // Make sure the achievement is valid and has not already been unlocked
            if (achievement == null || UserHasAchievement(session, achievement.Id, level) || level < 1 ||
                level > achievement.Levels)
            {
                return;
            }

            // Calculate the pixel value for this achievement
            var value = CalculateAchievementValue(achievement.PixelBase, achievement.PixelMultiplier, level);

            // Remove any previous badges for this achievement (old levels)
            var badgesToRemove = (from badge in session.GetPlayer().GetBadgeComponent().BadgeList
                where badge.Code.StartsWith(achievement.BadgeCode)
                select badge.Code).ToList();

            foreach (var remove in badgesToRemove)
            {
                session.GetPlayer().GetBadgeComponent().RemoveBadge(remove);
            }

            // Give the user the new badge
            session.GetPlayer()
                .GetBadgeComponent()
                .GiveBadge(FormatBadgeCode(achievement.BadgeCode, level, achievement.DynamicBadgeLevel), true);

            // Update or set the achievement level for the user
            if (session.GetPlayer().Achievements.ContainsKey(achievement.Id))
            {
                session.GetPlayer().Achievements[achievement.Id] = level;
                using (var db = PylenorEnvironment.GetDatabase().GetClient())
                {
                    db.ExecuteQuery("UPDATE user_achievements SET achievement_level = '" + level +
                                    "' WHERE user_id = '" + session.GetPlayer().Id + "' AND achievement_id = '" +
                                    achievement.Id + "' LIMIT 1");
                }
            }
            else
            {
                session.GetPlayer().Achievements.Add(achievement.Id, level);
                using (var db = PylenorEnvironment.GetDatabase().GetClient())
                {
                    db.ExecuteQuery(
                        "INSERT INTO user_achievements (user_id,achievement_id,achievement_level) VALUES ('" +
                        session.GetPlayer().Id + "','" + achievement.Id + "','" + level + "')");
                }
            }

            // Notify the user of the achievement gain
            var achievementUpdate = new ServerPacket(437);
            achievementUpdate.WriteUInt(achievement.Id);
            achievementUpdate.WriteInteger(level);
            achievementUpdate.WriteString(FormatBadgeCode(achievement.BadgeCode, level, achievement.DynamicBadgeLevel));

            if (level > 1)
            {
                achievementUpdate.WriteString(FormatBadgeCode(achievement.BadgeCode, (level - 1),
                    achievement.DynamicBadgeLevel));
            }
            else
            {
                achievementUpdate.WriteString("");
            }

            session.SendPacket(achievementUpdate);

            // Give the user the pixels he deserves
            session.GetPlayer().ActivityPoints += value;
            session.GetPlayer().UpdateActivityPointsBalance(true, value);
        }

        public int CalculateAchievementValue(int baseValue, double multiplier, int level)
        {
            return (baseValue + (50*level));
        }

        public string FormatBadgeCode(string badgeTemplate, int level, bool dyn)
        {
            if (!dyn)
            {
                return badgeTemplate;
            }

            return badgeTemplate + level;
        }
    }
}