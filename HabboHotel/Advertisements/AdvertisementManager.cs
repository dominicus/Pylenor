﻿using System;
using System.Collections.Generic;
using System.Data;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Advertisements
{
    internal class AdvertisementManager
    {
        public List<RoomAdvertisement> RoomAdvertisements;

        public AdvertisementManager()
        {
            RoomAdvertisements = new List<RoomAdvertisement>();
        }

        public void LoadRoomAdvertisements(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading advertisements");
            RoomAdvertisements.Clear();

            var data = dbClient.ReadDataTable("SELECT * FROM room_ads WHERE enabled = '1';");

            if (data == null) return;
            foreach (DataRow row in data.Rows)
            {
                RoomAdvertisements.Add(new RoomAdvertisement((uint) row["id"], (string) row["ad_image"],
                    (string) row["ad_link"], (int) row["views"], (int) row["views_limit"]));
            }
            Console.WriteLine("...completed!");
        }

        public RoomAdvertisement GetRandomRoomAdvertisement()
        {
            if (RoomAdvertisements.Count <= 0)
            {
                return null;
            }

            while (true)
            {
                var rndId = PylenorEnvironment.GetRandomNumber(0, (RoomAdvertisements.Count - 1));

                if (RoomAdvertisements[rndId] != null && !RoomAdvertisements[rndId].ExceededLimit)
                {
                    return RoomAdvertisements[rndId];
                }
            }
        }
    }
}