﻿namespace Pylenor.HabboHotel.Advertisements
{
    internal class RoomAdvertisement
    {
        public string AdImage;
        public string AdLink;
        public uint Id;
        public int Views;
        public int ViewsLimit;

        public RoomAdvertisement(uint id, string adImage, string adLink, int views, int viewsLimit)
        {
            Id = id;
            AdImage = adImage;
            AdLink = adLink;
            Views = views;
            ViewsLimit = viewsLimit;
        }

        public bool ExceededLimit
        {
            get
            {
                if (ViewsLimit <= 0)
                {
                    return false;
                }

                return Views >= ViewsLimit;
            }
        }

        public void OnView()
        {
            Views++;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE room_ads SET views = views + 1 WHERE id = '" + Id + "'");
            }
        }
    }
}