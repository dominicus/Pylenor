﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Core;
using Pylenor.HabboHotel.Items;
using Pylenor.HabboHotel.Pets;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Catalogs
{
    internal class Catalog
    {
        private readonly Marketplace _marketplace;

        private readonly VoucherHandler _voucherHandler;
        private ServerPacket[] _cache;
        public List<EcotronReward> EcotronRewards;
        public List<CatalogItem> Items;
        public Dictionary<int, CatalogPage> Pages;

        public Catalog()
        {
            _voucherHandler = new VoucherHandler();
            _marketplace = new Marketplace();
        }

        public void Initialize(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading catalogue");
            Pages = new Dictionary<int, CatalogPage>();
            Items = new List<CatalogItem>();
            EcotronRewards = new List<EcotronReward>();
            var table = new Hashtable();

            var data = dbClient.ReadDataTable("SELECT SQL_NO_CACHE * FROM catalog_pages ORDER BY order_num ASC;");
            var ecoData = dbClient.ReadDataTable("SELECT SQL_NO_CACHE * FROM ecotron_rewards ORDER BY item_id;");
            var itemsTable = dbClient.ReadDataTable("SELECT * FROM catalog_items;");
            if (itemsTable != null)
            {
                foreach (DataRow row in itemsTable.Rows)
                {
                    if (row["item_ids"].ToString() != "" && (int) row["amount"] > 0)
                    {
                        table.Add((uint) row["id"],
                            new CatalogItem((uint) row["id"], (int) row["page_id"], (string) row["catalog_name"],
                                (string) row["item_ids"],
                                (int) row["cost_credits"], (int) row["cost_pixels"], (int) row["amount"]));
                    }
                }
            }

            if (data != null)
            {
                foreach (DataRow row in data.Rows)
                {
                    var visible = false;
                    var enabled = false;

                    if (row["visible"].ToString() == "1")
                    {
                        visible = true;
                    }

                    if (row["enabled"].ToString() == "1")
                    {
                        enabled = true;
                    }

                    Pages.Add((int) row["id"], new CatalogPage((int) row["id"], (int) row["parent_id"],
                        (string) row["caption"], visible, enabled, (uint) row["min_rank"],
                        PylenorEnvironment.EnumToBool(row["club_only"].ToString()), (int) row["icon_color"],
                        (int) row["icon_image"], (string) row["page_layout"], (string) row["page_headline"],
                        (string) row["page_teaser"], (string) row["page_special"], (string) row["page_text1"],
                        (string) row["page_text2"], (string) row["page_text_details"], (string) row["page_text_teaser"],
                        ref table));
                }
            }

            if (ecoData != null)
            {
                foreach (DataRow row in ecoData.Rows)
                {
                    EcotronRewards.Add(new EcotronReward((uint) row["id"], (uint) row["display_id"],
                        (uint) row["item_id"], (uint) row["reward_level"]));
                }
            }
            Console.WriteLine("...completed!");
        }

        public void CacheCatalog()
        {
            PylenorEnvironment.GetLogging().Write("Loading catalogue cache");
            var length = PylenorEnvironment.GetGame().GetRoleManager().Roles.Count + 1;
            _cache = new ServerPacket[length];
            for (var i = 1; i < length; i++)
            {
                _cache[i] = SerializeIndexComposer(i);
            }
            foreach (var page in Pages.Values)
            {
                page.SetPacket();
            }
            Console.WriteLine("...completed!");
        }

        public CatalogItem GetItem(uint id)
        {
            return Pages.Values.SelectMany(page => page.Items).FirstOrDefault(item => item.Id == id);
        }

        public int GetTreeSize(int index, int treeId)
        {
            return Pages.Values.Count(page => page.MinRank <= index && page.ParentId == treeId);
        }

        public CatalogPage GetPage(int page)
        {
            return Pages.ContainsKey(page) ? Pages[page] : null;
        }

        public void HandlePurchase(Session session, int pageId, uint itemId, string extraData, bool isGift,
            string giftUser, string giftMessage)
        {
            var page = GetPage(pageId);

            if (page == null || !page.Enabled || !page.Visible)
            {
                return;
            }

            if (page.ClubOnly && !session.GetPlayer().GetSubscriptionManager().HasSubscription("habbo_club"))
            {
                return;
            }

            var item = page.GetItem(itemId);

            if (item == null)
            {
                return;
            }

            uint giftUserId = 0;
            var serverPacket = new ServerPacket(0);
            if (isGift)
            {
                if (!item.GetBaseItem().AllowGift)
                {
                    return;
                }

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("gift_user", giftUser);

                    try
                    {
                        giftUserId =
                            (uint)
                                dbClient.ReadDataRow(
                                    "SELECT SQL_NO_CACHE id FROM users WHERE username = @gift_user LIMIT 1")
                                    [0];
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }

                if (giftUserId == 0)
                {
                    serverPacket = new ServerPacket(76);
                    serverPacket.WriteBoolean(true);
                    serverPacket.WriteString(giftUser);
                    session.SendPacket(serverPacket);

                    return;
                }
            }

            var creditsError = false;
            var pixelError = false;

            if (session.GetPlayer().Credits < item.CreditsCost)
            {
                creditsError = true;
            }

            if (session.GetPlayer().ActivityPoints < item.PixelsCost)
            {
                pixelError = true;
            }

            if (creditsError || pixelError)
            {
                serverPacket = new ServerPacket(68);
                serverPacket.WriteBoolean(creditsError);
                serverPacket.WriteBoolean(pixelError);
                session.SendPacket(serverPacket);

                return;
            }

            if (isGift && item.GetBaseItem().Type.ToLower() == "e")
            {
                session.SendNotif("You can not send this item as a gift.");
                return;
            }

            // Extra Data is _NOT_ filtered at this point and MUST BE VERIFIED BELOW:
            switch (item.GetBaseItem().InteractionType.ToLower())
            {
                case "pet":

                    try
                    {
                        var bits = extraData.Split('\n');
                        var petName = bits[0];
                        var race = bits[1];
                        var color = bits[2];

                        // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                        int.Parse(race); // to trigger any possible errors

                        if (!CheckPetName(petName))
                        {
                            return;
                        }

                        if (race.Length != 3)
                        {
                            return;
                        }

                        if (color.Length != 6)
                        {
                            return;
                        }

                        extraData = extraData + "\n" + itemId;
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    break;

                case "roomeffect":

                    double number = 0;

                    try
                    {
                        number = double.Parse(extraData);
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    extraData = number.ToString(CultureInfo.InvariantCulture).Replace(',', '.');
                    break; // maintain extra data // todo: validate

                case "postit":

                    extraData = "FFFF33";
                    break;

                case "dimmer":

                    extraData = "1,1,1,#000000,255";
                    break;

                case "trophy":

                    extraData = session.GetPlayer().Username + Convert.ToChar(9) + DateTime.Now.Day + "-" +
                                DateTime.Now.Month + "-" + DateTime.Now.Year + Convert.ToChar(9) +
                                PylenorEnvironment.FilterInjectionChars(extraData, true);
                    break;

                default:

                    extraData = "";
                    break;
            }

            if (item.CreditsCost > 0)
            {
                session.GetPlayer().Credits -= item.CreditsCost;
                session.GetPlayer().UpdateCreditsBalance(true);
            }

            if (item.PixelsCost > 0)
            {
                session.GetPlayer().ActivityPoints -= item.PixelsCost;
                session.GetPlayer().UpdateActivityPointsBalance(true);
            }

            serverPacket = new ServerPacket(67);
            serverPacket.WriteUInt(item.GetBaseItem().ItemId);
            serverPacket.WriteString(item.GetBaseItem().Name);
            serverPacket.WriteInteger(item.CreditsCost);
            serverPacket.WriteInteger(item.PixelsCost);
            serverPacket.WriteInteger(0); // R63 fix
            serverPacket.WriteInteger(1);
            serverPacket.WriteString(item.GetBaseItem().Type.ToLower());
            serverPacket.WriteInteger(item.GetBaseItem().Sprite);
            serverPacket.WriteString("");
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(-1);
            serverPacket.WriteString("");
            session.SendPacket(serverPacket);

            if (isGift)
            {
                var genId = GenerateItemId();
                var present = GeneratePresent();

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("gift_message", "!" + giftMessage);
                    dbClient.AddParamWithValue("extra_data", extraData);

                    dbClient.ExecuteQuery("INSERT INTO user_items (id,user_id,base_item,extra_data) VALUES ('" + genId +
                                          "','" + giftUserId + "','" + present.ItemId + "',@gift_message)");
                    dbClient.ExecuteQuery("INSERT INTO user_presents (item_id,base_id,amount,extra_data) VALUES ('" +
                                          genId +
                                          "','" + item.GetBaseItem().ItemId + "','" + item.Amount + "',@extra_data)");
                }
                var receiver = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(giftUserId);

                if (receiver != null)
                {
                    receiver.SendNotif("You have received a gift! Check your inventory.");
                    receiver.GetPlayer().GetInventoryComponent().UpdateItems(true);
                }

                session.SendNotif("Gift sent successfully!");
            }
            else
            {
                DeliverItems(session, item.GetBaseItem(), item.Amount, extraData);
            }
        }

        public bool CheckPetName(string petName)
        {
            if (petName.Length < 1 || petName.Length > 16)
            {
                return false;
            }

            return PylenorEnvironment.IsValidAlphaNumeric(petName);
        }

        public void DeliverItems(Session session, Item item, int amount, string extraData)
        {
            switch (item.Type.ToLower())
            {
                case "i":
                case "s":

                    for (var i = 0; i < amount; i++)
                    {
                        var generatedId = GenerateItemId();

                        switch (item.InteractionType.ToLower())
                        {
                            case "pet":

                                var petData = extraData.Split('\n');
                                int petType;

                                switch (petData[3])
                                {
                                    // Valid ItemID's
                                    case "2349":
                                        petType = 5; // Pig
                                        break;

                                    case "2430":
                                        petType = 3; // Terrier
                                        break;

                                    case "2431":
                                        petType = 4; // Bear
                                        break;

                                    case "2432":
                                        petType = 1; // Cat
                                        break;

                                    case "2433":
                                        petType = 0; // Dog
                                        break;

                                    case "2434":
                                        petType = 2; // Crocodile
                                        break;

                                    default:
                                        petType = 6; // Error
                                        session.SendNotif(
                                            "Something went wrong! The item type could not be processed. Please do not try to buy this item anymore, instead inform support as soon as possible.");
                                        break;
                                }

                                if (petType != 6)
                                {
                                    var generatedPet = CreatePet(session.GetPlayer().Id, petData[0], petType, petData[1],
                                        petData[2]);

                                    session.GetPlayer().GetInventoryComponent().AddPet(generatedPet);
                                    session.GetPlayer().GetInventoryComponent().AddItem(generatedId, 320, "0");
                                }
                                else
                                {
                                    PylenorEnvironment.GetLogging()
                                        .WriteLine(
                                            "Pet Error: " + "Someone just tried to buy ItemID: " + petData[3] +
                                            " which is not a valid pet. (Catalog.cs)", LogLevel.Error);
                                }
                                break;

                            case "teleport":

                                var teleTwo = GenerateItemId();
                                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                                {
                                    dbClient.ExecuteQuery(
                                        "INSERT INTO tele_links (tele_one_id,tele_two_id) VALUES ('" +
                                        generatedId + "','" + teleTwo + "')");
                                    dbClient.ExecuteQuery(
                                        "INSERT INTO tele_links (tele_one_id,tele_two_id) VALUES ('" +
                                        teleTwo + "','" + generatedId + "')");
                                }

                                session.GetPlayer().GetInventoryComponent().AddItem(teleTwo, item.ItemId, "0");
                                session.GetPlayer().GetInventoryComponent().AddItem(generatedId, item.ItemId, "0");
                                break;

                            case "dimmer":
                                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                                {
                                    dbClient.ExecuteQuery(
                                        "INSERT INTO room_items_moodlight (item_id,enabled,current_preset,preset_one,preset_two,preset_three) VALUES ('" +
                                        generatedId + "','0','1','#000000,255,0','#000000,255,0','#000000,255,0')");
                                }

                                session.GetPlayer().GetInventoryComponent().AddItem(generatedId, item.ItemId, extraData);
                                break;

                            default:

                                session.GetPlayer().GetInventoryComponent().AddItem(generatedId, item.ItemId, extraData);
                                break;
                        }
                    }

                    session.GetPlayer().GetInventoryComponent().UpdateItems(true);
                    break;

                case "e":

                    for (var i = 0; i < amount; i++)
                    {
                        session.GetPlayer().GetAvatarEffectsInventoryComponent().AddEffect(item.Sprite, 3600);
                    }

                    break;

                case "h":

                    for (var i = 0; i < amount; i++)
                    {
                        session.GetPlayer().GetSubscriptionManager().AddOrExtendSubscription("habbo_club", 2678400);
                    }

                    if (!session.GetPlayer().GetBadgeComponent().HasBadge("HC1"))
                    {
                        session.GetPlayer().GetBadgeComponent().GiveBadge("HC1", true);
                    }

                    var serverPacket = new ServerPacket(7);
                    serverPacket.WriteString("habbo_club");

                    if (session.GetPlayer().GetSubscriptionManager().HasSubscription("habbo_club"))
                    {
                        double expire =
                            session.GetPlayer().GetSubscriptionManager().GetSubscription("habbo_club").ExpireTime;
                        var timeLeft = expire - PylenorEnvironment.GetUnixTimestamp();
                        var totalDaysLeft = (int) Math.Ceiling(timeLeft/86400);
                        var monthsLeft = totalDaysLeft/31;

                        if (monthsLeft >= 1) monthsLeft--;

                        serverPacket.WriteInteger(totalDaysLeft - (monthsLeft*31));
                        serverPacket.WriteBoolean(true);
                        serverPacket.WriteInteger(monthsLeft);
                    }
                    else
                    {
                        for (var i = 0; i < 3; i++)
                        {
                            serverPacket.WriteInteger(0);
                        }
                    }

                    session.SendPacket(serverPacket);

                    var rights = PylenorEnvironment.GetGame().GetRoleManager().GetRightsForHabbo(session.GetPlayer());

                    serverPacket = new ServerPacket(2);
                    serverPacket.WriteInteger(rights.Count);

                    foreach (var right in rights)
                    {
                        serverPacket.WriteString(right);
                    }

                    session.SendPacket(serverPacket);

                    break;


                case "v":

                    for (var i = 0; i < amount; i++)
                    {
                        session.GetPlayer().GetSubscriptionManager().AddOrExtendSubscription("habbo_vip", 2678400);
                    }

                    if (!session.GetPlayer().GetBadgeComponent().HasBadge("ACH_VipClub1"))
                    {
                        session.GetPlayer().GetBadgeComponent().GiveBadge("ACH_VipClub1", true);
                    }

                    var vPacket = new ServerPacket(7);
                    vPacket.WriteString("habbo_vip");

                    if (session.GetPlayer().GetSubscriptionManager().HasSubscription("habbo_vip"))
                    {
                        double expire =
                            session.GetPlayer().GetSubscriptionManager().GetSubscription("habbo_vip").ExpireTime;
                        var timeLeft = expire - PylenorEnvironment.GetUnixTimestamp();
                        var totalDaysLeft = (int) Math.Ceiling(timeLeft/86400);
                        var monthsLeft = totalDaysLeft/31;

                        if (monthsLeft >= 1) monthsLeft--;

                        vPacket.WriteInteger(totalDaysLeft - (monthsLeft*31));
                        vPacket.WriteBoolean(true);
                        vPacket.WriteInteger(monthsLeft);
                    }
                    else
                    {
                        for (var i = 0; i < 3; i++)
                        {
                            vPacket.WriteInteger(0);
                        }
                    }

                    session.SendPacket(vPacket);

                    var rights2 = PylenorEnvironment.GetGame().GetRoleManager().GetRightsForHabbo(session.GetPlayer());

                    vPacket = new ServerPacket(2);
                    vPacket.WriteInteger(rights2.Count);

                    foreach (var right in rights2)
                    {
                        vPacket.WriteString(right);
                    }

                    session.SendPacket(vPacket);

                    break;

                default:

                    session.SendNotif(
                        "Something went wrong! The item type could not be processed. Please do not try to buy this item anymore, instead inform support as soon as possible.");
                    break;
            }
        }

        public Item GeneratePresent()
        {
            var random = PylenorEnvironment.GetRandomNumber(0, 6);

            switch (random)
            {
                case 1:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(165); // present_gen1

                case 2:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(166); // present_gen2

                case 3:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(167); // present_gen3

                case 4:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(168); // present_gen4

                case 5:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(169); // present_gen5

                case 6:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(170); // present_gen6

                // ReSharper disable once RedundantCaseLabel
                case 0:
                default:

                    return PylenorEnvironment.GetGame().GetItemManager().GetItem(164); // present_gen
            }
        }

        public Pet CreatePet(uint userId, string name, int type, string race, string color)
        {
            DataRow row;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", userId);
                dbClient.AddParamWithValue("name", name);
                dbClient.AddParamWithValue("type", type);
                dbClient.AddParamWithValue("race", race);
                dbClient.AddParamWithValue("color", color);
                dbClient.AddParamWithValue("createstamp", PylenorEnvironment.GetUnixTimestamp());
                dbClient.ReadDataRow(
                    "INSERT INTO user_pets (user_id,name,type,race,color,expirience,energy,createstamp) VALUES (@userid,@name,@type,@race,@color,0,100,@createstamp)");
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", userId);
                dbClient.AddParamWithValue("name", name);
                row = dbClient.ReadDataRow(
                    "SELECT SQL_NO_CACHE * FROM user_pets WHERE user_id = @userid AND name = @name LIMIT 1");
            }
            return GeneratePetFromRow(row);
        }

        public Pet GeneratePetFromRow(DataRow row)
        {
            if (row == null)
            {
                return null;
            }

            return new Pet((uint) row["id"], (uint) row["user_id"], (uint) row["room_id"], (string) row["name"],
                (uint) row["type"], (string) row["race"], (string) row["color"], (int) row["expirience"],
                (int) row["energy"], (int) row["nutrition"], (int) row["respect"], (double) row["createstamp"],
                (int) row["x"], (int) row["y"], (double) row["z"]);
        }

        public uint GenerateItemId()
        {
            uint i;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                i =
                    (uint) dbClient.ReadDataRow("SELECT SQL_NO_CACHE id_generator FROM item_id_generator LIMIT 1;")[0];
                dbClient.ExecuteQuery("UPDATE item_id_generator SET id_generator = id_generator + 1 LIMIT 1");
            }
            return i;
        }

        public EcotronReward GetRandomEcotronReward()
        {
            uint level = 1;

            if (PylenorEnvironment.GetRandomNumber(1, 2000) == 2000)
            {
                level = 5;
            }
            else if (PylenorEnvironment.GetRandomNumber(1, 200) == 200)
            {
                level = 4;
            }
            else if (PylenorEnvironment.GetRandomNumber(1, 40) == 40)
            {
                level = 3;
            }
            else if (PylenorEnvironment.GetRandomNumber(1, 4) == 4)
            {
                level = 2;
            }

            var possibleRewards = GetEcotronRewardsForLevel(level);

            if (possibleRewards != null && possibleRewards.Count >= 1)
            {
                return possibleRewards[PylenorEnvironment.GetRandomNumber(0, (possibleRewards.Count - 1))];
            }
            return new EcotronReward(0, 0, 1479, 0); // eco lamp two :D
        }

        public List<EcotronReward> GetEcotronRewardsForLevel(uint level)
        {
            return EcotronRewards.Where(reward => reward.RewardLevel == level).ToList();
        }


        public ServerPacket SerializeIndexComposer(int index)
        {
            var packet = new ServerPacket(126);
            packet.WriteBoolean(true);
            packet.WriteInteger(0);
            packet.WriteInteger(0);
            packet.WriteInteger(-1);
            packet.WriteString("");
            packet.WriteInteger(GetTreeSize(index, -1));
            packet.WriteBoolean(true);
            foreach (var page in Pages.Values.Where(page => page.ParentId == -1))
            {
                page.Serialize(index, packet);
                foreach (var subPage in Pages.Values.Where(subPage => subPage.ParentId == page.Id))
                {
                    subPage.Serialize(index, packet);
                }
            }
            return packet;
        }

        public ServerPacket SerializePage(CatalogPage page)
        {
            var pageData = new ServerPacket(127);
            pageData.WriteInteger(page.Id);

            switch (page.Layout)
            {
                case "frontpage":

                    pageData.WriteString("frontpage3");
                    pageData.WriteInteger(3);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteString(page.LayoutTeaser);
                    pageData.WriteString("");
                    pageData.WriteInteger(11);
                    pageData.WriteString(page.Text1);
                    pageData.WriteString("");
                    pageData.WriteString(page.Text2);
                    pageData.WriteString(page.TextDetails);
                    pageData.WriteString("");
                    pageData.WriteString("#FAF8CC");
                    pageData.WriteString("#FAF8CC");
                    pageData.WriteString("Other ways to get more credits >");
                    pageData.WriteString("magic.credits");

                    break;

                case "recycler_info":

                    pageData.WriteString(page.Layout);
                    pageData.WriteInteger(2);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteString(page.LayoutTeaser);
                    pageData.WriteInteger(3);
                    pageData.WriteString(page.Text1);
                    pageData.WriteString(page.Text2);
                    pageData.WriteString(page.TextDetails);

                    break;

                case "recycler_prizes":
                    pageData.WriteString("recycler_prizes");
                    pageData.WriteInteger(1);
                    pageData.WriteString("catalog_recycler_headline3");
                    pageData.WriteInteger(1);
                    pageData.WriteString(page.Text1);

                    break;

                case "spaces":

                    pageData.WriteString(page.Layout);
                    pageData.WriteInteger(1);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteInteger(1);
                    pageData.WriteString(page.Text1);

                    break;

                case "recycler":

                    pageData.WriteString(page.Layout);
                    pageData.WriteInteger(2);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteString(page.LayoutTeaser);
                    pageData.WriteInteger(1);
                    pageData.WriteString(page.Text1, 10);
                    pageData.WriteString(page.Text2);
                    pageData.WriteString(page.TextDetails);

                    break;

                case "trophies":

                    pageData.WriteString("trophies");
                    pageData.WriteInteger(1);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteInteger(2);
                    pageData.WriteString(page.Text1);
                    pageData.WriteString(page.TextDetails);

                    break;

                case "pets":

                    pageData.WriteString("pets");
                    pageData.WriteInteger(2);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteString(page.LayoutTeaser);
                    pageData.WriteInteger(4);
                    pageData.WriteString(page.Text1);
                    pageData.WriteString("Give a name:");
                    pageData.WriteString("Pick a color:");
                    pageData.WriteString("Pick a race:");

                    break;

                case "club_buy":
                    pageData.WriteString("club_buy");
                    pageData.WriteInteger(1);
                    pageData.WriteString("habboclub_2");
                    pageData.WriteInteger(1);
                    break;

                case "club_gifts":
                    pageData.WriteString("club_gifts");
                    pageData.WriteInteger(1);
                    pageData.WriteString("habboclub_2");
                    pageData.WriteInteger(1);
                    pageData.WriteString("");
                    pageData.WriteInteger(1);
                    break;
                case "soundmachine":
                    pageData.WriteString(page.Layout);
                    pageData.WriteInteger(2);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteString(page.LayoutTeaser);
                    pageData.WriteInteger(2);
                    pageData.WriteString(page.Text1);
                    pageData.WriteString(page.TextDetails);
                    break;
                default:

                    pageData.WriteString(page.Layout);
                    pageData.WriteInteger(3);
                    pageData.WriteString(page.LayoutHeadline);
                    pageData.WriteString(page.LayoutTeaser);
                    pageData.WriteString(page.LayoutSpecial);
                    pageData.WriteInteger(3);
                    pageData.WriteString(page.Text1);
                    pageData.WriteString(page.TextDetails);
                    pageData.WriteString(page.TextTeaser);
                    break;
            }
            pageData.WriteInteger(page.Items.Count);
            foreach (var item in page.Items)
            {
                item.Serialize(pageData);
            }
            return pageData;
        }

        public VoucherHandler GetVoucherHandler()
        {
            return _voucherHandler;
        }

        public Marketplace GetMarketplace()
        {
            return _marketplace;
        }

        public ServerPacket SerializeIndex(uint rank)
        {
            if (rank < 1)
            {
                rank = 1;
            }

            if (rank > PylenorEnvironment.GetGame().GetRoleManager().Roles.Count)
            {
                rank = (uint) PylenorEnvironment.GetGame().GetRoleManager().Roles.Count;
            }
            return _cache[rank];
        }
    }
}