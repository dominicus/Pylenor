﻿using System;
using System.Collections.Generic;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.HabboHotel.Items;

namespace Pylenor.HabboHotel.Catalogs
{
    internal class CatalogItem
    {
        public int Amount;
        public int CreditsCost;
        public uint Id;
        public List<uint> ItemIds;
        public string Name;
        public int PageId;
        public int PixelsCost;

        public CatalogItem(uint id, int pageId, string name, string itemIds, int creditsCost, int pixelsCost, int amount)
        {
            Id = id;
            PageId = pageId;
            Name = name;
            ItemIds = new List<uint>();

            foreach (var itemId in itemIds.Split(','))
            {
                ItemIds.Add(uint.Parse(itemId));
            }

            CreditsCost = creditsCost;
            PixelsCost = pixelsCost;
            Amount = amount;
        }

        public bool IsDeal => ItemIds.Count > 1;

        public Item GetBaseItem()
        {
            return IsDeal ? null : PylenorEnvironment.GetGame().GetItemManager().GetItem(ItemIds[0]);
        }

        public void Serialize(ServerPacket packet)
        {
            if (IsDeal)
            {
                throw new NotImplementedException("Multipile item ids set for catalog item #" + Id +
                                                  ", but this is usupported at this point");
            }
            packet.WriteUInt(Id);
            packet.WriteString(Name);
            packet.WriteInteger(CreditsCost);
            packet.WriteInteger(PixelsCost);
            packet.WriteInteger(0); // Snow Credits?
            packet.WriteInteger(1);
            packet.WriteString(GetBaseItem().Type);
            packet.WriteInteger(GetBaseItem().Sprite);
            var text = "";
            if (Name.Contains("wallpaper_single") || Name.Contains("floor_single") || Name.Contains("landscape_single"))
            {
                var array = Name.Split('_');
                text = array[2];
            }
            else
            {
                if (Name.StartsWith("disc_"))
                {
                    text = Name.Split('_')[1];
                }
                else
                {
                    if (GetBaseItem().Name.StartsWith("poster_"))
                    {
                        text = GetBaseItem().Name.Split('_')[1];
                    }
                }
            }
            packet.WriteString(text);
            packet.WriteInteger(Amount);
            packet.WriteInteger(-1);
            packet.WriteInteger(0); //VIP?
        }
    }
}