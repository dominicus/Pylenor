﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Catalogs
{
    internal sealed class CatalogPage
    {
        public string Caption;
        public bool ClubOnly;
        public bool Enabled;

        public int IconColor;
        public int IconImage;
        public List<CatalogItem> Items;

        public string Layout;

        public string LayoutHeadline;
        public string LayoutSpecial;
        public string LayoutTeaser;

        public uint MinRank;
        public int ParentId;

        public string Text1;
        public string Text2;
        public string TextDetails;
        public string TextTeaser;

        public bool Visible;

        public CatalogPage(int id, int parentId, string caption, bool visible, bool enabled,
            uint minRank, bool clubOnly, int iconColor, int iconImage, string layout,
            string layoutHeadline,
            string layoutTeaser, string layoutSpecial, string text1, string text2, string textDetails,
            string textTeaser, ref Hashtable items)
        {
            Items = new List<CatalogItem>();

            Id = id;
            ParentId = parentId;
            Caption = caption;
            Visible = visible;
            Enabled = enabled;
            MinRank = minRank;
            ClubOnly = clubOnly;
            IconColor = iconColor;
            IconImage = iconImage;
            Layout = layout;
            LayoutHeadline = layoutHeadline;
            LayoutTeaser = layoutTeaser;
            LayoutSpecial = layoutSpecial;
            Text1 = text1;
            Text2 = text2;
            TextDetails = textDetails;
            TextTeaser = textTeaser;

            foreach (var item in items.Values.Cast<CatalogItem>().Where(item => item.PageId == id))
            {
                Items.Add(item);
            }
        }

        public ServerPacket Packet { get; private set; }

        public int Id { get; }

        public void SetPacket()
        {
            Packet = PylenorEnvironment.GetGame().GetCatalog().SerializePage(this);
        }

        public CatalogItem GetItem(uint id)
        {
            return Items.FirstOrDefault(item => item.Id == id);
        }

        public void Serialize(int index, ServerPacket packet)
        {
            packet.WriteInteger(IconColor);
            packet.WriteInteger(IconImage);
            packet.WriteInteger(Id);
            packet.WriteString(Caption);
            packet.WriteInteger(PylenorEnvironment.GetGame().GetCatalog().GetTreeSize(index, Id));
            packet.WriteBoolean(Visible);
        }
    }
}