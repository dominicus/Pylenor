﻿using Pylenor.HabboHotel.Items;

namespace Pylenor.HabboHotel.Catalogs
{
    internal class EcotronReward
    {
        public uint BaseId;
        public uint DisplayId;
        public uint Id;
        public uint RewardLevel;

        public EcotronReward(uint id, uint displayId, uint baseId, uint rewardLevel)
        {
            Id = id;
            DisplayId = displayId;
            BaseId = baseId;
            RewardLevel = rewardLevel;
        }

        public Item GetBaseItem()
        {
            return PylenorEnvironment.GetGame().GetItemManager().GetItem(BaseId);
        }
    }
}