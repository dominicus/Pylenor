﻿using System;
using System.Data;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Items;

namespace Pylenor.HabboHotel.Catalogs
{
    internal class Marketplace
    {
        public bool CanSellItem(UserItem item)
        {
            return item.GetBaseItem().AllowTrade && item.GetBaseItem().AllowMarketplaceSell;
        }

        public void SellItem(Session session, uint itemId, int sellingPrice)
        {
            var item = session.GetPlayer().GetInventoryComponent().GetItem(itemId);
            var serverPacket = new ServerPacket(610);
            if (item == null || sellingPrice > 10000 || !CanSellItem(item))
            {
                serverPacket.WriteBoolean(false);
                session.SendPacket(serverPacket);

                return;
            }

            var comission = CalculateComissionPrice(sellingPrice);
            var totalPrice = sellingPrice + comission;
            var itemType = 1;

            if (item.GetBaseItem().Type == "i")
            {
                itemType++;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("public_name", item.GetBaseItem().PublicName);
                dbClient.AddParamWithValue("extra_data", item.ExtraData);
                dbClient.ExecuteQuery(
                    "INSERT INTO catalog_marketplace_offers (item_id,user_id,asking_price,total_price,public_name,sprite_id,item_type,timestamp,extra_data) VALUES ('" +
                    item.BaseItem + "','" + session.GetPlayer().Id + "','" + sellingPrice + "','" + totalPrice +
                    "',@public_name,'" + item.GetBaseItem().Sprite + "','" + itemType + "','" +
                    PylenorEnvironment.GetUnixTimestamp() + "',@extra_data)");
            }

            session.GetPlayer().GetInventoryComponent().RemoveItem(itemId);

            serverPacket.WriteBoolean(true);
            session.SendPacket(serverPacket);
        }

        public int CalculateComissionPrice(float sellingPrice)
        {
            return (int) Math.Ceiling(sellingPrice/100);
        }

        public double FormatTimestamp()
        {
            return PylenorEnvironment.GetUnixTimestamp() - 172800;
        }

        public ServerPacket SerializeOffers(int minCost, int maxCost, string searchQuery, int filterMode)
        {
            // IgI`UJUIIY~JX]gXoAJISA
            // IgPYcaxwIIZVF{2}
            // IgPYcaxwIIZVF{2}X]g[yGHIb\]wIIXbK{2}X]
            // IgIXeGJIZOLQYZOLQYI

            var whereClause = new StringBuilder();
            string orderMode;
            whereClause.Append("WHERE state = '1' AND timestamp >= '" + FormatTimestamp() + "'");

            if (minCost >= 0)
            {
                whereClause.Append(" AND total_price >= " + minCost);
            }

            if (maxCost >= 0)
            {
                whereClause.Append(" AND total_price <= " + maxCost);
            }

            switch (filterMode)
            {
                // ReSharper disable once RedundantCaseLabel
                case 1:
                default:

                    orderMode = "ORDER BY asking_price DESC";
                    break;

                case 2:

                    orderMode = "ORDER BY asking_price ASC";
                    break;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("search_query", "%" + searchQuery + "%");

                if (searchQuery.Length >= 1)
                {
                    whereClause.Append(" AND public_name LIKE @search_query");
                }

                var data =
                    dbClient.ReadDataTable("SELECT * FROM catalog_marketplace_offers " + whereClause + " " + orderMode +
                                           " LIMIT 100;");
                var message = new ServerPacket(615);

                if (data != null)
                {
                    message.WriteInteger(data.Rows.Count);

                    foreach (DataRow row in data.Rows)
                    {
                        message.WriteUInt((uint) row["offer_id"]);
                        message.WriteInteger(2);
                        message.WriteInteger(int.Parse(row["item_type"].ToString()));
                        message.WriteInteger((int) row["sprite_id"]); // ??
                        message.WriteString("");
                        message.WriteInteger((int) row["total_price"]);
                        message.WriteInteger((int) row["sprite_id"]); // ??
                        message.WriteInteger((int) row["total_price"]); // average
                        message.WriteInteger(1); // offers
                    }
                }
                else
                {
                    message.WriteInteger(0);
                }
                return message;
            }
        }

        public ServerPacket SerializeOwnOffers(uint habboId)
        {
            var profits = 0;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                var data =
                    dbClient.ReadDataTable("SELECT * FROM catalog_marketplace_offers WHERE user_id = '" + habboId + "';");
                var rawProfit =
                    dbClient.ReadDataRow(
                        "SELECT SUM(asking_price) FROM catalog_marketplace_offers WHERE state = '2' AND user_id = '" +
                        habboId + "';")[0].ToString();

                if (rawProfit.Length > 0)
                {
                    profits = int.Parse(rawProfit);
                }

                var message = new ServerPacket(616);
                message.WriteInteger(profits);

                if (data != null)
                {
                    message.WriteInteger(data.Rows.Count);

                    foreach (DataRow row in data.Rows)
                    {
                        var minutesLeft =
                            (int)
                                Math.Floor((((double) row["timestamp"] + 172800) - PylenorEnvironment.GetUnixTimestamp())/
                                           60);
                        var state = int.Parse(row["state"].ToString());

                        if (minutesLeft <= 0)
                        {
                            state = 3;
                            minutesLeft = 0;
                        }

                        message.WriteUInt((uint) row["offer_id"]);
                        message.WriteInteger(state); // 1 = active, 2 = sold, 3 = expired
                        message.WriteInteger(int.Parse(row["item_type"].ToString())); // always 1 (??)
                        message.WriteInteger((int) row["sprite_id"]);
                        message.WriteString("");
                        message.WriteInteger((int) row["total_price"]); // ??
                        message.WriteInteger(minutesLeft);
                        message.WriteInteger((int) row["sprite_id"]);
                    }
                }
                else
                {
                    message.WriteInteger(0);
                }
                return message;
            }
        }
    }
}