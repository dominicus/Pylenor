﻿using System.Data;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Catalogs
{
    internal class VoucherHandler
    {
        public bool IsValidCode(string code)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("code", code);

                if (dbClient.ReadDataRow("SELECT null FROM credit_vouchers WHERE code = @code LIMIT 1") != null)
                {
                    return true;
                }
            }

            return false;
        }

        public int GetVoucherValue(string code)
        {
            DataRow data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("code", code);
                data = dbClient.ReadDataRow("SELECT value FROM credit_vouchers WHERE code = @code LIMIT 1");
            }

            if (data != null)
            {
                return (int) data[0];
            }

            return 0;
        }

        public void TryDeleteVoucher(string code)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("code", code);
                dbClient.ExecuteQuery("DELETE FROM credit_vouchers WHERE code = @code LIMIT 1");
            }
        }

        public void TryRedeemVoucher(Session session, string code)
        {
            if (!IsValidCode(code))
            {
                var error = new ServerPacket(213);
                error.AppendRawInt32(1);
                session.SendPacket(error);
                return;
            }

            var value = GetVoucherValue(code);

            TryDeleteVoucher(code);

            if (value > 0)
            {
                session.GetPlayer().Credits += value;
                session.GetPlayer().UpdateCreditsBalance(true);
            }

            session.SendPacket(new ServerPacket(212));
        }
    }
}