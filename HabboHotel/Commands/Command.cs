﻿using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Commands
{
    /// <summary>
    ///     Class Command.
    /// </summary>
    public abstract class Command
    {
        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public virtual string Description { get; set; }

        /// <summary>
        ///     Gets or sets the usage.
        /// </summary>
        /// <value>The usage.</value>
        public virtual string Usage { get; set; }

        /// <summary>
        ///     Gets or sets the alias.
        /// </summary>
        /// <value>The alias.</value>
        public virtual string Alias { get; set; }

        /// <summary>
        ///     Gets or sets the minimum parameters.
        /// </summary>
        /// <value>The minimum parameters.</value>
        public virtual short MinParams { get; set; }

        /// <summary>
        ///     Gets if the GameClient can execute the command.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public abstract bool CanExecute(Session session);

        /// <summary>
        ///     Executes the specified client.
        /// </summary>
        /// <param name="session">The client.</param>
        /// <param name="pms">The PMS.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public abstract void Execute(Session session, string[] pms);
    }
}