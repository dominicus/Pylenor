﻿using System.Collections.Generic;
using System.Linq;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Commands.List;

namespace Pylenor.HabboHotel.Commands
{
    /// <summary>
    ///     Class CommandsManager.
    /// </summary>
    public static class CommandManager
    {
        #region Definitions

        /// <summary>
        ///     The commands dictionary
        /// </summary>
        internal static SortedDictionary<string, Command> CommandsDictionary = new SortedDictionary<string, Command>();

        #endregion Definitions

        #region Initialization

        /// <summary>
        ///     Registers this instance.
        /// </summary>
        public static void Register()
        {
            #region General

            CommandsDictionary.Add("about", new About());

            #endregion

            #region Administrator

            CommandsDictionary.Add("ha", new HotelAlert());

            #endregion
        }

        #endregion Initialization

        #region Methods

        /// <summary>
        ///     Tries the execute.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="session">The session.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool TryExecute(Session session, string str)
        {
            if (string.IsNullOrEmpty(str) || session.GetPlayer() == null || !session.GetPlayer().InRoom) return false;

            var pms = str.Split(' ');
            var commandName = pms[0];


            if (!CommandsDictionary.ContainsKey(commandName)) return false;
            var command = CommandsDictionary[commandName];

            if (!command.CanExecute(session)) return false;

            if (command.MinParams == -2 || (command.MinParams == -1 && pms.Count() > 1) ||
                command.MinParams != -1 && command.MinParams == pms.Count() - 1)
            {
                command.Execute(session, pms.Skip(1).ToArray());
            }
            return true;
        }

        #endregion Methods
    }
}