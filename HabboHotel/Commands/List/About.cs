﻿using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Commands.List
{
    internal sealed class About : Command
    {
        public About()
        {
            MinParams = 0;
            Description = "Server information";
        }

        public override bool CanExecute(Session session)
        {
            return true;
        }

        public override void Execute(Session session, string[] pms)
        {
            var userCount = PylenorEnvironment.GetGame().GetClientManager().ClientCount;
            var roomCount = PylenorEnvironment.GetGame().GetRoomManager().LoadedRoomsCount;

            session.SendNotification(
                "Pylenor 1.0\n\nThanks/Credits;\nDominicus [Pylenor Emu]\nRoy [Uber Emu]\n\nUsers Online: " + userCount +
                "\nRooms Loaded: " + roomCount + "\nThank you for using Pylenor.", string.Empty);
        }
    }
}