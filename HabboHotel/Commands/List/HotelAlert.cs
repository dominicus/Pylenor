﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Commands.List
{
    internal sealed class HotelAlert : Command
    {
        public HotelAlert()
        {
            Description = "Sends an alert to everyone online.";
            MinParams = 2;
        }

        public override bool CanExecute(Session session)
        {
            return session.GetPlayer().HasFuse("fuse_admin");
        }

        public override void Execute(Session session, string[] pms)
        {
            var message2 = new ServerPacket(808);
            message2.WriteString(pms[0]);
            message2.WriteString(pms[1] + "\r\n- " + session.GetPlayer().Username);
            PylenorEnvironment.GetGame().GetClientManager().Broadcast(message2);
        }
    }
}