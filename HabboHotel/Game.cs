﻿using System;
using System.Threading;
using Pylenor.Core;
using Pylenor.HabboHotel.Achievements;
using Pylenor.HabboHotel.Advertisements;
using Pylenor.HabboHotel.Catalogs;
using Pylenor.HabboHotel.Commands;
using Pylenor.HabboHotel.GameClients;
using Pylenor.HabboHotel.Items;
using Pylenor.HabboHotel.Misc;
using Pylenor.HabboHotel.Navigators;
using Pylenor.HabboHotel.Roles;
using Pylenor.HabboHotel.RoomBots;
using Pylenor.HabboHotel.Rooms;
using Pylenor.HabboHotel.Support;
using Pylenor.Storage;

namespace Pylenor.HabboHotel
{
    internal class Game
    {
        private const string Version = "RELEASE63-35255-34886-201108111108";
        private readonly AchievementManager _achievementManager;
        private readonly BotManager _botManager;
        private AdvertisementManager _advertisementManager;
        private ModerationBanManager _banManager;
        private Catalog _catalog;

        private GameClientManager _clientManager;
        private HelpTool _helpTool;
        private ItemManager _itemManager;
        private ModerationTool _moderationTool;
        private Navigator _navigator;
        private PixelManager _pixelManager;
        private RoleManager _roleManager;
        private RoomManager _roomManager;
        private Thread _statisticsThread;

        public Game()
        {
            _clientManager = new GameClientManager();

            PylenorEnvironment.GetLogging().Write("Connecting to database");
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                Console.WriteLine("...completed!");
                PylenorEnvironment.Game = this;
                LoadServerSettings(dbClient);
                _banManager = new ModerationBanManager();
                _roleManager = new RoleManager();
                _helpTool = new HelpTool();
                _catalog = new Catalog();
                _navigator = new Navigator();
                _itemManager = new ItemManager();
                _roomManager = new RoomManager();
                _advertisementManager = new AdvertisementManager();
                _pixelManager = new PixelManager();
                _achievementManager = new AchievementManager();
                _moderationTool = new ModerationTool();
                _botManager = new BotManager();
                CommandManager.Register();

                _banManager.LoadBans(dbClient);
                _roleManager.LoadRoles(dbClient);
                _roleManager.LoadRights(dbClient);
                _helpTool.LoadCategories(dbClient);
                _helpTool.LoadTopics(dbClient);
                _moderationTool.LoadMessagePresets(dbClient);
                _moderationTool.LoadPendingTickets(dbClient);
                _itemManager.LoadItems(dbClient);
                _catalog.Initialize(dbClient);
                _catalog.CacheCatalog();
                _navigator.Initialize(dbClient);
                _roomManager.LoadModels(dbClient);
                _advertisementManager.LoadRoomAdvertisements(dbClient);
                _pixelManager.Start();
                _achievementManager.LoadAchievements(dbClient);
                _botManager.LoadBots(dbClient);

                DatabaseCleanup(1);

                _statisticsThread = new Thread(LowPriorityWorker.Process)
                {
                    Name = "Low Priority Worker",
                    Priority = ThreadPriority.Lowest
                };
                _statisticsThread.Start();

                PylenorEnvironment.GetLogging().WriteLine("Game version: " + Version);
                PylenorEnvironment.GetLogging().WriteLine("Initialized game.");
            }
        }

        public void DatabaseCleanup(int serverStatus)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET online = '0'");
                dbClient.ExecuteQuery("UPDATE rooms SET users_now = '0'");
                dbClient.ExecuteQuery("UPDATE user_roomvisits SET exit_timestamp = '" +
                                      PylenorEnvironment.GetUnixTimestamp() + "' WHERE exit_timestamp <= 0");
                dbClient.ExecuteQuery("UPDATE server_status SET status = '" + serverStatus +
                                      "', users_online = '0', rooms_loaded = '0', server_ver = '" +
                                      PylenorEnvironment.PrettyVersion + "', stamp = '" +
                                      PylenorEnvironment.GetUnixTimestamp() + "' LIMIT 1");
                dbClient.ExecuteQuery("OPTIMIZE TABLE `items`");
                // todo: more db cleanup stuff?
            }
        }

        public void Destroy()
        {
            if (_statisticsThread != null)
            {
                try
                {
                    _statisticsThread.Abort();
                }

                catch (ThreadAbortException)
                {
                }

                _statisticsThread = null;
            }

            DatabaseCleanup(0);

            if (GetClientManager() != null)
            {
                GetClientManager().Clear();
            }

            if (GetPixelManager() != null)
            {
                _pixelManager.KeepAlive = false;
            }

            _clientManager = null;
            _banManager = null;
            _roleManager = null;
            _helpTool = null;
            _catalog = null;
            _navigator = null;
            _itemManager = null;
            _roomManager = null;
            _advertisementManager = null;
            _pixelManager = null;

            PylenorEnvironment.GetLogging().WriteLine("Destroyed.");
        }

        public void LoadServerSettings(DatabaseClient db)
        {
            PylenorEnvironment.GetLogging().Write("Loading your settings");
            var dataRow = db.ReadDataRow("SELECT * FROM server_settings LIMIT 1");

            ServerConfiguration.RoomUserLimit = (int) dataRow["MaxRoomsPerUser"];

            ServerConfiguration.MOTD = (string) dataRow["motd"];

            ServerConfiguration.CreditingInterval = (int) dataRow["timer"];

            ServerConfiguration.CreditingAmount = (int) dataRow["credits"];
            ServerConfiguration.PointingAmount = (int) dataRow["pixels"];
            ServerConfiguration.PixelingAmount = (int) dataRow["points"];

            ServerConfiguration.PixelLimit = (int) dataRow["pixels_max"];
            ServerConfiguration.CreditLimit = (int) dataRow["credits_max"];
            ServerConfiguration.PointLimit = (int) dataRow["points_max"];

            ServerConfiguration.PetsPerRoomLimit = (int) dataRow["MaxPetsPerRoom"];

            ServerConfiguration.MarketplacePriceLimit = (int) dataRow["MaxMarketPlacePrice"];
            ServerConfiguration.MarketplaceTax = (int) dataRow["MarketPlaceTax"];

            ServerConfiguration.DDoSProtectionEnabled =
                PylenorEnvironment.EnumToBool(dataRow["enable_antiddos"].ToString());

            ServerConfiguration.HabboClubForClothes =
                PylenorEnvironment.EnumToBool(dataRow["vipclothesforhcusers"].ToString());

            ServerConfiguration.EnableChatlog = PylenorEnvironment.EnumToBool(dataRow["enable_chatlogs"].ToString());
            ServerConfiguration.EnableCommandLog = PylenorEnvironment.EnumToBool(dataRow["enable_cmdlogs"].ToString());
            ServerConfiguration.EnableRoomLog = PylenorEnvironment.EnumToBool(dataRow["enable_roomlogs"].ToString());

            ServerConfiguration.EnableExternalLinks = (string) dataRow["enable_externalchatlinks"];

            ServerConfiguration.EnableSSO = PylenorEnvironment.EnumToBool(dataRow["enable_securesessions"].ToString());

            ServerConfiguration.AllowFurniDrops =
                PylenorEnvironment.EnumToBool(dataRow["allow_friendfurnidrops"].ToString());

            ServerConfiguration.EnableRedeemCredits =
                PylenorEnvironment.EnumToBool(dataRow["enable_cmd_redeemcredits"].ToString());
            ServerConfiguration.EnableRedeemPixels =
                PylenorEnvironment.EnumToBool(dataRow["enable_cmd_redeempixels"].ToString());
            ServerConfiguration.EnableRedeemShells =
                PylenorEnvironment.EnumToBool(dataRow["enable_cmd_redeemshells"].ToString());

            ServerConfiguration.UnloadCrashedRooms =
                PylenorEnvironment.EnumToBool(dataRow["unload_crashedrooms"].ToString());

            ServerConfiguration.ShowUsersAndRoomsInAbout =
                PylenorEnvironment.EnumToBool(dataRow["ShowUsersAndRoomsInAbout"].ToString());

            ServerConfiguration.SleepTimer = (int) dataRow["idlesleep"];
            ServerConfiguration.KickTimer = (int) dataRow["idlekick"];

            ServerConfiguration.IPLastBan = PylenorEnvironment.EnumToBool(dataRow["ip_lastforbans"].ToString());

            ServerConfiguration.StaffPicksID = (int) dataRow["StaffPicksCategoryID"];

            ServerConfiguration.VIPHotelAlertInterval = (double) dataRow["vipha_interval"];
            ServerConfiguration.VIPHotelAlertLinkInterval = (double) dataRow["viphal_interval"];

            ServerConfiguration.PreventDoorPush =
                PylenorEnvironment.EnumToBool(dataRow["DisableOtherUsersToMovingOtherUsersToDoor"].ToString());
            Console.WriteLine("...completed!");
        }

        public GameClientManager GetClientManager()
        {
            return _clientManager;
        }

        public ModerationBanManager GetBanManager()
        {
            return _banManager;
        }

        public RoleManager GetRoleManager()
        {
            return _roleManager;
        }

        public HelpTool GetHelpTool()
        {
            return _helpTool;
        }

        public Catalog GetCatalog()
        {
            return _catalog;
        }

        public Navigator GetNavigator()
        {
            return _navigator;
        }

        public ItemManager GetItemManager()
        {
            return _itemManager;
        }

        public RoomManager GetRoomManager()
        {
            return _roomManager;
        }

        public AdvertisementManager GetAdvertisementManager()
        {
            return _advertisementManager;
        }

        public PixelManager GetPixelManager()
        {
            return _pixelManager;
        }

        public AchievementManager GetAchievementManager()
        {
            return _achievementManager;
        }

        public ModerationTool GetModerationTool()
        {
            return _moderationTool;
        }

        public BotManager GetBotManager()
        {
            return _botManager;
        }
    }
}