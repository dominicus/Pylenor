﻿using System.Collections.Generic;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.GameClients
{
    partial class GameClientManager
    {
        public readonly Dictionary<uint, Session> Clients;

        public GameClientManager()
        {
            Clients = new Dictionary<uint, Session>();
        }

        public int ClientCount => Clients.Count;

        public void Clear()
        {
            Clients.Clear();
        }

        public Session GetClient(uint clientId)
        {
            return Clients.ContainsKey(clientId) ? Clients[clientId] : null;
        }

        public void StartClient(uint clientId, Session session)
        {
            Clients.Add(clientId, session);
        }

        public bool RemoveClient(uint clientId)
        {
            return Clients.Remove(clientId);
        }

        public Session GetClientByHabbo(uint habboId)
        {
            return
                Clients.Values.Where(client => client.GetPlayer() != null)
                    .FirstOrDefault(client => client.GetPlayer().Id == habboId);
        }

        public Session GetClientByHabbo(string name)
        {
            return
                Clients.Values.Where(client => client.GetPlayer() != null)
                    .FirstOrDefault(client => client.GetPlayer().Username == name);
        }

        public void BroadcastMultiple(ServerPacket packet, ServerPacket secondPacket)
        {
            foreach (var client in Clients.Values.Where(client => client != null))
            {
                client.SendData(client.GetPlayer().InRoom ? packet.GetBytes() : secondPacket.GetBytes());
            }
        }

        public void BroadcastMessage(ServerPacket packet)
        {
            BroadcastMessage(packet, "");
        }

        public void BroadcastMessage(ServerPacket packet, string fuseRequirement)
        {
            foreach (
                var client in
                    Clients.Values.Where(client => fuseRequirement.Length > 0)
                        .Where(client => client.GetPlayer() != null && client.GetPlayer().HasFuse(fuseRequirement)))
            {
                client.SendPacket(packet);
            }
        }

        public void CheckEffects()
        {
            foreach (
                var client in
                    Clients.Values.Where(
                        client =>
                            client.GetPlayer() != null &&
                            client.GetPlayer().GetAvatarEffectsInventoryComponent() != null))
            {
                client.GetPlayer().GetAvatarEffectsInventoryComponent().CheckExpired();
            }
        }

        public void Broadcast(ServerPacket packet)
        {
            foreach (var client in Clients.Values.Where(client => client.GetPlayer() != null))
            {
                client.SendPacket(packet);
            }
        }
    }
}