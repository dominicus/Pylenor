﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Support;

namespace Pylenor.HabboHotel.GameClients
{
    partial class GameClientManager
    {
        public void LogClonesOut(string username)
        {
            foreach (var client in Clients.Values.Where(client => client.GetPlayer() != null &&
                                                                  string.Equals(client.GetPlayer().Username, username,
                                                                      StringComparison.CurrentCultureIgnoreCase)))
            {
                Clients[client.Id].Disconnect();
            }
        }

        public string GetNameById(uint id)
        {
            var cl = GetClientByHabbo(id);

            if (cl != null)
            {
                return cl.GetPlayer().Username;
            }

            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                row = dbClient.ReadDataRow("SELECT username FROM users WHERE id = '" + id + "' LIMIT 1");
            }

            if (row == null)
            {
                return "Unknown User";
            }

            return (string) row[0];
        }

        public void DeployHotelCreditsUpdate()
        {
            foreach (var client in Clients.Values.Where(client => client.GetPlayer() != null))
            {
                int newCredits;

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    newCredits =
                        (int)
                            dbClient.ReadDataRow("SELECT credits FROM users WHERE id = '" + client.GetPlayer().Id +
                                                 "' LIMIT 1")[0];
                }

                var oldBalance = client.GetPlayer().Credits;

                client.GetPlayer().Credits = newCredits;

                if (oldBalance < 3000)
                {
                    client.GetPlayer().UpdateCreditsBalance(false);
                    //Client.SendNotif("Credits Notification" + Convert.ToChar(13) + Convert.ToChar(13) + "We have refilled your credits to the set amount.");
                }
                else if (oldBalance >= 3000)
                {
                    client.SendNotif("Credits Notification" + Convert.ToChar(13) + Convert.ToChar(13) +
                                     "Sorry, your credit balance is too high and has not been refilled.");
                }
            }
        }

        public void CheckForAllBanConflicts()
        {
            var conflictsFound = new Dictionary<Session, ModerationBanException>();
            foreach (var client in Clients.Values)
            {
                try
                {
                    PylenorEnvironment.GetGame().GetBanManager().CheckForBanConflicts(client);
                }

                catch (ModerationBanException e)
                {
                    conflictsFound.Add(client, e);
                }
            }

            foreach (var data in conflictsFound)
            {
                data.Key.SendBanMessage(data.Value.Message);
                data.Key.Disconnect();
            }
        }

        public void CheckPixelUpdates()
        {
            try
            {
                foreach (
                    var client in
                        Clients.Values.Where(
                            client =>
                                client.GetPlayer() != null &&
                                PylenorEnvironment.GetGame().GetPixelManager().NeedsUpdate(client)))
                {
                    PylenorEnvironment.GetGame().GetPixelManager().GivePixels(client);
                }
            }

            catch (Exception e)
            {
                PylenorEnvironment.GetLogging().WriteLine("[GCMExt.CheckPixelUpdates]: " + e.Message);
            }
        }
    }
}