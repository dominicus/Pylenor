﻿using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    public abstract class FurniInteractor
    {
        public abstract void OnPlace(Session session, RoomItem item);
        public abstract void OnRemove(Session session, RoomItem item);
        public abstract void OnTrigger(Session session, RoomItem item, int request, bool userHasRights);
    }
}