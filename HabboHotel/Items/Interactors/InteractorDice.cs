using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorDice : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            var user = item.GetRoom().GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            if (item.GetRoom().TilesTouching(item.X, item.Y, user.X, user.Y))
            {
                if (item.ExtraData == "-1") return;
                if (request == -1)
                {
                    item.ExtraData = "0";
                    item.UpdateState();
                }
                else
                {
                    item.ExtraData = "-1";
                    item.UpdateState(false, true);
                    item.ReqUpdate(4);
                }
            }
            else
            {
                user.MoveTo(item.SquareInFront);
            }
        }
    }
}