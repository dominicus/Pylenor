using System;
using System.Collections.Generic;
using System.Linq;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorGate : FurniInteractor
    {
        private readonly int _modes;

        public InteractorGate(int modes)
        {
            _modes = (modes - 1);

            if (_modes < 0)
            {
                _modes = 0;
            }
        }

        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights)
            {
                return;
            }

            if (_modes == 0)
            {
                item.UpdateState(false, true);
            }

            var currentMode = 0;
            int newMode;

            try
            {
                currentMode = int.Parse(item.ExtraData);
            }
            catch (Exception)
            {
                // ignored
            }

            if (currentMode <= 0)
            {
                newMode = 1;
            }
            else if (currentMode >= _modes)
            {
                newMode = 0;
            }
            else
            {
                newMode = currentMode + 1;
            }

            if (newMode == 0)
            {
                if (item.GetRoom().SquareHasUsers(item.X, item.Y))
                {
                    return;
                }

                var points = item.GetRoom().GetAffectedTiles(item.GetBaseItem().Length, item.GetBaseItem().Width,
                    item.X, item.Y, item.Rot) ?? new Dictionary<int, AffectedTile>();

                if (points.Values.Any(tile => item.GetRoom().SquareHasUsers(tile.X, tile.Y)))
                {
                    return;
                }
            }

            item.ExtraData = newMode.ToString();
            item.UpdateState();
            item.GetRoom().GenerateMaps();
        }
    }
}