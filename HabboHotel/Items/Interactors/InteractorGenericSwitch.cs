using System;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorGenericSwitch : FurniInteractor
    {
        private readonly int _modes;

        public InteractorGenericSwitch(int modes)
        {
            _modes = (modes - 1);

            if (_modes < 0)
            {
                _modes = 0;
            }
        }

        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights)
            {
                return;
            }

            if (_modes == 0)
            {
                return;
            }

            var currentMode = 0;
            int newMode;

            try
            {
                currentMode = int.Parse(item.ExtraData);
            }
            catch (Exception)
            {
                // ignored
            }

            if (currentMode <= 0)
            {
                newMode = 1;
            }
            else if (currentMode >= _modes)
            {
                newMode = 0;
            }
            else
            {
                newMode = currentMode + 1;
            }

            item.ExtraData = newMode.ToString();
            item.UpdateState();
        }
    }
}