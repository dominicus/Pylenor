using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorHabboWheel : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
            item.ExtraData = "-1";
            item.ReqUpdate(10);
        }

        public override void OnRemove(Session session, RoomItem item)
        {
            item.ExtraData = "-1";
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights)
            {
                return;
            }

            if (item.ExtraData == "-1") return;
            item.ExtraData = "-1";
            item.UpdateState();
            item.ReqUpdate(10);
        }
    }
}