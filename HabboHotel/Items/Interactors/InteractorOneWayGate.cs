﻿using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorOneWayGate : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
            item.ExtraData = "0";

            if (item.InteractingUser == 0) return;
            var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser);

            if (user != null)
            {
                user.ClearMovement(true);
                user.UnlockWalking();
            }

            item.InteractingUser = 0;
        }

        public override void OnRemove(Session session, RoomItem item)
        {
            item.ExtraData = "0";

            if (item.InteractingUser == 0) return;
            var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser);

            if (user != null)
            {
                user.ClearMovement(true);
                user.UnlockWalking();
            }

            item.InteractingUser = 0;
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            var user = item.GetRoom().GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            if (user.Coordinate != item.SquareInFront && user.CanWalk)
            {
                user.MoveTo(item.SquareInFront);
                return;
            }

            if (!item.GetRoom().CanWalk(item.SquareBehind.X, item.SquareBehind.Y, item.Z, true))
            {
                return;
            }

            if (item.InteractingUser != 0) return;
            item.InteractingUser = user.HabboId;

            user.CanWalk = false;

            if (user.IsWalking && (user.GoalX != item.SquareInFront.X || user.GoalY != item.SquareInFront.Y))
            {
                user.ClearMovement(true);
            }

            user.AllowOverride = true;
            user.MoveTo(item.Coordinate);

            item.ReqUpdate(3);
        }
    }
}