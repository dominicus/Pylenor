using System;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorScoreboard : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights)
            {
                return;
            }

            var newMode = 0;

            try
            {
                newMode = int.Parse(item.ExtraData);
            }
            catch (Exception)
            {
                // ignored
            }

            if (request == 0)
            {
                if (newMode <= -1)
                {
                    newMode = 0;
                }
                else if (newMode >= 0)
                {
                    newMode = -1;
                }
            }
            else if (request >= 1)
            {
                if (request == 1)
                {
                    newMode--;

                    if (newMode < 0)
                    {
                        newMode = 0;
                    }
                }
                else if (request == 2)
                {
                    newMode++;

                    if (newMode >= 100)
                    {
                        newMode = 0;
                    }
                }
            }

            item.ExtraData = newMode.ToString();
            item.UpdateState();
        }
    }
}