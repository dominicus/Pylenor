using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorSpinningBottle : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
            item.ExtraData = "0";
            item.UpdateState(true, false);
        }

        public override void OnRemove(Session session, RoomItem item)
        {
            item.ExtraData = "0";
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (item.ExtraData == "-1") return;
            item.ExtraData = "-1";
            item.UpdateState(false, true);
            item.ReqUpdate(3);
        }
    }
}