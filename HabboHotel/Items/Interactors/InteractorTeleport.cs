using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorTeleport : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
            item.ExtraData = "0";

            if (item.InteractingUser != 0)
            {
                var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser);

                if (user != null)
                {
                    user.ClearMovement(true);
                    user.AllowOverride = false;
                    user.CanWalk = true;
                }

                item.InteractingUser = 0;
            }

            if (item.InteractingUser2 != 0)
            {
                var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser2);

                if (user != null)
                {
                    user.ClearMovement(true);
                    user.AllowOverride = false;
                    user.CanWalk = true;
                }

                item.InteractingUser2 = 0;
            }

            item.GetRoom().RegenerateUserMatrix();
        }

        public override void OnRemove(Session session, RoomItem item)
        {
            item.ExtraData = "0";

            if (item.InteractingUser != 0)
            {
                var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser);

                user?.UnlockWalking();

                item.InteractingUser = 0;
            }

            if (item.InteractingUser2 != 0)
            {
                var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser2);

                user?.UnlockWalking();

                item.InteractingUser2 = 0;
            }

            item.GetRoom().RegenerateUserMatrix();
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            // Is this user valid?
            var user = item.GetRoom().GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            // Alright. But is this user in the right position?
            if (user.Coordinate == item.Coordinate || user.Coordinate == item.SquareInFront)
            {
                // Fine. But is this tele even free?
                if (item.InteractingUser != 0)
                {
                    return;
                }

                user.TeleDelay = -1;
                item.InteractingUser = user.GetClient().GetPlayer().Id;
            }
            else if (user.CanWalk)
            {
                user.MoveTo(item.SquareInFront);
            }
        }
    }
}