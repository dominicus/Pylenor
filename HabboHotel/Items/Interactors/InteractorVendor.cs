using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Pathfinding;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorVendor : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
            item.ExtraData = "0";

            if (item.InteractingUser > 0)
            {
                var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser);

                if (user != null)
                {
                    user.CanWalk = true;
                }
            }
        }

        public override void OnRemove(Session session, RoomItem item)
        {
            item.ExtraData = "0";

            if (item.InteractingUser <= 0) return;
            var user = item.GetRoom().GetRoomUserByHabbo(item.InteractingUser);

            if (user != null)
            {
                user.CanWalk = true;
            }
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (item.ExtraData == "1" || item.GetBaseItem().VendingIds.Count < 1 || item.InteractingUser != 0) return;
            var user = item.GetRoom().GetRoomUserByHabbo(session.GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            if (!item.GetRoom().TilesTouching(user.X, user.Y, item.X, item.Y))
            {
                user.MoveTo(item.SquareInFront);
                return;
            }

            item.InteractingUser = session.GetPlayer().Id;

            user.CanWalk = false;
            user.ClearMovement(true);
            user.SetRot(Rotation.Calculate(user.X, user.Y, item.X, item.Y));

            item.ReqUpdate(2);

            item.ExtraData = "1";
            item.UpdateState(false, true);
        }
    }
}