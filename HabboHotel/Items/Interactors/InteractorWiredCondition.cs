﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredCondition : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights && session == null) return;
            item.Combine();
            var packet = new ServerPacket(652u);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            if (item.Extra1.Length > 0)
            {
                packet.AppendString(item.Extra1);
            }
            else
            {
                packet.WriteInteger(0);
            }
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(string.Empty);
            packet.WriteString("HH");
            session.SendPacket(packet);
        }
    }
}