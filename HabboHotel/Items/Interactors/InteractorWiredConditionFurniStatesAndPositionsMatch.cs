﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredConditionFurniStatesAndPositionsMatch : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights && session == null) return;
            item.Flip();
            var packet = new ServerPacket(652);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            if (item.Extra4.Length > 0)
            {
                packet.AppendString(item.Extra4);
            }
            else
            {
                packet.WriteInteger(0);
            }
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(string.Empty);
            packet.AppendString("J");
            packet.AppendString(item.Extra2.Length > 0 ? item.Extra2 : "HHH");
            session.SendPacket(packet);
        }
    }
}