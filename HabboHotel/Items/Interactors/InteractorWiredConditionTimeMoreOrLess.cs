﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredConditionTimeMoreOrLess : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights) return;
            var packet = new ServerPacket(650);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            packet.WriteInteger(0);
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(string.Empty);
            packet.AppendString("I");
            packet.AppendString(item.Extra2.Length > 0 ? item.Extra2 : "I");
            packet.WriteString("IKH");
            session.SendPacket(packet);
        }
    }
}