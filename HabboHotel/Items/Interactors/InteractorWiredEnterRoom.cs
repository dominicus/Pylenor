﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredEnterRoom : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights) return;
            var packet = new ServerPacket(650);
            packet.WriteInteger(0);
            packet.WriteInteger(0);
            packet.WriteInteger(0);
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(item.Extra1);
            packet.WriteString("HHSAHH");
            session.SendPacket(packet);
        }
    }
}