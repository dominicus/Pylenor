﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredGivePoints : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights) return;
            var packet = new ServerPacket(651);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            packet.WriteInteger(0);
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(string.Empty);
            packet.AppendString("J");
            if (item.Extra1.Length > 0)
            {
                packet.WriteInteger(Convert.ToInt32(item.Extra1));
            }
            else
            {
                packet.AppendString("QA");
            }
            packet.WriteInteger(item.Extra2.Length > 0 ? Convert.ToInt32(item.Extra2) : 1);
            session.SendPacket(packet);
        }
    }
}