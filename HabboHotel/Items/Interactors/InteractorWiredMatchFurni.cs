﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredMatchFurni : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights && session == null) return;
            item.Flip();
            var packet = new ServerPacket(651);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            if (item.Extra4.Length > 0)
            {
                packet.AppendString(item.Extra4);
            }
            else
            {
                packet.WriteInteger(0);
            }
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(string.Empty);
            packet.AppendString("K");
            packet.AppendString(item.Extra2.Length > 0 ? item.Extra2 : "HHH");
            packet.AppendString("IK");
            packet.WriteInteger(item.Extra5.Length > 0 ? Convert.ToInt32(item.Extra5) : 0);
            packet.WriteString("H");
            session.SendPacket(packet);
        }
    }
}