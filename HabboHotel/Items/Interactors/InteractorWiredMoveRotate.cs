﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal class InteractorWiredMoveRotate : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights) return;
            item.Flip();
            var packet = new ServerPacket(651);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            if (item.Extra4.Length > 0)
            {
                packet.WriteString(item.Extra4);
            }
            else
            {
                packet.WriteInteger(0);
            }
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            packet.WriteString(string.Empty);
            packet.AppendString("J");
            packet.WriteInteger(item.Extra1.Length > 0 ? Convert.ToInt32(item.Extra1) : 0);
            packet.WriteInteger(item.Extra2.Length > 0 ? Convert.ToInt32(item.Extra2) : 0);
            packet.WriteString("HPA");
            packet.WriteInteger(item.Extra5.Length > 0 ? Convert.ToInt32(item.Extra5) : 0);
            packet.WriteString("H");
            session.SendPacket(packet);
        }
    }
}