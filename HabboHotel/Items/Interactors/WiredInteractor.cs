﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Items.Interactors
{
    internal sealed class WiredInteractor : FurniInteractor
    {
        public override void OnPlace(Session session, RoomItem item)
        {
        }

        public override void OnRemove(Session session, RoomItem item)
        {
        }

        public override void OnTrigger(Session session, RoomItem item, int request, bool userHasRights)
        {
            if (!userHasRights) return;
            var packet = new ServerPacket(651);
            packet.WriteInteger(0);
            packet.WriteInteger(5);
            packet.WriteInteger(1);
            packet.WriteUInt(item.Id);
            packet.WriteInteger(item.GetBaseItem().Sprite);
            packet.WriteUInt(item.Id);
            session.SendPacket(packet);
        }
    }
}