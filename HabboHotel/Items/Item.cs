﻿using System.Collections.Generic;
using System.Globalization;

namespace Pylenor.HabboHotel.Items
{
    public class Item
    {
        public bool AllowGift;
        public bool AllowInventoryStack;
        public bool AllowMarketplaceSell;

        public bool AllowRecycle;
        public bool AllowTrade;
        public double Height;
        public List<double> HeightAdjust;

        public string InteractionType;
        public bool IsSeat;
        public int Length;

        public int Modes;
        public string Name;

        public string PublicName;

        public int Sprite;

        public bool Stackable;
        public string Type;

        public List<int> VendingIds;
        public bool Walkable;

        public int Width;

        public Item(uint id, int sprite, string publicName, string name, string type, int width, int length,
            double height, bool stackable, bool walkable, bool isSeat, bool allowRecycle, bool allowTrade,
            bool allowMarketplaceSell, bool allowGift, bool allowInventoryStack, string interactionType, int modes,
            string vendingIds, string heightAdjustable)
        {
            ItemId = id;
            Sprite = sprite;
            PublicName = publicName;
            Name = name;
            Type = type;
            Width = width;
            Length = length;
            Height = height;
            Stackable = stackable;
            Walkable = walkable;
            IsSeat = isSeat;
            AllowRecycle = allowRecycle;
            AllowTrade = allowTrade;
            AllowMarketplaceSell = allowMarketplaceSell;
            AllowGift = allowGift;
            AllowInventoryStack = allowInventoryStack;
            InteractionType = interactionType;
            Modes = modes;
            VendingIds = new List<int>();
            HeightAdjust = new List<double>();

            foreach (var vendingId in vendingIds.Split(','))
            {
                VendingIds.Add(int.Parse(vendingId));
            }

            foreach (var adjust in heightAdjustable.Split(','))
            {
                HeightAdjust.Add(double.Parse(adjust, NumberStyles.Number, CultureInfo.InvariantCulture));
            }
        }

        public uint ItemId { get; }
    }
}