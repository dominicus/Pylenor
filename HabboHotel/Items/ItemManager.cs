﻿using System;
using System.Collections.Generic;
using System.Data;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Items
{
    internal class ItemManager
    {
        private Dictionary<uint, Item> _items;

        public void LoadItems(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading items");
            _items = new Dictionary<uint, Item>();
            var itemData = dbClient.ReadDataTable("SELECT * FROM furniture;");

            if (itemData == null) return;
            foreach (DataRow row in itemData.Rows)
            {
                try
                {
                    _items.Add((uint) row["id"],
                        new Item((uint) row["id"], (int) row["sprite_id"], (string) row["public_name"],
                            (string) row["item_name"], (string) row["type"], (int) row["width"], (int) row["length"],
                            (double) row["stack_height"], PylenorEnvironment.EnumToBool(row["can_stack"].ToString()),
                            PylenorEnvironment.EnumToBool(row["is_walkable"].ToString()),
                            PylenorEnvironment.EnumToBool(row["can_sit"].ToString()),
                            PylenorEnvironment.EnumToBool(row["allow_recycle"].ToString()),
                            PylenorEnvironment.EnumToBool(row["allow_trade"].ToString()),
                            PylenorEnvironment.EnumToBool(row["allow_marketplace_sell"].ToString()),
                            PylenorEnvironment.EnumToBool(row["allow_gift"].ToString()),
                            PylenorEnvironment.EnumToBool(row["allow_inventory_stack"].ToString()),
                            (string) row["interaction_type"], (int) row["interaction_modes_count"],
                            (string) row["vending_ids"], row["height_adjustable"].ToString()));
                }
                catch (Exception)
                {
                    Console.WriteLine("Could not load item #" + (uint) row["Id"] +
                                      ", please verify the data is okay.");
                }
            }
            Console.WriteLine("...completed!");
        }

        public Item GetItem(uint id)
        {
            return _items.ContainsKey(id) ? _items[id] : null;
        }
    }
}