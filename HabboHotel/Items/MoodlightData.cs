﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Pylenor.HabboHotel.Items
{
    public class MoodlightData
    {
        public int CurrentPreset;
        public bool Enabled;

        public uint ItemId;
        public List<MoodlightPreset> Presets;

        public MoodlightData(uint itemId)
        {
            ItemId = itemId;

            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                row =
                    dbClient.ReadDataRow(
                        "SELECT enabled,current_preset,preset_one,preset_two,preset_three FROM room_items_moodlight WHERE item_id = '" +
                        itemId + "' LIMIT 1");
            }

            if (row == null)
            {
                throw new ArgumentException();
            }

            Enabled = PylenorEnvironment.EnumToBool(row["enabled"].ToString());
            CurrentPreset = (int) row["current_preset"];
            Presets = new List<MoodlightPreset>
            {
                GeneratePreset((string) row["preset_one"]),
                GeneratePreset((string) row["preset_two"]),
                GeneratePreset((string) row["preset_three"])
            };
        }

        public void Enable()
        {
            Enabled = true;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE room_items_moodlight SET enabled = '1' WHERE item_id = '" + ItemId +
                                      "' LIMIT 1");
            }
        }

        public void Disable()
        {
            Enabled = false;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE room_items_moodlight SET enabled = '0' WHERE item_id = '" + ItemId +
                                      "' LIMIT 1");
            }
        }

        public void UpdatePreset(int preset, string color, int intensity, bool bgOnly)
        {
            if (!IsValidColor(color) || !IsValidIntensity(intensity))
            {
                return;
            }

            string pr;

            switch (preset)
            {
                case 3:

                    pr = "three";
                    break;

                case 2:

                    pr = "two";
                    break;

                case 1:
                default:

                    pr = "one";
                    break;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE room_items_moodlight SET preset_" + pr + " = '" + color + "," + intensity +
                                      "," + PylenorEnvironment.BoolToEnum(bgOnly) + "' WHERE item_id = '" + ItemId +
                                      "' LIMIT 1");
            }

            GetPreset(preset).ColorCode = color;
            GetPreset(preset).ColorIntensity = intensity;
            GetPreset(preset).BackgroundOnly = bgOnly;
        }

        public MoodlightPreset GeneratePreset(string data)
        {
            var bits = data.Split(',');

            if (!IsValidColor(bits[0]))
            {
                bits[0] = "#000000";
            }

            return new MoodlightPreset(bits[0], int.Parse(bits[1]), PylenorEnvironment.EnumToBool(bits[2]));
        }

        public MoodlightPreset GetPreset(int i)
        {
            i--;

            return Presets[i] ?? new MoodlightPreset("#000000", 255, false);
        }

        public bool IsValidColor(string colorCode)
        {
            switch (colorCode)
            {
                case "#000000":
                case "#0053F7":
                case "#EA4532":
                case "#82F349":
                case "#74F5F5":
                case "#E759DE":
                case "#F2F851":

                    return true;

                default:

                    return false;
            }
        }

        public bool IsValidIntensity(int intensity)
        {
            if (intensity < 0 || intensity > 255)
            {
                return false;
            }

            return true;
        }

        public string GenerateExtraData()
        {
            var preset = GetPreset(CurrentPreset);
            var sb = new StringBuilder();

            sb.Append(Enabled ? 2 : 1);

            sb.Append(",");
            sb.Append(CurrentPreset);
            sb.Append(",");

            sb.Append(preset.BackgroundOnly ? 2 : 1);

            sb.Append(",");
            sb.Append(preset.ColorCode);
            sb.Append(",");
            sb.Append(preset.ColorIntensity);
            return sb.ToString();
        }
    }

    public class MoodlightPreset
    {
        public bool BackgroundOnly;
        public string ColorCode;
        public int ColorIntensity;

        public MoodlightPreset(string colorCode, int colorIntensity, bool backgroundOnly)
        {
            ColorCode = colorCode;
            ColorIntensity = colorIntensity;
            BackgroundOnly = backgroundOnly;
        }
    }
}