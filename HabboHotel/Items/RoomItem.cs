﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.HabboHotel.Items.Interactors;
using Pylenor.HabboHotel.Pathfinding;
using Pylenor.HabboHotel.Rooms;
using Pylenor.Util;

namespace Pylenor.HabboHotel.Items
{
    public class RoomItem
    {
        public uint BaseItem;
        public string ExtraData;
        public uint Id;

        internal string Extra1;
        internal string Extra2;
        internal string Extra3;
        internal string Extra4;
        internal string Extra5;

        public uint InteractingUser;
        public uint InteractingUser2;
        public uint RoomId;
        public int Rot;
        public int UpdateCounter;

        public bool UpdateNeeded;

        public string WallPos;

        public int X;
        public int Y;
        public double Z;
        internal double Double_1
        {
            get
            {
                double result;
                if (GetBaseItem().VendingIds.Count > 1)
                {
                    int index;
                    if (int.TryParse(ExtraData, out index))
                    {
                        result = Z + GetBaseItem().VendingIds[index];
                    }
                    else
                    {
                        result = Z + GetBaseItem().Height;
                    }
                }
                else
                {
                    result = Z + GetBaseItem().Height;
                }
                return result;
            }
        }
        private Dictionary<int, AffectedTile> _affectedTiles;

        internal Dictionary<int, AffectedTile> AffectedTiles => _affectedTiles;

        public RoomItem(uint id, uint roomId, uint baseItem, string extraData, int x, int y, double z, int rot,
            string wallPos)
        {
            Id = id;
            RoomId = roomId;
            BaseItem = baseItem;
            ExtraData = extraData;
            X = x;
            Y = y;
            Z = z;
            Rot = rot;
            WallPos = wallPos;
            UpdateNeeded = false;
            UpdateCounter = 0;
            InteractingUser = 0;
            InteractingUser2 = 0;

            if (GetBaseItem().InteractionType.ToLower() == "teleport")
            {
                ReqUpdate(0);
            }
        }

        public Coord Coordinate => new Coord(X, Y);

        public double TotalHeight => Z + GetBaseItem().Height;

        public bool IsWallItem => GetBaseItem().Type.ToLower() == "i";

        public bool IsFloorItem => GetBaseItem().Type.ToLower() == "s";

        public Coord SquareInFront
        {
            get
            {
                var sq = new Coord(X, Y);

                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (Rot)
                {
                    case 0:
                        sq.Y--;
                        break;
                    case 2:
                        sq.X++;
                        break;
                    case 4:
                        sq.Y++;
                        break;
                    case 6:
                        sq.X--;
                        break;
                }

                return sq;
            }
        }

        public Coord SquareBehind
        {
            get
            {
                var sq = new Coord(X, Y);

                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (Rot)
                {
                    case 0:
                        sq.Y++;
                        break;
                    case 2:
                        sq.X--;
                        break;
                    case 4:
                        sq.Y--;
                        break;
                    case 6:
                        sq.X++;
                        break;
                }

                return sq;
            }
        }

        public FurniInteractor Interactor
        {
            get
            {
                switch (GetBaseItem().InteractionType.ToLower())
                {
                    case "wired":
                        return new WiredInteractor();
                    case "wf_trg_onsay":
                        return new InteractorWiredOnSay();
                    case "wf_trg_enterroom":
                        return new InteractorWiredEnterRoom();
                    case "wf_act_saymsg":
                    case "wf_act_give_phx":
                    case "wf_cnd_phx":
                        return new InteractorSuperWired();
                    case "wf_trg_furnistate":
                    case "wf_trg_onfurni":
                    case "wf_trg_offfurni":
                    case "wf_act_moveuser":
                    case "wf_act_togglefurni":
                        return new InteractorWiredTriggerState();
                    case "wf_trg_gameend":
                    case "wf_trg_gamestart":
                        return new InteractorWiredTriggerGame();
                    case "wf_trg_timer":
                        return new InteractorWiredTriggerTimer();
                    case "wf_act_givepoints":
                        return new InteractorWiredGivePoints();
                    case "wf_trg_attime":
                        return new InteractorWiredAtTime();
                    case "wf_trg_atscore":
                        return new InteractorWiredAtScore();
                    case "wf_act_moverotate":
                        return new InteractorWiredMoveRotate();
                    case "wf_act_matchfurni":
                        return new InteractorWiredMatchFurni();
                    case "wf_cnd_trggrer_on_frn":
                    case "wf_cnd_furnis_hv_avtrs":
                    case "wf_cnd_has_furni_on":
                        return new InteractorWiredCondition();
                    case "wf_cnd_match_snapshot":
                        return new InteractorWiredConditionFurniStatesAndPositionsMatch();
                    case "wf_cnd_time_more_than":
                    case "wf_cnd_time_less_than":
                        return new InteractorWiredConditionTimeMoreOrLess();
                    case "wf_act_kick_user":
                        return new InteractorWiredKickUser();
                    case "teleport":
                        return new InteractorTeleport();
                    case "bottle":
                        return new InteractorSpinningBottle();
                    case "dice":
                        return new InteractorDice();
                    case "habbowheel":
                        return new InteractorHabboWheel();
                    case "loveshuffler":
                        return new InteractorLoveShuffler();
                    case "onewaygate":
                        return new InteractorOneWayGate();
                    case "alert":
                        return new InteractorAlert();
                    case "vendingmachine":
                        return new InteractorVendor();
                    case "gate":
                        return new InteractorGate(GetBaseItem().Modes);
                    case "scoreboard":
                        return new InteractorScoreboard();
                    case "default":
                    default:
                        return new InteractorGenericSwitch(GetBaseItem().Modes);
                }
            }
        }


        public bool WiredAtTimeNeedReset;
        public double WiredAtTimeTimer;
        public bool WiredNeedReset;
        public double WiredCounter;

        public void ProcessUpdates()
        {
            UpdateCounter--;

            if (UpdateCounter > 0) return;
            UpdateNeeded = false;
            UpdateCounter = 0;

            RoomUser user = null;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (GetBaseItem().InteractionType.ToLower())
            {
                case "onewaygate":

                    if (InteractingUser > 0)
                    {
                        user = GetRoom().GetRoomUserByHabbo(InteractingUser);
                    }

                    if (user != null && user.X == X && user.Y == Y)
                    {
                        ExtraData = "1";

                        user.MoveTo(SquareBehind);

                        ReqUpdate(0);
                        UpdateState(false, true);
                    }
                    else if (user != null && user.Coordinate == SquareBehind)
                    {
                        user.UnlockWalking();

                        ExtraData = "0";
                        InteractingUser = 0;

                        UpdateState(false, true);
                    }
                    else if (ExtraData == "1")
                    {
                        ExtraData = "0";
                        UpdateState(false, true);
                    }

                    if (user == null)
                    {
                        InteractingUser = 0;
                    }
                    break;
                case "teleport":
                    var keepDoorOpen = false;
                    var showTeleEffect = false;

                    // Do we have a primary user that wants to go somewhere?
                    if (InteractingUser > 0)
                    {
                        user = GetRoom().GetRoomUserByHabbo(InteractingUser);

                        // Is this user okay?
                        if (user != null)
                        {
                            // Is he in the tele?
                            if (user.Coordinate == Coordinate)
                            {
                                if (user.TeleDelay == -1)
                                {
                                    user.TeleDelay = 1;
                                }

                                if (TeleHandler.IsTeleLinked(Id))
                                {
                                    showTeleEffect = true;

                                    if (user.TeleDelay == 0)
                                    {
                                        // Woop! No more delay.
                                        var teleId = TeleHandler.GetLinkedTele(Id);
                                        var roomId = TeleHandler.GetTeleRoomId(teleId);

                                        // Do we need to tele to the same room or gtf to another?
                                        if (roomId == RoomId)
                                        {
                                            var item = GetRoom().GetItem(teleId);

                                            if (item == null)
                                            {
                                                user.UnlockWalking();
                                            }
                                            else
                                            {
                                                // Set pos
                                                user.SetPos(item.X, item.Y, item.Z);
                                                user.SetRot(item.Rot);

                                                // Force tele effect update (dirty)
                                                item.ExtraData = "2";
                                                item.UpdateState(false, true);

                                                // Set secondary interacting user
                                                item.InteractingUser2 = InteractingUser;
                                            }
                                        }
                                        else
                                        {
                                            // Let's run the teleport delegate to take futher care of this..
                                            PylenorEnvironment.GetGame()
                                                .GetRoomManager()
                                                .AddTeleAction(new TeleUserData(user, roomId, teleId));
                                        }

                                        // We're done with this tele. We have another one to bother.
                                        InteractingUser = 0;
                                    }
                                    else
                                    {
                                        // We're linked, but there's a delay, so decrease the delay and wait it out.
                                        user.TeleDelay--;
                                    }
                                }
                                else
                                {
                                    // This tele is not linked, so let's gtfo.
                                    // Open the door
                                    keepDoorOpen = true;

                                    user.UnlockWalking();
                                    InteractingUser = 0;

                                    // Move out of the tele
                                    user.MoveTo(SquareInFront);
                                }
                            }
                            // Is he in front of the tele?
                            else if (user.Coordinate == SquareInFront)
                            {
                                // Open the door
                                keepDoorOpen = true;

                                // Lock his walking. We're taking control over him. Allow overriding so he can get in the tele.
                                if (user.IsWalking && (user.GoalX != X || user.GoalY != Y))
                                {
                                    user.ClearMovement(true);
                                }

                                user.CanWalk = false;
                                user.AllowOverride = true;

                                // Move into the tele
                                user.MoveTo(Coordinate);
                            }
                            // Not even near, do nothing and move on for the next user.
                            else
                            {
                                InteractingUser = 0;
                            }
                        }
                        else
                        {
                            // Invalid user, do nothing and move on for the next user. 
                            InteractingUser = 0;
                        }
                    }

                    // Do we have a secondary user that wants to get out of the tele?
                    if (InteractingUser2 > 0)
                    {
                        var user2 = GetRoom().GetRoomUserByHabbo(InteractingUser2);

                        // Is this user okay?
                        if (user2 != null)
                        {
                            // If so, open the door, unlock the user's walking, and try to push him out in the right direction. We're done with him!
                            keepDoorOpen = true;
                            user2.UnlockWalking();
                            user2.MoveTo(SquareInFront);
                        }

                        // This is a one time thing, whether the user's valid or not.
                        InteractingUser2 = 0;
                    }

                    // Set the new item state, by priority
                    if (keepDoorOpen)
                    {
                        if (ExtraData != "1")
                        {
                            ExtraData = "1";
                            UpdateState(false, true);
                        }
                    }
                    else if (showTeleEffect)
                    {
                        if (ExtraData != "2")
                        {
                            ExtraData = "2";
                            UpdateState(false, true);
                        }
                    }
                    else
                    {
                        if (ExtraData != "0")
                        {
                            ExtraData = "0";
                            UpdateState(false, true);
                        }
                    }

                    // We're constantly going!
                    ReqUpdate(1);
                    break;
                case "bottle":
                    ExtraData = PylenorEnvironment.GetRandomNumber(0, 7).ToString();
                    UpdateState();
                    break;
                case "dice":
                    ExtraData = PylenorEnvironment.GetRandomNumber(1, 6).ToString();
                    UpdateState();
                    break;
                case "habbowheel":
                    ExtraData = PylenorEnvironment.GetRandomNumber(1, 10).ToString();
                    UpdateState();
                    break;
                case "loveshuffler":
                    if (ExtraData == "0")
                    {
                        ExtraData = PylenorEnvironment.GetRandomNumber(1, 4).ToString();
                        ReqUpdate(20);
                    }
                    else if (ExtraData != null && ExtraData != "-1")
                    {
                        ExtraData = "-1";
                    }

                    UpdateState(false, true);
                    break;
                case "alert":
                    if (ExtraData == "1")
                    {
                        ExtraData = "0";
                        UpdateState(false, true);
                    }
                    break;
                case "vendingmachine":
                    if (ExtraData == "1")
                    {
                        user = GetRoom().GetRoomUserByHabbo(InteractingUser);

                        if (user != null && user.Coordinate == SquareInFront)
                        {
                            var randomDrink =
                                GetBaseItem().VendingIds[
                                    PylenorEnvironment.GetRandomNumber(0, (GetBaseItem().VendingIds.Count - 1))];
                            user.CarryItem(randomDrink);
                        }

                        InteractingUser = 0;
                        ExtraData = "0";

                        user?.UnlockWalking();
                        UpdateState(false, true);
                    }
                    break;
                case "wf_trg_onsay":
                case "wf_trg_enterroom":
                case "wf_trg_furnistate":
                case "wf_trg_onfurni":
                case "wf_trg_offfurni":
                case "wf_trg_gameend":
                case "wf_trg_gamestart":
                case "wf_trg_atscore":
                case "wf_act_saymsg":
                case "wf_act_togglefurni":
                case "wf_act_givepoints":
                case "wf_act_moverotate":
                case "wf_act_matchfurni":
                case "wf_act_give_phx":
                case "wf_cnd_trggrer_on_frn":
                case "wf_cnd_furnis_hv_avtrs":
                case "wf_cnd_has_furni_on":
                case "wf_cnd_match_snapshot":
                case "wf_cnd_phx":
                case "bb_teleport":
                    if (ExtraData == "1")
                    {
                        ExtraData = "0";
                        UpdateState(false, true);
                    }
                    break;
                case "wf_trg_timer":
                    if (ExtraData == "1")
                    {
                        ExtraData = "0";
                        UpdateState(false, true);
                    }
                    if (Extra1.Length > 0)
                    {
                        GetRoom().HandleTimer(this);
                        ReqUpdate(Convert.ToInt32(double.Parse(Extra1, CustomCultureInfo.GetCustomCultureInfo()) * 2.0));
                    }
                    break;
            }
        }

        public void ReqUpdate(int cycles)
        {
            UpdateCounter = cycles;
            UpdateNeeded = true;
        }

        public void UpdateState()
        {
            UpdateState(true, true);
        }

        public void UpdateState(bool inDb, bool inRoom)
        {
            if (inDb)
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("extra_data", ExtraData);
                    dbClient.ExecuteQuery("UPDATE room_items SET extra_data = @extra_data WHERE id = '" + Id +
                                          "' LIMIT 1");
                }
            }

            if (!inRoom) return;
            var message = new ServerPacket(IsFloorItem ? 88 : (uint) 85);

            if (IsFloorItem)
            {
                message.WriteString(Id.ToString());
                message.WriteString(ExtraData);
            }
            else
            {
                Serialize(message);
            }

            GetRoom().SendMessage(message);
        }

        public void Serialize(ServerPacket packet)
        {
            if (IsFloorItem)
            {
                packet.WriteUInt(Id);
                packet.WriteInteger(GetBaseItem().Sprite);
                packet.WriteInteger(X);
                packet.WriteInteger(Y);
                packet.WriteInteger(Rot);
                packet.WriteString(Z.ToString(CultureInfo.InvariantCulture).Replace(',', '.'));
                packet.WriteInteger(0);
                packet.WriteString(ExtraData);
                packet.WriteInteger(-1);
                packet.WriteBoolean(GetBaseItem().InteractionType.ToLower() == "default");
            }
            else if (IsWallItem)
            {
                packet.WriteString(Id.ToString());
                packet.WriteInteger(GetBaseItem().Sprite);
                packet.WriteString(WallPos);

                switch (GetBaseItem().InteractionType.ToLower())
                {
                    case "postit":

                        packet.WriteString(ExtraData.Split(' ')[0]);
                        break;

                    default:

                        packet.WriteString(ExtraData);
                        break;
                }
                packet.WriteBoolean(GetBaseItem().InteractionType.ToLower() == "default");
            }
        }

        internal void Flip()
        {
            if (Extra3 == "") return;
            var collection = Extra3.Split(',');
            IEnumerable<string> enumerable = new List<string>(collection);
            var list = enumerable.ToList();
            var flag = false;
            if (list.Count > 5)
            {
                Extra3 = "";
                Extra4 = "";
            }
            else
            {
                foreach (var current in enumerable)
                {
                    RoomItem @class = null;
                    if (current.Length > 0)
                    {
                        @class = GetRoom().GetItem(Convert.ToUInt32(current));
                    }
                    if (@class != null) continue;
                    list.Remove(current);
                    flag = true;
                }
                if (!flag) return;
                Extra4 = OldEncoding.EncodeVl64(list.Count);
                foreach (var value in list.Select(t => Convert.ToInt32(t)))
                {
                    Extra4 += OldEncoding.EncodeVl64(value);
                    Extra3 = Extra3 + "," + Convert.ToString(value);
                }
                Extra3 = Extra3.Substring(1);
            }
        }

        public void Combine()
        {
            if (string.IsNullOrEmpty(Extra2)) return;
            var collection = Extra2.Split(',');
            IEnumerable<string> enumerable = new List<string>(collection);
            var list = enumerable.ToList();
            var flag = false;
            foreach (var current in from current in enumerable let @class = GetRoom().GetItem(Convert.ToUInt32(current)) where @class == null select current)
            {
                list.Remove(current);
                flag = true;
            }
            if (!flag) return;
            Extra1 = OldEncoding.EncodeVl64(list.Count);
            foreach (var num in list.Select(t => Convert.ToInt32(t)))
            {
                Extra1 += OldEncoding.EncodeVl64(num);
            }
            Extra2 = string.Join(",", list.ToArray());
        }

        public Item GetBaseItem()
        {
            return PylenorEnvironment.GetGame().GetItemManager().GetItem(BaseItem);
        }

        public Room GetRoom()
        {
            return PylenorEnvironment.GetGame().GetRoomManager().GetRoom(RoomId);
        }
    }
}