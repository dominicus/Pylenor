﻿using System.Data;

namespace Pylenor.HabboHotel.Items
{
    internal class TeleHandler
    {
        public static uint GetLinkedTele(uint teleId)
        {
            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                row =
                    dbClient.ReadDataRow("SELECT tele_two_id FROM tele_links WHERE tele_one_id = '" + teleId +
                                         "' LIMIT 1");
            }

            if (row == null)
            {
                return 0;
            }

            return (uint) row[0];
        }

        public static uint GetTeleRoomId(uint teleId)
        {
            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                row = dbClient.ReadDataRow("SELECT room_id FROM room_items WHERE id = '" + teleId + "' LIMIT 1");
            }

            if (row == null)
            {
                return 0;
            }

            return (uint) row[0];
        }

        public static bool IsTeleLinked(uint teleId)
        {
            var linkId = GetLinkedTele(teleId);

            if (linkId == 0)
            {
                return false;
            }

            var roomId = GetTeleRoomId(linkId);

            if (roomId == 0)
            {
                return false;
            }

            return true;
        }
    }
}