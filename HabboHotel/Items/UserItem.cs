﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Items
{
    public class UserItem
    {
        public uint BaseItem;
        public string ExtraData;
        public uint Id;

        public UserItem(uint id, uint baseItem, string extraData)
        {
            Id = id;
            BaseItem = baseItem;
            ExtraData = extraData;
        }

        public void Serialize(ServerPacket packet, bool inventory)
        {
            packet.WriteUInt(Id);
            packet.WriteString(GetBaseItem().Type.ToUpper());
            packet.WriteUInt(Id);
            packet.WriteInteger(GetBaseItem().Sprite);

            if (GetBaseItem().Name.Contains("a2"))
            {
                packet.WriteInteger(3);
            }
            else if (GetBaseItem().Name.Contains("wallpaper"))
            {
                packet.WriteInteger(2);
            }
            else if (GetBaseItem().Name.Contains("landscape"))
            {
                packet.WriteInteger(4);
            }
            else
            {
                packet.WriteInteger(0);
            }

            packet.WriteString(ExtraData);

            packet.WriteBoolean(GetBaseItem().AllowRecycle);
            packet.WriteBoolean(GetBaseItem().AllowTrade);
            packet.WriteBoolean(GetBaseItem().AllowInventoryStack);
            packet.WriteBoolean(PylenorEnvironment.GetGame().GetCatalog().GetMarketplace().CanSellItem(this));

            packet.WriteInteger(-1);

            if (GetBaseItem().Type.ToLower() != "s") return;
            packet.WriteString("");
            packet.WriteInteger(-1);
        }

        public Item GetBaseItem()
        {
            return PylenorEnvironment.GetGame().GetItemManager().GetItem(BaseItem);
        }
    }
}