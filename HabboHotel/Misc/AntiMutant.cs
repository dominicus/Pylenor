﻿using System;
using System.Linq;

namespace Pylenor.HabboHotel.Misc
{
    internal class AntiMutant
    {
        public static bool ValidateLook(string look, string gender)
        {
            var hasHead = false;
            var hasLegs = false;
            var hasCheated = false;

            if (look.Length < 1)
            {
                return false;
            }

            try
            {
                var sets = look.Split('.');

                if (sets.Length < 4)
                {
                    return false;
                }

                foreach (var parts in sets.Select(set => set.Split('-')))
                {
                    if (parts.Length < 3)
                    {
                        return false;
                    }

                    var name = parts[0];
                    var type = int.Parse(parts[1]);
                    var color = int.Parse(parts[1]);
                    var color2 = parts[1];

                    if (type <= 0 || color < 0)
                    {
                        return false;
                    }

                    if (name.Length != 2)
                    {
                        return false;
                    }

                    if (name == "hd")
                    {
                        hasHead = true;
                    }
                    if (name == "lg")
                    {
                        hasLegs = true;
                    }

                    if (color2 == "999")
                    {
                        hasCheated = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return hasHead && hasLegs && !hasCheated && (gender == "M" || gender == "F");
        }
    }
}