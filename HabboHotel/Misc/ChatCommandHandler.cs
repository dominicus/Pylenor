﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Items;
using Pylenor.HabboHotel.Pathfinding;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.Misc
{
    internal class ChatCommandHandler
    {
        public static bool Parse(Session session, string input)
        {
            var Params = input.Split(' ');

            try
            {
                string targetUser;
                Session targetClient;
                RoomUser targetRoomUser;
                Room targetRoom;
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (Params[0].ToLower())
                {
                        #region HM 

                    case "update_inventory":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            session.GetPlayer().GetInventoryComponent().UpdateItems(true);
                            return true;
                        }

                        return false;
                    case "summonall":
                    {
                        var currentRoom = session.GetPlayer().CurrentRoom;
                        if (!session.GetPlayer().HasFuse("fuse_sysadmin")) return true;
                        foreach (
                            var c in
                                PylenorEnvironment.GetGame()
                                    .GetClientManager()
                                    .Clients.Values.Where(c => c.GetPlayer() != null))
                        {
                            if (c.GetPlayer().Username == session.GetPlayer().Username)
                            {
                                session.SendNotif("You have summoned everyone");
                            }
                            else
                            {
                                c.PrepareRoom(currentRoom.RoomId, currentRoom.Password);
                                c.SendNotif(session.GetPlayer().Username + "Has Summoned you");
                            }
                        }


                        return true;
                    }
                    case "update_bots":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                            {
                                PylenorEnvironment.GetGame().GetBotManager().LoadBots(dbClient);
                            }
                            return true;
                        }

                        return false;
                    case "update_catalog":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                            {
                                PylenorEnvironment.GetGame().GetCatalog().Initialize(dbClient);
                            }
                            PylenorEnvironment.GetGame().GetClientManager().BroadcastMessage(new ServerPacket(441));

                            return true;
                        }

                        return false;
                    case "update_help":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                            {
                                PylenorEnvironment.GetGame().GetHelpTool().LoadCategories(dbClient);
                                PylenorEnvironment.GetGame().GetHelpTool().LoadTopics(dbClient);
                            }
                            session.SendNotif("Reloaded help categories and topics successfully.");

                            return true;
                        }

                        return false;
                    case "update_navigator":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                            {
                                PylenorEnvironment.GetGame().GetNavigator().Initialize(dbClient);
                            }
                            session.SendNotif("Re-initialized navigator successfully.");

                            return true;
                        }

                        return false;

                    /*case "idletime":

                        if (Session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            TargetRoom = UberEnvironment.GetGame().GetRoomManager().GetRoom(Session.GetPlayer().CurrentRoomId);
                            TargetRoomUser = TargetRoom.GetRoomUserByHabbo(Session.GetPlayer().Id);

                            TargetRoomUser.IdleTime = 600;

                            return true;
                        }

                        return false;*/
                    case "t":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            targetRoom =
                                PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

                            if (targetRoom == null)
                            {
                                return false;
                            }

                            targetRoomUser = targetRoom.GetRoomUserByHabbo(session.GetPlayer().Id);

                            if (targetRoomUser == null)
                            {
                                return false;
                            }

                            session.SendNotif("X: " + targetRoomUser.X + " - Y: " + targetRoomUser.Y + " - Z: " +
                                              targetRoomUser.Z + " - Rot: " + targetRoomUser.RotBody);

                            return true;
                        }

                        return false;
                    case "override":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            targetRoom =
                                PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

                            if (targetRoom == null)
                            {
                                return false;
                            }

                            targetRoomUser = targetRoom.GetRoomUserByHabbo(session.GetPlayer().Id);

                            if (targetRoomUser == null)
                            {
                                return false;
                            }

                            if (targetRoomUser.AllowOverride)
                            {
                                targetRoomUser.AllowOverride = false;
                                session.SendNotif("Walking override disabled.");
                            }
                            else
                            {
                                targetRoomUser.AllowOverride = true;
                                session.SendNotif("Walking override enabled.");
                            }

                            return true;
                        }

                        return false;
                    case "moonwalkon":
                        Rotation.MoonwalkEnabled = 1;
                        session.SendNotif("Moonwalk is now enabled.");
                        Console.WriteLine("Someone has enabled their moonwalk.");
                        break;
                    case "moonwalkoff":
                        Rotation.MoonwalkEnabled = 0;
                        session.SendNotif("Moonwalk is now disabled..");
                        Console.WriteLine("Someone has turned their moonwalk off.");
                        break;
                    case "update_defs":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                            {
                                PylenorEnvironment.GetGame().GetItemManager().LoadItems(dbClient);
                            }
                            session.SendNotif("Item defenitions reloaded successfully.");
                            return true;
                        }

                        return false;
                    case "whosonline":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            DataTable onlineData;
                            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                            {
                                onlineData = dbClient.ReadDataTable("SELECT username FROM users WHERE online = '1';");
                            }
                            var message = onlineData.Rows.Cast<DataRow>()
                                .Aggregate("Users Online:\r", (current, user) => current + user["username"] + "\r");
                            session.SendNotif(message);
                            return true;
                        }

                        return false;

                        #endregion

                        #region General Commands

                    case "pickall":
                        targetRoom =
                            PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

                        if (targetRoom != null && targetRoom.CheckRights(session, true))
                        {
                            var toRemove = new List<RoomItem>();

                            toRemove.AddRange(targetRoom.Items);

                            foreach (var item in toRemove)
                            {
                                targetRoom.RemoveFurniture(session, item.Id);
                                session.GetPlayer()
                                    .GetInventoryComponent()
                                    .AddItem(item.Id, item.BaseItem, item.ExtraData);
                            }

                            session.GetPlayer().GetInventoryComponent().UpdateItems(true);
                            return true;
                        }

                        return false;
                    case "empty":
                        session.GetPlayer().GetInventoryComponent().ClearItems();

                        return true;

                        #endregion

                        #region Moderation Commands

                    case "invisible":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            if (session.GetPlayer().SpectatorMode)
                            {
                                session.GetPlayer().SpectatorMode = false;
                                session.SendNotif("Spectator mode disabled. Reload the room to apply changes.");
                            }
                            else
                            {
                                session.GetPlayer().SpectatorMode = true;
                                session.SendNotif("Spectator mode enabled. Reload the room to apply changes.");
                            }

                            return true;
                        }

                        return false;
                    case "commands":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                            session.SendNotif("Hello Administrator. Below are your commands~\n\n" +
                                              ":ha (message)\n:shutup <user> <message>\n:roomkick <message>\n\nMore are available.");
                        else
                            session.SendNotif("Hello user. Below are your commands~\n\n" +
                                              ":commands - Shows this dialogue.\n:about - Tells you information about our emulator.\n:moonwalkon - Turns moonwalk on\n:moonwalkoff - Turns moonwalk off\n\nThats all for now, Enjoy!");
                        break;
                    case "ha":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            var notice = input.Substring(3);
                            var hotelAlert = new ServerPacket(139);
                            hotelAlert.WriteString("Message from Hotel Management:\r\n" + notice + "\r\n-" +
                                                   session.GetPlayer().Username);
                            PylenorEnvironment.GetGame().GetClientManager().BroadcastMessage(hotelAlert);

                            return true;
                        }
                        return false;
                    case "credits":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Params[1]);
                            if (targetClient != null)
                            {
                                int creditsToAdd;
                                if (int.TryParse(Params[2], out creditsToAdd))
                                {
                                    targetClient.GetPlayer().Credits = targetClient.GetPlayer().Credits + creditsToAdd;
                                    targetClient.GetPlayer().UpdateCreditsBalance(true);
                                    targetClient.SendNotif(session.GetPlayer().Username + " has awarded you " +
                                                           creditsToAdd + " credits!");
                                    session.SendNotif("Credit balance updated successfully.");
                                    return true;
                                }
                                session.SendNotif("Please enter a valid amount of credits.");
                                return false;
                            }
                            session.SendNotif("User could not be found.");
                            return false;
                        }
                        return false;
                    case "pixels":
                        if (session.GetPlayer().HasFuse("fuse_sysadmin"))
                        {
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Params[1]);
                            if (targetClient != null)
                            {
                                int creditsToAdd;
                                if (int.TryParse(Params[2], out creditsToAdd))
                                {
                                    targetClient.GetPlayer().ActivityPoints = targetClient.GetPlayer().ActivityPoints +
                                                                              creditsToAdd;
                                    targetClient.GetPlayer().UpdateActivityPointsBalance(true);
                                    targetClient.SendNotif(session.GetPlayer().Username + " has awarded you " +
                                                           creditsToAdd + " pixels!");
                                    session.SendNotif("Pixel balance updated successfully.");
                                    return true;
                                }
                                session.SendNotif("Please enter a valid amount of pixels.");
                                return false;
                            }
                            session.SendNotif("User could not be not found.");
                            return false;
                        }
                        return false;
                    case "ban":
                        if (session.GetPlayer().HasFuse("fuse_ban"))
                        {
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Params[1]);

                            if (targetClient == null)
                            {
                                session.SendNotif("User not found.");
                                return true;
                            }

                            if (targetClient.GetPlayer().Rank >= session.GetPlayer().Rank)
                            {
                                session.SendNotif("You are not allowed to ban that user.");
                                return true;
                            }

                            var banTime = 0;

                            try
                            {
                                banTime = int.Parse(Params[2]);
                            }
                            catch (FormatException)
                            {
                            }

                            if (banTime <= 600)
                            {
                                session.SendNotif(
                                    "Ban time is in seconds and must be at least than 600 seconds (ten minutes). For more specific preset ban times, use the mod tool.");
                            }

                            PylenorEnvironment.GetGame()
                                .GetBanManager()
                                .BanUser(targetClient, session.GetPlayer().Username, banTime, MergeParams(Params, 3),
                                    false);
                            return true;
                        }

                        return false;
                    case "superban":
                        if (session.GetPlayer().HasFuse("fuse_superban"))
                        {
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Params[1]);

                            if (targetClient == null)
                            {
                                session.SendNotif("User not found.");
                                return true;
                            }

                            if (targetClient.GetPlayer().Rank >= session.GetPlayer().Rank)
                            {
                                session.SendNotif("You are not allowed to ban that user.");
                                return true;
                            }

                            PylenorEnvironment.GetGame()
                                .GetBanManager()
                                .BanUser(targetClient, session.GetPlayer().Username, 360000000, MergeParams(Params, 2),
                                    false);
                            return true;
                        }

                        return false;
                    case "roomkick":
                        if (session.GetPlayer().HasFuse("fuse_roomkick"))
                        {
                            targetRoom =
                                PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

                            if (targetRoom == null)
                            {
                                return false;
                            }

                            var genericMsg = true;
                            var modMsg = MergeParams(Params, 1);

                            if (modMsg.Length > 0)
                            {
                                genericMsg = false;
                            }

                            foreach (
                                var roomUser in
                                    targetRoom.UserList.Where(
                                        roomUser => roomUser.GetClient().GetPlayer().Rank < session.GetPlayer().Rank))
                            {
                                if (!genericMsg)
                                {
                                    roomUser.GetClient().SendNotif("You have been kicked by an moderator: " + modMsg);
                                }

                                targetRoom.RemoveUserFromRoom(roomUser.GetClient(), true, genericMsg);
                            }

                            return true;
                        }

                        return false;
                    case "roomalert":
                        if (session.GetPlayer().HasFuse("fuse_roomalert"))
                        {
                            targetRoom =
                                PylenorEnvironment.GetGame().GetRoomManager().GetRoom(session.GetPlayer().CurrentRoomId);

                            if (targetRoom == null)
                            {
                                return false;
                            }

                            var msg = MergeParams(Params, 1);

                            foreach (var roomUser in targetRoom.UserList.Where(roomUser => !roomUser.IsBot))
                            {
                                roomUser.GetClient().SendNotif(msg);
                            }

                            return true;
                        }

                        return false;
                    case "mute":
                        if (session.GetPlayer().HasFuse("fuse_mute"))
                        {
                            targetUser = Params[1];
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(targetUser);

                            if (targetClient?.GetPlayer() == null)
                            {
                                session.SendNotif("Could not find user: " + targetUser);
                                return true;
                            }

                            if (targetClient.GetPlayer().Rank >= session.GetPlayer().Rank)
                            {
                                session.SendNotif("You are not allowed to (un)mute that user.");
                                return true;
                            }

                            targetClient.GetPlayer().Mute();
                            return true;
                        }

                        return false;
                    case "unmute":
                        if (session.GetPlayer().HasFuse("fuse_mute"))
                        {
                            targetUser = Params[1];
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(targetUser);

                            if (targetClient?.GetPlayer() == null)
                            {
                                session.SendNotif("Could not find user: " + targetUser);
                                return true;
                            }

                            if (targetClient.GetPlayer().Rank >= session.GetPlayer().Rank)
                            {
                                session.SendNotif("You are not allowed to (un)mute that user.");
                                return true;
                            }

                            targetClient.GetPlayer().Unmute();
                            return true;
                        }

                        return false;
                    case "alert":
                        if (session.GetPlayer().HasFuse("fuse_alert"))
                        {
                            targetUser = Params[1];
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(targetUser);

                            if (targetClient == null)
                            {
                                session.SendNotif("Could not find user: " + targetUser);
                                return true;
                            }

                            targetClient.SendNotif(MergeParams(Params, 2), session.GetPlayer().HasFuse("fuse_admin"));
                            return true;
                        }

                        return false;
                    case "softkick":
                    case "kick":
                        if (session.GetPlayer().HasFuse("fuse_kick"))
                        {
                            targetUser = Params[1];
                            targetClient = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(targetUser);

                            if (targetClient == null)
                            {
                                session.SendNotif("Could not find user: " + targetUser);
                                return true;
                            }

                            if (session.GetPlayer().Rank <= targetClient.GetPlayer().Rank)
                            {
                                session.SendNotif("You are not allowed to kick that user.");
                                return true;
                            }

                            if (targetClient.GetPlayer().CurrentRoomId < 1)
                            {
                                session.SendNotif("That user is not in a room and can not be kicked.");
                                return true;
                            }

                            targetRoom =
                                PylenorEnvironment.GetGame()
                                    .GetRoomManager()
                                    .GetRoom(targetClient.GetPlayer().CurrentRoomId);

                            if (targetRoom == null)
                            {
                                return true;
                            }

                            targetRoom.RemoveUserFromRoom(targetClient, true, false);

                            if (Params.Length > 2)
                            {
                                targetClient.SendNotif(
                                    "A moderator has kicked you from the room for the following reason: " +
                                    MergeParams(Params, 2));
                            }
                            else
                            {
                                targetClient.SendNotif("A moderator has kicked you from the room.");
                            }

                            return true;
                        }

                        return false;

                        #endregion
                }
            }
            catch
            {
                // ignored
            }

            return false;
        }

        public static string MergeParams(string[] Params, int start)
        {
            var mergedParams = new StringBuilder();

            for (var i = 0; i < Params.Length; i++)
            {
                if (i < start)
                {
                    continue;
                }

                if (i > start)
                {
                    mergedParams.Append(" ");
                }

                mergedParams.Append(Params[i]);
            }

            return mergedParams.ToString();
        }
    }
}