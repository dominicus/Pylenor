﻿using System;
using System.Threading;

namespace Pylenor.HabboHotel.Misc
{
    public class LowPriorityWorker
    {
        public static void Process()
        {
            while (true)
            {
                var ts = DateTime.Now - PylenorEnvironment.ServerStarted;
                const int status = 1;
                var usersOnline = PylenorEnvironment.GetGame().GetClientManager().ClientCount;
                var roomsLoaded = PylenorEnvironment.GetGame().GetRoomManager().LoadedRoomsCount;

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("UPDATE server_status SET stamp = '" + PylenorEnvironment.GetUnixTimestamp() +
                                          "', status = '" + status + "', users_online = '" + usersOnline +
                                          "', rooms_loaded = '" + roomsLoaded + "', server_ver = '" +
                                          PylenorEnvironment.PrettyVersion + "' LIMIT 1");
                }
                Console.Title = "Pylenor 1.0 | Online Users: " + usersOnline + " | Rooms Loaded: " + roomsLoaded +
                                " | Uptime: " + ts.Days + " days, " + ts.Hours + " hours and " + ts.Minutes + " minutes";
                PylenorEnvironment.GetGame().GetClientManager().CheckEffects();

                Thread.Sleep(5000);
            }
        }
    }
}