﻿using System;
using System.Threading;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Misc
{
    internal class PixelManager
    {
        private const int RcvEveryMins = 15;
        private const int RcvAmount = 50;

        private readonly Thread _workerThread;

        public bool KeepAlive;

        public PixelManager()
        {
            KeepAlive = true;
            _workerThread = new Thread(Process)
            {
                Name = "Pixel Manager",
                Priority = ThreadPriority.Lowest
            };
        }

        public void Start()
        {
            PylenorEnvironment.GetLogging().Write("Starting reward timer");
            _workerThread.Start();
            Console.WriteLine("...completed!");
        }

        private void Process()
        {
            try
            {
                while (KeepAlive)
                {
                    if (PylenorEnvironment.GetGame() != null && PylenorEnvironment.GetGame().GetClientManager() != null)
                    {
                        PylenorEnvironment.GetGame().GetClientManager().CheckPixelUpdates();
                    }

                    Thread.Sleep(15000);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        public bool NeedsUpdate(Session client)
        {
            var passedMins = (PylenorEnvironment.GetUnixTimestamp() - client.GetPlayer().LastActivityPointsUpdate)/60;

            return passedMins >= RcvEveryMins;
        }

        public void GivePixels(Session client)
        {
            var timestamp = PylenorEnvironment.GetUnixTimestamp();

            client.GetPlayer().LastActivityPointsUpdate = timestamp;
            client.GetPlayer().ActivityPoints += RcvAmount;
            client.GetPlayer().UpdateActivityPointsBalance(true, RcvAmount);
        }
    }
}