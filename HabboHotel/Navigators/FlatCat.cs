﻿namespace Pylenor.HabboHotel.Navigators
{
    internal class FlatCat
    {
        public string Caption;
        public int Id;
        public int MinRank;

        public FlatCat(int id, string caption, int minRank)
        {
            Id = id;
            Caption = caption;
            MinRank = minRank;
        }
    }
}