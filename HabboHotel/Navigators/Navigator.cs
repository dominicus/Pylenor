﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Navigators
{
    internal class Navigator
    {
        private List<FlatCat> _privateCategories;
        public Dictionary<int, string> PublicCategories;

        public Dictionary<int, PublicItem> PublicItems;

        public void Initialize(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading navigator");
            PublicCategories = new Dictionary<int, string>();
            _privateCategories = new List<FlatCat>();
            PublicItems = new Dictionary<int, PublicItem>();

            var dPubCats = dbClient.ReadDataTable("SELECT id,caption FROM navigator_pubcats WHERE enabled = '1';");
            var dPrivCats =
                dbClient.ReadDataTable("SELECT id,caption,min_rank FROM navigator_flatcats WHERE enabled = '1';");
            var dPubItems =
                dbClient.ReadDataTable(
                    "SELECT id,bannertype,caption,image,image_type,room_id,category_id,category_parent_id FROM navigator_publics WHERE enabled = '1' ORDER BY ordernum ASC;");

            if (dPubCats != null)
            {
                foreach (DataRow row in dPubCats.Rows)
                {
                    PublicCategories.Add((int) row["id"], (string) row["caption"]);
                }
            }

            if (dPrivCats != null)
            {
                foreach (DataRow row in dPrivCats.Rows)
                {
                    _privateCategories.Add(new FlatCat((int) row["id"], (string) row["caption"], (int) row["min_rank"]));
                }
            }

            if (dPubItems == null) return;
            foreach (DataRow row in dPubItems.Rows)
            {
                PublicItems.Add((int) row["id"],
                    new PublicItem((int) row["id"], int.Parse(row["bannertype"].ToString()), (string) row["caption"],
                        (string) row["image"],
                        ((row["image_type"].ToString().ToLower() == "internal")
                            ? PublicImageType.Internal
                            : PublicImageType.External),
                        (uint) row["room_id"], (int) row["category_id"], (int) row["category_parent_id"]));
            }
            Console.WriteLine("...completed!");
        }

        public int GetCountForParent(int parentId)
        {
            return PublicItems.Values.Count(item => item.ParentId == parentId || parentId == -1);
        }

        public FlatCat GetFlatCat(int id)
        {
            return _privateCategories.FirstOrDefault(flatCat => flatCat.Id == id);
        }

        public ServerPacket SerializeFlatCategories()
        {
            var cats = new ServerPacket(221);
            cats.WriteInteger(_privateCategories.Count);

            foreach (var flatCat in _privateCategories)
            {
                if (flatCat.Id > 0)
                {
                    cats.WriteBoolean(true);
                }

                cats.WriteInteger(flatCat.Id);
                cats.WriteString(flatCat.Caption);
            }

            cats.WriteString("");

            return cats;
        }

        public ServerPacket SerializePublicRooms()
        {
            var frontpage = new ServerPacket(450);
            frontpage.WriteInteger(GetCountForParent(-1));

            foreach (var pub in PublicItems.Values)
            {
                pub.Serialize(frontpage);
            }

            return frontpage;
        }

        public ServerPacket SerializeFavoriteRooms(Session session)
        {
            var rooms = new ServerPacket(451);
            rooms.WriteInteger(0);
            rooms.WriteInteger(6);
            rooms.WriteString("");
            rooms.WriteInteger(session.GetPlayer().FavoriteRooms.Count);
            foreach (var id in session.GetPlayer().FavoriteRooms)
            {
                PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(id).Serialize(rooms, false);
            }

            return rooms;
        }

        public ServerPacket SerializeRecentRooms(Session session)
        {
            DataTable data;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT * FROM user_roomvisits ORDER BY entry_timestamp DESC LIMIT 50;");
            }
            var validRecentRooms = new List<RoomData>();
            var roomsListed = new List<uint>();

            if (data != null)
            {
                foreach (
                    var rData in
                        data.Rows.Cast<DataRow>()
                            .Select(
                                row =>
                                    PylenorEnvironment.GetGame()
                                        .GetRoomManager()
                                        .GenerateRoomData((uint) row["room_id"]))
                            .Where(rData => rData != null && !rData.IsPublicRoom && !roomsListed.Contains(rData.Id)))
                {
                    validRecentRooms.Add(rData);
                    roomsListed.Add(rData.Id);
                }
            }

            var rooms = new ServerPacket(451);
            rooms.WriteInteger(0);
            rooms.WriteInteger(7);
            rooms.WriteString("");
            rooms.WriteInteger(validRecentRooms.Count);

            foreach (var validRecentRoom in validRecentRooms)
            {
                validRecentRoom.Serialize(rooms, false);
            }

            return rooms;
        }

        public ServerPacket SerializeEventListing(Session session, int category)
        {
            var packet = new ServerPacket(451);
            packet.WriteInteger(category);
            packet.WriteInteger(12);
            packet.WriteString("");

            var eventRooms = PylenorEnvironment.GetGame().GetRoomManager().GetEventRoomsForCategory(category);
            packet.WriteInteger(eventRooms.Count);
            foreach (var room in eventRooms)
            {
                var data = new RoomData();
                data.Fill(room);
                data.Serialize(packet, true);
            }

            return packet;
        }

        public ServerPacket SerializePopularRoomTags()
        {
            var tags = new Dictionary<string, int>();
            DataTable data;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data =
                    dbClient.ReadDataTable(
                        "SELECT tags,users_now FROM rooms WHERE roomtype = 'private' AND users_now > 0 ORDER BY users_now DESC LIMIT 50");
            }
            if (data != null)
            {
                foreach (DataRow row in data.Rows)
                {
                    var roomTags = row["tags"].ToString().Split(',').ToList();

                    foreach (var tag in roomTags)
                    {
                        if (tags.ContainsKey(tag))
                        {
                            tags[tag] += (int) row["users_now"];
                        }
                        else
                        {
                            tags.Add(tag, (int) row["users_now"]);
                        }
                    }
                }
            }

            var sortedTags = new List<KeyValuePair<string, int>>(tags);

            sortedTags.Sort(
                (firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value)
                );

            var packet = new ServerPacket(452);
            packet.WriteInteger(sortedTags.Count);

            foreach (var tagData in sortedTags)
            {
                packet.WriteString(tagData.Key);
                packet.WriteInteger(tagData.Value);
            }

            return packet;
        }

        public ServerPacket SerializeSearchResults(string searchQuery)
        {
            DataTable data = null;

            searchQuery = PylenorEnvironment.FilterInjectionChars(searchQuery.ToLower()).Trim();

            if (searchQuery.Length > 0)
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("query", searchQuery + "%");
                    dbClient.AddParamWithValue("tags_query", "%" + searchQuery + "%");
                    data =
                        dbClient.ReadDataTable(
                            "SELECT * FROM rooms WHERE caption LIKE @query AND roomtype = 'private' OR tags LIKE @tags_query AND roomtype = 'private' OR owner LIKE @query AND roomtype = 'private' ORDER BY users_now DESC LIMIT 30");
                }
            }

            var results = new List<RoomData>();

            if (data != null)
            {
                foreach (DataRow row in data.Rows)
                {
                    var rData = new RoomData();
                    rData.Fill(row);
                    results.Add(rData);
                }
            }

            var packet = new ServerPacket(451);
            packet.WriteInteger(1);
            packet.WriteInteger(9);
            packet.WriteString(searchQuery);
            packet.WriteInteger(results.Count);

            foreach (var room in results)
            {
                room.Serialize(packet, false);
            }

            return packet;
        }

        public ServerPacket SerializeRoomListing(Session session, int mode)
        {
            var rooms = new ServerPacket(451);

            if (mode >= -1)
            {
                rooms.WriteInteger(mode);
                rooms.WriteInteger(1);
            }
            else if (mode == -2)
            {
                rooms.WriteInteger(0);
                rooms.WriteInteger(2);
            }
            else if (mode == -3)
            {
                rooms.WriteInteger(0);
                rooms.WriteInteger(5);
            }
            else if (mode == -4)
            {
                rooms.WriteInteger(0);
                rooms.WriteInteger(3);
            }
            else if (mode == -5)
            {
                rooms.WriteInteger(0);
                rooms.WriteInteger(4);
            }

            rooms.WriteString("");

            DataTable data;

            switch (mode)
            {
                case -5:

                    var friendRooms = new List<uint>();

                    // lock (Session.GetPlayer().GetMessenger().GetBuddies())
                    friendRooms.AddRange(from buddy in session.GetPlayer().GetMessenger().GetBuddies()
                        select PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(buddy.Id)
                        into client
                        where client?.GetPlayer() != null && client.GetPlayer().CurrentRoomId > 0
                        select client.GetPlayer().CurrentRoomId);

                    var query = new StringBuilder("SELECT * FROM rooms WHERE");

                    // ReSharper disable once InconsistentNaming
                    var _i = 0;

                    foreach (var room in friendRooms)
                    {
                        if (_i > 0)
                        {
                            query.Append(" OR");
                        }

                        query.Append(" id = '" + room + "'");

                        _i++;
                    }

                    query.Append(" ORDER BY users_now DESC LIMIT 40");
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        data = _i > 0 ? dbClient.ReadDataTable(query.ToString()) : null;
                    }
                    break;

                case -4:

                    var friendsNames =
                        session.GetPlayer().GetMessenger().GetBuddies().Select(buddy => buddy.Username).ToList();

                    var builder = new StringBuilder("SELECT * FROM rooms WHERE");

                    var i = 0;

                    foreach (var name in friendsNames)
                    {
                        if (i > 0)
                        {
                            builder.Append(" OR");
                        }

                        builder.Append(" owner = '" + name + "'");

                        i++;
                    }

                    builder.Append(" ORDER BY users_now DESC LIMIT 40");
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        data = i > 0 ? dbClient.ReadDataTable(builder.ToString()) : null;
                    }
                    break;

                case -3:
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        data =
                            dbClient.ReadDataTable("SELECT * FROM rooms WHERE owner = '" + session.GetPlayer().Username +
                                                   "' ORDER BY id ASC;");
                    }
                    break;

                case -2:

                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        data =
                            dbClient.ReadDataTable(
                                "SELECT * FROM rooms WHERE score > 0 AND roomtype = 'private' ORDER BY score DESC LIMIT 40;");
                    }
                    break;

                case -1:

                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        data =
                            dbClient.ReadDataTable(
                                "SELECT * FROM rooms WHERE users_now > 0 AND roomtype = 'private' ORDER BY users_now DESC LIMIT 40;");
                    }
                    break;

                default:

                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        data =
                            dbClient.ReadDataTable("SELECT * FROM rooms WHERE category = '" + mode +
                                                   "' AND roomtype = 'private' ORDER BY users_now DESC LIMIT 40;");
                    }
                    break;
            }

            if (data == null)
            {
                rooms.WriteInteger(0);
            }
            else
            {
                rooms.WriteInteger(data.Rows.Count);

                foreach (DataRow row in data.Rows)
                {
                    PylenorEnvironment.GetGame()
                        .GetRoomManager()
                        .GenerateRoomData((uint) row["id"])
                        .Serialize(rooms, false);
                }
            }

            return rooms;
        }
    }
}