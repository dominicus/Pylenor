﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.Navigators
{
    internal enum PublicImageType
    {
        Internal = 0,
        External = 1
    }

    internal class PublicItem
    {
        public string Caption;
        public int CategoryId;
        public string Image;
        public PublicImageType ImageType;

        public int ParentId;

        public uint RoomId;

        public int Type;

        public PublicItem(int id, int type, string caption, string image, PublicImageType imageType, uint roomId,
            int categoryId, int parentId)
        {
            Id = id;
            Type = type;
            Caption = caption;
            Image = image;
            ImageType = imageType;
            RoomId = roomId;
            CategoryId = categoryId;
            ParentId = parentId;
        }

        public int Id { get; }

        public bool IsCategory => CategoryId > 0;

        public RoomData RoomData
            => IsCategory ? new RoomData() : PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData(RoomId);

        public void Serialize(ServerPacket packet)
        {
            packet.WriteInteger(Id);

            packet.WriteString(IsCategory ? Caption : RoomData.Name);

            packet.WriteString(RoomData.Description);
            packet.WriteInteger(Type);
            packet.WriteString(Caption);
            packet.WriteString((ImageType == PublicImageType.External) ? Image : "");

            if (!IsCategory)
            {
                packet.WriteUInt(0);
                packet.WriteInteger(RoomData.UsersNow);
                packet.WriteInteger(3);
                packet.WriteString((ImageType == PublicImageType.Internal) ? Image : "");
                packet.WriteUInt(1337);
                packet.WriteInteger(0);
                packet.WriteString(RoomData.CCTs);
                packet.WriteInteger(RoomData.UsersMax);
                packet.WriteUInt(RoomId);
            }
            else
            {
                packet.WriteInteger(0);
                packet.WriteInteger(4);
                packet.WriteInteger(CategoryId);
            }
        }
    }
}