﻿using System.Data;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Navigators
{
    internal class VoucherHandler
    {
        public bool IsValidCode(string code)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                if (dbClient.ReadDataRow("SELECT null FROM credit_vouchers WHERE code = '" + code + "' LIMIT 1") != null)
                {
                    return true;
                }
            }

            return false;
        }

        public int GetVoucherValue(string code)
        {
            DataRow data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataRow("SELECT value FROM credit_vouchers WHERE code = '" + code + "' LIMIT 1");
            }

            if (data != null)
            {
                return (int) data[0];
            }

            return 0;
        }

        public void TryRedeemVoucher(Session session, string code)
        {
            if (!IsValidCode(code))
            {
                session.SendPacket(new ServerPacket(213));
            }

            var value = GetVoucherValue(code);

            if (value >= 0)
            {
                session.GetPlayer().Credits += value;
                session.GetPlayer().UpdateCreditsBalance(true);
            }

            session.SendPacket(new ServerPacket(212));
        }
    }
}