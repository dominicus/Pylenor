﻿namespace Pylenor.HabboHotel.Pathfinding
{
    public struct Coord
    {
        internal int X;
        internal int Y;

        internal Coord(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(Coord a, Coord b)
        {
            if (Equals(a, b))
            {
                return true;
            }

            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Coord a, Coord b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return X ^ Y;
        }

        public override bool Equals(object obj)
        {
            return base.GetHashCode().Equals(obj.GetHashCode());
        }
    }
}