﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.Pathfinding
{
    internal class Pathfinder
    {
        private readonly int _mapSizeX;
        private readonly int _mapSizeY;

        private readonly Room _room;
        private readonly CompleteSquare[,] _squares;
        private readonly RoomUser _user;
        private Point[] _movements;

        public Pathfinder(Room room, RoomUser user)
        {
            _room = room;
            var model = room.Model;
            _user = user;

            if (model == null || user == null)
            {
                return;
            }

            InitMovements(4);

            _mapSizeX = model.MapSizeX;
            _mapSizeY = model.MapSizeY;

            _squares = new CompleteSquare[_mapSizeX, _mapSizeY];

            for (var x = 0; x < _mapSizeX; x++)
            {
                for (var y = 0; y < _mapSizeY; y++)
                {
                    _squares[x, y] = new CompleteSquare(x, y);
                }
            }
        }

        private IEnumerable<Point> GetSquares()
        {
            for (var x = 0; x < _mapSizeX; x++)
            {
                for (var y = 0; y < _mapSizeY; y++)
                {
                    yield return new Point(x, y);
                }
            }
        }

        private IEnumerable<Point> ValidMoves(int x, int y)
        {
            return from movePoint in _movements
                let newX = x + movePoint.X
                let newY = y + movePoint.Y
                where ValidCoordinates(newX, newY) &&
                      IsSquareOpen(newX, newY)
                select new Point(newX, newY);
        }

        public List<Coord> FindPath()
        {
            // Locate the user, and set the distance to zero
            var userX = _user.X;
            var userY = _user.Y;

            _squares[_user.X, _user.Y].DistanceSteps = 0;

            // Find all possible moves
            while (true)
            {
                var madeProgress = false;

                foreach (var mainPoint in GetSquares())
                {
                    var x = mainPoint.X;
                    var y = mainPoint.Y;

                    if (!IsSquareOpen(x, y)) continue;
                    var passHere = _squares[x, y].DistanceSteps;

                    foreach (var movePoint in ValidMoves(x, y))
                    {
                        var newX = movePoint.X;
                        var newY = movePoint.Y;
                        var newPass = passHere + 1;

                        if (_squares[newX, newY].DistanceSteps <= newPass) continue;
                        _squares[newX, newY].DistanceSteps = newPass;
                        madeProgress = true;
                    }
                }

                if (!madeProgress)
                {
                    break;
                }
            }

            // Locate the goal
            var goalX = _user.GoalX;
            var goalY = _user.GoalY;

            if (goalX == -1 || goalY == -1)
            {
                return null;
            }

            // Now trace the shortest possible route to our goal
            var path = new List<Coord> {new Coord(_user.GoalX, _user.GoalY)};


            while (true)
            {
                var lowestPoint = Point.Empty;
                var lowest = 100;

                foreach (var movePoint in ValidMoves(goalX, goalY))
                {
                    var count = _squares[movePoint.X, movePoint.Y].DistanceSteps;

                    if (count >= lowest) continue;
                    lowest = count;

                    lowestPoint.X = movePoint.X;
                    lowestPoint.Y = movePoint.Y;
                }

                if (lowest != 100)
                {
                    _squares[lowestPoint.X, lowestPoint.Y].IsPath = true;
                    goalX = lowestPoint.X;
                    goalY = lowestPoint.Y;

                    path.Add(new Coord(lowestPoint.X, lowestPoint.Y));
                }
                else
                {
                    break;
                }

                if (goalX == userX && goalY == userY)
                {
                    break;
                }
            }

            return path;
        }

        private bool IsSquareOpen(int x, int y)
        {
            if (_room.ValidTile(x, y) && _user.AllowOverride)
            {
                return true;
            }

            if (_user.X == x && _user.Y == y)
            {
                return true;
            }

            var isLastStep = _user.GoalX == x && _user.GoalY == y;

            return _room.CanWalk(x, y, 0, isLastStep);
        }

        private bool ValidCoordinates(int x, int y)
        {
            return x >= 0 && y >= 0 && x <= _mapSizeX && y <= _mapSizeY;
        }

        public void InitMovements(int movementCount)
        {
            if (movementCount == 4)
            {
                _movements = new[]
                {
                    new Point(0, 1),
                    new Point(0, -1),
                    new Point(1, 0),
                    new Point(1, 1),
                    new Point(1, -1),
                    new Point(-1, 0),
                    new Point(-1, 1),
                    new Point(-1, -1)
                };
            }
            else
            {
                _movements = new[]
                {
                    new Point(-1, -1),
                    new Point(0, -1),
                    new Point(1, -1),
                    new Point(1, 0),
                    new Point(1, 1),
                    new Point(0, 1),
                    new Point(-1, 1),
                    new Point(-1, 0)
                };
            }
        }
    }

    internal class CompleteSquare
    {
        public int X;
        public int Y;

        public CompleteSquare(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int DistanceSteps { get; set; } = 100;

        public bool IsPath { get; set; }
    }
}