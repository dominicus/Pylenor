﻿namespace Pylenor.HabboHotel.Pathfinding
{
    internal class ShitPathfinder
    {
        public static Coord GetNextStep(int x, int y, int goalX, int goalY)
        {
            var next = new Coord(-1, -1);

            if (x > goalX && y > goalY)
            {
                next = new Coord(x - 1, y - 1);
            }
            else if (x < goalX && y < goalY)
            {
                next = new Coord(x + 1, y + 1);
            }
            else if (x > goalX && y < goalY)
            {
                next = new Coord(x - 1, y + 1);
            }
            else if (x < goalX && y > goalY)
            {
                next = new Coord(x + 1, y - 1);
            }
            else if (x > goalX)
            {
                next = new Coord(x - 1, y);
            }
            else if (x < goalX)
            {
                next = new Coord(x + 1, y);
            }
            else if (y < goalY)
            {
                next = new Coord(x, y + 1);
            }
            else if (y > goalY)
            {
                next = new Coord(x, y - 1);
            }

            return next;
        }
    }
}