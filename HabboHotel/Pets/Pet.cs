﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.Pets
{
    public class Pet
    {
        public string Color;

        public double CreationStamp;
        public int Energy;

        public int[] ExperienceLevels =
        {
            0, 100, 200, 400, 600, 900, 1300, 1800, 2400, 3200, 4300, 5700, 7600, 10100, 13300, 17500, 23000, 30200,
            39600,
            51900
        };

        public int Expirience;
        public string Name;
        public int Nutrition;
        public uint OwnerId;
        public uint PetId;
        public bool PlacedInRoom;
        public string Race;

        public int Respect;

        public uint RoomId;

        public uint Type;
        public int VirtualId;
        public int X;
        public int Y;
        public double Z;

        public Pet(uint petId, uint ownerId, uint roomId, string name, uint type, string race, string color,
            int expirience, int energy, int nutrition, int respect, double creationStamp, int x, int y, double z)
        {
            PetId = petId;
            OwnerId = ownerId;
            RoomId = roomId;
            Name = name;
            Type = type;
            Race = race;
            Color = color;
            Expirience = expirience;
            Energy = energy;
            Nutrition = nutrition;
            Respect = respect;
            CreationStamp = creationStamp;
            X = x;
            Y = y;
            Z = z;
            PlacedInRoom = false;
        }

        public Room Room => !IsInRoom ? null : PylenorEnvironment.GetGame().GetRoomManager().GetRoom(RoomId);

        public bool IsInRoom => (RoomId > 0);

        public int Level
        {
            get
            {
                for (var level = 0; level < ExperienceLevels.Length; ++level)
                {
                    if (Expirience < ExperienceLevels[level])
                        return level + 1;
                }
                return ExperienceLevels.Length + 1;
            }
        }

        public int MaxLevel => 20;

        public int ExpirienceGoal => ExperienceLevels[Level - 1];

        public int MaxEnergy => 100;

        public int MaxNutrition => 150;

        public int Age => (int) Math.Floor((PylenorEnvironment.GetUnixTimestamp() - CreationStamp)/86400);

        public string Look => Type + " " + Race + " " + Color;

        public string OwnerName => PylenorEnvironment.GetGame().GetClientManager().GetNameById(OwnerId);

        public void OnRespect()
        {
            Respect++;

            var message = new ServerPacket(440);
            message.WriteUInt(PetId);
            message.WriteInteger(Expirience + 10);
            Room.SendMessage(message);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("petid", PetId);
                dbClient.ExecuteQuery("UPDATE user_pets SET respect = respect + 1 WHERE id = @petid LIMIT 1");
            }

            if (Expirience <= 51900)
            {
                AddExpirience(10);
            }
        }

        public void AddExpirience(int amount)
        {
            Expirience = Expirience + amount;

            if (Expirience > 51900)
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("petid", PetId);
                dbClient.AddParamWithValue("expirience", Expirience);
                dbClient.ExecuteQuery("UPDATE user_pets SET expirience = @expirience WHERE id = @petid LIMIT 1");
            }

            if (Room != null)
            {
                var message = new ServerPacket(609);
                message.WriteUInt(PetId);
                message.WriteInteger(VirtualId);
                message.WriteInteger(amount);
                Room.SendMessage(message);

                if (Expirience > ExpirienceGoal)
                {
                    // Level the pet

                    var chatMessage = new ServerPacket(24);
                    chatMessage.WriteInteger(VirtualId);
                    chatMessage.WriteString("*leveled up to level " + Level + " *");
                    chatMessage.WriteInteger(0);

                    Room.SendMessage(chatMessage);
                }
            }
        }

        public void PetEnergy(bool add)
        {
            int maxE;

            if (add)
            {
                if (Energy == 100) // If Energy is 100, no point.
                    return;

                if (Energy > 85)
                {
                    maxE = MaxEnergy - Energy;
                }
                else
                {
                    maxE = 10;
                }
            }
            else
            {
                maxE = 15;
            } // Remove Max Energy as 15

            var r = PylenorEnvironment.GetRandomNumber(4, maxE);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                if (!add)
                {
                    Energy = Energy - r;

                    if (Energy < 0)
                    {
                        dbClient.AddParamWithValue("pid", PetId);
                        dbClient.ExecuteQuery("UPDATE user_pets SET energy = 1 WHERE id = @pid LIMIT 1");

                        Energy = 1;

                        r = 1;
                    }

                    dbClient.AddParamWithValue("r", r);
                    dbClient.AddParamWithValue("petid", PetId);
                    dbClient.ExecuteQuery("UPDATE user_pets SET energy = energy - @r WHERE id = @petid LIMIT 1");
                }
                else
                {
                    dbClient.AddParamWithValue("r", r);
                    dbClient.AddParamWithValue("petid", PetId);
                    dbClient.ExecuteQuery("UPDATE user_pets SET energy = energy + @r WHERE id = @petid LIMIT 1");

                    Energy = Energy + r;
                }
            }
        }

        public void SerializeInventory(ServerPacket packet)
        {
            packet.WriteUInt(PetId);
            packet.WriteString(Name);
            packet.WriteString(Look);
        }

        public ServerPacket SerializeInfo()
        {
            var nfo = new ServerPacket(601);
            nfo.WriteUInt(PetId);
            nfo.WriteString(Name);
            nfo.WriteInteger(Level);
            nfo.WriteInteger(MaxLevel);
            nfo.WriteInteger(Expirience);
            nfo.WriteInteger(ExpirienceGoal);
            nfo.WriteInteger(Energy);
            nfo.WriteInteger(MaxEnergy);
            nfo.WriteInteger(Nutrition);
            nfo.WriteInteger(MaxNutrition);
            nfo.WriteString(Look);
            nfo.WriteInteger(Respect);
            nfo.WriteUInt(OwnerId);
            nfo.WriteInteger(Age);
            nfo.WriteString(OwnerName);
            return nfo;
        }
    }
}