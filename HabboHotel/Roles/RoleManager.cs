﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.HabboHotel.Users;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Roles
{
    internal class RoleManager
    {
        /// <summary>
        ///     Creates an instance of RoleManager.
        /// </summary>
        public RoleManager()
        {
            Roles = new Dictionary<uint, string>();
            Rights = new Dictionary<string, uint>();
            SubRights = new Dictionary<string, string>();
        }

        /// <summary>
        ///     Dictionary containing all rights.
        /// </summary>
        public Dictionary<string, uint> Rights { get; }

        /// <summary>
        ///     Dictioanry containing all Roles.
        /// </summary>
        public Dictionary<uint, string> Roles { get; }

        /// <summary>
        ///     Dictionary containing all sub rights.
        /// </summary>
        public Dictionary<string, string> SubRights { get; }

        /// <summary>
        ///     Loading all roles.
        /// </summary>
        /// <param name="dbClient"></param>
        public void LoadRoles(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading ranks");
            ClearRoles();

            var data = dbClient.ReadDataTable("SELECT id,name,badgeid FROM ranks ORDER BY id ASC;");

            if (data != null)
            {
                foreach (DataRow row in data.Rows)
                {
                    Roles.Add((uint) row["id"], row["badgeid"].ToString());
                }
            }
            Console.WriteLine("...completed!");
        }

        public void LoadRights(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading fuserights");
            ClearRights();

            var data = dbClient.ReadDataTable("SELECT fuse,rank FROM fuserights;");
            var subData = dbClient.ReadDataTable("SELECT fuse,sub FROM fuserights_subs;");

            if (data != null)
            {
                foreach (DataRow row in data.Rows)
                {
                    Rights.Add(row["fuse"].ToString().ToLower(), (uint) row["rank"]);
                }
            }

            if (subData != null)
            {
                foreach (DataRow row in subData.Rows)
                {
                    SubRights.Add((string) row["fuse"], (string) row["sub"]);
                }
            }
            Console.WriteLine("...completed!");
        }

        /// <summary>
        ///     Checks if the <see cref="rankId" /> has <seealso cref="fuse" />.
        /// </summary>
        /// <param name="rankId"></param>
        /// <param name="fuse"></param>
        /// <returns>Boolean whether that rank has right or not.</returns>
        public bool RankHasRight(uint rankId, string fuse)
        {
            if (!ContainsRight(fuse))
            {
                return false;
            }

            var minRank = Rights[fuse];

            return rankId >= minRank;
        }

        public bool SubHasRight(string sub, string fuse)
        {
            return SubRights.ContainsKey(fuse) && SubRights[fuse] == sub;
        }

        public List<string> GetRightsForHabbo(Player player)
        {
            var userRights = new List<string>();

            userRights.AddRange(GetRightsForRank(player.Rank));

            foreach (var subscriptionId in player.GetSubscriptionManager().SubList)
            {
                userRights.AddRange(GetRightsForSub(subscriptionId));
            }

            return userRights;
        }

        public List<string> GetRightsForRank(uint rankId)
        {
            var userRights = new List<string>();

            foreach (var data in Rights.Where(data => rankId >= data.Value && !userRights.Contains(data.Key)))
            {
                userRights.Add(data.Key);
            }
            return userRights;
        }

        public List<string> GetRightsForSub(string subId)
        {
            return (from data in SubRights where data.Value == subId select data.Key).ToList();
        }

        public bool ContainsRight(string right)
        {
            return Rights.ContainsKey(right);
        }

        public void ClearRoles()
        {
            Roles.Clear();
        }

        public void ClearRights()
        {
            Rights.Clear();
        }
    }
}