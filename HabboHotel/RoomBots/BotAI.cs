﻿using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.RoomBots
{
    public abstract class BotAi
    {
        private uint _roomId;
        private int _roomUserId;
        public int BaseId;

        public void Init(int baseId, int roomUserId, uint roomId)
        {
            BaseId = baseId;
            _roomUserId = roomUserId;
            _roomId = roomId;
        }

        public Room GetRoom()
        {
            return PylenorEnvironment.GetGame().GetRoomManager().GetRoom(_roomId);
        }

        public RoomUser GetRoomUser()
        {
            return GetRoom().GetRoomUserByVirtualId(_roomUserId);
        }

        public RoomBot GetBotData()
        {
            return GetRoomUser().BotData;
        }

        public abstract void OnSelfEnterRoom();
        public abstract void OnSelfLeaveRoom(bool kicked);
        public abstract void OnUserEnterRoom(RoomUser user);
        public abstract void OnUserLeaveRoom(Session client);
        public abstract void OnUserSay(RoomUser user, string message);
        public abstract void OnUserShout(RoomUser user, string message);
        public abstract void OnTimerTick();
    }
}