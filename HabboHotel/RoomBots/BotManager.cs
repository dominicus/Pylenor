﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.RoomBots
{
    internal class BotManager
    {
        private List<RoomBot> _bots;

        public BotManager()
        {
            _bots = new List<RoomBot>();
        }

        public void LoadBots(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading bots");
            _bots = new List<RoomBot>();

            var data = dbClient.ReadDataTable("SELECT * FROM bots;");

            if (data == null) return;
            foreach (DataRow row in data.Rows)
            {
                _bots.Add(new RoomBot((uint) row["id"], (uint) row["room_id"], (string) row["ai_type"],
                    (string) row["walk_mode"],
                    (string) row["name"], (string) row["motto"], (string) row["look"], (int) row["x"],
                    (int) row["y"], (int) row["z"],
                    (int) row["rotation"], (int) row["min_x"], (int) row["min_y"], (int) row["max_x"],
                    (int) row["max_y"]));
            }
            Console.WriteLine("...completed!");
        }

        public bool RoomHasBots(uint roomId)
        {
            return GetBotsForRoom(roomId).Count >= 1;
        }

        public List<RoomBot> GetBotsForRoom(uint roomId)
        {
            return _bots.Where(bot => bot.RoomId == roomId).ToList();
        }

        public RoomBot GetBot(uint botId)
        {
            return _bots.FirstOrDefault(bot => bot.BotId == botId);
        }
    }
}