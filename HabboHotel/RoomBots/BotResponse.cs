﻿using System.Collections.Generic;
using System.Linq;

namespace Pylenor.HabboHotel.RoomBots
{
    public class BotResponse
    {
        private uint _id;
        public uint BotId;

        public List<string> Keywords;

        public string ResponseText;
        public string ResponseType;

        public int ServeId;

        public BotResponse(uint id, uint botId, string keywords, string responseText, string responseType, int serveId)
        {
            _id = id;
            BotId = botId;
            Keywords = new List<string>();
            ResponseText = responseText;
            ResponseType = responseType;
            ServeId = serveId;

            foreach (var keyword in keywords.Split(';'))
            {
                Keywords.Add(keyword.ToLower());
            }
        }

        public bool KeywordMatched(string message)
        {
            return Keywords.Any(keyword => message.ToLower().Contains(keyword.ToLower()));
        }
    }
}