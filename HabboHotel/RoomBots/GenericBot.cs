﻿using System;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.RoomBots
{
    internal class GenericBot : BotAi
    {
        private int _actionTimer;
        private int _speechTimer;

        public GenericBot(int virtualId)
        {
            _speechTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 250);
            _actionTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 30);
        }

        public override void OnSelfEnterRoom()
        {
        }

        public override void OnSelfLeaveRoom(bool kicked)
        {
        }

        public override void OnUserEnterRoom(RoomUser user)
        {
        }

        public override void OnUserLeaveRoom(Session client)
        {
        }

        public override void OnUserSay(RoomUser user, string message)
        {
            if (GetRoom().TileDistance(GetRoomUser().X, GetRoomUser().Y, user.X, user.Y) > 8)
            {
                return;
            }

            var response = GetBotData().GetResponse(message);

            if (response == null)
            {
                return;
            }

            switch (response.ResponseType.ToLower())
            {
                case "say":

                    GetRoomUser().Chat(null, response.ResponseText, false);
                    break;

                case "shout":

                    GetRoomUser().Chat(null, response.ResponseText, true);
                    break;

                case "whisper":

                    var tellMsg = new ServerPacket(25);
                    tellMsg.WriteInteger(GetRoomUser().VirtualId);
                    tellMsg.WriteString(response.ResponseText);
                    tellMsg.WriteBoolean(false);

                    user.GetClient().SendPacket(tellMsg);
                    break;
            }

            if (response.ServeId >= 1)
            {
                user.CarryItem(response.ServeId);
            }
        }

        public override void OnUserShout(RoomUser user, string message)
        {
            if (PylenorEnvironment.GetRandomNumber(0, 10) >= 5)
            {
                GetRoomUser().Chat(null, "There's no need to shout!", true); // shout nag
            }
        }

        public override void OnTimerTick()
        {
            if (_speechTimer <= 0)
            {
                if (GetBotData().RandomSpeech.Count > 0)
                {
                    var speech = GetBotData().GetRandomSpeech();
                    GetRoomUser().Chat(null, speech.Message, speech.Shout);
                }

                _speechTimer = PylenorEnvironment.GetRandomNumber(10, 300);
            }
            else
            {
                _speechTimer--;
            }

            if (_actionTimer <= 0)
            {
                int randomX;
                int randomY;

                switch (GetBotData().WalkingMode.ToLower())
                {
                    default:
                    case "stand":

                        // (8) Why is my life so boring?
                        break;

                    case "freeroam":

                        randomX = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeX);
                        randomY = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeY);

                        GetRoomUser().MoveTo(randomX, randomY);

                        break;

                    case "specified_range":

                        randomX = PylenorEnvironment.GetRandomNumber(GetBotData().MinX, GetBotData().MaxX);
                        randomY = PylenorEnvironment.GetRandomNumber(GetBotData().MinY, GetBotData().MaxY);

                        GetRoomUser().MoveTo(randomX, randomY);

                        break;
                }

                _actionTimer = PylenorEnvironment.GetRandomNumber(1, 30);
            }
            else
            {
                _actionTimer--;
            }
        }
    }
}