﻿using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.RoomBots
{
    internal class GuideBot : BotAi
    {
        private int _actionTimer;
        private int _speechTimer;

        public GuideBot()
        {
            _speechTimer = 0;
            _actionTimer = 0;
        }

        public override void OnSelfEnterRoom()
        {
            GetRoomUser().Chat(null, "Hi and Welcome! I am a bot Guide and I'm here to help you.", false);
            GetRoomUser()
                .Chat(null,
                    "This is your own room, you can always come back to room by clicking the nest icon on the left.",
                    false);
            GetRoomUser()
                .Chat(null,
                    "If you want to explore the Player by yourself, click on the orange hotel icon on the left (we call it navigator).",
                    false);
            GetRoomUser()
                .Chat(null,
                    "You will find cool rooms and fun events with other people in them, feel free to visit them.", false);
            GetRoomUser()
                .Chat(null, "I can give you tips and hints on what to do here, just ask me a question :)", false);
        }

        public override void OnSelfLeaveRoom(bool kicked)
        {
        }

        public override void OnUserEnterRoom(RoomUser user)
        {
        }

        public override void OnUserLeaveRoom(Session client)
        {
            if (GetRoom().Owner.ToLower() == client.GetPlayer().Username.ToLower())
            {
                GetRoom().RemoveBot(GetRoomUser().VirtualId, false);
            }
        }

        public override void OnUserSay(RoomUser user, string message)
        {
            if (GetRoom().TileDistance(GetRoomUser().X, GetRoomUser().Y, user.X, user.Y) > 8)
            {
                return;
            }

            var response = GetBotData().GetResponse(message);

            if (response == null)
            {
                return;
            }

            switch (response.ResponseType.ToLower())
            {
                case "say":

                    GetRoomUser().Chat(null, response.ResponseText, false);
                    break;

                case "shout":

                    GetRoomUser().Chat(null, response.ResponseText, true);
                    break;

                case "whisper":

                    var tellMsg = new ServerPacket(25);
                    tellMsg.WriteInteger(GetRoomUser().VirtualId);
                    tellMsg.WriteString(response.ResponseText);
                    tellMsg.WriteBoolean(false);

                    user.GetClient().SendPacket(tellMsg);
                    break;
            }

            if (response.ServeId >= 1)
            {
                user.CarryItem(response.ServeId);
            }
        }

        public override void OnUserShout(RoomUser user, string message)
        {
        }

        public override void OnTimerTick()
        {
            if (_speechTimer <= 0)
            {
                if (GetBotData().RandomSpeech.Count > 0)
                {
                    var speech = GetBotData().GetRandomSpeech();
                    GetRoomUser().Chat(null, speech.Message, speech.Shout);
                }

                _speechTimer = PylenorEnvironment.GetRandomNumber(0, 150);
            }
            else
            {
                _speechTimer--;
            }

            if (_actionTimer <= 0)
            {
                var randomX = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeX);
                var randomY = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeY);

                GetRoomUser().MoveTo(randomX, randomY);

                _actionTimer = PylenorEnvironment.GetRandomNumber(0, 30);
            }
            else
            {
                _actionTimer--;
            }
        }
    }
}