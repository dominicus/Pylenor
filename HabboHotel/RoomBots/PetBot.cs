﻿using System;
using System.Globalization;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Pathfinding;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.RoomBots
{
    internal class PetBot : BotAi
    {
        private int _actionTimer;
        private int _energyTimer;
        private int _speechTimer;

        // TO-DO: This needs cleaning up BADLY, If anyone wants to attempt cleaning up my mess, go for it (Y) (Half done - Shorty)

        public PetBot(int virtualId)
        {
            _speechTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 60);
            _actionTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 30 + virtualId);
            _energyTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 60);
        }

        private void RemovePetStatus()
        {
            var pet = GetRoomUser();

            // Remove Status
            pet.Statusses.Remove("sit");
            pet.Statusses.Remove("lay");
            pet.Statusses.Remove("snf");
            pet.Statusses.Remove("eat");
            pet.Statusses.Remove("ded");
            pet.Statusses.Remove("jmp");
        }

        public override void OnSelfEnterRoom()
        {
            var randomX = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeX);
            var randomY = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeY);
            GetRoomUser().MoveTo(randomX, randomY);
        }

        public override void OnSelfLeaveRoom(bool kicked)
        {
        }

        public override void OnUserEnterRoom(RoomUser user)
        {
            if (string.Equals(user.GetClient().GetPlayer().Username, GetRoomUser().PetData.OwnerName,
                StringComparison.CurrentCultureIgnoreCase))
            {
                GetRoomUser().Chat(null, "*drools over master*", false);
            }
        }

        public override void OnUserLeaveRoom(Session client)
        {
        }

        #region Commands

        public override void OnUserSay(RoomUser user, string message)
        {
            var pet = GetRoomUser();

            if (message.ToLower().Equals(pet.PetData.Name.ToLower()))
            {
                pet.SetRot(Rotation.Calculate(pet.X, pet.Y, user.X, user.Y));
                return;
            }

            if (message.ToLower().StartsWith(pet.PetData.Name.ToLower() + " ") &&
                user.GetClient().GetPlayer().Username.ToLower() == GetRoomUser().PetData.OwnerName.ToLower())
            {
                var command = message.Substring(pet.PetData.Name.ToLower().Length + 1);

                var r = PylenorEnvironment.GetRandomNumber(1, 8); // Made Random

                if (pet.PetData.Energy > 10 && r < 6 || pet.PetData.Level > 15)
                {
                    RemovePetStatus(); // Remove Status

                    switch (command)
                    {
                        // TODO - Level you can use the commands at...

                            #region free

                        case "free":
                            RemovePetStatus();

                            var randomX = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeX);
                            var randomY = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeY);

                            pet.MoveTo(randomX, randomY);

                            pet.PetData.AddExpirience(10); // Give XP

                            break;

                            #endregion

                            #region here

                        case "come":
                        case "here":

                            RemovePetStatus();

                            var newX = user.X;
                            var newY = user.Y;

                            _actionTimer = 30; // Reset ActionTimer

                            #region Rotation

                            if (user.RotBody == 4)
                            {
                                newY = user.Y + 1;
                            }
                            else if (user.RotBody == 0)
                            {
                                newY = user.Y - 1;
                            }
                            else if (user.RotBody == 6)
                            {
                                newX = user.X - 1;
                            }
                            else if (user.RotBody == 2)
                            {
                                newX = user.X + 1;
                            }
                            else if (user.RotBody == 3)
                            {
                                newX = user.X + 1;
                                newY = user.Y + 1;
                            }
                            else if (user.RotBody == 1)
                            {
                                newX = user.X + 1;
                                newY = user.Y - 1;
                            }
                            else if (user.RotBody == 7)
                            {
                                newX = user.X - 1;
                                newY = user.Y - 1;
                            }
                            else if (user.RotBody == 5)
                            {
                                newX = user.X - 1;
                                newY = user.Y + 1;
                            }

                            #endregion

                            pet.PetData.AddExpirience(10); // Give XP

                            pet.MoveTo(newX, newY);
                            break;

                            #endregion

                            #region sit

                        case "sit":
                            // Remove Status
                            RemovePetStatus();

                            pet.PetData.AddExpirience(10); // Give XP

                            // Add Status
                            pet.Statusses.Add("sit", pet.Z.ToString(CultureInfo.InvariantCulture));
                            _actionTimer = 25;
                            _energyTimer = 10;
                            break;

                            #endregion

                            #region lay

                        case "lay":
                        case "down":
                            // Remove Status
                            RemovePetStatus();

                            // Add Status
                            pet.Statusses.Add("lay", pet.Z.ToString(CultureInfo.InvariantCulture));

                            pet.PetData.AddExpirience(10); // Give XP

                            _actionTimer = 30;
                            _energyTimer = 5;
                            break;

                            #endregion

                            #region dead

                        case "play dead":
                        case "dead":
                            // Remove Status
                            RemovePetStatus();

                            // Add Status 
                            pet.Statusses.Add("ded", pet.Z.ToString(CultureInfo.InvariantCulture));

                            pet.PetData.AddExpirience(10); // Give XP

                            // Don't move to speak for a set amount of time.
                            _speechTimer = 45;
                            _actionTimer = 30;

                            break;

                            #endregion

                            #region sleep

                        case "sleep":
                            // Remove Status
                            RemovePetStatus();

                            pet.Chat(null, "ZzzZZZzzzzZzz", false);
                            pet.Statusses.Add("lay", pet.Z.ToString(CultureInfo.InvariantCulture));

                            pet.PetData.AddExpirience(10); // Give XP

                            // Don't move to speak for a set amount of time.
                            _energyTimer = 5;
                            _speechTimer = 30;
                            _actionTimer = 45;
                            break;

                            #endregion

                            #region jump

                        case "jump":
                            // Remove Status
                            RemovePetStatus();

                            // Add Status 
                            pet.Statusses.Add("jmp", pet.Z.ToString(CultureInfo.InvariantCulture));

                            pet.PetData.AddExpirience(10); // Give XP

                            // Don't move to speak for a set amount of time.
                            _energyTimer = 5;
                            _speechTimer = 10;
                            _actionTimer = 5;
                            break;

                            #endregion

                        default:
                            string[] speech = {"*confused*", "What?", "Huh?", "What is that?", "hmm..?"};

                            var randomSpeech = new Random();
                            pet.Chat(null, speech[randomSpeech.Next(0, speech.Length - 1)], false);
                            break;
                    }
                    pet.PetData.PetEnergy(false); // Remove Energy
                    pet.PetData.PetEnergy(false); // Remove Energy
                }
                else
                {
                    RemovePetStatus(); // Remove Status

                    if (pet.PetData.Energy < 10)
                    {
                        string[] speech =
                        {
                            "ZzZzzzzz", "*sleeps*", "Tired... *sleeps*", "ZzZzZZzzzZZz", "zzZzzZzzz",
                            "... Yawnn ..", "ZzZzzZ"
                        };

                        var randomSpeech = new Random();
                        pet.Chat(null, speech[randomSpeech.Next(0, speech.Length - 1)], false);

                        pet.Statusses.Add("lay", pet.Z.ToString(CultureInfo.InvariantCulture));

                        _speechTimer = 50;
                        _actionTimer = 45;
                        _energyTimer = 5;
                    }
                    else
                    {
                        string[] speech =
                        {
                            "*sigh*", "*refuses*", " ... ", "Who do you think you are?", "you do it",
                            "Grr!", "*laughs*", "Why?"
                        };

                        var randomSpeech = new Random();
                        pet.Chat(null, speech[randomSpeech.Next(0, speech.Length - 1)], false);

                        pet.PetData.PetEnergy(false); // Remove Energy
                    }
                }
            }
        }

        #endregion

        public override void OnUserShout(RoomUser user, string message)
        {
        }

        public override void OnTimerTick()
        {
            #region Speech

            if (_speechTimer <= 0)
            {
                var pet = GetRoomUser();

                if (pet != null)
                {
                    var randomSpeech = new Random();
                    RemovePetStatus();

                    switch (pet.PetData.Type)
                    {
                        case 0:
                        case 3:
                        {
                            string[] speech =
                            {
                                "woof woof woof!!!", "hooooowl", "wooooof!", "Woof Woof!", "sit", "lay",
                                "snf", "Woof"
                            };

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                        case 1:
                        {
                            // Cat

                            string[] speech = {"meow", "meow...meOW", "muew..muew", "lay", "sit", "lay"};

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                        case 2:
                        {
                            // Crocodile

                            string[] speech =
                            {
                                "Rrrr....Grrrrrg....", "*Mellow*", "Tick tock tick....",
                                "*feels like eating my owner*", "Nom Nom Nom", ".. Yawwnn!", "snf"
                            };

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                        case 4:
                        {
                            // Bear

                            string[] speech =
                            {
                                "*pack of fresh salmon please*", "Rawrrr!", "*sniff sniff*", "Yawnnnn..",
                                "Rawr! ... Rawrrrrr?", "snf"
                            };

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                        case 5:
                        {
                            // Pig

                            string[] speech =
                            {
                                "Oink Oink..", "*Mellow*", "Sniff... Sniff..", "snf", "Oink!", "snf", "lay",
                                "oink"
                            };

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                        case 6:
                        {
                            // Lion

                            string[] speech =
                            {
                                "Rawr..", "Sniff!", "Meow..err..roar", "ugh", "Roar!", "snf", "lay",
                                "roar"
                            };

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                        case 7:
                        {
                            // Rhino

                            string[] speech =
                            {
                                "Stomp.. stomp..", "*Sniff*", "Shhhh", "yay", "Stomp!", "snf", "lay", "stomp"
                            };

                            var rSpeech = speech[randomSpeech.Next(0, speech.Length - 1)];

                            if (rSpeech.Length != 3)
                            {
                                pet.Chat(null, rSpeech, false);
                            }
                            else
                            {
                                if (!pet.Statusses.ContainsKey(rSpeech))
                                    pet.Statusses.Add(rSpeech, pet.Z.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                            break;
                    }
                }
                _speechTimer = PylenorEnvironment.GetRandomNumber(20, 120);
            }
            else
            {
                _speechTimer--;
            }

            #endregion

            if (_actionTimer <= 0)
            {
                // Remove Status
                RemovePetStatus();

                var randomX = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeX);
                var randomY = PylenorEnvironment.GetRandomNumber(0, GetRoom().Model.MapSizeY);
                GetRoomUser().MoveTo(randomX, randomY);

                _actionTimer = PylenorEnvironment.GetRandomNumber(15, 40 + GetRoomUser().PetData.VirtualId);
            }
            else
            {
                _actionTimer--;
            }

            if (_energyTimer <= 0)
            {
                RemovePetStatus(); // Remove Status

                var pet = GetRoomUser();

                pet.PetData.PetEnergy(true); // Add Energy

                _energyTimer = PylenorEnvironment.GetRandomNumber(30, 120); // 2 Min Max
            }
            else
            {
                _energyTimer--;
            }
        }
    }
}