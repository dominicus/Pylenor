﻿namespace Pylenor.HabboHotel.RoomBots
{
    public class RandomSpeech
    {
        public string Message;
        public bool Shout;

        public RandomSpeech(string message, bool shout)
        {
            Message = message;
            Shout = shout;
        }
    }
}