﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Pylenor.HabboHotel.RoomBots
{
    public class RoomBot
    {
        public string AiType;
        public uint BotId;
        public string Look;
        public int MaxX;
        public int MaxY;

        public int MinX;
        public int MinY;
        public string Motto;

        public string Name;

        public List<RandomSpeech> RandomSpeech;
        public List<BotResponse> Responses;
        public uint RoomId;
        public int Rot;
        public string WalkingMode;

        public int X;
        public int Y;
        public int Z;

        public RoomBot(uint botId, uint roomId, string aiType, string walkingMode, string name, string motto,
            string look,
            int x, int y, int z, int rot, int minX, int minY, int maxX, int maxY)
        {
            BotId = botId;
            RoomId = roomId;
            AiType = aiType;
            WalkingMode = walkingMode;
            Name = name;
            Motto = motto;
            Look = look;
            X = x;
            Y = y;
            Z = z;
            Rot = rot;
            MinX = minX;
            MinY = minY;
            MaxX = maxX;
            MaxY = maxY;

            LoadRandomSpeech();
            LoadResponses();
        }

        public bool IsPet => (AiType.ToLower() == "pet");

        public void LoadRandomSpeech()
        {
            RandomSpeech = new List<RandomSpeech>();

            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT * FROM bots_speech WHERE bot_id = '" + BotId + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                RandomSpeech.Add(new RandomSpeech((string) row["text"],
                    PylenorEnvironment.EnumToBool(row["shout"].ToString())));
            }
        }

        public void LoadResponses()
        {
            Responses = new List<BotResponse>();

            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT * FROM bots_responses WHERE bot_id = '" + BotId + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                Responses.Add(new BotResponse((uint) row["id"], (uint) row["bot_id"], (string) row["keywords"],
                    (string) row["response_text"], row["mode"].ToString(), (int) row["serve_id"]));
            }
        }

        public BotResponse GetResponse(string message)
        {
            return Responses.FirstOrDefault(response => response.KeywordMatched(message));
        }

        public RandomSpeech GetRandomSpeech()
        {
            return RandomSpeech[PylenorEnvironment.GetRandomNumber(0, (RandomSpeech.Count - 1))];
        }

        public BotAi GenerateBotAi(int virtualId)
        {
            switch (AiType.ToLower())
            {
                default:
                case "generic":

                    return new GenericBot(virtualId);

                case "guide":

                    return new GuideBot();

                case "pet":

                    return new PetBot(virtualId);
            }
        }
    }
}