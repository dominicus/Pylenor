﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pylenor.HabboHotel.Rooms
{
    public class AffectedTile
    {
        public AffectedTile(int x, int y, int i)
        {
            X = x;
            Y = y;
            I = i;
        }

        public int X { get; }

        public int Y { get; }

        public int I { get; }
    }
}
