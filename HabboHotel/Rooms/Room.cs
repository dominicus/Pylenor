﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Core;
using Pylenor.HabboHotel.Items;
using Pylenor.HabboHotel.Pathfinding;
using Pylenor.HabboHotel.Pets;
using Pylenor.HabboHotel.RoomBots;

namespace Pylenor.HabboHotel.Rooms
{
    public enum MatrixState
    {
        Blocked = 0,
        Walkable = 1,
        WalkableLaststep = 2
    }

    public class Room
    {
        private readonly Dictionary<uint, double> _bans;

        private int _idleTime;
        public List<Trade> ActiveTrades;
        public bool AllowPets;
        public bool AllowPetsEating;
        public bool AllowWalkthrough;
        public Dictionary<uint, Thread> BallThread;
        public Coord[,] BedMatrix;
        public int Category;
        public string CcTs;
        public string Description;

        public RoomEvent Event;
        public string Floor;
        public Dictionary<uint, Thread> HasThread;

        public List<uint> HasWaterEffect;
        public double[,] HeightMatrix;
        public bool Hidewall;

        public List<RoomItem> Items;

        public bool KeepAlive;
        public string Landscape;

        public MatrixState[,] Matrix;
        public string ModelName;
        public MoodlightData MoodlightData;

        public RoomIcon MyIcon;

        public string Name;
        public string Owner;
        public string Password;
        public int Score;
        public int State;
        public List<string> Tags;
        public double[,] TopStackHeight;
        public string Type;
        public int UserCounter;

        public List<RoomUser> UserList;
        public bool[,] UserMatrix;
        public int UsersMax;
        public int UsersNow;

        public List<uint> UsersWithRights;

        public string Wallpaper;

        public Room(uint id, string name, string description, string type, string owner, int category,
            int state, int usersMax, string modelName, string ccTs, int score, List<string> tags, bool allowPets,
            bool allowPetsEating, bool allowWalkthrough, bool hidewall, RoomIcon icon, string password, string wallpaper,
            string floor,
            string landscape)
        {
            RoomId = id;
            Name = name;
            Description = description;
            Owner = owner;
            Category = category;
            Type = type;
            State = state;
            UsersNow = 0;
            UsersMax = usersMax;
            ModelName = modelName;
            CcTs = ccTs;
            Score = score;
            Tags = tags;
            AllowPets = allowPets;
            AllowPetsEating = allowPetsEating;
            AllowWalkthrough = allowWalkthrough;
            Hidewall = hidewall;
            UserCounter = 0;
            UserList = new List<RoomUser>();
            MyIcon = icon;
            Password = password;
            _bans = new Dictionary<uint, double>();
            Event = null;
            Wallpaper = wallpaper;
            Floor = floor;
            Landscape = landscape;
            Items = new List<RoomItem>();
            ActiveTrades = new List<Trade>();
            UserMatrix = new bool[Model.MapSizeX, Model.MapSizeY];

            _idleTime = 0;

            KeepAlive = true;

            HasWaterEffect = new List<uint>();
            HasThread = new Dictionary<uint, Thread>();
            BallThread = new Dictionary<uint, Thread>();

            LoadRights();
            LoadFurniture();

            GenerateMaps();
        }

        public bool HasOngoingEvent => Event != null;

        public RoomIcon Icon
        {
            get { return MyIcon; }

            set { MyIcon = value; }
        }

        public int UserCount => UserList.Count(user => !user.IsBot);

        public int TagCount => Tags.Count;

        public RoomModel Model => PylenorEnvironment.GetGame().GetRoomManager().GetModel(ModelName);

        public uint RoomId { get; }

        public List<RoomItem> FloorItems => Items.Where(item => item.IsFloorItem).ToList();

        public List<RoomItem> WallItems => Items.Where(item => item.IsWallItem).ToList();

        public bool CanTradeInRoom => !IsPublic;

        public bool IsPublic => Type == "public";

        public int PetCount
        {
            get { return UserList.Count(user => user != null && user.IsPet); }
        }

        public void InitBots()
        {
            var bots = PylenorEnvironment.GetGame().GetBotManager().GetBotsForRoom(RoomId);

            foreach (var bot in bots)
            {
                DeployBot(bot);
            }
        }

        public void InitPets()
        {
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("roomid", RoomId);
                data = dbClient.ReadDataTable("SELECT * FROM user_pets WHERE room_id = @roomid;");
            }

            if (data == null)
            {
                return;
            }

            foreach (
                var pet in
                    from DataRow row in data.Rows
                    select PylenorEnvironment.GetGame().GetCatalog().GeneratePetFromRow(row))
            {
                DeployBot(
                    new RoomBot(pet.PetId, RoomId, "pet", "freeroam", pet.Name, "", pet.Look, pet.X, pet.Y, (int) pet.Z,
                        0, 0, 0, 0, 0), pet);
            }
        }

        public RoomUser DeployBot(RoomBot bot)
        {
            return DeployBot(bot, null);
        }

        public RoomUser DeployBot(RoomBot bot, Pet petData)
        {
            var botUser = new RoomUser(0, RoomId, UserCounter++);

            if ((bot.X > 0 && bot.Y > 0) && bot.X < Model.MapSizeX && bot.Y < Model.MapSizeY)
            {
                botUser.SetPos(bot.X, bot.Y, bot.Z);
                botUser.SetRot(bot.Rot);
            }
            else
            {
                bot.X = Model.DoorX;
                bot.Y = Model.DoorY;

                botUser.SetPos(Model.DoorX, Model.DoorY, Model.DoorZ);
                botUser.SetRot(Model.DoorOrientation);
            }

            UserMatrix[bot.X, bot.Y] = true;

            botUser.BotData = bot;
            botUser.BotAi = bot.GenerateBotAi(botUser.VirtualId);

            if (botUser.IsPet)
            {
                botUser.BotAi.Init((int) bot.BotId, botUser.VirtualId, RoomId);
                botUser.PetData = petData;
                botUser.PetData.VirtualId = botUser.VirtualId;
            }
            else
            {
                botUser.BotAi.Init(-1, botUser.VirtualId, RoomId);
            }

            UserList.Add(botUser);
            UpdateUserStatus(botUser);
            botUser.UpdateNeeded = true;

            var enterMessage = new ServerPacket(28);
            enterMessage.WriteInteger(1);
            botUser.Serialize(enterMessage);
            SendMessage(enterMessage);

            botUser.BotAi.OnSelfEnterRoom();

            return botUser;
        }

        public void RemoveBot(int virtualId, bool kicked)
        {
            var user = GetRoomUserByVirtualId(virtualId);

            if (user == null || !user.IsBot)
            {
                return;
            }

            user.BotAi.OnSelfLeaveRoom(kicked);

            var leaveMessage = new ServerPacket(29);
            leaveMessage.AppendRawInt32(user.VirtualId);
            SendMessage(leaveMessage);

            UserMatrix[user.X, user.Y] = false;

            UserList.Remove(user);
        }

        internal RoomUser GetUserByCoordinate(int x, int y)
        {
            return UserList?.FirstOrDefault(@class => @class != null && (@class.X == x && @class.Y == y));
        }

        internal bool CanFindUserByCoordinate(int x, int y)
        {
            return GetUserByCoordinate(x, y) != null;
        }

        public void OnUserSay(RoomUser user, string message, bool shout)
        {
            foreach (var usr in UserList.Where(usr => usr.IsBot))
            {
                if (shout)
                {
                    usr.BotAi.OnUserShout(user, message);
                }
                else
                {
                    usr.BotAi.OnUserSay(user, message);
                }
            }
        }

        public void RegenerateUserMatrix()
        {
            UserMatrix = new bool[Model.MapSizeX, Model.MapSizeY];
            foreach (var user in UserList.Where(user => user != null))
            {
                UserMatrix[user.X, user.Y] = true;
            }
        }

        public void GenerateMaps()
        {
            // Create matrix arrays
            Matrix = new MatrixState[Model.MapSizeX, Model.MapSizeY];
            BedMatrix = new Coord[Model.MapSizeX, Model.MapSizeY];
            HeightMatrix = new double[Model.MapSizeX, Model.MapSizeY];
            TopStackHeight = new double[Model.MapSizeX, Model.MapSizeY];

            // Fill in the basic data based purely on the heightmap
            for (var line = 0; line < Model.MapSizeY; line++)
            {
                for (var chr = 0; chr < Model.MapSizeX; chr++)
                {
                    Matrix[chr, line] = MatrixState.Blocked;
                    BedMatrix[chr, line] = new Coord(chr, line);
                    HeightMatrix[chr, line] = 0;
                    TopStackHeight[chr, line] = 0.0;

                    if (chr == Model.DoorX && line == Model.DoorY)
                    {
                        Matrix[chr, line] = MatrixState.WalkableLaststep;
                    }
                    else if (Model.SqState[chr, line] == SquareState.Open)
                    {
                        Matrix[chr, line] = MatrixState.Walkable;
                    }
                    else if (Model.SqState[chr, line] == SquareState.Seat)
                    {
                        Matrix[chr, line] = MatrixState.WalkableLaststep;
                    }
                }
            }

            // Loop through the items in the room
            foreach (
                var item in
                    Items.Where(item => item.GetBaseItem().Type.ToLower() == "s")
                        .Where(item => !(item.GetBaseItem().Height <= 0)))
            {
                // Make sure we're the highest item here!
                if (TopStackHeight[item.X, item.Y] <= item.Z)
                {
                    TopStackHeight[item.X, item.Y] = item.Z;

                    // If this item is walkable and on the floor, allow users to walk here.
                    if (item.GetBaseItem().Walkable)
                    {
                        Matrix[item.X, item.Y] = MatrixState.Walkable;
                        HeightMatrix[item.X, item.Y] = item.GetBaseItem().Height;
                    }
                    // If this item is a gate, open, and on the floor, allow users to walk here.
                    else if (item.Z <= (Model.SqFloorHeight[item.X, item.Y] + 0.1) &&
                             item.GetBaseItem().InteractionType.ToLower() == "gate" && item.ExtraData == "1")
                    {
                        Matrix[item.X, item.Y] = MatrixState.Walkable;
                    }
                    // If this item is a set or a bed, make it's square walkable (but only if last step)
                    else if (item.GetBaseItem().IsSeat || item.GetBaseItem().InteractionType.ToLower() == "bed")
                    {
                        Matrix[item.X, item.Y] = MatrixState.WalkableLaststep;
                    }
                    // Finally, if it's none of those, block the square.
                    else
                    {
                        Matrix[item.X, item.Y] = MatrixState.Blocked;
                    }
                }

                var points = GetAffectedTiles(item.GetBaseItem().Length, item.GetBaseItem().Width, item.X, item.Y,
                    item.Rot) ?? new Dictionary<int, AffectedTile>();

                foreach (var tile in points.Values)
                {
                    // Make sure we're the highest item here!
                    if (TopStackHeight[tile.X, tile.Y] <= item.Z)
                    {
                        TopStackHeight[tile.X, tile.Y] = item.Z;

                        // If this item is walkable and on the floor, allow users to walk here.
                        if (item.GetBaseItem().Walkable)
                        {
                            Matrix[tile.X, tile.Y] = MatrixState.Walkable;
                            HeightMatrix[tile.X, tile.Y] = item.GetBaseItem().Height;
                        }
                        // If this item is a gate, open, and on the floor, allow users to walk here.
                        else if (item.Z <= (Model.SqFloorHeight[item.X, item.Y] + 0.1) &&
                                 item.GetBaseItem().InteractionType.ToLower() == "gate" && item.ExtraData == "1")
                        {
                            Matrix[tile.X, tile.Y] = MatrixState.Walkable;
                        }
                        // If this item is a set or a bed, make it's square walkable (but only if last step)
                        else if (item.GetBaseItem().IsSeat ||
                                 item.GetBaseItem().InteractionType.ToLower() == "bed")
                        {
                            Matrix[tile.X, tile.Y] = MatrixState.WalkableLaststep;
                        }
                        // Finally, if it's none of those, block the square.
                        else
                        {
                            Matrix[tile.X, tile.Y] = MatrixState.Blocked;
                        }
                    }

                    // Set bad maps
                    if (item.GetBaseItem().InteractionType.ToLower() != "bed") continue;
                    if (item.Rot == 0 || item.Rot == 4)
                    {
                        BedMatrix[tile.X, tile.Y].Y = item.Y;
                    }

                    if (item.Rot == 2 || item.Rot == 6)
                    {
                        BedMatrix[tile.X, tile.Y].X = item.X;
                    }
                }
            }
        }

        public void LoadRights()
        {
            UsersWithRights = new List<uint>();

            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("roomid", RoomId);
                data = dbClient.ReadDataTable("SELECT user_id FROM room_rights WHERE room_id = @roomid;");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                UsersWithRights.Add((uint) row["user_id"]);
            }
        }

        public void LoadFurniture()
        {
            Items.Clear();

            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("roomid", RoomId);
                data = dbClient.ReadDataTable("SELECT * FROM room_items WHERE room_id = @roomid;");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                var item = new RoomItem((uint) row["id"], RoomId, (uint) row["base_item"], (string) row["extra_data"],
                    (int) row["x"], (int) row["y"], (double) row["z"], (int) row["rot"], (string) row["wall_pos"]);

                switch (item.GetBaseItem().InteractionType.ToLower())
                {
                    case "dimmer":

                        if (MoodlightData == null)
                        {
                            MoodlightData = new MoodlightData(item.Id);
                        }

                        break;
                }

                Items.Add(item);
            }
        }

        public bool CheckRights(Session session)
        {
            return CheckRights(session, false);
        }

        public bool CheckRights(Session session, bool requireOwnership)
        {
            if (session.GetPlayer().Username.ToLower() == Owner.ToLower())
            {
                return true;
            }

            if (session.GetPlayer().HasFuse("fuse_admin") || session.GetPlayer().HasFuse("fuse_any_room_controller"))
            {
                return true;
            }

            if (!requireOwnership)
            {
                if (session.GetPlayer().HasFuse("fuse_any_room_rights"))
                {
                    return true;
                }

                if (UsersWithRights.Contains(session.GetPlayer().Id))
                {
                    return true;
                }
            }

            return false;
        }

        public RoomItem GetItem(uint id)
        {
            return Items.FirstOrDefault(item => item.Id == id);
        }

        public void RemoveFurniture(Session session, uint id)
        {
            var item = GetItem(id);

            if (item == null)
            {
                return;
            }

            item.Interactor.OnRemove(session, item);

            if (item.IsWallItem)
            {
                var message = new ServerPacket(84);
                message.AppendRawUInt(item.Id);
                message.WriteString("");
                message.WriteBoolean(false);
                SendMessage(message);
            }
            else if (item.IsFloorItem)
            {
                var message = new ServerPacket(94);
                message.AppendRawUInt(item.Id);
                message.WriteString("");
                message.WriteBoolean(false);
                SendMessage(message);
            }

            Items.Remove(item);
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM room_items WHERE id = '" + id + "' AND room_id = '" + RoomId +
                                      "' LIMIT 1");
            }

            GenerateMaps();
            UpdateUserStatusses();
        }

        public bool CanWalk(int x, int y, double z, bool lastStep)
        {
            if (x < 0 || x >= Model.MapSizeX || y >= Model.MapSizeY || y < 0)
            {
                return false;
            }

            if (SquareHasUsers(x, y, lastStep))
            {
                return false;
            }

            if (Matrix[x, y] == MatrixState.Blocked)
            {
                return false;
            }
            return Matrix[x, y] != MatrixState.WalkableLaststep || lastStep;
        }

        public void ProcessRoom()
        {
            var i = 0;

            // Loop through all furni and process them if they want to be processed
            foreach (var item in Items.Where(item => item == null || item.UpdateNeeded))
            {
                item?.ProcessUpdates();
            }

            // Loop through all users and bots and process them
            var remove = new List<uint>();
            foreach (var user in UserList.Where(user => user != null))
            {
                user.IdleTime++;

                if (!user.IsAsleep && user.IdleTime >= 600)
                {
                    user.IsAsleep = true;

                    var fallAsleep = new ServerPacket(486);
                    fallAsleep.WriteInteger(user.VirtualId);
                    fallAsleep.WriteBoolean(true);
                    SendMessage(fallAsleep);
                }

                if (user.NeedsAutokick && !remove.Contains(user.HabboId))
                {
                    remove.Add(user.HabboId);
                }

                if (user.CarryItemId > 0)
                {
                    user.CarryTimer--;

                    if (user.CarryTimer <= 0)
                    {
                        user.CarryItem(0);
                    }
                }

                var invalidSetStep = false;

                if (user.SetStep)
                {
                    if (CanWalk(user.SetX, user.SetY, 0, true) || user.AllowOverride || AllowWalkthrough)
                    {
                        UserMatrix[user.X, user.Y] = false;

                        user.X = user.SetX;
                        user.Y = user.SetY;
                        user.Z = user.SetZ;

                        UserMatrix[user.X, user.Y] = true;

                        UpdateUserStatus(user);
                    }
                    else
                    {
                        invalidSetStep = true;
                    }

                    user.SetStep = false;
                }

                if (user.PathRecalcNeeded)
                {
                    var pathfinder = new Pathfinder(this, user);

                    user.GoalX = user.PathRecalcX;
                    user.GoalY = user.PathRecalcY;

                    user.Path.Clear();
                    user.Path = pathfinder.FindPath();

                    if (user.Path.Count > 1)
                    {
                        user.PathStep = 1;
                        user.IsWalking = true;
                        user.PathRecalcNeeded = false;
                    }
                    else
                    {
                        user.PathRecalcNeeded = false;
                        user.Path.Clear();
                    }
                }

                if (user.IsWalking)
                {
                    if (invalidSetStep || user.PathStep >= user.Path.Count ||
                        user.GoalX == user.X && user.Y == user.GoalY)
                    {
                        user.Path.Clear();
                        user.IsWalking = false;
                        user.RemoveStatus("mv");
                        user.PathRecalcNeeded = false;

                        if (user.X == Model.DoorX && user.Y == Model.DoorY && !remove.Contains(user.HabboId) &&
                            !user.IsBot)
                        {
                            remove.Add(user.HabboId);
                        }

                        UpdateUserStatus(user);
                    }
                    else
                    {
                        var k = (user.Path.Count - user.PathStep) - 1;
                        var nextStep = user.Path[k];
                        user.PathStep++;

                        var nextX = nextStep.X;
                        var nextY = nextStep.Y;

                        user.RemoveStatus("mv");

                        var lastStep = nextX == user.GoalX && nextY == user.GoalY;

                        if (CanWalk(nextX, nextY, 0, lastStep) || user.AllowOverride)
                        {
                            var nextZ = SqAbsoluteHeight(nextX, nextY);

                            user.Statusses.Remove("lay");
                            user.Statusses.Remove("sit");
                            user.AddStatus("mv",
                                nextX + "," + nextY + "," +
                                nextZ.ToString(CultureInfo.InvariantCulture).Replace(',', '.'));

                            var newRot = Rotation.Calculate(user.X, user.Y, nextX, nextY);

                            user.RotBody = newRot;
                            user.RotHead = newRot;

                            user.SetStep = true;
                            user.SetX = BedMatrix[nextX, nextY].X;
                            user.SetY = BedMatrix[nextX, nextY].Y;
                            user.SetZ = nextZ;
                        }
                        else
                        {
                            user.IsWalking = false;
                        }
                    }

                    user.UpdateNeeded = true;
                }
                else
                {
                    if (user.Statusses.ContainsKey("mv"))
                    {
                        user.RemoveStatus("mv");
                        user.UpdateNeeded = true;
                    }
                }

                if (user.IsBot)
                {
                    user.BotAi.OnTimerTick();
                }
                else
                {
                    i++; // we do not count bots. we do not take kindly to their kind 'round 'ere.
                }
            }

            foreach (var toRemove in remove)
            {
                RemoveUserFromRoom(PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(toRemove), true,
                    false);
            }

            // Update idle time
            if (i >= 1)
            {
                _idleTime = 0;
            }
            else
            {
                _idleTime++;
            }

            // If room has been idle for a while
            if (_idleTime >= 60)
            {
                PylenorEnvironment.GetLogging()
                    .WriteLine("[RoomMgr] Requesting unload of idle room - ID#: " + RoomId, LogLevel.Debug);
                PylenorEnvironment.GetGame().GetRoomManager().RequestRoomUnload(RoomId);
            }

            var updates = SerializeStatusUpdates(false);

            if (updates != null)
            {
                SendMessage(updates);
            }
        }

        public void Destroy()
        {

            SendMessage(new ServerPacket(18));

            _idleTime = 0;
            KeepAlive = false;
            UserList.Clear();
        }

        public ServerPacket SerializeStatusUpdates(bool all)
        {
            var users = new List<RoomUser>();

            foreach (var user in UserList)
            {
                if (!all)
                {
                    if (!user.UpdateNeeded)
                    {
                        continue;
                    }

                    user.UpdateNeeded = false;
                }

                users.Add(user);
            }

            if (users.Count == 0)
            {
                return null;
            }

            var message = new ServerPacket(34);
            message.WriteInteger(users.Count);

            foreach (var user in users)
            {
                user.SerializeStatus(message);
            }

            return message;
        }

        public int ItemCountByType(string interactionType)
        {
            return Items.Count(item => string.Equals(item.GetBaseItem().InteractionType, interactionType, StringComparison.CurrentCultureIgnoreCase));
        }

        public void UpdateUserStatusses()
        {
            foreach (var user in UserList)
            {
                UpdateUserStatus(user);
            }
        }

        public double SqAbsoluteHeight(int x, int y)
        {
            var itemsOnSquare = GetFurniObjects(x, y);
            double[] highestStack = {0};

            var deduct = false;
            var deductable = 0.0;

            if (itemsOnSquare == null)
            {
                itemsOnSquare = new List<RoomItem>();
            }

            foreach (var item in itemsOnSquare.Where(item => item.TotalHeight > highestStack[0]))
            {
                if (item.GetBaseItem().IsSeat || item.GetBaseItem().InteractionType.ToLower() == "bed")
                {
                    deduct = true;
                    deductable = item.GetBaseItem().Height;
                }
                else
                {
                    deduct = false;
                }

                highestStack[0] = item.TotalHeight;
            }

            var floorHeight = Model.SqFloorHeight[x, y];
            var stackHeight = highestStack[0] - Model.SqFloorHeight[x, y];

            if (deduct)
            {
                stackHeight -= deductable;
            }

            if (stackHeight < 0)
            {
                stackHeight = 0;
            }

            return (floorHeight + stackHeight);
        }

        public void UpdateUserStatus(RoomUser user)
        {
            if (HasThread.ContainsKey(user.HabboId))
            {
                HasThread[user.HabboId].Abort();
                HasThread.Remove(user.HabboId);
            }

            if (BallThread.ContainsKey(user.HabboId))
            {
                BallThread[user.HabboId].Abort();
                BallThread.Remove(user.HabboId);
            }

            if (user.Statusses.ContainsKey("lay") || user.Statusses.ContainsKey("sit"))
            {
                user.Statusses.Remove("lay");
                user.Statusses.Remove("sit");
                user.UpdateNeeded = true;
            }

            var newZ = SqAbsoluteHeight(user.X, user.Y);

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (newZ != user.Z)
            {
                user.Z = newZ;
                user.UpdateNeeded = true;
            }

            if (Model.SqState[user.X, user.Y] == SquareState.Seat)
            {
                if (!user.Statusses.ContainsKey("sit"))
                {
                    user.Statusses.Add("sit", "1.0");
                }

                user.Z = Model.SqFloorHeight[user.X, user.Y];
                user.RotHead = Model.SqSeatRot[user.X, user.Y];
                user.RotBody = Model.SqSeatRot[user.X, user.Y];

                user.UpdateNeeded = true;
            }

            var ballX = 0;
            var ballY = 0;

            var rot = user.RotBody;
            if (rot == 3)
            {
                ballX = user.X + 1;
                ballY = user.Y + 1;
            }
            if (rot == 4)
            {
                ballX = user.X;
                ballY = user.Y + 1;
            }
            if (rot == 1)
            {
                ballX = user.X + 1;
                ballY = user.Y - 1;
            }
            if (rot == 2)
            {
                ballX = user.X + 1;
                ballY = user.Y;
            }
            if (rot == 0)
            {
                ballX = user.X;
                ballY = user.Y - 1;
            }
            if (rot == 5)
            {
                ballX = user.X - 1;
                ballY = user.Y + 1;
            }
            if (rot == 6)
            {
                ballX = user.X - 1;
                ballY = user.Y;
            }
            if (rot == 7)
            {
                ballX = user.X - 1;
                ballY = user.Y - 1;
            }

            var ball = GetFurniObjects(ballX, ballY) ?? new List<RoomItem>();

            foreach (var ballT in from item in ball
                where item.GetBaseItem().InteractionType.ToLower() == "ball"
                select new Thread(delegate() { BallProcess(item, user); }))
            {
                ballT.Start();
                BallThread.Add(user.HabboId, ballT);
            }

            var itemsOnSquare = GetFurniObjects(user.X, user.Y);

            if (itemsOnSquare == null)
            {
                itemsOnSquare = new List<RoomItem>();

                if (HasWaterEffect.Contains(user.HabboId))
                {
                    var message = new ServerPacket(485);
                    message.WriteInteger(user.VirtualId);
                    message.WriteInteger(0);
                    SendMessage(message);
                    HasWaterEffect.Remove(user.HabboId);
                    user.UpdateNeeded = true;
                }
            }

            foreach (var item in itemsOnSquare)
            {
                if (item.GetBaseItem().IsSeat)
                {
                    if (!user.Statusses.ContainsKey("sit"))
                    {
                        user.Statusses.Add("sit",
                            item.GetBaseItem().Height.ToString(CultureInfo.InvariantCulture).Replace(',', '.'));
                    }

                    user.Z = item.Z;
                    user.RotHead = item.Rot;
                    user.RotBody = item.Rot;

                    user.UpdateNeeded = true;
                }

                if (item.GetBaseItem().InteractionType.ToLower() == "bed")
                {
                    if (!user.Statusses.ContainsKey("lay"))
                    {
                        user.Statusses.Add("lay",
                            item.GetBaseItem().Height.ToString(CultureInfo.InvariantCulture).Replace(',', '.') + " null");
                    }

                    user.Z = item.Z;
                    user.RotHead = item.Rot;
                    user.RotBody = item.Rot;

                    user.UpdateNeeded = true;
                }

                if (item.GetBaseItem().InteractionType.ToLower() == "shower")
                {
                    item.ExtraData = "1";
                    item.UpdateState(false, true);
                }

                if ((item.GetBaseItem().InteractionType.ToLower() != "water1") && HasWaterEffect.Contains(user.HabboId) ||
                    user.Statusses.ContainsKey("sit") || user.Statusses.ContainsKey("lay"))
                {
                    var message = new ServerPacket(485);
                    message.WriteInteger(user.VirtualId);
                    message.WriteInteger(0);
                    SendMessage(message);
                    user.UpdateNeeded = true;
                    HasWaterEffect.Remove(user.HabboId);
                }

                if ((item.GetBaseItem().InteractionType.ToLower() != "water2") && HasWaterEffect.Contains(user.HabboId) ||
                    user.Statusses.ContainsKey("sit") || user.Statusses.ContainsKey("lay"))
                {
                    var message = new ServerPacket(485);
                    message.WriteInteger(user.VirtualId);
                    message.WriteInteger(0);
                    SendMessage(message);
                    user.UpdateNeeded = true;
                    HasWaterEffect.Remove(user.HabboId);
                }

                if ((item.GetBaseItem().InteractionType.ToLower() == "water1") && !HasWaterEffect.Contains(user.HabboId) &&
                    !user.Statusses.ContainsKey("lay") && !user.Statusses.ContainsKey("sit"))
                {
                    var effectId = 0;
                    if (item.GetBaseItem().Name == "bw_water_1")
                    {
                        effectId = 30;
                    }
                    var message = new ServerPacket(485);
                    message.WriteInteger(user.VirtualId);
                    message.WriteInteger(effectId);
                    SendMessage(message);
                    user.UpdateNeeded = true;
                    HasWaterEffect.Add(user.HabboId);
                }

                if ((item.GetBaseItem().InteractionType.ToLower() == "water2") && !HasWaterEffect.Contains(user.HabboId) &&
                    !user.Statusses.ContainsKey("lay") && !user.Statusses.ContainsKey("sit"))
                {
                    var effectId = 0;
                    if (item.GetBaseItem().Name == "bw_water_2")
                    {
                        effectId = 29;
                    }
                    var message = new ServerPacket(485);
                    message.WriteInteger(user.VirtualId);
                    message.WriteInteger(effectId);
                    SendMessage(message);
                    user.UpdateNeeded = true;
                    HasWaterEffect.Add(user.HabboId);
                }


                if (item.GetBaseItem().InteractionType.ToLower() != "roller") continue;
                try
                {
                    var roller = new Thread(delegate() { RollerProcess(item, user); });
                    roller.Start();
                    HasThread.Add(user.HabboId, roller);
                }
                catch
                {
                    // ignored
                }
            }
        }

        public void RollerProcess(RoomItem itemNow, RoomUser userNow)
        {
            Thread.Sleep(2000);

            var nextx = 0;
            var nexty = 0;

            var rot = itemNow.Rot;
            if (rot == 4)
            {
                nextx = itemNow.X;
                nexty = itemNow.Y + 1;
            }
            if (rot == 2)
            {
                nextx = itemNow.X + 1;
                nexty = itemNow.Y;
            }
            if (rot == 0)
            {
                nextx = itemNow.X;
                nexty = itemNow.Y - 1;
            }
            if (rot == 6)
            {
                nextx = itemNow.X - 1;
                nexty = itemNow.Y;
            }

            userNow.MoveTo(nextx, nexty);
        }

        public void BallProcess(RoomItem itemNow, RoomUser userNow)
        {
            Thread.Sleep(500);
            var newX = 0;
            var newY = 0;
            var rot = userNow.RotBody;
            if (rot == 3)
            {
                newX = userNow.X + 2;
                newY = userNow.Y + 2;
            }
            if (rot == 4)
            {
                newX = userNow.X;
                newY = userNow.Y + 2;
            }
            if (rot == 1)
            {
                newX = userNow.X + 2;
                newY = userNow.Y - 2;
            }
            if (rot == 2)
            {
                newX = userNow.X + 2;
                newY = userNow.Y;
            }
            if (rot == 0)
            {
                newX = userNow.X;
                newY = userNow.Y - 2;
            }
            if (rot == 5)
            {
                newX = userNow.X - 2;
                newY = userNow.Y + 2;
            }
            if (rot == 6)
            {
                newX = userNow.X - 2;
                newY = userNow.Y;
            }
            if (rot == 7)
            {
                newX = userNow.X - 2;
                newY = userNow.Y - 2;
            }
            SetFloorItem(userNow.GetClient(), itemNow, newX, newY, itemNow.Rot, false);
        }

        public bool ValidTile(int x, int y)
        {
            return x >= 0 && y >= 0 && x < Model.MapSizeX && y < Model.MapSizeY;
        }

        public void TurnHeads(int x, int y, uint senderId)
        {
            foreach (var user in UserList.Where(user => user.HabboId != senderId))
            {
                user.SetRot(Rotation.Calculate(user.X, user.Y, x, y), true);
            }
        }

        public List<RoomItem> GetFurniObjects(int x, int y)
        {
            var results = new List<RoomItem>();

            foreach (var item in FloorItems)
            {
                if (item.X == x && item.Y == y)
                {
                    results.Add(item);
                }

                var pointList = GetAffectedTiles(item.GetBaseItem().Length, item.GetBaseItem().Width, item.X, item.Y,
                    item.Rot);

                results.AddRange(from tile in pointList.Values where tile.X == x && tile.Y == y select item);
            }

            return results.Count > 0 ? results : null;
        }

        public RoomItem FindItem(uint id)
        {
            return Items.FirstOrDefault(item => item.Id == id);
        }

        public Dictionary<int, AffectedTile> GetAffectedTiles(int length, int width, int posX, int posY, int rotation)
        {
            var x = 0;

            var pointList = new Dictionary<int, AffectedTile>();

            if (length > 1)
            {
                if (rotation == 0 || rotation == 4)
                {
                    for (var i = 1; i < length; i++)
                    {
                        pointList.Add(x++, new AffectedTile(posX, posY + i, i));

                        for (var j = 1; j < width; j++)
                        {
                            pointList.Add(x++, new AffectedTile(posX + j, posY + i, (i < j) ? j : i));
                        }
                    }
                }
                else if (rotation == 2 || rotation == 6)
                {
                    for (var i = 1; i < length; i++)
                    {
                        pointList.Add(x++, new AffectedTile(posX + i, posY, i));

                        for (var j = 1; j < width; j++)
                        {
                            pointList.Add(x++, new AffectedTile(posX + i, posY + j, (i < j) ? j : i));
                        }
                    }
                }
            }

            if (width > 1)
            {
                if (rotation == 0 || rotation == 4)
                {
                    for (var i = 1; i < width; i++)
                    {
                        pointList.Add(x++, new AffectedTile(posX + i, posY, i));

                        for (var j = 1; j < length; j++)
                        {
                            pointList.Add(x++, new AffectedTile(posX + i, posY + j, (i < j) ? j : i));
                        }
                    }
                }
                else if (rotation == 2 || rotation == 6)
                {
                    for (var i = 1; i < width; i++)
                    {
                        pointList.Add(x++, new AffectedTile(posX, posY + i, i));

                        for (var j = 1; j < length; j++)
                        {
                            pointList.Add(x++, new AffectedTile(posX + j, posY + i, (i < j) ? j : i));
                        }
                    }
                }
            }

            return pointList;
        }

        public bool SquareHasUsers(int x, int y, bool lastStep)
        {
            if (AllowWalkthrough && !lastStep)
            {
                return false;
            }

            return SquareHasUsers(x, y);
        }

        public bool SquareHasUsers(int x, int y)
        {
            var coord = BedMatrix[x, y];
            return UserMatrix[coord.X, coord.Y];
        }

        //This function is based on the one from "Holograph Emulator"
        public string WallPositionCheck(string wallPosition)
        {
            //:w=3,2 l=9,63 l
            try
            {
                if (wallPosition.Contains(Convert.ToChar(13)))
                {
                    return null;
                }
                if (wallPosition.Contains(Convert.ToChar(9)))
                {
                    return null;
                }

                var posD = wallPosition.Split(' ');
                if (posD[2] != "l" && posD[2] != "r")
                    return null;

                var widD = posD[0].Substring(3).Split(',');
                var widthX = int.Parse(widD[0]);
                var widthY = int.Parse(widD[1]);
                if (widthX < 0 || widthY < 0 || widthX > 200 || widthY > 200)
                    return null;

                var lenD = posD[1].Substring(2).Split(',');
                var lengthX = int.Parse(lenD[0]);
                var lengthY = int.Parse(lenD[1]);
                if (lengthX < 0 || lengthY < 0 || lengthX > 200 || lengthY > 200)
                    return null;

                return ":w=" + widthX + "," + widthY + " " + "l=" + lengthX + "," + lengthY + " " + posD[2];
            }
            catch
            {
                return null;
            }
        }

        public bool TilesTouching(int x1, int y1, int x2, int y2)
        {
            if (!(Math.Abs(x1 - x2) > 1 || Math.Abs(y1 - y2) > 1)) return true;
            return x1 == x2 && y1 == y2;
        }

        public int TileDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        #region User handling (General)

        public void AddUserToRoom(Session session, bool spectator)
        {
            var user = new RoomUser(session.GetPlayer().Id, RoomId, UserCounter++);

            if (spectator)
            {
                user.IsSpectator = true;
            }
            else
            {
                user.SetPos(Model.DoorX, Model.DoorY, Model.DoorZ);
                user.SetRot(Model.DoorOrientation);

                if (CheckRights(session, true))
                {
                    user.AddStatus("flatcrtl", "useradmin");
                }
                else if (CheckRights(session))
                {
                    user.AddStatus("flatcrtl", "");
                }

                if (!user.IsBot && user.GetClient().GetPlayer().IsTeleporting)
                {
                    var item = GetItem(user.GetClient().GetPlayer().TeleporterId);

                    if (item != null)
                    {
                        user.SetPos(item.X, item.Y, item.Z);
                        user.SetRot(item.Rot);

                        item.InteractingUser2 = session.GetPlayer().Id;
                        item.ExtraData = "2";
                        item.UpdateState(false, true);
                    }
                }

                user.GetClient().GetPlayer().IsTeleporting = false;
                user.GetClient().GetPlayer().TeleporterId = 0;

                var enterMessage = new ServerPacket(28);
                enterMessage.WriteInteger(1);
                user.Serialize(enterMessage);
                SendMessage(enterMessage);
            }

            UserList.Add(user);
            session.GetPlayer().OnEnterRoom(RoomId);

            if (spectator) return;
            UpdateUserCount();

            foreach (var usr in UserList.Where(usr => usr.IsBot))
            {
                usr.BotAi.OnUserEnterRoom(user);
            }
        }

        public void RemoveUserFromRoom(Session session, bool notifyClient, bool notifyKick)
        {
            try
            {
                if (session == null)
                {
                    return;
                }

                var user = GetRoomUserByHabbo(session.GetPlayer().Id);

                if (!UserList.Remove(GetRoomUserByHabbo(session.GetPlayer().Id)))
                {
                    return;
                }

                if (notifyClient)
                {
                    if (notifyKick)
                    {
                        var genericError = new ServerPacket(33);
                        genericError.WriteInteger(4008);
                        session.SendPacket(genericError);
                    }

                    session.SendPacket(new ServerPacket(18));
                }

                var petsToRemove = new List<RoomUser>();

                if (!user.IsSpectator)
                {
                    UserMatrix[user.X, user.Y] = false;

                    var leaveMessage = new ServerPacket(29);
                    leaveMessage.AppendRawInt32(user.VirtualId);
                    SendMessage(leaveMessage);

                    if (session.GetPlayer() != null)
                    {
                        if (HasActiveTrade(session.GetPlayer().Id))
                        {
                            TryStopTrade(session.GetPlayer().Id);
                        }

                        if (string.Equals(session.GetPlayer().Username, Owner, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (HasOngoingEvent)
                            {
                                var room =
                                    PylenorEnvironment.GetGame()
                                        .GetRoomManager()
                                        .GetRoom(session.GetPlayer().CurrentRoomId);

                                if (room?.Event == null)
                                {
                                    return;
                                }

                                Event = null;

                                var message = new ServerPacket(370);
                                message.WriteString("-1");
                                SendMessage(message);
                            }
                        }

                        session.GetPlayer().OnLeaveRoom();
                    }
                }

                if (!user.IsSpectator)
                {
                    UpdateUserCount();

                    var bots = UserList.Where(usr => usr.IsBot).ToList();

                    foreach (var bot in bots)
                    {
                        bot.BotAi.OnUserLeaveRoom(session);

                        if (bot.IsPet && bot.PetData.OwnerId == session.GetPlayer().Id && !CheckRights(session, true))
                        {
                            petsToRemove.Add(bot);
                        }
                    }
                }

                foreach (var toRemove in petsToRemove)
                {
                    session.GetPlayer().GetInventoryComponent().AddPet(toRemove.PetData);
                    RemoveBot(toRemove.VirtualId, false);
                }
            }
            catch
            {
                PylenorEnvironment.GetLogging().WriteLine("error in removeruserfromroom", LogLevel.Error);
            }
        }

        public RoomUser GetPet(uint petId)
        {
            return
                UserList.FirstOrDefault(
                    user =>
                        user != null &&
                        (user.IsBot && user.IsPet && user.PetData != null && user.PetData.PetId == petId));
        }

        public bool RoomContainsPet(uint petId)
        {
            return (GetPet(petId) != null);
        }

        public void UpdateUserCount()
        {
            UsersNow = UserCount;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE rooms SET users_now = '" + UsersNow + "' WHERE id = '" + RoomId +
                                      "' LIMIT 1");
            }
        }

        public RoomUser GetRoomUserByVirtualId(int virtualId)
        {
            return UserList.FirstOrDefault(user => user.VirtualId == virtualId);
        }

        public RoomUser GetRoomUserByHabbo(uint id)
        {
            return UserList.Where(user => !user.IsBot).FirstOrDefault(user => user.HabboId == id);
        }

        public RoomUser GetRoomUserByHabbo(string name)
        {
            return
                UserList.Where(user => !user.IsBot && user.GetClient().GetPlayer() != null)
                    .FirstOrDefault(
                        user =>
                            string.Equals(user.GetClient().GetPlayer().Username, name,
                                StringComparison.CurrentCultureIgnoreCase));
        }

        #endregion

        #region Communication

        public void SendMessage(ServerPacket packet)
        {
            try
            {
                foreach (var user in UserList.Where(user => !user.IsBot && user.GetClient() != null))
                {
                    user.GetClient().SendPacket(packet);
                }
            }
            catch (InvalidOperationException)
            {
            }
        }

        public void SendMessageToUsersWithRights(ServerPacket packet)
        {
            foreach (var user in UserList.Where(user => !user.IsBot).Where(user => CheckRights(user.GetClient())))
            {
                user.GetClient().SendPacket(packet);
            }
        }

        #endregion

        #region Room Bans

        public bool UserIsBanned(uint id)
        {
            return _bans.ContainsKey(id);
        }

        public void RemoveBan(uint id)
        {
            _bans.Remove(id);
        }

        public void AddBan(uint id)
        {
            _bans.Add(id, PylenorEnvironment.GetUnixTimestamp());
        }

        public bool HasBanExpired(uint id)
        {
            if (!UserIsBanned(id))
            {
                return true;
            }

            var diff = PylenorEnvironment.GetUnixTimestamp() - _bans[id];

            return diff > 900;
        }

        #endregion

        #region Trading

        public bool HasActiveTrade(RoomUser user)
        {
            return !user.IsBot && HasActiveTrade(user.GetClient().GetPlayer().Id);
        }

        public bool HasActiveTrade(uint userId)
        {
            return ActiveTrades.Any(trade => trade.ContainsUser(userId));
        }

        public Trade GetUserTrade(RoomUser user)
        {
            return user.IsBot ? null : GetUserTrade(user.GetClient().GetPlayer().Id);
        }

        public Trade GetUserTrade(uint userId)
        {
            return ActiveTrades.FirstOrDefault(trade => trade.ContainsUser(userId));
        }

        public void TryStartTrade(RoomUser userOne, RoomUser userTwo)
        {
            if (userOne == null || userTwo == null || userOne.IsBot || userTwo.IsBot || userOne.IsTrading ||
                userTwo.IsTrading || HasActiveTrade(userOne) || HasActiveTrade(userTwo))
            {
                if (userOne != null && (HasActiveTrade(userTwo) && !userOne.IsTrading))
                {
                    userOne.GetClient().SendNotif("This user is already trading with someone else.");
                }

                return;
            }

            ActiveTrades.Add(new Trade(userOne.GetClient().GetPlayer().Id, userTwo.GetClient().GetPlayer().Id, RoomId));
        }

        public void TryStopTrade(uint userId)
        {
            var trade = GetUserTrade(userId);

            if (trade == null)
            {
                return;
            }

            trade.CloseTrade(userId);
            ActiveTrades.Remove(trade);
        }

        #endregion

        #region Furni handling and stacking

        public bool SetFloorItem(Session session, RoomItem item, int newX, int newY, int newRot, bool newItem)
        {
            // Find affected tiles
            var affectedTiles = GetAffectedTiles(item.GetBaseItem().Length, item.GetBaseItem().Width, newX, newY, newRot);

            // Verify tiles are valid
            if (!ValidTile(newX, newY))
            {
                return false;
            }

            if (affectedTiles.Values.Any(tile => !ValidTile(tile.X, tile.Y)))
            {
                return false;
            }

            // Start calculating new Z coordinate
            var newZ = Model.SqFloorHeight[newX, newY];

            // Is the item trying to stack on itself!?
            if (item.Rot == newRot && item.X == newX && item.Y == newY && item.Z != newZ)
            {
                return false;
            }

            // Make sure this tile is open and there are no users here
            if (Model.SqState[newX, newY] != SquareState.Open)
            {
                return false;
            }

            if (affectedTiles.Values.Any(tile => Model.SqState[tile.X, tile.Y] != SquareState.Open))
            {
                return false;
            }

            // And that we have no users
            if (!item.GetBaseItem().IsSeat)
            {
                if (SquareHasUsers(newX, newY))
                {
                    return false;
                }

                if (affectedTiles.Values.Any(tile => SquareHasUsers(tile.X, tile.Y)))
                {
                    return false;
                }
            }

            // Find affected objects
            var itemsOnTile = GetFurniObjects(newX, newY);
            var itemsAffected = new List<RoomItem>();
            var itemsComplete = new List<RoomItem>();

            foreach (
                var temp in
                    affectedTiles.Values.Select(tile => GetFurniObjects(tile.X, tile.Y)).Where(temp => temp != null))
            {
                itemsAffected.AddRange(temp);
            }

            if (itemsOnTile == null) itemsOnTile = new List<RoomItem>();

            itemsComplete.AddRange(itemsOnTile);
            itemsComplete.AddRange(itemsAffected);

            // Check for items in the stack that do not allow stacking on top of them
            if (itemsComplete.Where(I => I.Id != item.Id).Any(I => !I.GetBaseItem().Stackable))
            {
                return false;
            }

            // If this is a rotating action, maintain item at current height
            if (item.Rot != newRot && item.X == newX && item.Y == newY)
            {
                newZ = item.Z;
            }

            // Are there any higher objects in the stack!?
            newZ = (from I in itemsComplete where I.Id != item.Id select I.TotalHeight).Concat(new[] {newZ}).Max();

            // Verify the rotation is correct
            if (newRot != 0 && newRot != 2 && newRot != 4 && newRot != 6 && newRot != 8)
            {
                newRot = 0;
            }

            item.X = newX;
            item.Y = newY;
            item.Z = newZ;
            item.Rot = newRot;

            item.Interactor.OnPlace(session, item);

            if (newItem)
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.AddParamWithValue("extra_data", item.ExtraData);
                    dbClient.ExecuteQuery(
                        "INSERT INTO room_items (id,room_id,base_item,extra_data,x,y,z,rot,wall_pos) VALUES ('" +
                        item.Id + "','" + RoomId + "','" + item.BaseItem + "',@extra_data,'" + item.X + "','" + item.Y +
                        "','" + item.Z + "','" + item.Rot + "','')");
                }

                Items.Add(item);

                var message = new ServerPacket(93);
                item.Serialize(message);
                SendMessage(message);
            }
            else
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("UPDATE room_items SET x = '" + item.X + "', y = '" + item.Y + "', z = '" +
                                          item.Z + "', rot = '" + item.Rot + "', wall_pos = '' WHERE id = '" + item.Id +
                                          "' LIMIT 1");
                }

                var message = new ServerPacket(95);
                item.Serialize(message);
                SendMessage(message);
            }

            GenerateMaps();
            UpdateUserStatusses();

            return true;
        }

        public bool SetWallItem(Session session, RoomItem item)
        {
            item.Interactor.OnPlace(session, item);

            switch (item.GetBaseItem().InteractionType.ToLower())
            {
                case "dimmer":

                    if (MoodlightData == null)
                    {
                        MoodlightData = new MoodlightData(item.Id);
                        item.ExtraData = MoodlightData.GenerateExtraData();
                    }

                    break;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("extra_data", item.ExtraData);
                dbClient.ExecuteQuery(
                    "INSERT INTO room_items (id,room_id,base_item,extra_data,x,y,z,rot,wall_pos) VALUES ('" + item.Id +
                    "','" + RoomId + "','" + item.BaseItem + "',@extra_data,'0','0','0','0','" + item.WallPos + "')");
            }

            Items.Add(item);

            var message = new ServerPacket(83);
            item.Serialize(message);
            SendMessage(message);

            return true;
        }

        #endregion

        public void HandleTimer(RoomItem roomItem)
        {
            //method_21(null, item, "Timer");
        }

        private RoomItem GetTopItem(int x, int y)
        {
            return Items.Where(item => item != null).Where(item => item.X == x && item.Y == y).FirstOrDefault(item => HeightMatrix[x, y] == item.Double_1);
        }

        public bool ProcessWired(RoomUser user, RoomItem item, string interactor)
        {
            return false;
        }
    }
}