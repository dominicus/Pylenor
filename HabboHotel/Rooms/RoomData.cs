﻿using System.Collections.Generic;
using System.Data;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Rooms
{
    public class RoomData
    {
        public bool AllowPets;
        public bool AllowPetsEating;
        public bool AllowWalkthrough;
        public int Category;
        public string CCTs;
        public string Description;
        public RoomEvent Event;
        public string Floor;
        public bool Hidewall;
        public uint Id;
        public string Landscape;
        public string ModelName;
        public string Name;
        public string Owner;
        public string Password;
        public int Score;
        public int State;
        public List<string> Tags;
        public string Type;
        public int UsersMax;
        public int UsersNow;
        public string Wallpaper;

        public bool IsPublicRoom
        {
            get { return Type.ToLower() == "public"; }
        }

        public RoomIcon Icon { get; private set; }

        public int TagCount
        {
            get { return Tags.Count; }
        }

        public RoomModel Model
        {
            get { return PylenorEnvironment.GetGame().GetRoomManager().GetModel(ModelName); }
        }

        public void FillNull(uint Id)
        {
            this.Id = Id;
            Name = "Unknown Room";
            Description = "-";
            Type = "private";
            Owner = "-";
            Category = 0;
            UsersNow = 0;
            UsersMax = 0;
            ModelName = "NO_MODEL";
            CCTs = "";
            Score = 0;
            Tags = new List<string>();
            AllowPets = true;
            AllowPetsEating = false;
            AllowWalkthrough = true;
            Hidewall = false;
            Password = "";
            Wallpaper = "0.0";
            Floor = "0.0";
            Landscape = "0.0";
            Event = null;
            Icon = new RoomIcon(1, 1, new Dictionary<int, int>());
        }

        public void Fill(DataRow Row)
        {
            Id = (uint) Row["id"];
            Name = (string) Row["caption"];
            Description = (string) Row["description"];
            Type = (string) Row["roomtype"];
            Owner = (string) Row["owner"];

            switch (Row["state"].ToString().ToLower())
            {
                case "open":

                    State = 0;
                    break;

                case "password":

                    State = 2;
                    break;

                case "locked":
                default:

                    State = 1;
                    break;
            }

            Category = (int) Row["category"];
            UsersNow = (int) Row["users_now"];
            UsersMax = (int) Row["users_max"];
            ModelName = (string) Row["model_name"];
            CCTs = (string) Row["public_ccts"];
            Score = (int) Row["score"];
            Tags = new List<string>();
            AllowPets = PylenorEnvironment.EnumToBool(Row["allow_pets"].ToString());
            AllowPetsEating = PylenorEnvironment.EnumToBool(Row["allow_pets_eat"].ToString());
            AllowWalkthrough = PylenorEnvironment.EnumToBool(Row["allow_walkthrough"].ToString());
            Hidewall = PylenorEnvironment.EnumToBool(Row["allow_hidewall"].ToString());
            Password = (string) Row["password"];
            Wallpaper = (string) Row["wallpaper"];
            Floor = (string) Row["floor"];
            Landscape = (string) Row["landscape"];
            Event = null;

            var IconItems = new Dictionary<int, int>();

            if (Row["icon_items"].ToString() != "")
            {
                foreach (var Bit in Row["icon_items"].ToString().Split('|'))
                {
                    IconItems.Add(int.Parse(Bit.Split(',')[0]), int.Parse(Bit.Split(',')[1]));
                }
            }

            Icon = new RoomIcon((int) Row["icon_bg"], (int) Row["icon_fg"], IconItems);

            foreach (var Tag in Row["tags"].ToString().Split(','))
            {
                Tags.Add(Tag);
            }
        }

        public void Fill(Room Room)
        {
            Id = Room.RoomId;
            Name = Room.Name;
            Description = Room.Description;
            Type = Room.Type;
            Owner = Room.Owner;
            Category = Room.Category;
            State = Room.State;
            UsersNow = Room.UsersNow;
            UsersMax = Room.UsersMax;
            ModelName = Room.ModelName;
            CCTs = Room.CcTs;
            Score = Room.Score;
            Tags = Room.Tags;
            AllowPets = Room.AllowPets;
            AllowPetsEating = Room.AllowPetsEating;
            AllowWalkthrough = Room.AllowWalkthrough;
            Hidewall = Room.Hidewall;
            Icon = Room.Icon;
            Password = Room.Password;
            Event = Room.Event;
            Wallpaper = Room.Wallpaper;
            Floor = Room.Floor;
            Landscape = Room.Landscape;
        }

        public void Serialize(ServerPacket packet, bool ShowEvents)
        {
            packet.WriteUInt(Id);

            if (Event == null || !ShowEvents)
            {
                packet.WriteBoolean(false);
                packet.WriteString(Name);
                packet.WriteString(Owner);
                packet.WriteInteger(State); // room state
                packet.WriteInteger(UsersNow);
                packet.WriteInteger(UsersMax);
                packet.WriteString(Description);
                packet.WriteInteger(0); // dunno!
                packet.WriteBoolean(true); // can trade?
                packet.WriteInteger(Score);
                packet.WriteInteger(Category);
                packet.WriteString("");
                packet.WriteInteger(TagCount);

                foreach (var Tag in Tags)
                {
                    packet.WriteString(Tag);
                }
            }
            else
            {
                packet.WriteBoolean(true);
                packet.WriteString(Event.Name);
                packet.WriteString(Owner);
                packet.WriteInteger(State);
                packet.WriteInteger(UsersNow);
                packet.WriteInteger(UsersMax);
                packet.WriteString(Event.Description);
                packet.WriteBoolean(true);
                packet.WriteBoolean(true);
                packet.WriteInteger(Score);
                packet.WriteInteger(Event.Category);
                packet.WriteString(Event.StartTime);
                packet.WriteInteger(Event.Tags.Count);

                foreach (var Tag in Event.Tags)
                {
                    packet.WriteString(Tag);
                }
            }

            Icon.Serialize(packet);

            packet.WriteBoolean(true); //allowpets
            packet.WriteBoolean(false); //allowpetseat
        }
    }
}