﻿using System;
using System.Collections.Generic;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Rooms
{
    public class RoomEvent
    {
        public int Category;
        public string Description;
        public string Name;

        public uint RoomId;
        public string StartTime;
        public List<string> Tags;

        public RoomEvent(uint roomId, string name, string description, int category, List<string> tags)
        {
            RoomId = roomId;
            Name = name;
            Description = description;
            Category = category;
            Tags = tags;
            StartTime = DateTime.Now.ToShortTimeString();
        }

        public ServerPacket Serialize(Session session)
        {
            var message = new ServerPacket(370);
            message.WriteString(session.GetPlayer().Id + "");
            message.WriteString(session.GetPlayer().Username);
            message.WriteString(RoomId + "");
            message.WriteInteger(Category);
            message.WriteString(Name);
            message.WriteString(Description);
            message.WriteString(StartTime);
            message.WriteInteger(Tags.Count);

            foreach (var tag in Tags)
            {
                message.WriteString(tag);
            }

            return message;
        }
    }
}