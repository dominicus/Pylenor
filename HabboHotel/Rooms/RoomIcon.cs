﻿using System.Collections.Generic;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Rooms
{
    public class RoomIcon
    {
        public int BackgroundImage;
        public int ForegroundImage;
        public Dictionary<int, int> Items;

        public RoomIcon(int backgroundImage, int foregroundImage, Dictionary<int, int> items)
        {
            BackgroundImage = backgroundImage;
            ForegroundImage = foregroundImage;
            Items = items;
        }

        public void Serialize(ServerPacket packet)
        {
            packet.WriteInteger(BackgroundImage);
            packet.WriteInteger(ForegroundImage);
            packet.WriteInteger(Items.Count);

            foreach (var item in Items)
            {
                packet.WriteInteger(item.Key);
                packet.WriteInteger(item.Value);
            }
        }
    }
}