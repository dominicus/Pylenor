﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Pylenor.Communication.Sessions;
using Pylenor.Core;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Rooms
{
    internal class RoomManager
    {
        private readonly Dictionary<string, RoomModel> _models;

        private readonly Dictionary<uint, Room> _rooms;

        private readonly List<uint> _roomsToUnload;
        private readonly List<TeleUserData> _teleActions;
        public readonly int MaxPetsPerRoom = 5;

        public RoomManager()
        {
            _rooms = new Dictionary<uint, Room>();
            _models = new Dictionary<string, RoomModel>();
            _teleActions = new List<TeleUserData>();

            var engineThread = new Thread(ProcessEngine)
            {
                Name = "Room Engine",
                Priority = ThreadPriority.AboveNormal
            };
            engineThread.Start();

            _roomsToUnload = new List<uint>();
        }

        public int LoadedRoomsCount => _rooms.Count;

        public void AddTeleAction(TeleUserData act)
        {
            _teleActions.Add(act);
        }

        public List<Room> GetEventRoomsForCategory(int category)
        {
            return
                _rooms.Values.Where(room => room.Event != null)
                    .Where(room => category <= 0 || room.Event.Category == category)
                    .ToList();
            ;
        }

        public void ProcessEngine()
        {
            Thread.Sleep(5000);

            while (true)
            {
                var executionStart = DateTime.Now;

                try
                {
                    foreach (var room in _rooms.Values.Where(room => room.KeepAlive))
                    {
                        room.ProcessRoom();
                    }

                    foreach (var room in _roomsToUnload)
                    {
                        UnloadRoom(room);
                    }

                    _roomsToUnload.Clear();
                    foreach (var teleAction in _teleActions)
                    {
                        teleAction?.Execute();
                    }

                    _teleActions.Clear();
                }

                catch (InvalidOperationException)
                {
                    PylenorEnvironment.GetLogging().WriteLine("InvalidOpException in Room Manager..", LogLevel.Error);
                }

                finally
                {
                    var executionComplete = DateTime.Now;
                    var diff = executionComplete - executionStart;

                    var sleepTime = 500 - diff.TotalMilliseconds;

                    if (sleepTime < 0)
                    {
                        sleepTime = 0;
                    }

                    if (sleepTime > 500)
                    {
                        sleepTime = 500;
                    }

                    Thread.Sleep((int) Math.Floor(sleepTime));
                }
            }
        }

        public void LoadModels(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading room models");
            _models.Clear();
            var data =
                dbClient.ReadDataTable(
                    "SELECT id,door_x,door_y,door_z,door_dir,heightmap,public_items,club_only FROM room_models");
            if (data == null) return;
            foreach (DataRow row in data.Rows)
            {
                _models.Add((string) row["id"], new RoomModel((string) row["id"], (int) row["door_x"],
                    (int) row["door_y"], (double) row["door_z"], (int) row["door_dir"], (string) row["heightmap"],
                    (string) row["public_items"], PylenorEnvironment.EnumToBool(row["club_only"].ToString())));
            }
            Console.WriteLine("...completed!");
        }

        public RoomModel GetModel(string model)
        {
            return _models.ContainsKey(model) ? _models[model] : null;
        }

        public RoomData GenerateNullableRoomData(uint roomId)
        {
            if (GenerateRoomData(roomId) != null)
            {
                return GenerateRoomData(roomId);
            }

            var data = new RoomData();
            data.FillNull(roomId);
            return data;
        }

        public RoomData GenerateRoomData(uint roomId)
        {
            var data = new RoomData();

            if (IsRoomLoaded(roomId))
            {
                data.Fill(GetRoom(roomId));
            }
            else
            {
                using (var db = PylenorEnvironment.GetDatabase().GetClient())
                {
                    var row = db.ReadDataRow("SELECT * FROM rooms WHERE id = '" + roomId + "' LIMIT 1");

                    if (row != null)
                    {
                        data.Fill(row);
                    }
                }
            }

            return data;
        }

        public bool IsRoomLoaded(uint roomId)
        {
            return GetRoom(roomId) != null;
        }

        public void LoadRoom(uint id)
        {
            if (IsRoomLoaded(id))
            {
                return;
            }

            var data = GenerateRoomData(id);

            if (data == null) return;
            var room = new Room(data.Id, data.Name, data.Description, data.Type, data.Owner, data.Category,
                data.State,
                data.UsersMax, data.ModelName, data.CCTs, data.Score, data.Tags, data.AllowPets,
                data.AllowPetsEating,
                data.AllowWalkthrough, data.Hidewall, data.Icon, data.Password, data.Wallpaper, data.Floor,
                data.Landscape);

            _rooms.Add(room.RoomId, room);

            room.InitBots();
            room.InitPets();

            PylenorEnvironment.GetLogging()
                .WriteLine("[RoomMgr] Loaded room: \"" + room.Name + "\" (ID: " + id + ")", LogLevel.Information);
        }

        public void RequestRoomUnload(uint id)
        {
            if (!IsRoomLoaded(id)) return;
            GetRoom(id).KeepAlive = false;
            _roomsToUnload.Add(id);
        }

        public void UnloadRoom(uint id)
        {
            var room = GetRoom(id);

            if (room == null)
            {
                return;
            }

            room.Destroy();
            
            _rooms.Remove(id);

            PylenorEnvironment.GetLogging()
                .WriteLine("[RoomMgr] Unloaded room [ID: " + id + "]", LogLevel.Information);
        }

        public Room GetRoom(uint roomId)
        {
            return _rooms.Where(room => room.Value.RoomId == roomId).Select(room => room.Value).FirstOrDefault();
        }

        public RoomData CreateRoom(Session session, string name, string model)
        {
            name = PylenorEnvironment.FilterInjectionChars(name);

            if (!_models.ContainsKey(model))
            {
                session.SendNotif("Sorry, this room model has not been added yet. Try again later.");
                return null;
            }

            if (_models[model].ClubOnly && !session.GetPlayer().HasFuse("fuse_use_special_room_layouts"))
            {
                session.SendNotif("You must be an Club member to use that room layout.");
                return null;
            }

            if (name.Length < 3)
            {
                session.SendNotif("Room name is too short for room creation!");
                return null;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("caption", name);
                dbClient.AddParamWithValue("model", model);
                dbClient.AddParamWithValue("username", session.GetPlayer().Username);
                dbClient.ExecuteQuery(
                    "INSERT INTO rooms (roomtype,caption,owner,model_name) VALUES ('private',@caption,@username,@model)");
            }
            uint roomId;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("caption", name);
                dbClient.AddParamWithValue("username", session.GetPlayer().Username);
                roomId = (uint)
                    dbClient.ReadDataRow(
                        "SELECT id FROM rooms WHERE owner = @username AND caption = @caption ORDER BY id DESC")[0];
            }
            return GenerateRoomData(roomId);
        }
    }

    internal class TeleUserData
    {
        private readonly uint _roomId;
        private readonly uint _teleId;
        private readonly RoomUser _user;

        public TeleUserData(RoomUser user, uint roomId, uint teleId)
        {
            _user = user;
            _roomId = roomId;
            _teleId = teleId;
        }

        public void Execute()
        {
            if (_user == null || _user.IsBot)
            {
                return;
            }

            _user.GetClient().GetPlayer().IsTeleporting = true;
            _user.GetClient().GetPlayer().TeleporterId = _teleId;
            _user.GetClient().PrepareRoom(_roomId, "");
        }
    }
}