﻿using System;
using System.Globalization;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Util;

namespace Pylenor.HabboHotel.Rooms
{
    public enum SquareState
    {
        Open = 0,
        Blocked = 1,
        Seat = 2
    }

    public class RoomModel
    {
        public bool ClubOnly;
        public int DoorOrientation;

        public int DoorX;
        public int DoorY;
        public double DoorZ;

        public string Heightmap;

        public int MapSizeX;
        public int MapSizeY;
        public string Name;
        public double[,] SqFloorHeight;
        public int[,] SqSeatRot;

        public SquareState[,] SqState;

        public string StaticFurniMap;

        public RoomModel(string name, int doorX, int doorY, double doorZ, int doorOrientation, string heightmap,
            string staticFurniMap, bool clubOnly)
        {
            Name = name;

            DoorX = doorX;
            DoorY = doorY;
            DoorZ = doorZ;
            DoorOrientation = doorOrientation;

            Heightmap = heightmap.ToLower();
            StaticFurniMap = staticFurniMap;

            var tmpHeightmap = heightmap.Split(Convert.ToChar(13));

            MapSizeX = tmpHeightmap[0].Length;
            MapSizeY = tmpHeightmap.Length;

            ClubOnly = clubOnly;

            SqState = new SquareState[MapSizeX, MapSizeY];
            SqFloorHeight = new double[MapSizeX, MapSizeY];
            SqSeatRot = new int[MapSizeX, MapSizeY];

            for (var y = 0; y < MapSizeY; y++)
            {
                if (y > 0)
                {
                    tmpHeightmap[y] = tmpHeightmap[y].Substring(1);
                }

                for (var x = 0; x < MapSizeX; x++)
                {
                    var square = tmpHeightmap[y].Substring(x, 1).Trim().ToLower();

                    if (square == "x")
                    {
                        SqState[x, y] = SquareState.Blocked;
                    }
                    else if (IsNumeric(square, NumberStyles.Integer))
                    {
                        SqState[x, y] = SquareState.Open;
                        SqFloorHeight[x, y] = double.Parse(square);
                    }
                }
            }

            SqFloorHeight[doorX, doorY] = doorZ;

            // SOHd1017benchQDRBHJHd1019benchSDRBHRAHd1021benchQERBHJHd1023benchSERBHRAHd1117benchQDSBHJHd1119benchSDSBHRAHd1121benchQESBHJHd1123benchSESBHRAHb1132koc_chairPHSBIPAHd1217benchQDPCHJHd1219benchSDPCHRAHd1221benchQEPCHJHd1223benchSEPCHRAHb1231koc_chairSGPCIJHa1232koc_tablePHPCIHHb1233koc_chairQHPCIRAHd1317benchQDQCHJHd1319benchSDQCHRAHd1321benchQEQCHJHd1323benchSEQCHRAHb1325koc_chairQFQCIPAHb1332koc_chairPHQCIHHd1417benchQDRCHJHd1419benchSDRCHRAHd1421benchQERCHJHd1423benchSERCHRAHa1425koc_tableQFRCIHHb1426koc_chairRFRCIRAHd1517benchQDSCHJHd1519benchSDSCHRAHd1521benchQESCHJHd1523benchSESCHRAHb1525koc_chairQFSCIHHb1529koc_chairQGSCIJHa1530koc_tableRGSCIHHb1531koc_chairSGSCIRAHb1630koc_chairRGPDIHHc2425chairf1QFPFIJHc2433chairf1QHPFIRAHd2517benchQDQFHJHd2519benchSDQFHRAHd2521benchQEQFHJHd2523benchSEQFHRAHc2525chairf1QFQFIJHc2533chairf1QHQFIRAHd2617benchQDRFHJHd2619benchSDRFHRAHd2621benchQERFHJHd2623benchSERFHRAHc2625chairf1QFRFIJHc2633chairf1QHRFIRAHd2717benchQDSFHJHd2719benchSDSFHRAHd2721benchQESFHJHd2723benchSESFHRAHd2817benchQDPGHJHd2819benchSDPGHRAHd2821benchQEPGHJHd2823benchSEPGHRAHd2917benchQDQGHJHd2919benchSDQGHRAHd2921benchQEQGHJHd2923benchSEQG`hFFRA

            var tmpFurnimap = staticFurniMap;
            var pointer = 0;

            var num = OldEncoding.DecodeVl64(tmpFurnimap);
            pointer += OldEncoding.EncodeVl64(num).Length;

            for (var i = 0; i < num; i++)
            {
                var junk = OldEncoding.DecodeVl64(tmpFurnimap.Substring(pointer));
                // probably not junk, but dunno what it is
                pointer += OldEncoding.EncodeVl64(junk).Length;

                pointer += 1;

                // probably not junk, but dunno what it is
                pointer += tmpFurnimap.Substring(pointer).Split(Convert.ToChar(2))[0].Length;

                pointer += 1;

                var modelName = tmpFurnimap.Substring(pointer).Split(Convert.ToChar(2))[0];
                pointer += tmpFurnimap.Substring(pointer).Split(Convert.ToChar(2))[0].Length;

                pointer += 1;

                var x = OldEncoding.DecodeVl64(tmpFurnimap.Substring(pointer));
                pointer += OldEncoding.EncodeVl64(x).Length;

                var y = OldEncoding.DecodeVl64(tmpFurnimap.Substring(pointer));
                pointer += OldEncoding.EncodeVl64(y).Length;

                var junk4 = OldEncoding.DecodeVl64(tmpFurnimap.Substring(pointer));
                // probably not junk, but dunno what it is
                pointer += OldEncoding.EncodeVl64(junk4).Length;

                var junk5 = OldEncoding.DecodeVl64(tmpFurnimap.Substring(pointer));
                // probably not junk, but dunno what it is
                pointer += OldEncoding.EncodeVl64(junk5).Length;

                SqState[x, y] = SquareState.Blocked;

                if (!modelName.Contains("bench") && !modelName.Contains("chair") && !modelName.Contains("stool") &&
                    !modelName.Contains("seat") && !modelName.Contains("sofa")) continue;
                SqState[x, y] = SquareState.Seat;
                SqSeatRot[x, y] = junk5;
            }
        }

        public bool IsNumeric(string val, NumberStyles numberStyle)
        {
            double result;
            return double.TryParse(val, numberStyle,
                CultureInfo.CurrentCulture, out result);
        }

        public ServerPacket SerializeHeightmap()
        {
            var heightMap = new StringBuilder();

            foreach (var mapBit in Heightmap.Split("\r\n".ToCharArray()))
            {
                if (mapBit == "")
                {
                    continue;
                }

                heightMap.Append(mapBit);
                heightMap.Append(Convert.ToChar(13));
            }

            var message = new ServerPacket(31);
            message.WriteString(heightMap.ToString());
            return message;
        }

        public ServerPacket SerializeRelativeHeightmap()
        {
            var message = new ServerPacket(470);

            var tmpHeightmap = Heightmap.Split(Convert.ToChar(13));

            for (var y = 0; y < MapSizeY; y++)
            {
                if (y > 0)
                {
                    tmpHeightmap[y] = tmpHeightmap[y].Substring(1);
                }

                for (var x = 0; x < MapSizeX; x++)
                {
                    var square = tmpHeightmap[y].Substring(x, 1).Trim().ToLower();

                    if (DoorX == x && DoorY == y)
                    {
                        square = (int) DoorZ + "";
                    }

                    message.AppendString(square);
                }

                message.AppendString("" + Convert.ToChar(13));
            }

            return message;
        }
    }
}