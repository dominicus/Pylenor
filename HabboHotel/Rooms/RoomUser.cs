﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Commands;
using Pylenor.HabboHotel.Pathfinding;
using Pylenor.HabboHotel.Pets;
using Pylenor.HabboHotel.RoomBots;

namespace Pylenor.HabboHotel.Rooms
{
    public class RoomUser
    {
        public bool AllowOverride;
        public BotAi BotAi;

        public RoomBot BotData;

        public bool CanWalk;

        public int CarryItemId;
        public int CarryTimer;

        public int DanceId;

        public int GoalX;
        public int GoalY;
        public uint HabboId;

        public int IdleTime;
        public bool IsAsleep;

        public bool IsSpectator;

        public bool IsWalking;

        public List<Coord> Path;

        public bool PathRecalcNeeded;
        public int PathRecalcX;
        public int PathRecalcY;
        public int PathStep;

        public Pet PetData;
        public uint RoomId;
        public int RotBody;

        public int RotHead;

        public bool SetStep;
        public int SetX;
        public int SetY;
        public double SetZ;

        public Dictionary<string, string> Statusses;

        public int TeleDelay;
        public bool UpdateNeeded;
        public int VirtualId;

        public int X;
        public int Y;
        public double Z;

        public RoomUser(uint habboId, uint roomId, int virtualId)
        {
            HabboId = habboId;
            RoomId = roomId;
            VirtualId = virtualId;
            IdleTime = 0;
            X = 0;
            Y = 0;
            Z = 0;
            RotHead = 0;
            RotBody = 0;
            UpdateNeeded = true;
            Statusses = new Dictionary<string, string>();
            Path = new List<Coord>();
            PathStep = 0;
            TeleDelay = -1;

            AllowOverride = false;
            CanWalk = true;

            IsSpectator = false;
        }

        public Coord Coordinate => new Coord(X, Y);

        public bool IsPet => (IsBot && BotData.IsPet);

        public bool IsDancing => DanceId >= 1;

        public bool NeedsAutokick => !IsBot && IdleTime >= 1800;

        public bool IsTrading => !IsBot && Statusses.ContainsKey("trd");

        public bool IsBot => BotData != null;

        public void Unidle()
        {
            IdleTime = 0;

            if (!IsAsleep) return;
            IsAsleep = false;

            var message = new ServerPacket(486);
            message.WriteInteger(VirtualId);
            message.WriteBoolean(false);

            GetRoom().SendMessage(message);
        }

        public void Chat(Session session, string message, bool shout)
        {
            Unidle();

            if (!IsBot && GetClient().GetPlayer().Muted)
            {
                GetClient().SendNotif("You are muted.");
                return;
            }

            if (message.StartsWith(":") && session != null && CommandManager.TryExecute(session, message.Substring(1)))
            {
                return;
            }

            var chatMessage = new ServerPacket(shout ? 26u : 24u);
            chatMessage.WriteInteger(VirtualId);
            chatMessage.WriteString(message);
            chatMessage.WriteInteger(GetSpeechEmotion(message));

            GetRoom().TurnHeads(X, Y, HabboId);
            foreach (
                var user in
                    GetRoom()
                        .UserList.Where(user => !user.IsBot && !user.IsPet && user.GetClient() != null)
                        .Where(
                            user =>
                                session != null &&
                                !user.GetClient().GetPlayer().MutedUsers.Contains(session.GetPlayer().Id)))
            {
                user.GetClient().SendPacket(chatMessage);
            }

            if (!IsBot)
            {
                GetRoom().OnUserSay(this, message, shout);
            }

            if (IsBot) return;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("message", message);
                if (session != null)
                    dbClient.ExecuteQuery(
                        "INSERT INTO chatlogs (user_id,room_id,hour,minute,timestamp,message,user_name,full_date) VALUES ('" +
                        session.GetPlayer().Id + "','" + GetRoom().RoomId + "','" + DateTime.Now.Hour + "','" +
                        DateTime.Now.Minute + "','" + PylenorEnvironment.GetUnixTimestamp() + "',@message,'" +
                        session.GetPlayer().Username + "','" + DateTime.Now.ToLongDateString() + "')");
            }
        }

        public int GetSpeechEmotion(string message)
        {
            message = message.ToLower();

            if (message.Contains(":)") || message.Contains(":d") || message.Contains("=]") ||
                message.Contains("=d") || message.Contains(":>"))
            {
                return 1;
            }

            if (message.Contains(">:(") || message.Contains(":@"))
            {
                return 2;
            }

            if (message.Contains(":o") || message.Contains(";o"))
            {
                return 3;
            }

            if (message.Contains(":(") || message.Contains(";<") || message.Contains(":<") || message.Contains("=[") ||
                message.Contains(":'(") || message.Contains("='["))
            {
                return 4;
            }

            return 0;
        }

        public void ClearMovement(bool update)
        {
            IsWalking = false;
            PathRecalcNeeded = false;
            Path = new List<Coord>();
            Statusses.Remove("mv");
            GoalX = 0;
            GoalY = 0;
            SetStep = false;
            SetX = 0;
            SetY = 0;
            SetZ = 0;

            if (update)
            {
                UpdateNeeded = true;
            }
        }

        public void MoveTo(Coord c)
        {
            MoveTo(c.X, c.Y);
        }

        public void MoveTo(int x, int y)
        {
            Unidle();

            PathRecalcNeeded = true;
            PathRecalcX = x;
            PathRecalcY = y;
        }

        public void UnlockWalking()
        {
            AllowOverride = false;
            CanWalk = true;
        }

        public void SetPos(int x, int y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public void CarryItem(int item)
        {
            CarryItemId = item;

            CarryTimer = item > 0 ? 240 : 0;

            var message = new ServerPacket(482);
            message.WriteInteger(VirtualId);
            message.WriteInteger(item);
            GetRoom().SendMessage(message);
        }

        public void SetRot(int rotation)
        {
            SetRot(rotation, false);
        }

        public void SetRot(int rotation, bool headOnly)
        {
            if (Statusses.ContainsKey("lay") || IsWalking)
            {
                return;
            }

            var diff = RotBody - rotation;

            RotHead = RotBody;

            if (Statusses.ContainsKey("sit") || headOnly)
            {
                if (RotBody == 2 || RotBody == 4)
                {
                    if (diff > 0)
                    {
                        RotHead = RotBody - 1;
                    }
                    else if (diff < 0)
                    {
                        RotHead = RotBody + 1;
                    }
                }
                else if (RotBody == 0 || RotBody == 6)
                {
                    if (diff > 0)
                    {
                        RotHead = RotBody - 1;
                    }
                    else if (diff < 0)
                    {
                        RotHead = RotBody + 1;
                    }
                }
            }
            else if (diff <= -2 || diff >= 2)
            {
                RotHead = rotation;
                RotBody = rotation;
            }
            else
            {
                RotHead = rotation;
            }

            UpdateNeeded = true;
        }

        public void AddStatus(string key, string value)
        {
            Statusses[key] = value;
        }

        public void RemoveStatus(string key)
        {
            if (Statusses.ContainsKey(key))
            {
                Statusses.Remove(key);
            }
        }

        public void ResetStatus()
        {
            Statusses = new Dictionary<string, string>();
        }

        public void Serialize(ServerPacket packet)
        {
            // @\Ihqu@UMeth0d13haiihr-893-45.hd-180-8.ch-875-62.lg-280-62.sh-290-62.ca-1813-.he-1601-[IMRAPD4.0JImMcIrDK
            // MSadiePull up a pew and have a brew!hr-500-45.hd-600-1.ch-823-75.lg-716-76.sh-730-62.he-1602-75IRBPA2.0PAK

            if (IsSpectator)
            {
                return;
            }

            if (!IsBot)
            {
                packet.WriteUInt(GetClient().GetPlayer().Id);
                packet.WriteString(GetClient().GetPlayer().Username);
                packet.WriteString(GetClient().GetPlayer().Motto);
                packet.WriteString(GetClient().GetPlayer().Look);
                packet.WriteInteger(VirtualId);
                packet.WriteInteger(X);
                packet.WriteInteger(Y);
                packet.WriteString(Z.ToString(CultureInfo.InvariantCulture).Replace(',', '.'));
                packet.WriteInteger(2);
                packet.WriteInteger(1);
                packet.WriteString(GetClient().GetPlayer().Gender.ToLower());
                packet.WriteInteger(-1);
                packet.WriteInteger(-1);
                packet.WriteInteger(-1);
                packet.WriteString("");
                packet.WriteInteger(0); // R63 fix
            }
            else
            {
                //btmFZoef0 008 D98961JRBQA0.0PAJH
                packet.WriteInteger(BotAi.BaseId);
                packet.WriteString(BotData.Name);
                packet.WriteString(BotData.Motto);
                packet.WriteString(BotData.Look);
                packet.WriteInteger(VirtualId);
                packet.WriteInteger(X);
                packet.WriteInteger(Y);
                packet.WriteString(Z.ToString(CultureInfo.InvariantCulture).Replace(',', '.'));
                packet.WriteInteger(4);
                packet.WriteInteger((BotData.AiType.ToLower() == "pet") ? 2 : 3);

                if (BotData.AiType.ToLower() == "pet")
                {
                    packet.WriteInteger(0);
                }
            }
        }

        public void SerializeStatus(ServerPacket packet)
        {
            if (IsSpectator)
            {
                return;
            }

            packet.WriteInteger(VirtualId);
            packet.WriteInteger(X);
            packet.WriteInteger(Y);
            packet.WriteString(Z.ToString(CultureInfo.InvariantCulture).Replace(',', '.'));
            packet.WriteInteger(RotHead);
            packet.WriteInteger(RotBody);
            packet.AppendString("/");

            foreach (var status in Statusses)
            {
                packet.AppendString(status.Key);
                packet.AppendString(" ");
                packet.AppendString(status.Value);
                packet.AppendString("/");
            }

            packet.WriteString("/");
        }

        public Session GetClient()
        {
            return IsBot ? null : PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(HabboId);
        }

        private Room GetRoom()
        {
            return PylenorEnvironment.GetGame().GetRoomManager().GetRoom(RoomId);
        }
    }
}