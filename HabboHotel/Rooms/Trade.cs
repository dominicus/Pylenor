﻿using System.Collections.Generic;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Items;

namespace Pylenor.HabboHotel.Rooms
{
    public class Trade
    {
        private readonly uint _oneId;
        private readonly uint _roomId;
        private readonly uint _twoId;
        private readonly List<TradeUser> _users;
        private int _tradeStage;

        public Trade(uint userOneId, uint userTwoId, uint roomId)
        {
            _oneId = userOneId;
            _twoId = userTwoId;

            _users = new List<TradeUser>(2) {new TradeUser(userOneId, roomId), new TradeUser(userTwoId, roomId)};
            _tradeStage = 1;
            _roomId = roomId;

            foreach (var user in _users.Where(user => !user.GetRoomUser().Statusses.ContainsKey("trd")))
            {
                user.GetRoomUser().AddStatus("trd", "");
                user.GetRoomUser().UpdateNeeded = true;
            }

            var message = new ServerPacket(104);
            message.WriteUInt(userOneId);
            message.WriteBoolean(true);
            message.WriteUInt(userTwoId);
            message.WriteBoolean(true);
            SendMessageToUsers(message);
        }

        public bool AllUsersAccepted
        {
            get { return _users.All(user => user.HasAccepted); }
        }

        public bool ContainsUser(uint id)
        {
            return _users.Any(user => user.UserId == id);
        }

        public TradeUser GetTradeUser(uint id)
        {
            return _users.FirstOrDefault(user => user.UserId == id);
        }

        public void OfferItem(uint userId, UserItem item)
        {
            var user = GetTradeUser(userId);

            if (user == null || item == null || !item.GetBaseItem().AllowTrade || user.HasAccepted || _tradeStage != 1)
            {
                return;
            }

            ClearAccepted();

            user.OfferedItems.Add(item);
            UpdateTradeWindow();
        }

        public void TakeBackItem(uint userId, UserItem item)
        {
            var user = GetTradeUser(userId);

            if (user == null || item == null || user.HasAccepted || _tradeStage != 1)
            {
                return;
            }

            ClearAccepted();

            user.OfferedItems.Remove(item);
            UpdateTradeWindow();
        }

        public void Accept(uint userId)
        {
            var user = GetTradeUser(userId);

            if (user == null || _tradeStage != 1)
            {
                return;
            }

            user.HasAccepted = true;

            var message = new ServerPacket(109);
            message.WriteUInt(userId);
            message.WriteBoolean(true);
            SendMessageToUsers(message);

            if (!AllUsersAccepted) return;
            SendMessageToUsers(new ServerPacket(111));
            _tradeStage++;
            ClearAccepted();
        }

        public void Unaccept(uint userId)
        {
            var user = GetTradeUser(userId);

            if (user == null || _tradeStage != 1 || AllUsersAccepted)
            {
                return;
            }

            user.HasAccepted = false;

            var message = new ServerPacket(109);
            message.WriteUInt(userId);
            message.WriteBoolean(false);
            SendMessageToUsers(message);
        }

        public void CompleteTrade(uint userId)
        {
            var user = GetTradeUser(userId);

            if (user == null || _tradeStage != 2)
            {
                return;
            }

            user.HasAccepted = true;

            var message = new ServerPacket(109);
            message.WriteUInt(userId);
            message.WriteBoolean(true);
            SendMessageToUsers(message);

            if (!AllUsersAccepted) return;
            _tradeStage = 999;
            DeliverItems();
            CloseTradeClean();
        }

        public void ClearAccepted()
        {
            foreach (var user in _users)
            {
                user.HasAccepted = false;
            }
        }

        public void UpdateTradeWindow()
        {
            var message = new ServerPacket(108);

            foreach (var user in _users)
            {
                message.WriteUInt(user.UserId);
                message.WriteInteger(user.OfferedItems.Count);

                foreach (var item in user.OfferedItems)
                {
                    message.WriteUInt(item.Id);
                    message.WriteString(item.GetBaseItem().Type.ToLower());
                    message.WriteUInt(item.Id);
                    message.WriteInteger(item.GetBaseItem().Sprite);
                    message.WriteBoolean(true);
                    message.WriteBoolean(true);
                    message.WriteString("");
                    message.WriteBoolean(false);
                    // xmas 09 furni had a special furni tag here, with wired day (wat?)
                    message.WriteBoolean(false);
                    // xmas 09 furni had a special furni tag here, wired month (wat?)
                    message.WriteBoolean(false);
                    // xmas 09 furni had a special furni tag here, wired year (wat?)

                    if (item.GetBaseItem().Type.ToLower() == "s")
                    {
                        message.WriteInteger(-1);
                    }
                }
            }

            SendMessageToUsers(message);
        }

        public void DeliverItems()
        {
            // List items
            var itemsOne = GetTradeUser(_oneId).OfferedItems;
            var itemsTwo = GetTradeUser(_twoId).OfferedItems;

            // Verify they are still in user inventory
            if (
                itemsOne.Any(
                    I => GetTradeUser(_oneId).GetClient().GetPlayer().GetInventoryComponent().GetItem(I.Id) == null))
            {
                GetTradeUser(_oneId).GetClient().SendNotif("Trade failed.");
                GetTradeUser(_twoId).GetClient().SendNotif("Trade failed.");

                return;
            }
            if (
                itemsTwo.Any(
                    I => GetTradeUser(_twoId).GetClient().GetPlayer().GetInventoryComponent().GetItem(I.Id) == null))
            {
                GetTradeUser(_oneId).GetClient().SendNotif("Trade failed.");
                GetTradeUser(_twoId).GetClient().SendNotif("Trade failed.");

                return;
            }

            // Deliver them
            foreach (var I in itemsOne)
            {
                GetTradeUser(_oneId).GetClient().GetPlayer().GetInventoryComponent().RemoveItem(I.Id);
                GetTradeUser(_twoId)
                    .GetClient()
                    .GetPlayer()
                    .GetInventoryComponent()
                    .AddItem(I.Id, I.BaseItem, I.ExtraData);
            }
            foreach (var I in itemsTwo)
            {
                GetTradeUser(_twoId).GetClient().GetPlayer().GetInventoryComponent().RemoveItem(I.Id);
                GetTradeUser(_oneId)
                    .GetClient()
                    .GetPlayer()
                    .GetInventoryComponent()
                    .AddItem(I.Id, I.BaseItem, I.ExtraData);
            }

            // Update inventories
            GetTradeUser(_oneId).GetClient().GetPlayer().GetInventoryComponent().UpdateItems(false);
            GetTradeUser(_twoId).GetClient().GetPlayer().GetInventoryComponent().UpdateItems(false);
        }

        public void CloseTradeClean()
        {
            foreach (var user in _users)
            {
                user.GetRoomUser().RemoveStatus("trd");
                user.GetRoomUser().UpdateNeeded = true;
            }

            SendMessageToUsers(new ServerPacket(112));
            GetRoom().ActiveTrades.Remove(this);
        }

        public void CloseTrade(uint userId)
        {
            foreach (var user in _users.Where(user => user.GetRoomUser() != null))
            {
                user.GetRoomUser().RemoveStatus("trd");
                user.GetRoomUser().UpdateNeeded = true;
            }
            var message = new ServerPacket(110);
            message.WriteUInt(userId);
            SendMessageToUsers(message);
        }

        public void SendMessageToUsers(ServerPacket packet)
        {
            foreach (var user in _users)
            {
                user.GetClient().SendPacket(packet);
            }
        }

        private Room GetRoom()
        {
            return PylenorEnvironment.GetGame().GetRoomManager().GetRoom(_roomId);
        }
    }

    public class TradeUser
    {
        private readonly uint _roomId;
        public List<UserItem> OfferedItems;
        public uint UserId;

        public TradeUser(uint userId, uint roomId)
        {
            UserId = userId;
            _roomId = roomId;
            HasAccepted = false;
            OfferedItems = new List<UserItem>();
        }

        public bool HasAccepted { get; set; }

        public RoomUser GetRoomUser()
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(_roomId);

            return room?.GetRoomUserByHabbo(UserId);
        }

        public Session GetClient()
        {
            return PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(UserId);
        }
    }
}