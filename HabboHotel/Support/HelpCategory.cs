﻿namespace Pylenor.HabboHotel.Support
{
    internal class HelpCategory
    {
        public string Caption;

        public HelpCategory(uint id, string caption)
        {
            CategoryId = id;
            Caption = caption;
        }

        public uint CategoryId { get; }
    }
}