﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Support
{
    internal class HelpTool
    {
        public Dictionary<uint, HelpCategory> Categories;

        public List<HelpTopic> ImportantTopics;
        public List<HelpTopic> KnownIssues;
        public Dictionary<uint, HelpTopic> Topics;

        public HelpTool()
        {
            Categories = new Dictionary<uint, HelpCategory>();
            Topics = new Dictionary<uint, HelpTopic>();

            ImportantTopics = new List<HelpTopic>();
            KnownIssues = new List<HelpTopic>();
        }

        public void LoadCategories(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading help subjects");
            Categories.Clear();
            var categoryData = dbClient.ReadDataTable("SELECT * FROM help_subjects;");

            if (categoryData == null)
            {
                return;
            }

            foreach (DataRow row in categoryData.Rows)
            {
                Categories.Add((uint) row["id"], new HelpCategory((uint) row["id"], (string) row["caption"]));
            }
            Console.WriteLine("...completed!");
        }

        public HelpCategory GetCategory(uint categoryId)
        {
            return Categories.ContainsKey(categoryId) ? Categories[categoryId] : null;
        }

        public void ClearCategories()
        {
            Categories.Clear();
        }

        public void LoadTopics(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading help topics");
            Topics.Clear();

            var topicData = dbClient.ReadDataTable("SELECT * FROM help_topics;");

            if (topicData == null)
            {
                return;
            }

            foreach (DataRow row in topicData.Rows)
            {
                var newTopic = new HelpTopic((uint) row["id"], (string) row["title"], (string) row["body"],
                    (uint) row["subject"]);

                Topics.Add((uint) row["id"], newTopic);

                var importance = int.Parse(row["known_issue"].ToString());

                if (importance != 1)
                {
                    if (importance == 2)
                    {
                        ImportantTopics.Add(newTopic);
                    }
                }
                else
                {
                    KnownIssues.Add(newTopic);
                }
            }
            Console.WriteLine("...completed!");
        }

        public HelpTopic GetTopic(uint topicId)
        {
            return Topics.ContainsKey(topicId) ? Topics[topicId] : null;
        }

        public void ClearTopics()
        {
            Topics.Clear();

            ImportantTopics.Clear();
            KnownIssues.Clear();
        }

        public int ArticlesInCategory(uint categoryId)
        {
            return Topics.Values.Count(topic => topic.CategoryId == categoryId);
        }

        public ServerPacket SerializeFrontpage()
        {
            var frontpage = new ServerPacket(518);
            frontpage.WriteInteger(ImportantTopics.Count);

            foreach (var topic in ImportantTopics)
            {
                frontpage.WriteUInt(topic.TopicId);
                frontpage.WriteString(topic.Caption);
            }

            frontpage.WriteInteger(KnownIssues.Count);

            foreach (var issue in KnownIssues)
            {
                frontpage.WriteUInt(issue.TopicId);
                frontpage.WriteString(issue.Caption);
            }

            return frontpage;
        }

        public ServerPacket SerializeIndex()
        {
            var index = new ServerPacket(519);
            index.WriteInteger(Categories.Count);

            foreach (var category in Categories.Values)
            {
                index.WriteUInt(category.CategoryId);
                index.WriteString(category.Caption);
                index.WriteInteger(ArticlesInCategory(category.CategoryId));
            }

            return index;
        }

        public ServerPacket SerializeTopic(HelpTopic topic)
        {
            var top = new ServerPacket(520);
            top.WriteUInt(topic.TopicId);
            top.WriteString(topic.Body);
            return top;
        }

        public ServerPacket SerializeSearchResults(string query)
        {
            DataTable results;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("query", query);
                results = dbClient.ReadDataTable(
                    "SELECT id,title FROM help_topics WHERE title LIKE @query OR body LIKE @query LIMIT 25;");
            }
            var search = new ServerPacket(521);

            if (results == null)
            {
                search.WriteBoolean(false);
                return search;
            }

            search.WriteInteger(results.Rows.Count);

            foreach (DataRow row in results.Rows)
            {
                search.WriteUInt((uint) row["id"]);
                search.WriteString((string) row["title"]);
            }

            return search;
        }

        public ServerPacket SerializeCategory(HelpCategory category)
        {
            var cat = new ServerPacket(522);
            cat.WriteUInt(category.CategoryId);
            cat.WriteString("");
            cat.WriteInteger(ArticlesInCategory(category.CategoryId));

            foreach (var topic in Topics.Values.Where(topic => topic.CategoryId == category.CategoryId))
            {
                cat.WriteUInt(topic.TopicId);
                cat.WriteString(topic.Caption);
            }
            return cat;
        }
    }
}