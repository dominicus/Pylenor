﻿namespace Pylenor.HabboHotel.Support
{
    internal class HelpTopic
    {
        public string Body;

        public string Caption;

        public uint CategoryId;

        public HelpTopic(uint id, string caption, string body, uint categoryId)
        {
            TopicId = id;
            Caption = caption;
            Body = body;
            CategoryId = categoryId;
        }

        public uint TopicId { get; }
    }
}