﻿namespace Pylenor.HabboHotel.Support
{
    internal enum ModerationBanType
    {
        Ip = 0,
        Username = 1
    }

    internal class ModerationBan
    {
        public double Expire;
        public string ReasonMessage;
        public ModerationBanType Type;
        public string Variable;

        public ModerationBan(ModerationBanType type, string variable, string reasonMessage, double expire)
        {
            Type = type;
            Variable = variable;
            ReasonMessage = reasonMessage;
            Expire = expire;
        }

        public bool Expired => PylenorEnvironment.GetUnixTimestamp() >= Expire;
    }
}