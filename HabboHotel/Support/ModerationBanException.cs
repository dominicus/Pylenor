﻿using System;

namespace Pylenor.HabboHotel.Support
{
    public class ModerationBanException : Exception
    {
        public ModerationBanException(string reason) : base(reason)
        {
        }
    }
}