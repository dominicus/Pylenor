﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Sessions;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Support
{
    internal class ModerationBanManager
    {
        public List<ModerationBan> Bans;

        public ModerationBanManager()
        {
            Bans = new List<ModerationBan>();
        }

        public void LoadBans(DatabaseClient dbClient)
        {
            PylenorEnvironment.GetLogging().Write("Loading bans");
            Bans.Clear();

            var banData = dbClient.ReadDataTable("SELECT bantype,value,reason,expire FROM bans WHERE expire > '" +
                                                 PylenorEnvironment.GetUnixTimestamp() + "';");

            if (banData == null)
            {
                return;
            }

            foreach (DataRow row in banData.Rows)
            {
                var type = ModerationBanType.Ip;

                if ((string) row["bantype"] == "user")
                {
                    type = ModerationBanType.Username;
                }

                Bans.Add(new ModerationBan(type, (string) row["value"], (string) row["reason"], (double) row["expire"]));
            }
            Console.WriteLine("...completed!");
        }

        public void CheckForBanConflicts(Session client)
        {
            foreach (var ban in Bans.Where(ban => !ban.Expired))
            {
                if (ban.Type == ModerationBanType.Ip && client.IpAddress == ban.Variable)
                {
                    throw new ModerationBanException(ban.ReasonMessage);
                }

                if (client.GetPlayer() == null) continue;
                if (ban.Type == ModerationBanType.Username &&
                    string.Equals(client.GetPlayer().Username, ban.Variable, StringComparison.CurrentCultureIgnoreCase))
                {
                    throw new ModerationBanException(ban.ReasonMessage);
                }
            }
        }

        // PENDING REWRITE
        public void BanUser(Session client, string moderator, double lengthSeconds, string reason, bool ipBan)
        {
            var type = ModerationBanType.Username;
            var var = client.GetPlayer().Username;
            var rawVar = "user";
            var expire = PylenorEnvironment.GetUnixTimestamp() + lengthSeconds;

            if (ipBan)
            {
                type = ModerationBanType.Ip;
                var = client.IpAddress;
                rawVar = "ip";
            }

            Bans.Add(new ModerationBan(type, var, reason, expire));
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("INSERT INTO bans (bantype,value,reason,expire,added_by,added_date) VALUES ('" +
                                      rawVar + "','" + var + "','" + reason + "','" + expire + "','" + moderator +
                                      "','" +
                                      DateTime.Now.ToLongDateString() + "');");
            }

            if (ipBan)
            {
                DataTable usersAffected;
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    usersAffected = dbClient.ReadDataTable("SELECT id FROM users WHERE ip_last = '" + var + "';");
                }
                if (usersAffected != null)
                {
                    foreach (DataRow row in usersAffected.Rows)
                    {
                        using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                        {
                            dbClient.ExecuteQuery("UPDATE user_info SET bans = bans + 1 WHERE user_id = '" +
                                                  (uint) row["id"] + "' LIMIT 1;");
                        }
                    }
                }
            }
            else
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("UPDATE user_info SET bans = bans + 1 WHERE user_id = '" +
                                          client.GetPlayer().Id +
                                          "' LIMIT 1");
                }
            }

            client.SendBanMessage("You have been banned: " + reason);
            client.Disconnect();
        }
    }
}