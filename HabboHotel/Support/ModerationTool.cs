﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;
using Pylenor.Storage;

namespace Pylenor.HabboHotel.Support
{
    internal class ModerationTool
    {
        #region Message Presets

        public void LoadMessagePresets(DatabaseClient db)
        {
            UserMessagePresets.Clear();
            RoomMessagePresets.Clear();

            var data = db.ReadDataTable("SELECT type,message FROM moderation_presets WHERE enabled = '1';");

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                var message = (string) row["message"];

                switch (row["type"].ToString().ToLower())
                {
                    case "message":
                        UserMessagePresets.Add(message);
                        break;
                    case "roommessage":
                        RoomMessagePresets.Add(message);
                        break;
                    default:
                        Console.WriteLine((row["type"]) + " type not found! [#MODERATIONTOOL]");
                        break;
                }
            }
        }

        #endregion

        #region General

        private readonly List<SupportTicket> _tickets;

        public List<string> UserMessagePresets;
        public List<string> RoomMessagePresets;

        public ModerationTool()
        {
            _tickets = new List<SupportTicket>();
            UserMessagePresets = new List<string>();
            RoomMessagePresets = new List<string>();
        }

        public ServerPacket SerializeTool()
        {
            var message = new ServerPacket(531);
            message.WriteInteger(-1);
            message.WriteInteger(UserMessagePresets.Count);

            foreach (var preset in UserMessagePresets)
            {
                message.WriteString(preset);
            }

            message.WriteUInt(6); // Amount of Mod Actions

            message.WriteString("Room Problems"); // modaction Cata
            message.WriteUInt(8); // ModAction Count
            message.WriteString("Door Blocking"); // mod action Cata
            message.WriteString("Please stop blocking the doors."); // Msg
            message.WriteString("Ban Last Warning"); // Mod Action Cata
            message.WriteString("This is your last warning or you will be banned."); // Msg
            message.WriteString("Player Support Issue"); // Mod Action Cata
            message.WriteString("Please contact player support for this issue, thank you."); // Msg
            message.WriteString("Filter Bypass"); // Mod Cata
            message.WriteString("Please stop bypassing the filter, this is your only warning."); // Msg
            message.WriteString("Kick Abuse"); // Mod Cata
            message.WriteString("Please stop kicking users without a valid reason, thank you."); // Msg
            message.WriteString("Ban Room"); // Mod Cata
            message.WriteString("Please stop banning people without a good reason"); // Msg
            message.WriteString("Unacceptable Room Name"); // Mod Cata
            message.WriteString("Your room name is unacceptable, please change it or expect further action.");
            // Msg
            message.WriteString("Items not loading"); // Mod Cata
            message.WriteString("Please contact an administrator for this issue."); // Msg

            message.WriteString("Player Support"); // modaction Cata
            message.WriteUInt(8); // ModAction Count
            message.WriteString("Bug Report"); // mod action Cata
            message.WriteString("Thanks for reporting this bug, we appreciate your help."); // Msg
            message.WriteString("Login Problem"); // Mod Action Cata
            message.WriteString("We will contact an administrator and work on fixing your issue."); // Msg
            message.WriteString("Help Support"); // Mod Action Cata
            message.WriteString("Please Contact The Player Support For this problem"); // Msg
            message.WriteString("Call for Help Misuse"); // Mod Cata
            message.WriteString("Please do not abuse the Call for Help, it is for genuine use only."); // Msg
            message.WriteString("Privacy"); // Mod Cata
            message.WriteString("Please do not give out your personal details."); // Msg
            message.WriteString("Technical Issue"); // Mod Cata
            message.WriteString("Please contact an administrator with your technical issue."); // Msg
            message.WriteString("Cache Issue"); // Mod Cata
            message.WriteString("Please reload your client and try again, thank you."); // Msg
            message.WriteString("Other Issues"); // Mod Cata
            message.WriteString("Please reload your client and try again, thank you."); // Msg

            message.WriteString("Users Problems"); // modaction Cata
            message.WriteUInt(8); // ModAction Count
            message.WriteString("Scamming"); // mod action Cata
            message.WriteString("We cannot monitor scamming within the hotel, please becareful next time.");
            // Msg
            message.WriteString("Trading Scam"); // Mod Action Cata
            message.WriteString("We cannot monitor trading within the hotel, please becareful next time.");
            // Msg
            message.WriteString("Disconnection"); // Mod Action Cata
            message.WriteString("Please contact an administrator with your issue."); // Msg
            message.WriteString("Room Blocking"); // Mod Cata
            message.WriteString("Please send a Call for Help with the users name, thank you."); // Msg
            message.WriteString("Freezing"); // Mod Cata
            message.WriteString("Please reload the client and try again, thank you."); // Msg
            message.WriteString("Website Issue"); // Mod Cata
            message.WriteString("Please contact an administrator with the error, thank you."); // Msg
            message.WriteString("Login Issue"); // Mod Cata
            message.WriteString("Please say the users name and we will look into it, thank you."); // Msg
            message.WriteString("Updates"); // Mod Cata
            message.WriteString("We always try to maintain the best updates."); // Msg

            message.WriteString("Debug Problems"); // modaction Cata
            message.WriteUInt(8); // ModAction Count
            message.WriteString("Lag"); // mod action Cata
            message.WriteString("We are now looking into this issue, thank you."); // Msg
            message.WriteString("Disconnection"); // Mod Action Cata
            message.WriteString("What happens and how happens please explain us"); // Msg
            message.WriteString("SSO Problem"); // Mod Action Cata
            message.WriteString("Please logout of the website and then log back in, thank you."); // Msg
            message.WriteString("Char Filter"); // Mod Cata
            message.WriteString("Can you say the users name and explain to us what happens."); // Msg
            message.WriteString("System check"); // Mod Cata
            message.WriteString("We are already looking into this issue, thank you."); // Msg
            message.WriteString("Missing Feature"); // Mod Cata
            message.WriteString("We are working on this feature sometime into the near future, thank you.");
            // Msg
            message.WriteString("Feature Error"); // Mod Cata
            message.WriteString("We are working on fixing this issue, thank you."); // Msg
            message.WriteString("Flash Player Issue"); // Mod Cata
            message.WriteString("We recommend you try installing flash player again, thank you."); // Msg

            message.WriteString("Unused Category"); // Category
            message.WriteUInt(8); // Amount of Sub-category
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message

            message.WriteString("Unused Category"); // Category
            message.WriteUInt(8); // Amount of Sub-category
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message
            message.WriteString("Example Category"); // Sub-category
            message.WriteString("This is an example message."); // Preset message

            message.WriteString("");
            message.WriteString("");
            message.WriteString("");
            message.WriteString("");
            message.WriteString("");
            message.WriteString("");
            message.WriteString("");

            message.WriteInteger(RoomMessagePresets.Count);

            foreach (var preset in RoomMessagePresets)
            {
                message.WriteString(preset);
            }
            message.WriteString("");
            return message;
        }

        #endregion

        #region Support Tickets

        public void LoadPendingTickets(DatabaseClient db)
        {
            _tickets.Clear();

            var data = db.ReadDataTable(
                "SELECT id,score,type,status,sender_id,reported_id,moderator_id,message,room_id,room_name,timestamp FROM moderation_tickets WHERE status = 'open' OR status = 'picked';");

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                var ticket = new SupportTicket((uint) row["id"], (int) row["score"], (int) row["type"],
                    (uint) row["sender_id"], (uint) row["reported_id"], (string) row["message"], (uint) row["room_id"],
                    (string) row["room_name"], (double) row["timestamp"]);

                if (row["status"].ToString().ToLower() == "picked")
                {
                    ticket.Pick((uint) row["moderator_id"], false);
                }

                _tickets.Add(ticket);
            }
        }

        public void SendNewTicket(Session session, int category, uint reportedUser, string message)
        {
            if (session.GetPlayer().CurrentRoomId <= 0)
            {
                return;
            }
            var database = PylenorEnvironment.GetDatabase().GetClient();

            var data =
                PylenorEnvironment.GetGame()
                    .GetRoomManager()
                    .GenerateNullableRoomData(session.GetPlayer().CurrentRoomId);

            database.AddParamWithValue("message", message);
            database.AddParamWithValue("name", data.Name);

            database.ExecuteQuery(
                "INSERT INTO moderation_tickets (score,type,status,sender_id,reported_id,moderator_id,message,room_id,room_name,timestamp) VALUES (1,'" +
                category + "','open','" + session.GetPlayer().Id + "','" + reportedUser + "','0',@message,'" + data.Id +
                "',@name,'" + PylenorEnvironment.GetUnixTimestamp() + "');");
            database.ExecuteQuery("UPDATE user_info SET cfhs = cfhs + 1 WHERE user_id = '" + session.GetPlayer().Id +
                                  "' LIMIT 1;");

            var ticketId = (uint)
                database.ReadDataRow("SELECT id FROM moderation_tickets WHERE sender_id = '" + session.GetPlayer().Id +
                                     "' ORDER BY id DESC LIMIT 1;")[0];

            var ticket = new SupportTicket(ticketId, 1, category, session.GetPlayer().Id, reportedUser, message, data.Id,
                data.Name, PylenorEnvironment.GetUnixTimestamp());

            _tickets.Add(ticket);

            SendTicketToModerators(ticket);
        }

        public void SendOpenTickets(Session session)
        {
            foreach (
                var ticket in
                    _tickets.Where(ticket => ticket.Status != TicketStatus.Open && ticket.Status != TicketStatus.Picked)
                )
            {
                session.SendPacket(ticket.Serialize());
            }
        }

        public SupportTicket GetTicket(uint ticketId)
        {
            return _tickets.FirstOrDefault(ticket => ticket.TicketId == ticketId);
        }

        public void PickTicket(Session session, uint ticketId)
        {
            var ticket = GetTicket(ticketId);

            if (ticket == null || ticket.Status != TicketStatus.Open)
            {
                return;
            }

            ticket.Pick(session.GetPlayer().Id, true);
            SendTicketToModerators(ticket);
        }

        public void ReleaseTicket(Session session, uint ticketId)
        {
            var ticket = GetTicket(ticketId);

            if (ticket == null || ticket.Status != TicketStatus.Picked || ticket.ModeratorId != session.GetPlayer().Id)
            {
                return;
            }

            ticket.Release(true);
            SendTicketToModerators(ticket);
        }

        public void CloseTicket(Session session, uint ticketId, int result)
        {
            var ticket = GetTicket(ticketId);

            if (ticket == null || ticket.Status != TicketStatus.Picked || ticket.ModeratorId != session.GetPlayer().Id)
            {
                return;
            }

            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(ticket.SenderId);

            TicketStatus newStatus;
            int resultCode;

            switch (result)
            {
                case 1:

                    resultCode = 1;
                    newStatus = TicketStatus.Invalid;
                    break;

                case 2:

                    resultCode = 2;
                    newStatus = TicketStatus.Abusive;
                    using (var db = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        db.ExecuteQuery("UPDATE user_info SET cfhs_abusive = cfhs_abusive + 1 WHERE user_id = '" +
                                        ticket.SenderId + "' LIMIT 1");
                    }
                    break;

                case 3:
                default:

                    resultCode = 0;
                    newStatus = TicketStatus.Resolved;
                    break;
            }

            if (client != null)
            {
                var closeIssue = new ServerPacket(540);
                closeIssue.WriteInteger(resultCode);
                client.SendPacket(closeIssue);
            }

            ticket.Close(newStatus, true);
            SendTicketToModerators(ticket);
        }

        public bool UsersHasPendingTicket(uint id)
        {
            return _tickets.Any(ticket => ticket.SenderId == id && ticket.Status == TicketStatus.Open);
        }

        public void DeletePendingTicketForUser(uint id)
        {
            foreach (var ticket in _tickets.Where(ticket => ticket.SenderId == id))
            {
                ticket.Delete(true);
                SendTicketToModerators(ticket);
                return;
            }
        }

        public void SendTicketToModerators(SupportTicket ticket)
        {
            PylenorEnvironment.GetGame().GetClientManager().BroadcastMessage(ticket.Serialize(), "fuse_mod");
        }

        #endregion

        #region Room Moderation

        public void PerformRoomAction(Session modSession, uint roomId, bool kickUsers, bool lockRoom,
            bool inappropriateRoom)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(roomId);

            if (room == null)
            {
                return;
            }

            if (lockRoom)
            {
                room.State = 1;

                using (var db = PylenorEnvironment.GetDatabase().GetClient())
                {
                    db.ExecuteQuery("UPDATE rooms SET state = 'locked' WHERE id = '" + room.RoomId + "' LIMIT 1");
                }
            }

            if (inappropriateRoom)
            {
                room.Name = "Inappropriate to Hotel Managament";
                room.Description = "Inappropriate to Hotel Management";
                room.Tags.Clear();

                using (var db = PylenorEnvironment.GetDatabase().GetClient())
                {
                    db.ExecuteQuery(
                        "UPDATE rooms SET caption = 'Inappropriate to Hotel Management', description = 'Inappropriate to Hotel Management', tags = '' WHERE id = '" +
                        room.RoomId + "' LIMIT 1");
                }
            }

            if (!kickUsers) return;
            var toRemove =
                room.UserList.Where(
                    user => !user.IsBot && user.GetClient().GetPlayer().Rank < modSession.GetPlayer().Rank)
                    .ToList();

            foreach (var remove in toRemove)
            {
                room.RemoveUserFromRoom(remove.GetClient(), true, false);
            }
        }

        public void RoomAlert(uint roomId, bool caution, string message)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(roomId);

            if (room == null || message.Length <= 1)
            {
                return;
            }

            var queryBuilder = new StringBuilder();
            var j = 0;

            foreach (var user in room.UserList.Where(user => !user.IsBot))
            {
                user.GetClient().SendNotif(message, caution);

                if (j > 0)
                {
                    queryBuilder.Append(" OR ");
                }

                queryBuilder.Append("user_id = '" + user.GetClient().GetPlayer().Id + "'");
                j++;
            }

            if (caution)
            {
                using (var db = PylenorEnvironment.GetDatabase().GetClient())
                {
                    db.ExecuteQuery("UPDATE user_info SET cautions = cautions + 1 WHERE " + queryBuilder + " LIMIT " + j);
                }
            }
        }

        public ServerPacket SerializeRoomTool(RoomData data)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(data.Id);
            uint ownerId = 0;
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                try
                {
                    ownerId =
                        (uint) db.ReadDataRow("SELECT id FROM users WHERE username = '" + data.Owner + "' LIMIT 1")[0];
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            var message = new ServerPacket(538);
            message.WriteUInt(data.Id);
            message.WriteInteger(data.UsersNow); // user count

            if (room != null)
            {
                message.WriteBoolean((room.GetRoomUserByHabbo(data.Owner) != null));
            }
            else
            {
                message.WriteBoolean(false);
            }

            message.WriteUInt(ownerId);
            message.WriteString(data.Owner);
            message.WriteUInt(data.Id);
            message.WriteString(data.Name);
            message.WriteString(data.Description);
            message.WriteInteger(data.TagCount);

            foreach (var tag in data.Tags)
            {
                message.WriteString(tag);
            }

            if (room != null)
            {
                message.WriteBoolean(room.HasOngoingEvent);

                if (room.Event != null)
                {
                    message.WriteString(room.Event.Name);
                    message.WriteString(room.Event.Description);
                    message.WriteInteger(room.Event.Tags.Count);

                    foreach (var tag in room.Event.Tags)
                    {
                        message.WriteString(tag);
                    }
                }
            }
            else
            {
                message.WriteBoolean(false);
            }

            return message;
        }

        #endregion

        #region User Moderation

        public void KickUser(Session modSession, uint userId, string message, bool soft)
        {
            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(userId);

            if (client == null || client.GetPlayer().CurrentRoomId < 1 ||
                client.GetPlayer().Id == modSession.GetPlayer().Id)
            {
                return;
            }

            if (client.GetPlayer().Rank >= modSession.GetPlayer().Rank)
            {
                modSession.SendNotif("You do not have permission to kick that user.");
                return;
            }

            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(client.GetPlayer().CurrentRoomId);

            if (room == null)
            {
                return;
            }

            room.RemoveUserFromRoom(client, true, false);

            if (soft) return;
            client.SendNotif(message);
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                db.ExecuteQuery("UPDATE user_info SET cautions = cautions + 1 WHERE user_id = '" + userId + "' LIMIT 1");
            }
        }

        public void AlertUser(Session modSession, uint userId, string message, bool caution)
        {
            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(userId);

            if (client == null || client.GetPlayer().Id == modSession.GetPlayer().Id)
            {
                return;
            }

            if (caution && client.GetPlayer().Rank >= modSession.GetPlayer().Rank)
            {
                modSession.SendNotif(
                    "You do not have permission to caution that user, sending as a regular message instead.");
                caution = false;
            }

            client.SendNotif(message, caution);

            if (!caution) return;
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                db.ExecuteQuery("UPDATE user_info SET cautions = cautions + 1 WHERE user_id = '" + userId +
                                "' LIMIT 1");
            }
        }

        public void BanUser(Session modSession, uint userId, int length, string message)
        {
            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(userId);

            if (client == null || client.GetPlayer().Id == modSession.GetPlayer().Id)
            {
                return;
            }

            if (client.GetPlayer().Rank >= modSession.GetPlayer().Rank)
            {
                modSession.SendNotif("You do not have permission to ban that user.");
                return;
            }

            double dLength = length;

            PylenorEnvironment.GetGame()
                .GetBanManager()
                .BanUser(client, modSession.GetPlayer().Username, dLength, message, false);
        }

        #endregion

        #region User Info

        public ServerPacket SerializeUserInfo(uint userId)
        {
            DataRow user;
            DataRow info;

            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                user = db.ReadDataRow("SELECT * FROM users WHERE id = '" + userId + "' LIMIT 1");
            }

            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                info = db.ReadDataRow("SELECT * FROM user_info WHERE user_id = '" + userId + "' LIMIT 1");
            }

            if (user == null)
            {
                throw new ArgumentException();
            }

            var message = new ServerPacket(533);

            message.WriteUInt((uint) user["id"]);
            message.WriteString((string) user["username"]);

            if (info != null)
            {
                message.WriteInteger(
                    (int) Math.Ceiling((PylenorEnvironment.GetUnixTimestamp() - (double) info["reg_timestamp"])/60));
                message.WriteInteger(
                    (int) Math.Ceiling((PylenorEnvironment.GetUnixTimestamp() - (double) info["login_timestamp"])/60));
            }
            else
            {
                message.WriteInteger(0);
                message.WriteInteger(0);
            }

            message.WriteBoolean(user["online"].ToString() == "1");

            if (info != null)
            {
                message.WriteInteger((int) info["cfhs"]);
                message.WriteInteger((int) info["cfhs_abusive"]);
                message.WriteInteger((int) info["cautions"]);
                message.WriteInteger((int) info["bans"]);
            }
            else
            {
                message.WriteInteger(0); // cfhs
                message.WriteInteger(0); // abusive cfhs
                message.WriteInteger(0); // cautions
                message.WriteInteger(0); // bans
            }

            return message;
        }

        public ServerPacket SerializeRoomVisits(uint userId)
        {
            DataTable data;
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = db.ReadDataTable("SELECT room_id,hour,minute FROM user_roomvisits WHERE user_id = '" + userId +
                                        "' ORDER BY entry_timestamp DESC LIMIT 50;");
            }
            var message = new ServerPacket(537);
            message.WriteUInt(userId);
            message.WriteString(PylenorEnvironment.GetGame().GetClientManager().GetNameById(userId));

            if (data != null)
            {
                message.WriteInteger(data.Rows.Count);
                foreach (DataRow row in data.Rows)
                {
                    var roomData =
                        PylenorEnvironment.GetGame().GetRoomManager().GenerateNullableRoomData((uint) row["room_id"]);

                    message.WriteBoolean(roomData.IsPublicRoom);
                    message.WriteUInt(roomData.Id);
                    message.WriteString(roomData.Name);
                    message.WriteInteger((int) row["hour"]);
                    message.WriteInteger((int) row["minute"]);
                }
            }
            else
            {
                message.WriteInteger(0);
            }

            return message;
        }

        #endregion

        #region Chatlogs

        public ServerPacket SerializeUserChatlog(uint userId)
        {
            DataTable visits;
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                visits = db.ReadDataTable(
                    "SELECT room_id,entry_timestamp,exit_timestamp FROM user_roomvisits WHERE user_id = '" + userId +
                    "' ORDER BY entry_timestamp DESC LIMIT 5;");
            }
            var message = new ServerPacket(536);
            message.WriteUInt(userId);
            message.WriteString(PylenorEnvironment.GetGame().GetClientManager().GetNameById(userId));

            if (visits != null)
            {
                message.WriteInteger(visits.Rows.Count);

                foreach (DataRow visit in visits.Rows)
                {
                    if ((double) visit["exit_timestamp"] <= 0.0)
                    {
                        visit["exit_timestamp"] = PylenorEnvironment.GetUnixTimestamp();
                    }
                    DataTable chatlogs;
                    using (var db = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        chatlogs = db.ReadDataTable(
                            "SELECT user_id,user_name,hour,minute,message FROM chatlogs WHERE room_id = '" +
                            (uint) visit["room_id"] + "' AND timestamp > '" + (double) visit["entry_timestamp"] +
                            "' AND timestamp < '" + (double) visit["exit_timestamp"] + "' ORDER BY timestamp DESC");
                    }
                    var roomData =
                        PylenorEnvironment.GetGame().GetRoomManager().GenerateNullableRoomData((uint) visit["room_id"]);

                    message.WriteBoolean(roomData.IsPublicRoom);
                    message.WriteUInt(roomData.Id);
                    message.WriteString(roomData.Name);

                    if (chatlogs != null)
                    {
                        message.WriteInteger(chatlogs.Rows.Count);
                        foreach (DataRow log in chatlogs.Rows)
                        {
                            message.WriteInteger((int) log["hour"]);
                            message.WriteInteger((int) log["minute"]);
                            message.WriteUInt((uint) log["user_id"]);
                            message.WriteString((string) log["user_name"]);
                            message.WriteString((string) log["message"]);
                        }
                    }
                    else
                    {
                        message.WriteInteger(0);
                    }
                }
            }
            else
            {
                message.WriteInteger(0);
            }

            return message;
        }

        public ServerPacket SerializeTicketChatlog(SupportTicket ticket, RoomData roomData, double timestamp)
        {
            DataTable data;
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = db.ReadDataTable("SELECT user_id,user_name,hour,minute,message FROM chatlogs WHERE room_id = '" +
                                        roomData.Id + "' AND timestamp >= '" + (timestamp - 300) +
                                        "' AND timestamp <= '" +
                                        timestamp + "' ORDER BY timestamp DESC;");
            }
            var message = new ServerPacket(534);
            message.WriteUInt(ticket.TicketId);
            message.WriteUInt(ticket.SenderId);
            message.WriteUInt(ticket.ReportedId);
            message.WriteBoolean(roomData.IsPublicRoom);
            message.WriteUInt(roomData.Id);
            message.WriteString(roomData.Name);

            if (data != null)
            {
                message.WriteInteger(data.Rows.Count);

                foreach (DataRow row in data.Rows)
                {
                    message.WriteInteger((int) row["hour"]);
                    message.WriteInteger((int) row["minute"]);
                    message.WriteUInt((uint) row["user_id"]);
                    message.WriteString((string) row["user_name"]);
                    message.WriteString((string) row["message"]);
                }
            }
            else
            {
                message.WriteInteger(0);
            }

            return message;
        }

        public ServerPacket SerializeRoomChatlog(uint roomId)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(roomId);

            if (room == null)
            {
                throw new ArgumentException();
            }

            var isPublic = room.Type.ToLower() == "public";
            DataTable data;
            using (var db = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = db.ReadDataTable("SELECT user_id,user_name,hour,minute,message FROM chatlogs WHERE room_id = '" +
                                        room.RoomId + "' ORDER BY timestamp DESC LIMIT 150;");
            }
            var message = new ServerPacket(535);
            message.WriteBoolean(isPublic);
            message.WriteUInt(room.RoomId);
            message.WriteString(room.Name);

            if (data != null)
            {
                message.WriteInteger(data.Rows.Count);

                foreach (DataRow row in data.Rows)
                {
                    message.WriteInteger((int) row["hour"]);
                    message.WriteInteger((int) row["minute"]);
                    message.WriteUInt((uint) row["user_id"]);
                    message.WriteString((string) row["user_name"]);
                    message.WriteString((string) row["message"]);
                }
            }
            else
            {
                message.WriteInteger(0);
            }

            return message;
        }

        #endregion
    }
}