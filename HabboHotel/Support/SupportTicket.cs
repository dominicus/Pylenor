﻿using System;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Support
{
    internal enum TicketStatus
    {
        Open = 0,
        Picked = 1,
        Resolved = 2,
        Abusive = 3,
        Invalid = 4,
        Deleted = 5
    }

    internal class SupportTicket
    {
        public string Message;
        public uint ModeratorId;
        public uint ReportedId;

        public uint RoomId;
        public string RoomName;
        public int Score;

        public uint SenderId;

        public TicketStatus Status;

        public double Timestamp;
        public int Type;

        public SupportTicket(uint id, int score, int type, uint senderId, uint reportedId, string message, uint roomId,
            string roomName, double timestamp)
        {
            TicketId = id;
            Score = score;
            Type = type;
            Status = TicketStatus.Open;
            SenderId = senderId;
            ReportedId = reportedId;
            ModeratorId = 0;
            Message = message;
            RoomId = roomId;
            RoomName = roomName;
            Timestamp = timestamp;
        }

        public int TabId
        {
            get
            {
                switch (Status)
                {
                    case TicketStatus.Open:
                        return 1;
                    case TicketStatus.Picked:
                        return 2;
                    case TicketStatus.Resolved:
                        return 3;
                    case TicketStatus.Abusive:
                        return 4;
                    case TicketStatus.Invalid:
                        return 5;
                    case TicketStatus.Deleted:
                        return 6;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public uint TicketId { get; }

        public void Pick(uint moderatorId, bool updateInDb)
        {
            Status = TicketStatus.Picked;
            ModeratorId = moderatorId;

            if (!updateInDb) return;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE moderation_tickets SET status = 'picked', moderator_id = '" + moderatorId +
                                      "' WHERE id = '" + TicketId + "' LIMIT 1");
            }
        }

        public void Close(TicketStatus newStatus, bool updateInDb)
        {
            Status = newStatus;

            if (!updateInDb) return;
            string dbType;

            switch (newStatus)
            {
                case TicketStatus.Abusive:
                    dbType = "abusive";
                    break;
                case TicketStatus.Invalid:
                    dbType = "invalid";
                    break;
                case TicketStatus.Open:
                    dbType = "open";
                    break;
                case TicketStatus.Picked:
                    dbType = "picked";
                    break;
                case TicketStatus.Deleted:
                    dbType = "deleted";
                    break;
                case TicketStatus.Resolved:
                default:
                    dbType = "resolved";
                    break;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE moderation_tickets SET status = '" + dbType + "' WHERE id = '" + TicketId +
                                      "' LIMIT 1");
            }
        }

        public void Release(bool updateInDb)
        {
            Status = TicketStatus.Open;

            if (updateInDb)
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("UPDATE moderation_tickets SET status = 'open' WHERE id = '" + TicketId +
                                          "' LIMIT 1");
                }
            }
        }

        public void Delete(bool updateInDb)
        {
            Status = TicketStatus.Deleted;

            if (updateInDb)
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("UPDATE moderation_tickets SET status = 'deleted' WHERE id = '" + TicketId +
                                          "' LIMIT 1");
                }
            }
        }

        public ServerPacket Serialize()
        {
            var message = new ServerPacket(530);
            message.WriteUInt(TicketId);
            message.WriteInteger(TabId);
            message.WriteInteger(11); // ??
            message.WriteInteger(Type);
            message.WriteInteger(11); // ??
            message.WriteInteger(Score);
            message.WriteUInt(SenderId);
            message.WriteString(PylenorEnvironment.GetGame().GetClientManager().GetNameById(SenderId));
            message.WriteUInt(ReportedId);
            message.WriteString(PylenorEnvironment.GetGame().GetClientManager().GetNameById(ReportedId));
            message.WriteUInt(ModeratorId);
            message.WriteString(PylenorEnvironment.GetGame().GetClientManager().GetNameById(ModeratorId));
            message.WriteString(Message);
            message.WriteUInt(RoomId);
            message.WriteString(RoomName);
            return message;
        }
    }
}