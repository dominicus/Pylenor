﻿using System.Data;

namespace Pylenor.HabboHotel.Users.Authenticator
{
    internal class Authenticator
    {
        public static Player TryLoginPlayer(string authTicket)
        {
            DataRow row;

            if (authTicket.Length < 10)
            {
                throw new IncorrectLoginException("Invalid authorization/SSO ticket");
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("auth_ticket", authTicket);
                row = dbClient.ReadDataRow("SELECT * FROM users WHERE auth_ticket = @auth_ticket LIMIT 1");
            }

            if (row == null)
            {
                throw new IncorrectLoginException("Invalid authorization/SSO ticket");
            }

            if (!PylenorEnvironment.GetGame().GetRoleManager().RankHasRight((uint) row["rank"], "fuse_login"))
            {
                throw new IncorrectLoginException(
                    "Not permitted to log in due to role/right restriction (fuse_login is missing)");
            }

            if (row["newbie_status"].ToString() == "0")
            {
                throw new IncorrectLoginException("Not permitted to log in; you are still a noob.");
            }

            return GenerateHabbo(row, authTicket);
        }

        public static Player GenerateHabbo(DataRow data, string authTicket)
        {
            return new Player((uint) data["id"], (string) data["username"], (string) data["real_name"], authTicket,
                (uint) data["rank"], (string) data["motto"], (string) data["look"], (string) data["gender"],
                (int) data["credits"], (int) data["activity_points"], (double) data["activity_points_lastupdate"],
                PylenorEnvironment.EnumToBool(data["is_muted"].ToString()), (uint) data["home_room"],
                (int) data["respect"], (int) data["daily_respect_points"], (int) data["daily_pet_respect_points"],
                (int) data["newbie_status"], (data["mutant_penalty"].ToString() != "0"),
                PylenorEnvironment.EnumToBool(data["block_newfriends"].ToString()));
        }
    }
}