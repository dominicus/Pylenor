﻿using System;

namespace Pylenor.HabboHotel.Users.Authenticator
{
    public class IncorrectLoginException : Exception
    {
        public IncorrectLoginException(string reason) : base(reason)
        {
        }
    }
}