﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Users.Badges
{
    public class BadgeComponent
    {
        private readonly uint _userId;

        public BadgeComponent(uint userId)
        {
            BadgeList = new List<Badge>();
            _userId = userId;
        }

        public int Count => BadgeList.Count;

        public int EquippedCount
        {
            get
            {
                var i = 0;

                i += BadgeList.Count(badge => badge.Slot > 0);

                return i;
            }
        }

        public List<Badge> BadgeList { get; }

        public Badge GetBadge(string badge)
        {
            return BadgeList.FirstOrDefault(b => string.Equals(badge, b.Code, StringComparison.CurrentCultureIgnoreCase));
        }

        public bool HasBadge(string badge)
        {
            return GetBadge(badge) != null;
        }

        public void GiveBadge(string badge, bool inDatabase)
        {
            GiveBadge(badge, 0, inDatabase);
        }

        public void GiveBadge(string badge, int slot, bool inDatabase)
        {
            if (HasBadge(badge))
            {
                return;
            }

            if (inDatabase)
            {
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("INSERT INTO user_badges (user_id,badge_id,badge_slot) VALUES ('" + _userId +
                                          "','" + badge + "','" + slot + "')");
                }
            }

            BadgeList.Add(new Badge(badge, slot));
        }

        public void SetBadgeSlot(string badge, int slot)
        {
            var b = GetBadge(badge);

            if (b == null)
            {
                return;
            }

            b.Slot = slot;
        }

        public void ResetSlots()
        {
            foreach (var badge in BadgeList)
            {
                badge.Slot = 0;
            }
        }

        public void RemoveBadge(string badge)
        {
            if (!HasBadge(badge))
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM user_badges WHERE badge_id = '" + badge + "' AND user_id = '" +
                                      _userId + "' LIMIT 1");
            }

            BadgeList.Remove(GetBadge(badge));
        }

        public void LoadBadges()
        {
            BadgeList.Clear();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data =
                    dbClient.ReadDataTable("SELECT badge_id,badge_slot FROM user_badges WHERE user_id = '" + _userId +
                                           "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                GiveBadge((string) row["badge_id"], (int) row["badge_slot"], false);
            }
        }

        public ServerPacket Serialize()
        {
            var equippedBadges = new List<Badge>();

            var message = new ServerPacket(229);
            message.WriteInteger(Count);

            foreach (var badge in BadgeList)
            {
                message.WriteUInt(0);
                message.WriteString(badge.Code);

                if (badge.Slot > 0)
                {
                    equippedBadges.Add(badge);
                }
            }

            message.WriteInteger(equippedBadges.Count);

            foreach (var badge in equippedBadges)
            {
                message.WriteInteger(badge.Slot);
                message.WriteString(badge.Code);
            }

            return message;
        }
    }
}