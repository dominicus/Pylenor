﻿namespace Pylenor.HabboHotel.Users.Inventory
{
    public class AvatarEffect
    {
        public bool Activated;
        public int EffectId;
        public double StampActivated;
        public int TotalDuration;

        public AvatarEffect(int effectId, int totalDuration, bool activated, double activateTimestamp)
        {
            EffectId = effectId;
            TotalDuration = totalDuration;
            Activated = activated;
            StampActivated = activateTimestamp;
        }

        public int TimeLeft
        {
            get
            {
                if (!Activated)
                {
                    return -1;
                }

                var diff = PylenorEnvironment.GetUnixTimestamp() - StampActivated;

                if (diff >= TotalDuration)
                {
                    return 0;
                }

                return (int) (TotalDuration - diff);
            }
        }

        public bool HasExpired
        {
            get
            {
                if (TimeLeft == -1)
                {
                    return false;
                }

                if (TimeLeft <= 0)
                {
                    return true;
                }

                return false;
            }
        }

        public void Activate()
        {
            Activated = true;
            StampActivated = PylenorEnvironment.GetUnixTimestamp();
        }
    }
}