﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Rooms;

namespace Pylenor.HabboHotel.Users.Inventory
{
    public class AvatarEffectsInventoryComponent
    {
        private readonly List<AvatarEffect> _effects;
        private readonly uint _userId;
        public int CurrentEffect;

        public AvatarEffectsInventoryComponent(uint userId)
        {
            _effects = new List<AvatarEffect>();
            _userId = userId;
            CurrentEffect = -1;
        }

        public int Count => _effects.Count;

        public void LoadEffects()
        {
            _effects.Clear();

            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT * FROM user_effects WHERE user_id = '" + _userId + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                var effect = new AvatarEffect((int) row["effect_id"], (int) row["total_duration"],
                    PylenorEnvironment.EnumToBool(row["is_activated"].ToString()), (double) row["activated_stamp"]);

                if (effect.HasExpired)
                {
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        dbClient.ExecuteQuery("DELETE FROM user_effects WHERE user_id = '" + _userId +
                                              "' AND effect_id = '" + effect.EffectId + "' LIMIT 1");
                    }

                    continue;
                }

                _effects.Add(effect);
            }
        }

        public void AddEffect(int effectId, int duration)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery(
                    "INSERT INTO user_effects (user_id,effect_id,total_duration,is_activated,activated_stamp) VALUES ('" +
                    _userId + "','" + effectId + "','" + duration + "','0','0')");
            }

            _effects.Add(new AvatarEffect(effectId, duration, false, 0));

            var effectAdded = new ServerPacket(461);
            effectAdded.WriteInteger(effectId);
            effectAdded.WriteInteger(duration);
            GetClient().SendPacket(effectAdded);
        }

        public void StopEffect(int effectId)
        {
            var effect = GetEffect(effectId, true);

            if (effect == null || !effect.HasExpired)
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM user_effects WHERE user_id = '" + _userId + "' AND effect_id = '" +
                                      effectId + "' AND is_activated = '1' LIMIT 1");
            }

            _effects.Remove(effect);
            var effectExpired = new ServerPacket(463);
            effectExpired.WriteInteger(effectId);
            GetClient().SendPacket(effectExpired);

            if (CurrentEffect >= 0)
            {
                ApplyEffect(-1);
            }
        }

        public void ApplyEffect(int effectId)
        {
            if (!HasEffect(effectId, true))
            {
                return;
            }

            var room = GetUserRoom();

            var user = room?.GetRoomUserByHabbo(GetClient().GetPlayer().Id);

            if (user == null)
            {
                return;
            }

            CurrentEffect = effectId;

            var message = new ServerPacket(485);
            message.WriteInteger(user.VirtualId);
            message.WriteInteger(effectId);
            room.SendMessage(message);
        }

        public void EnableEffect(int effectId)
        {
            var effect = GetEffect(effectId, false);

            if (effect == null || effect.HasExpired || effect.Activated)
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE user_effects SET is_activated = '1', activated_stamp = '" +
                                      PylenorEnvironment.GetUnixTimestamp() + "' WHERE user_id = '" + _userId +
                                      "' AND effect_id = '" + effectId + "' LIMIT 1");
            }

            effect.Activate();

            var effectActivated = new ServerPacket(462);
            effectActivated.WriteInteger(effect.EffectId);
            effectActivated.WriteInteger(effect.TotalDuration);
            GetClient().SendPacket(effectActivated);
        }

        public bool HasEffect(int effectId, bool ifEnabledOnly)
        {
            if (effectId == -1)
            {
                return true;
            }

            return _effects.Where(effect => !ifEnabledOnly || effect.Activated)
                .Where(effect => !effect.HasExpired)
                .Any(effect => effect.EffectId == effectId);
        }

        public AvatarEffect GetEffect(int effectId, bool ifEnabledOnly)
        {
            return
                _effects.Where(effect => !ifEnabledOnly || effect.Activated)
                    .FirstOrDefault(effect => effect.EffectId == effectId);
        }

        public ServerPacket Serialize()
        {
            var message = new ServerPacket(460);
            message.WriteInteger(Count);

            foreach (var effect in _effects)
            {
                message.WriteInteger(effect.EffectId);
                message.WriteInteger(effect.TotalDuration);
                message.WriteBoolean(!effect.Activated);
                message.WriteInteger(effect.TimeLeft);
            }

            return message;
        }

        public void CheckExpired()
        {
            var toRemove = (from effect in _effects where effect.HasExpired select effect.EffectId).ToList();

            foreach (var trmv in toRemove)
            {
                StopEffect(trmv);
            }
        }

        private Session GetClient()
        {
            return PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(_userId);
        }

        private Room GetUserRoom()
        {
            return PylenorEnvironment.GetGame().GetRoomManager().GetRoom(GetClient().GetPlayer().CurrentRoomId);
        }
    }
}