﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.HabboHotel.Items;
using Pylenor.HabboHotel.Pets;

namespace Pylenor.HabboHotel.Users.Inventory
{
    public class InventoryComponent
    {
        private readonly List<UserItem> _items;
        private readonly List<Pet> _pets;
        public uint UserId;

        public InventoryComponent(uint userId)
        {
            UserId = userId;
            _items = new List<UserItem>();
            _pets = new List<Pet>();
        }

        public int ItemCount => _items.Count;

        public int PetCount => _pets.Count;

        private Session Client => PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(UserId);

        public void ClearItems()
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", UserId);
                dbClient.ExecuteQuery("DELETE FROM user_items WHERE user_id = @userid");
            }

            UpdateItems(true);
        }

        public void ClearPets()
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", UserId);
                dbClient.ExecuteQuery("DELETE FROM user_pets WHERE user_id = @userid AND room_id = 0");
            }

            UpdatePets(true);
        }

        public void LoadInventory()
        {
            _items.Clear();
            DataTable userItems;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", UserId);
                userItems =
                    dbClient.ReadDataTable("SELECT id,base_item,extra_data FROM user_items WHERE user_id = @userid;");
            }

            if (userItems != null)
            {
                foreach (DataRow row in userItems.Rows)
                {
                    _items.Add(new UserItem((uint) row["id"], (uint) row["base_item"],
                        (string) row["extra_data"]));
                }
            }

            _pets.Clear();
            DataTable userPets;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", UserId);
                userPets = dbClient.ReadDataTable("SELECT * FROM user_pets WHERE user_id = @userid AND room_id <= 0;");
            }

            if (userPets == null) return;
            foreach (DataRow row in userPets.Rows)
            {
                _pets.Add(PylenorEnvironment.GetGame().GetCatalog().GeneratePetFromRow(row));
            }
        }

        public void UpdateItems(bool fromDatabase)
        {
            if (fromDatabase)
            {
                LoadInventory();
            }
            Client.SendPacket(new ServerPacket(101));
        }

        public void UpdatePets(bool fromDatabase)
        {
            if (fromDatabase)
            {
                LoadInventory();
            }

            Client.SendPacket(SerializePetInventory());
        }

        public Pet GetPet(uint id)
        {
            return _pets.FirstOrDefault(pet => pet != null && pet.PetId == id);
        }

        public UserItem GetItem(uint id)
        {
            return _items.FirstOrDefault(item => item != null && item.Id == id);
        }

        public void AddItem(uint id, uint baseItem, string extraData)
        {
            _items.Add(new UserItem(id, baseItem, extraData));

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("extra_data", extraData);
                dbClient.ExecuteQuery("INSERT INTO user_items (id,user_id,base_item,extra_data) VALUES ('" + id +
                                      "','" + UserId + "','" + baseItem + "',@extra_data);");
            }
        }

        public void AddPet(Pet pet)
        {
            if (pet == null)
            {
                return;
            }

            pet.PlacedInRoom = false;

            _pets.Add(pet);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("botid", pet.PetId);
                dbClient.ExecuteQuery("UPDATE user_pets SET room_id = 0, x = 0, y = 0, z = 0 WHERE id = @botid LIMIT 1");
            }

            var addMessage = new ServerPacket(603);
            pet.SerializeInventory(addMessage);
            Client.SendPacket(addMessage);
        }

        public bool RemovePet(uint petId)
        {
            foreach (var pet in _pets.Where(pet => pet.PetId == petId))
            {
                _pets.Remove(pet);

                var removeMessage = new ServerPacket(604);
                removeMessage.WriteUInt(petId);
                Client.SendPacket(removeMessage);

                return true;
            }

            return false;
        }

        public void MovePetToRoom(uint petId, uint roomId)
        {
            if (!RemovePet(petId)) return;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("roomid", roomId);
                dbClient.AddParamWithValue("petid", petId);
                dbClient.ExecuteQuery(
                    "UPDATE user_pets SET room_id = @roomid, x = 0, y = 0, z = 0 WHERE id = @petid LIMIT 1;");
            }
        }

        public void RemoveItem(uint id)
        {
            var packet = new ServerPacket(99);
            packet.WriteUInt(id);
            Client.SendPacket(packet);

            _items.Remove(GetItem(id));

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM user_items WHERE id = '" + id + "' LIMIT 1;");
            }
        }

        public ServerPacket SerializeItemInventory()
        {
            var message = new ServerPacket(140);

            message.WriteString("S");

            message.WriteInteger(1);
            message.WriteInteger(1);

            message.WriteInteger(ItemCount);

            foreach (var item in _items.Where(item => item != null))
            {
                item.Serialize(message, true);
            }
            return message;
        }

        public ServerPacket SerializePetInventory()
        {
            var message = new ServerPacket(600);
            message.WriteInteger(_pets.Count);

            foreach (var pet in _pets)
            {
                pet.SerializeInventory(message);
            }

            return message;
        }
    }
}