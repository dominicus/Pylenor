﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;

namespace Pylenor.HabboHotel.Users.Messenger
{
    public class HabboMessenger
    {
        private readonly uint _userId;

        private List<MessengerBuddy> _buddies;
        private List<MessengerRequest> _requests;
        public bool AppearOffline;

        public HabboMessenger(uint userId)
        {
            _buddies = new List<MessengerBuddy>();
            _requests = new List<MessengerRequest>();
            _userId = userId;
        }

        public void LoadBuddies()
        {
            _buddies = new List<MessengerBuddy>();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data =
                    dbClient.ReadDataTable("SELECT user_two_id FROM messenger_friendships WHERE user_one_id = '" +
                                           _userId + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                _buddies.Add(new MessengerBuddy((uint) row["user_two_id"]));
            }
        }

        public void LoadRequests()
        {
            _requests = new List<MessengerRequest>();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT * FROM messenger_requests WHERE to_id = '" + _userId + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                _requests.Add(new MessengerRequest((uint) row["id"], (uint) row["to_id"], (uint) row["from_id"]));
            }
        }

        public void ClearBuddies()
        {
            _buddies.Clear();
        }

        public void ClearRequests()
        {
            _requests.Clear();
        }

        public MessengerRequest GetRequest(uint requestId)
        {
            return _requests.FirstOrDefault(request => request != null && request.RequestId == requestId);
        }

        public void OnStatusChanged(bool instantUpdate)
        {
            foreach (
                var client in
                    _buddies.Where(buddy => buddy != null)
                        .Select(buddy => PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(buddy.Id))
                        .Where(client => client?.GetPlayer() != null && client.GetPlayer().GetMessenger() != null))
            {
                client.GetPlayer().GetMessenger().SetUpdateNeeded(_userId);

                if (instantUpdate)
                {
                    client.GetPlayer().GetMessenger().ForceUpdate();
                }
            }
        }

        public bool SetUpdateNeeded(uint userId)
        {
            foreach (var buddy in _buddies.Where(buddy => buddy == null || buddy.Id == userId))
            {
                if (buddy != null) buddy.UpdateNeeded = true;
                return true;
            }
            return false;
        }

        public void ForceUpdate()
        {
            GetClient().SendPacket(SerializeUpdates());
        }

        public bool RequestExists(uint userOne, uint userTwo)
        {
            if (userOne == userTwo)
            {
                return true;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                if (
                    dbClient.ReadDataRow("SELECT * FROM messenger_requests WHERE to_id = '" + userOne +
                                         "' AND from_id = '" + userTwo + "' LIMIT 1") != null)
                {
                    return true;
                }

                if (
                    dbClient.ReadDataRow("SELECT * FROM messenger_requests WHERE to_id = '" + userTwo +
                                         "' AND from_id = '" + userOne + "' LIMIT 1") != null)
                {
                    return true;
                }
            }

            return false;
        }

        public bool FriendshipExists(uint userOne, uint userTwo)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                if (
                    dbClient.ReadDataRow("SELECT * FROM messenger_friendships WHERE user_one_id = '" + userOne +
                                         "' AND user_two_id = '" + userTwo + "' LIMIT 1") != null)
                {
                    return true;
                }

                if (
                    dbClient.ReadDataRow("SELECT * FROM messenger_friendships WHERE user_one_id = '" + userTwo +
                                         "' AND user_two_id = '" + userOne + "' LIMIT 1") != null)
                {
                    return true;
                }
            }

            return false;
        }

        public void HandleAllRequests()
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("DELETE FROM messenger_requests WHERE to_id = '" + _userId + "'");
            }

            ClearRequests();
        }

        public void HandleRequest(uint fromId)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("userid", _userId);
                dbClient.AddParamWithValue("fromid", fromId);
                dbClient.ExecuteQuery(
                    "DELETE FROM messenger_requests WHERE to_id = @userid AND from_id = @fromid LIMIT 1");
            }

            if (GetRequest(fromId) != null)
            {
                _requests.Remove(GetRequest(fromId));
            }
        }

        public void CreateFriendship(uint userTwo)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("toid", userTwo);
                dbClient.AddParamWithValue("userid", _userId);
                dbClient.ExecuteQuery(
                    "INSERT INTO messenger_friendships (user_one_id,user_two_id) VALUES (@userid,@toid)");
                dbClient.ExecuteQuery(
                    "INSERT INTO messenger_friendships (user_one_id,user_two_id) VALUES (@toid,@userid)");
            }

            OnNewFriendship(userTwo);

            var user = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(userTwo);

            if (user?.GetPlayer().GetMessenger() != null)
            {
                user.GetPlayer().GetMessenger().OnNewFriendship(_userId);
            }
        }

        public void DestroyFriendship(uint userTwo)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("toid", userTwo);
                dbClient.AddParamWithValue("userid", _userId);
                dbClient.ExecuteQuery(
                    "DELETE FROM messenger_friendships WHERE user_one_id = @toid AND user_two_id = @userid LIMIT 1");
                dbClient.ExecuteQuery(
                    "DELETE FROM messenger_friendships WHERE user_one_id = @userid AND user_two_id = @toid LIMIT 1");
            }

            OnDestroyFriendship(userTwo);

            var user = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(userTwo);

            if (user?.GetPlayer().GetMessenger() != null)
            {
                user.GetPlayer().GetMessenger().OnDestroyFriendship(_userId);
            }
        }

        public void OnNewFriendship(uint friend)
        {
            var buddy = new MessengerBuddy(friend) {UpdateNeeded = true};

            _buddies.Add(buddy);

            ForceUpdate();
        }

        public void OnDestroyFriendship(uint friend)
        {
            foreach (var buddy in _buddies.Where(buddy => buddy.Id == friend))
            {
                _buddies.Remove(buddy);
                break;
            }

            var serverPacket = new ServerPacket(13);
            serverPacket.WriteInteger(0);
            serverPacket.WriteInteger(1);
            serverPacket.WriteInteger(-1);
            serverPacket.WriteUInt(friend);
            GetClient().SendPacket(serverPacket);
        }

        public void RequestBuddy(string userQuery)
        {
            DataRow row;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("query", userQuery.ToLower());
                row = dbClient.ReadDataRow("SELECT id,block_newfriends FROM users WHERE username = @query LIMIT 1");
            }

            if (row == null)
            {
                return;
            }
            if (PylenorEnvironment.EnumToBool(row["block_newfriends"].ToString()))
            {
                var serverPacket = new ServerPacket(260);
                serverPacket.WriteInteger(39);
                serverPacket.WriteInteger(3);
                GetClient().SendPacket(serverPacket);
                return;
            }

            var toId = (uint) row["id"];

            if (RequestExists(_userId, toId))
            {
                return;
            }

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("toid", toId);
                dbClient.AddParamWithValue("userid", _userId);
                dbClient.ExecuteQuery("INSERT INTO messenger_requests (to_id,from_id) VALUES (@toid,@userid)");
            }

            var toUser = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(toId);

            if (toUser?.GetPlayer() == null)
            {
                return;
            }

            uint requestId;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("toid", toId);
                dbClient.AddParamWithValue("userid", _userId);
                requestId =
                    (uint)
                        dbClient.ReadInt32(
                            "SELECT id FROM messenger_requests WHERE to_id = @toid AND from_id = @userid ORDER BY id DESC LIMIT 1");
            }

            var request = new MessengerRequest(requestId, toId, _userId);

            toUser.GetPlayer().GetMessenger().OnNewRequest(requestId, toId, _userId);

            var newFriendNotif = new ServerPacket(132);
            request.Serialize(newFriendNotif);
            toUser.SendPacket(newFriendNotif);
        }

        public void OnNewRequest(uint request, uint toId, uint userId)
        {
            _requests.Add(new MessengerRequest(request, toId, userId));
        }

        public void SendInstantMessage(uint toId, string message)
        {
            if (!FriendshipExists(toId, _userId))
            {
                DeliverInstantMessageError(6, toId);
                return;
            }

            var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(toId);

            if (client?.GetPlayer().GetMessenger() == null)
            {
                DeliverInstantMessageError(5, toId);
                return;
            }

            if (GetClient().GetPlayer().Muted)
            {
                DeliverInstantMessageError(4, toId);
                return;
            }

            if (client.GetPlayer().Muted)
            {
                DeliverInstantMessageError(3, toId); // No return, as this is just a warning.
            }

            // todo: is busy??

            client.GetPlayer().GetMessenger().DeliverInstantMessage(message, _userId);
        }

        public void DeliverInstantMessage(string message, uint conversationId)
        {
            var instantMessage = new ServerPacket(134);
            instantMessage.WriteUInt(conversationId);
            instantMessage.AppendString(message);
            GetClient().SendPacket(instantMessage);
        }

        public void DeliverInstantMessageError(int errorId, uint conversationId)
        {
            /*
             * 3                =     Your friend is muted and cannot reply.
             * 4                =     Your message was not sent because you are muted.
             * 5                =     Your friend is not online.
             * 6                =     Receiver is not your friend anymore.
             * 7                =     Your friend is busy.
             * Anything else    =     Unknown im error <error no.>
             */

            var error = new ServerPacket(261);
            error.WriteInteger(errorId);
            error.WriteUInt(conversationId);
            GetClient().SendPacket(error);
        }

        public ServerPacket SerializeFriends()
        {
            var friends = new ServerPacket(12);
            friends.WriteInteger(600);
            friends.WriteInteger(200);
            friends.WriteInteger(600);
            friends.WriteInteger(900);
            friends.WriteBoolean(false);
            friends.WriteInteger(_buddies.Count);

            foreach (var buddy in _buddies)
            {
                buddy.Serialize(friends, false);
            }
            return friends;
        }

        public ServerPacket SerializeUpdates()
        {
            var updateBuddies = new List<MessengerBuddy>();
            var updateCount = 0;

            foreach (var buddy in _buddies.Where(buddy => buddy.UpdateNeeded))
            {
                updateCount++;
                updateBuddies.Add(buddy);
                buddy.UpdateNeeded = false;
            }

            var updates = new ServerPacket(13);
            updates.WriteInteger(0);
            updates.WriteInteger(updateCount);
            updates.WriteInteger(0);

            foreach (var buddy in updateBuddies)
            {
                buddy.Serialize(updates, false);
                updates.WriteBoolean(false);
            }

            return updates;
        }

        public ServerPacket SerializeRequests()
        {
            var reqs = new ServerPacket(314);
            reqs.WriteInteger(_requests.Count);
            reqs.WriteInteger(_requests.Count);

            foreach (var request in _requests)
            {
                request.Serialize(reqs);
            }
            return reqs;
        }

        public ServerPacket PerformSearch(string searchQuery)
        {
            DataTable results;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.AddParamWithValue("query", searchQuery + "%");
                results = dbClient.ReadDataTable("SELECT id FROM users WHERE username LIKE @query LIMIT 50;");
            }

            var friendData = new List<DataRow>();
            var othersData = new List<DataRow>();

            if (results != null)
            {
                foreach (DataRow row in results.Rows)
                {
                    if (FriendshipExists(_userId, (uint) row["id"]))
                    {
                        friendData.Add(row);

                        continue;
                    }

                    othersData.Add(row);
                }
            }

            var search = new ServerPacket(435);

            search.WriteInteger(friendData.Count);

            foreach (var row in friendData)
            {
                new MessengerBuddy((uint) row["id"]).Serialize(search, true);
            }

            search.WriteInteger(othersData.Count);

            foreach (var row in othersData)
            {
                new MessengerBuddy((uint) row["id"]).Serialize(search, true);
            }

            return search;
        }

        private Session GetClient()
        {
            return PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(_userId);
        }

        public List<MessengerBuddy> GetBuddies()
        {
            return _buddies;
        }
    }
}