﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Users.Messenger
{
    public class MessengerBuddy
    {
        public bool UpdateNeeded;

        public MessengerBuddy(uint userId)
        {
            Id = userId;
            UpdateNeeded = false;
        }

        public uint Id { get; }

        public string Username
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                if (client != null)
                {
                    return client.GetPlayer().Username;
                }
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    return dbClient.ReadString("SELECT username FROM users WHERE id = '" + Id + "' LIMIT 1");
                }
            }
        }

        public string RealName
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                if (client != null)
                {
                    return client.GetPlayer().RealName;
                }
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    return dbClient.ReadString("SELECT real_name FROM users WHERE id = '" + Id + "' LIMIT 1");
                }
            }
        }

        public string Look
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                return client != null ? client.GetPlayer().Look : "";
            }
        }

        public string Motto
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                return client != null ? client.GetPlayer().Motto : "";
            }
        }

        public string LastOnline
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                if (client != null) return "";
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    return dbClient.ReadString("SELECT last_online FROM users WHERE id = '" + Id + "' LIMIT 1");
                }
            }
        }

        public bool IsOnline
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                return client != null && !client.GetPlayer().GetMessenger().AppearOffline;
            }
        }

        public bool InRoom
        {
            get
            {
                if (!IsOnline)
                {
                    return false;
                }

                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);

                return client != null && client.GetPlayer().InRoom;
            }
        }

        public void Serialize(ServerPacket packet, bool search)
        {
            /*Fs
             * H
             * RG
             * 
             * hCNZ
             * name
             * motto
             * H
             * H
             * blank
             * I
             * look
             * laston
             * hBDCRbobbya12HHIkP`wUbobbya12174HHIjiu@Pbobbya123HHIhmT\Xbobbya1234HHIhkLLUbobbya12345HHIkDaXPbobbyaanlandHHIj|yHQbobbyabcHHIiSklTbobbyabcdefHHIhGkIQBobbyAbreuHHIjIo`Ybobbyace123HHHkVemUbobbyacebestHHIibWTFbobbyackda cool 1!!!!!!!!HHIiuQ^SBobbyage13HHIjZTsPBobbyaid1HHIkyVOUbobbyakabobHHIjQoUTBOBBYANDGREGHHIiHqJFbobbyandkurtMutant:Snow.(Blizzard).MMHHIhe\yObobbyandro1HHIhA[qXbobbyandsamHHIiWAjQBobbyAndyHHIjcKwWbobbyangleHHHhsvOXBobbyanna[RAF] Leading AircraftmanHHHiyoWYbobbyannaDevilHHHieZAUBobbyapebang baby bangHHIjIRMYbobbyape001HHIkBEiUbobbyape1HHIiQWzWBOBBYAUSTIN50HHHi~LHZBobbyAvacardoHHIhupsTBobbyAwsomeN:Bobby A:13HHI
             */

            /*
             * @LXVBPrXVBXaCHQAkP\USGammer12345IHHH07-04-2010 18:59:45jMI_SHensedIHHH09-04-2010 14:02:47kFzYZOskrarIHHH12-12-2009 17:28:07kk}[Z0skr4rIHHH10-04-2010 09:32:11kTDyZguy72IHHH23-02-2010 10:58:35PYH
             * 
             * 
             * 
             */

            if (search)
            {
                packet.WriteUInt(Id);
                packet.WriteString(Username);
                packet.WriteString(Motto);
                packet.WriteBoolean(IsOnline);
                packet.WriteBoolean(InRoom);
                packet.WriteString("");
                packet.WriteBoolean(false);
                packet.WriteString(Look);
                packet.WriteString(LastOnline);
                packet.WriteString(RealName); // ????????????
            }
            else
            {
                packet.WriteUInt(Id);
                packet.WriteString(Username);
                packet.WriteBoolean(true);
                packet.WriteBoolean(IsOnline);
                packet.WriteBoolean(InRoom);
                packet.WriteString(Look);
                packet.WriteBoolean(false);
                packet.WriteString(Motto);
                packet.WriteString(LastOnline);
                packet.WriteString(RealName);
            }
        }
    }
}