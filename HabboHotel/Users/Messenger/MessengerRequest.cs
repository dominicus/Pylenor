﻿using Pylenor.Communication.Packets.Outgoing;

namespace Pylenor.HabboHotel.Users.Messenger
{
    public class MessengerRequest
    {
        public MessengerRequest(uint requestId, uint toUser, uint fromUser)
        {
            To = toUser;
            RequestId = fromUser;
        }

        public uint RequestId { get; }

        public uint To { get; }

        public uint From => RequestId;

        public string SenderUsername
        {
            get
            {
                var client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(From);

                if (client != null)
                {
                    return client.GetPlayer().Username;
                }
                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    return dbClient.ReadString("SELECT username FROM users WHERE id = '" + From + "' LIMIT 1");
                }
            }
        }

        public void Serialize(ServerPacket request)
        {
            // BDhqu@UMeth0d1322033860

            request.WriteUInt(RequestId);
            request.WriteString(SenderUsername);
            request.WriteString(RequestId.ToString());
        }
    }
}