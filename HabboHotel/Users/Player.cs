﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Core;
using Pylenor.HabboHotel.Rooms;
using Pylenor.HabboHotel.Users.Badges;
using Pylenor.HabboHotel.Users.Inventory;
using Pylenor.HabboHotel.Users.Messenger;
using Pylenor.HabboHotel.Users.Subscriptions;

namespace Pylenor.HabboHotel.Users
{
    public class Player
    {
        private readonly AvatarEffectsInventoryComponent _avatarEffectsInventoryComponent;
        private readonly BadgeComponent _badgeComponent;
        private readonly InventoryComponent _inventoryComponent;
        private HabboMessenger _messenger;

        private SubscriptionManager _subscriptionManager;
        public Dictionary<uint, int> Achievements;
        public int ActivityPoints;

        public string AuthTicket;

        public bool BlockNewFriends;

        public bool CalledGuideBot;

        public int Credits;
        public uint CurrentRoomId;
        public int DailyPetRespectPoints;
        public int DailyRespectPoints;
        public bool Disconnected;

        public List<uint> FavoriteRooms;
        public string Gender;
        public uint HomeRoom;
        public uint Id;

        public bool IsTeleporting;
        public double LastActivityPointsUpdate;
        public bool LoadingChecksPassed;

        public uint LoadingRoom;
        public string Look;
        public string MachineId;

        public string Motto;
        public bool MutantPenalty;

        public bool Muted;
        public List<uint> MutedUsers;

        public int NewbieStatus;
        public List<RoomData> OwnedRooms;

        public uint Rank;
        public List<uint> RatedRooms;
        public string RealName;

        public int Respect;
        public bool SpectatorMode;
        public List<string> Tags;
        public uint TeleporterId;

        public string Username;

        public Player(uint id, string username, string realName, string authTicket, uint rank, string motto, string look,
            string gender, int credits, int activityPoints, double lastActivityPointsUpdate, bool muted, uint homeRoom,
            int respect, int dailyRespectPoints, int dailyPetRespectPoints, int newbieStatus, bool mutantPenalty,
            bool blockNewFriends)
        {
            Id = id;
            Username = username;
            RealName = realName;
            AuthTicket = authTicket;
            Rank = rank;
            Motto = motto;
            Look = look.ToLower();
            Gender = gender.ToLower();
            Credits = credits;
            ActivityPoints = activityPoints;
            LastActivityPointsUpdate = lastActivityPointsUpdate;
            Muted = muted;
            LoadingRoom = 0;
            LoadingChecksPassed = false;
            CurrentRoomId = 0;
            HomeRoom = homeRoom;
            FavoriteRooms = new List<uint>();
            MutedUsers = new List<uint>();
            Tags = new List<string>();
            Achievements = new Dictionary<uint, int>();
            RatedRooms = new List<uint>();
            Respect = respect;
            DailyRespectPoints = dailyRespectPoints;
            DailyPetRespectPoints = dailyPetRespectPoints;
            NewbieStatus = newbieStatus;
            CalledGuideBot = false;
            MutantPenalty = mutantPenalty;
            BlockNewFriends = blockNewFriends;

            IsTeleporting = false;
            TeleporterId = 0;

            _subscriptionManager = new SubscriptionManager(id);
            _badgeComponent = new BadgeComponent(id);
            _inventoryComponent = new InventoryComponent(id);
            _avatarEffectsInventoryComponent = new AvatarEffectsInventoryComponent(id);

            SpectatorMode = false;
            Disconnected = false;
            OwnedRooms = new List<RoomData>();
            var db = PylenorEnvironment.GetDatabase().GetClient();
            db.AddParamWithValue("name", Username);
            var rooms = db.ReadDataTable("SELECT * FROM rooms WHERE owner = @name ORDER BY Id ASC LIMIT " +
                                         ServerConfiguration.RoomUserLimit);
            foreach (
                DataRow row in rooms.Rows
                )
            {
                OwnedRooms.Add(PylenorEnvironment.GetGame().GetRoomManager().GenerateRoomData((uint) row["id"]));
            }
            PylenorEnvironment.GetLogging().WriteLine(username + " has logged in.", LogLevel.Debug);
        }

        public bool InRoom => CurrentRoomId >= 1;

        public Room CurrentRoom
            => CurrentRoomId <= 0 ? null : PylenorEnvironment.GetGame().GetRoomManager().GetRoom(CurrentRoomId);

        public void LoadData()
        {
            _subscriptionManager.LoadSubscriptions();
            _badgeComponent.LoadBadges();
            _inventoryComponent.LoadInventory();
            _avatarEffectsInventoryComponent.LoadEffects();

            LoadAchievements();
            LoadFavorites();
            LoadMutedUsers();
            LoadTags();
        }

        public bool HasFuse(string fuse)
        {
            return PylenorEnvironment.GetGame().GetRoleManager().RankHasRight(Rank, fuse) ||
                   GetSubscriptionManager()
                       .SubList.Any(
                           subscriptionId =>
                               PylenorEnvironment.GetGame().GetRoleManager().SubHasRight(subscriptionId, fuse));
        }

        public void LoadFavorites()
        {
            FavoriteRooms.Clear();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT room_id FROM user_favorites WHERE user_id = '" + Id + "'");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                FavoriteRooms.Add((uint) row["room_id"]);
            }
        }

        public void LoadMutedUsers()
        {
            MutedUsers.Clear();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT ignore_id FROM user_ignores WHERE user_id = '" + Id + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                MutedUsers.Add((uint) row["ignore_id"]);
            }
        }

        public void LoadTags()
        {
            Tags.Clear();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT tag FROM user_tags WHERE user_id = '" + Id + "';");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                Tags.Add((string) row["tag"]);
            }

            if (Tags.Count >= 5)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 7, 1);
            }
        }

        public void LoadAchievements()
        {
            Achievements.Clear();
            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data =
                    dbClient.ReadDataTable(
                        "SELECT achievement_id,achievement_level FROM user_achievements WHERE user_id = '" + Id + "'");
            }

            if (data == null)
            {
                return;
            }

            foreach (DataRow row in data.Rows)
            {
                Achievements.Add((uint) row["achievement_id"], (int) row["achievement_level"]);
            }
        }

        public void OnDisconnect()
        {
            if (Disconnected)
            {
                return;
            }

            PylenorEnvironment.GetLogging().WriteLine(Username + " has logged out.", LogLevel.Debug);

            Disconnected = true;
            var now = DateTime.Now;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET last_online = '" + now + "', online = '0' WHERE id = '" + Id +
                                      "' LIMIT 1");
            }

            if (InRoom)
            {
                PylenorEnvironment.GetGame()
                    .GetRoomManager()
                    .GetRoom(CurrentRoomId)
                    .RemoveUserFromRoom(GetClient(), false, false);
            }

            if (_messenger != null)
            {
                _messenger.AppearOffline = true;
                _messenger.OnStatusChanged(true);
                _messenger = null;
            }

            if (_subscriptionManager == null) return;
            _subscriptionManager.Clear();
            _subscriptionManager = null;

            // todo: drop events, kick bots, etc
        }

        public void OnEnterRoom(uint roomId)
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery(
                    "INSERT INTO user_roomvisits (user_id,room_id,entry_timestamp,exit_timestamp,hour,minute) VALUES ('" +
                    Id + "','" + roomId + "','" + PylenorEnvironment.GetUnixTimestamp() + "','0','" + DateTime.Now.Hour +
                    "','" + DateTime.Now.Minute + "')");
            }

            CurrentRoomId = roomId;

            _messenger.OnStatusChanged(false);

            DataTable data;

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                data = dbClient.ReadDataTable("SELECT * FROM user_roomvisits WHERE user_id = '" + Id + "';");
            }

            var i = 0;

            if (data != null)
            {
                i += data.Rows.Cast<DataRow>().Count();
            }

            if (i >= 5)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 1);
            }
            if (i >= 15)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 2);
            }
            if (i >= 30)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 3);
            }
            if (i >= 50)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 4);
            }
            if (i >= 60)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 5);
            }
            if (i >= 80)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 6);
            }
            if (i >= 120)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 7);
            }
            if (i >= 140)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 8);
            }
            if (i >= 160)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 9);
            }
            if (i >= 200)
            {
                PylenorEnvironment.GetGame().GetAchievementManager().UnlockAchievement(GetClient(), 12, 10);
            }
        }

        public void OnLeaveRoom()
        {
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE user_roomvisits SET exit_timestamp = '" +
                                      PylenorEnvironment.GetUnixTimestamp() + "' WHERE room_id = '" + CurrentRoomId +
                                      "' AND user_id = '" + Id + "' ORDER BY entry_timestamp DESC LIMIT 1");
            }

            CurrentRoomId = 0;

            _messenger?.OnStatusChanged(false);
        }

        public void InitMessenger()
        {
            if (GetMessenger() != null)
            {
                return;
            }

            _messenger = new HabboMessenger(Id);

            _messenger.LoadBuddies();
            _messenger.LoadRequests();

            GetClient().SendPacket(_messenger.SerializeFriends());
            GetClient().SendPacket(_messenger.SerializeRequests());

            _messenger.OnStatusChanged(true);
        }


        public void UpdateCreditsBalance(bool inDatabase)
        {
            var creditBalance = new ServerPacket(6);
            creditBalance.WriteString(Credits + ".0");
            GetClient().SendPacket(creditBalance);

            if (!inDatabase) return;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET credits = '" + Credits + "' WHERE id = '" + Id + "' LIMIT 1");
            }
        }

        public void UpdateActivityPointsBalance(bool inDatabase)
        {
            UpdateActivityPointsBalance(inDatabase, 0);
        }

        public void UpdateActivityPointsBalance(bool inDatabase, int notifAmount)
        {
            var packet = new ServerPacket(438);
            packet.WriteInteger(ActivityPoints);
            packet.WriteInteger(notifAmount);
            GetClient().SendPacket(packet);

            if (!inDatabase) return;
            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery("UPDATE users SET activity_points = '" + ActivityPoints +
                                      "', activity_points_lastupdate = '" + LastActivityPointsUpdate +
                                      "' WHERE id = '" + Id + "' LIMIT 1");
            }
        }

        public void Mute()
        {
            if (Muted) return;
            GetClient().SendNotif("You have been muted by an moderator.");
            Muted = true;
        }

        public void Unmute()
        {
            if (!Muted) return;
            GetClient().SendNotif("You have been unmuted by an moderator.");
            Muted = false;
        }

        private Session GetClient()
        {
            return PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(Id);
        }

        public SubscriptionManager GetSubscriptionManager()
        {
            return _subscriptionManager;
        }

        public HabboMessenger GetMessenger()
        {
            return _messenger;
        }

        public BadgeComponent GetBadgeComponent()
        {
            return _badgeComponent;
        }

        public InventoryComponent GetInventoryComponent()
        {
            return _inventoryComponent;
        }

        public AvatarEffectsInventoryComponent GetAvatarEffectsInventoryComponent()
        {
            return _avatarEffectsInventoryComponent;
        }

        public void Whisper(string str)
        {
            var room = PylenorEnvironment.GetGame().GetRoomManager().GetRoom(CurrentRoomId);

            if (room == null) return;
            var roomUser = room.GetRoomUserByHabbo(Id);

            var packet = new ServerPacket(25u);

            packet.WriteInteger(roomUser.VirtualId);
            packet.WriteString(str);
            packet.WriteBoolean(false);

            GetClient().SendPacket(packet);
        }
    }
}