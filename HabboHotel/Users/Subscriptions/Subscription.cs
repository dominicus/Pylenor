﻿namespace Pylenor.HabboHotel.Users.Subscriptions
{
    public class Subscription
    {
        public Subscription(string caption, int timeActivated, int timeExpire)
        {
            SubscriptionId = caption;
            ExpireTime = timeExpire;
        }

        public string SubscriptionId { get; }

        public int ExpireTime { get; private set; }

        public bool IsValid()
        {
            return !(ExpireTime <= PylenorEnvironment.GetUnixTimestamp());
        }

        public void ExtendSubscription(int time)
        {
            ExpireTime += time;
        }
    }
}