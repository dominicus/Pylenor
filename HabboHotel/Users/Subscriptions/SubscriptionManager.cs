﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Pylenor.HabboHotel.Users.Subscriptions
{
    public class SubscriptionManager
    {
        private readonly Dictionary<string, Subscription> _subscriptions;
        private readonly uint _userId;

        public SubscriptionManager(uint userId)
        {
            _userId = userId;

            _subscriptions = new Dictionary<string, Subscription>();
        }

        public List<string> SubList
        {
            get { return _subscriptions.Values.Select(subscription => subscription.SubscriptionId).ToList(); }
        }

        public void LoadSubscriptions()
        {
            var subscriptionData =
                PylenorEnvironment.GetDatabase()
                    .GetClient()
                    .ReadDataTable("SELECT * FROM user_subscriptions WHERE user_id = '" + _userId + "';");

            if (subscriptionData == null) return;
            foreach (DataRow row in subscriptionData.Rows)
            {
                _subscriptions.Add((string) row["subscription_id"],
                    new Subscription((string) row["subscription_id"], (int) row["timestamp_activated"],
                        (int) row["timestamp_expire"]));
            }
        }

        public void Clear()
        {
            _subscriptions.Clear();
        }

        public Subscription GetSubscription(string subscriptionId)
        {
            return _subscriptions.ContainsKey(subscriptionId) ? _subscriptions[subscriptionId] : null;
        }

        public bool HasSubscription(string subscriptionId)
        {
            if (!_subscriptions.ContainsKey(subscriptionId))
            {
                return false;
            }

            var sub = _subscriptions[subscriptionId];

            return sub.IsValid();
        }

        public void AddOrExtendSubscription(string subscriptionId, int durationSeconds)
        {
            subscriptionId = subscriptionId.ToLower();

            if (_subscriptions.ContainsKey(subscriptionId))
            {
                var sub = _subscriptions[subscriptionId];
                sub.ExtendSubscription(durationSeconds);

                using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                {
                    dbClient.ExecuteQuery("UPDATE user_subscriptions SET timestamp_expire = '" + sub.ExpireTime +
                                          "' WHERE user_id = '" + _userId + "' AND subscription_id = '" + subscriptionId +
                                          "' LIMIT 1");
                }

                return;
            }

            var timeCreated = (int) PylenorEnvironment.GetUnixTimestamp();
            var timeExpire = ((int) PylenorEnvironment.GetUnixTimestamp() + durationSeconds);

            var newSub = new Subscription(subscriptionId, timeCreated, timeExpire);

            using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
            {
                dbClient.ExecuteQuery(
                    "INSERT INTO user_subscriptions (user_id,subscription_id,timestamp_activated,timestamp_expire) VALUES ('" +
                    _userId + "','" + subscriptionId + "','" + timeCreated + "','" + timeExpire + "')");
            }

            _subscriptions.Add(newSub.SubscriptionId.ToLower(), newSub);
        }
    }
}