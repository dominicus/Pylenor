﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Pylenor.Communication.Packets.Outgoing;
using Pylenor.Communication.Sessions;
using Pylenor.Core;

namespace Pylenor.Net
{
    internal class MusSocket
    {
        public HashSet<string> AllowedIps;
        public Socket MsSocket;

        public string MusIp;
        public int MusPort;

        public MusSocket(string musIp, int musPort, IEnumerable<string> allowedIps, int backlog)
        {
            MusIp = musIp;
            MusPort = musPort;

            AllowedIps = new HashSet<string>();

            foreach (var ip in allowedIps)
            {
                AllowedIps.Add(ip);
            }

            try
            {
                MsSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                MsSocket.Bind(new IPEndPoint(IPAddress.Parse(MusIp), MusPort));
                MsSocket.Listen(backlog);

                MsSocket.BeginAccept(OnEvent_NewConnection, MsSocket);

                // UberEnvironment.GetLogging().WriteLine("MUS Socket " + musIp + ":" + musPort);
                PylenorEnvironment.GetLogging().WriteLine("Initializing uberEmulator...");
            }

            catch (Exception e)
            {
                throw new Exception("MUS Socket Error - " + e.Message);
            }
        }

        public void OnEvent_NewConnection(IAsyncResult iAr)
        {
            var socket = ((Socket) iAr.AsyncState).EndAccept(iAr);
            var ip = socket.RemoteEndPoint.ToString().Split(':')[0];

            if (AllowedIps.Contains(ip))
            {
                // ReSharper disable once ObjectCreationAsStatement
                new MusConnection(socket);
            }
            else
            {
                socket.Close();
            }

            MsSocket.BeginAccept(OnEvent_NewConnection, MsSocket);
        }
    }

    internal class MusConnection
    {
        private readonly byte[] _buffer = new byte[1024];
        private readonly Socket _socket;

        public MusConnection(Socket socket)
        {
            _socket = socket;

            try
            {
                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, OnEvent_RecieveData, null);
            }

            catch
            {
                TryClose();
            }
        }

        public void TryClose()
        {
            try
            {
                _socket.Close();
            }
            catch
            {
                // ignored
            }
        }

        public void OnEvent_RecieveData(IAsyncResult iAr)
        {
            try
            {
                var bytes = _socket.EndReceive(iAr);
                var data = Encoding.Default.GetString(_buffer, 0, bytes);

                if (data.Length > 0)
                {
                    ProcessCommand(data);
                }
            }
            catch
            {
                // ignored
            }

            TryClose();
        }

        public void ProcessCommand(string data)
        {
            var header = data.Split(Convert.ToChar(1))[0];
            var param = data.Split(Convert.ToChar(1))[1];

            PylenorEnvironment.GetLogging().WriteLine("[MUSConnection.ProcessCommand]: " + data);

            Session client;

            switch (header.ToLower())
            {
                case "updatecredits":
                {
                    if (param == "ALL")
                    {
                        PylenorEnvironment.GetGame().GetClientManager().DeployHotelCreditsUpdate();
                    }
                    else
                    {
                        client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(uint.Parse(param));

                        if (client == null)
                        {
                            return;
                        }

                        int newCredits;

                        using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                        {
                            newCredits =
                                (int)
                                    dbClient.ReadDataRow("SELECT credits FROM users WHERE id = '" +
                                                         client.GetPlayer().Id +
                                                         "' LIMIT 1")[0];
                        }

                        client.GetPlayer().Credits = newCredits;
                        client.GetPlayer().UpdateCreditsBalance(false);
                    }

                    break;
                }
                case "reloadbans":
                {
                    using (var dbClient = PylenorEnvironment.GetDatabase().GetClient())
                    {
                        PylenorEnvironment.GetGame().GetBanManager().LoadBans(dbClient);
                    }
                    PylenorEnvironment.GetGame().GetClientManager().CheckForAllBanConflicts();
                    break;
                }
                case "signout":
                {
                    PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(uint.Parse(param)).Disconnect();
                    break;
                }
                case "updatetags":
                {
                    client = PylenorEnvironment.GetGame().GetClientManager().GetClientByHabbo(uint.Parse(param));
                    client.GetPlayer().LoadTags();
                    break;
                }
                case "ha":
                {
                    var hotelAlert = new ServerPacket(139);
                    hotelAlert.WriteString("packet from the Hotel Management: " + param);
                    PylenorEnvironment.GetGame().GetClientManager().BroadcastMessage(hotelAlert);
                    break;
                }
                case "reboot":
                {
                    PylenorEnvironment.Destroy();
                    break;
                }
                default:
                {
                    PylenorEnvironment.GetLogging().WriteLine("Unrecognized MUS packet: " + data, LogLevel.Error);
                    break;
                }
            }
        }
    }
}