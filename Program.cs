﻿using System;
using System.IO;
using System.Security.Permissions;
using Pylenor.Core;

namespace Pylenor
{
    public class Program
    {
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        public static void Main(string[] args)
        {
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += MyHandler;

            try
            {
                PylenorEnvironment.Initialize();

                while (true)
                {
                    CommandParser.Parse(Console.ReadLine());
                }
            }

            catch (Exception e)
            {
                Console.Write(e.Message);
                Console.ReadKey(true);
            }
        }

        private static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            var e = (Exception) args.ExceptionObject;

            #region Logging

            #region Write Errors to a log file

            // Create a writer and open the file:
            StreamWriter log;

            if (!File.Exists("errorlog.txt"))
            {
                log = new StreamWriter("errorlog.txt");
            }
            else
            {
                log = File.AppendText("errorlog.txt");
            }

            // Write to the file:
            log.WriteLine(DateTime.Now);
            log.WriteLine(e.ToString());
            log.WriteLine();

            // Close the stream:
            log.Close();

            #endregion

            #endregion

            Console.WriteLine("Unhandled Exception : " + e);
            Console.ReadKey(true);
        }
    }
}