﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using MySql.Data.MySqlClient;
using Pylenor.Communication;
using Pylenor.Communication.Packets;
using Pylenor.Core;
using Pylenor.HabboHotel;
using Pylenor.Storage;

namespace Pylenor
{
    internal class PylenorEnvironment
    {
        private static Logging _logging;
        private static ConfigurationData _configuration;
        private static DatabaseManager _databaseManager;
        private static Encoding _defaultEncoding;
        private static ServerSocket _connectionManager;
        internal static DateTime ServerStarted;
        public static PacketManager _packetManager;
        internal static Game Game { get; set; }

        public static string PrettyVersion => "Pylenor 1.0 (Build 1)";

        public static void Initialize()
        {
            Console.SetBufferSize(110, 35);
            Console.SetWindowSize(150, 35);
            ServerStarted = DateTime.Now;
            Console.Title = PrettyVersion;
            Console.Clear();

            _defaultEncoding = Encoding.Default;

            _logging = new Logging {MinimumLogLevel = LogLevel.Debug};

            try
            {
                _configuration = new ConfigurationData("config.conf");

                if (GetConfig().Data["db.password"].Length == 0)
                {
                    throw new Exception(
                        "For security reasons, your MySQL password cannot be left blank. Please change your password to start the server.");
                }

                var dbServer = new DatabaseServer(
                    GetConfig().Data["db.hostname"],
                    uint.Parse(GetConfig().Data["db.port"]),
                    GetConfig().Data["db.username"],
                    GetConfig().Data["db.password"]);

                var db = new Database(
                    GetConfig().Data["db.name"],
                    uint.Parse(GetConfig().Data["db.pool.minsize"]),
                    uint.Parse(GetConfig().Data["db.pool.maxsize"]));

                _databaseManager = new DatabaseManager(dbServer, db);

                Game = new Game();


                /*_connectionManager = new TcpConnectionManager(
                    GetConfig().Data["game.tcp.bindip"],
                    int.Parse(GetConfig().Data["game.tcp.port"]),
                    int.Parse(GetConfig().Data["game.tcp.conlimit"]));
                _connectionManager.GetListener().Start();*/
                var settings = new ServerSocketSettings
                {
                    MaxConnections = 10000,
                    NumOfSaeaForRec = 10000,
                    NumOfSaeaForSend = 20000,
                    Backlog = 50,
                    MaxSimultaneousAcceptOps = 15,
                    BufferSize = 512,
                    Endpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 30000)
                };

                _packetManager = new PacketManager();
                _packetManager.Register();

                _connectionManager = new ServerSocket(settings);
                _connectionManager.Init();
                _connectionManager.StartListen();
                GetLogging().WriteLine("The environment has initialized successfully. Ready for connections.");
            }

            catch (KeyNotFoundException)
            {
                _logging.WriteLine("Please check your configuration file - some values appear to be missing.",
                    LogLevel.Error);
                _logging.WriteLine("Press any key to shut down ...", LogLevel.Error);

                Console.ReadKey(true);
                Destroy();
            }

            catch (InvalidOperationException e)
            {
                _logging.WriteLine("Failed to initialize: " + e.Message, LogLevel.Error);
                _logging.WriteLine("Press any key to shut down ...", LogLevel.Error);

                Console.ReadKey(true);
                Destroy();
            }
        }

        public static bool EnumToBool(string Enum)
        {
            return Enum == "1";
        }

        public static string BoolToEnum(bool Bool)
        {
            return Bool ? "1" : "0";
        }

        public static int GetRandomNumber(int min, int max)
        {
            RandomBase quick = new Quick();
            return quick.Next(min, max);
        }

        public static double GetUnixTimestamp()
        {
            return (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }

        public static string FilterInjectionChars(string input)
        {
            return FilterInjectionChars(input, false);
        }

        public static string DoFilter(string Input, bool allowLineBreaks, bool bool_3)
        {
            Input = Input.Replace(Convert.ToChar(1), ' ');
            Input = Input.Replace(Convert.ToChar(2), ' ');
            Input = Input.Replace(Convert.ToChar(9), ' ');
            if (!allowLineBreaks)
            {
                Input = Input.Replace(Convert.ToChar(13), ' ');
            }
            if (bool_3)
            {
                Input = Input.Replace('\'', ' ');
            }
            return Input;
        }

        public static string FilterInjectionChars(string input, bool allowLinebreaks)
        {
            input = input.Replace(Convert.ToChar(1), ' ');
            input = input.Replace(Convert.ToChar(2), ' ');
            input = input.Replace(Convert.ToChar(3), ' '); // Was commented
            input = input.Replace(Convert.ToChar(9), ' ');

            if (!allowLinebreaks)
            {
                input = input.Replace(Convert.ToChar(13), ' ');
            }

            return input;
        }

        public static bool IsValidAlphaNumeric(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
            {
                return false;
            }

            return inputStr.All(t => char.IsLetter(t) || (char.IsNumber(t)));
        }

        public static ConfigurationData GetConfig()
        {
            return _configuration;
        }

        public static Logging GetLogging()
        {
            return _logging;
        }

        public static DatabaseManager GetDatabase()
        {
            return _databaseManager;
        }

        public static Encoding GetDefaultEncoding()
        {
            return _defaultEncoding;
        }

        public static ServerSocket GetConnectionManager()
        {
            return _connectionManager;
        }

        public static Game GetGame()
        {
            return Game;
        }

        public static void Destroy()
        {
            GetLogging().WriteLine("Destroying environment...");

            if (GetGame() != null)
            {
                GetGame().Destroy();
                Game = null;
            }

            if (GetConnectionManager() != null)
            {
                GetLogging().WriteLine("Destroying connection manager.");
                _connectionManager = null;
            }

            if (GetDatabase() != null)
            {
                GetLogging().WriteLine("Destroying database manager.");
                MySqlConnection.ClearAllPools();
                _databaseManager = null;
            }

            _logging.WriteLine("Uninitialized successfully. Closing.");

            Environment.Exit(0);
        }
    }
}