﻿using System.Collections.Generic;
using System.Text;
using Pylenor.Util;

namespace Pylenor.Messages
{
    internal class ServerPacket
    {
        private List<byte> _body;

        public ServerPacket(uint messageId)
        {
            Init(messageId);
        }

        public uint Id { get; private set; }

        public string Header => PylenorEnvironment.GetDefaultEncoding().GetString(Base64Encoding.EncodeUInt32(Id, 2));

        public int Length => _body.Count;

        public override string ToString()
        {
            return Header + PylenorEnvironment.GetDefaultEncoding().GetString(_body.ToArray());
        }

        public string ToBodyString()
        {
            return PylenorEnvironment.GetDefaultEncoding().GetString(_body.ToArray());
        }

        public void Clear()
        {
            _body.Clear();
        }

        public void Init(uint messageId)
        {
            Id = messageId;
            _body = new List<byte>();
        }

        public void AppendByte(byte b)
        {
            _body.Add(b);
        }

        public void AppendBytes(byte[] data)
        {
            if (data == null || data.Length == 0)
            {
                return;
            }

            _body.AddRange(data);
        }

        public void AppendString(string s, Encoding encoding)
        {
            if (string.IsNullOrEmpty(s))
            {
                return;
            }

            AppendBytes(encoding.GetBytes(s));
        }

        public void AppendString(string s)
        {
            AppendString(s, PylenorEnvironment.GetDefaultEncoding());
        }

        public void AppendStringWithBreak(string s)
        {
            AppendStringWithBreak(s, 2);
        }

        public void AppendStringWithBreak(string s, byte breakChar)
        {
            AppendString(s);
            AppendByte(breakChar);
        }

        public void AppendInt32(int i)
        {
            AppendBytes(WireEncoding.EncodeInt32(i));
        }

        public void AppendRawInt32(int i)
        {
            AppendString(i.ToString(), Encoding.ASCII);
        }

        public void AppendUInt(uint i)
        {
            AppendInt32((int)i);
        }

        public void AppendRawUInt(uint i)
        {
            AppendRawInt32((int)i);
        }

        public void AppendBoolean(bool Bool)
        {
            if (Bool)
            {
                _body.Add(WireEncoding.Positive);
                return;
            }

            _body.Add(WireEncoding.Negative);
        }

        public byte[] GetBytes()
        {
            var data = new byte[Length + 3];
            var header = Base64Encoding.EncodeUInt32(Id, 2);

            data[0] = header[0];
            data[1] = header[1];

            for (var i = 0; i < Length; i++)
            {
                data[i + 2] = _body[i];
            }

            data[data.Length - 1] = 1;

            return data;
        }
    }
}