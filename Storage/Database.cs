﻿namespace Pylenor.Storage
{
    internal class Database
    {
        public string DatabaseName;
        public uint PoolMaxSize;
        public uint PoolMinSize;

        public Database(string databaseName, uint poolMinSize, uint poolMaxSize)
        {
            DatabaseName = databaseName;

            PoolMinSize = poolMinSize;
            PoolMaxSize = poolMaxSize;
        }
    }
}