﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Pylenor.Storage
{
    internal sealed class DatabaseClient : IDisposable
    {
        private readonly MySqlCommand _command;
        private readonly MySqlConnection _connection;

        public DatabaseClient(DatabaseManager manager)
        {
            _connection = new MySqlConnection(manager.ConnectionString);
            _command = _connection.CreateCommand();
            _connection.Open();
        }

        public void Dispose()
        {
            _connection.Close();
            _command.Dispose();
            _connection.Dispose();
        }

        public void AddParamWithValue(string sParam, object val)
        {
            _command.Parameters.AddWithValue(sParam, val);
        }

        public void ExecuteQuery(string sQuery, int timeout = 30)
        {
            _command.CommandTimeout = timeout;
            _command.CommandText = sQuery;
            _command.ExecuteScalar();
            _command.CommandText = null;
        }

        public DataTable ReadDataTable(string query, int timeout = 30)
        {
            var dataTable = new DataTable();
            _command.CommandTimeout = timeout;
            _command.CommandText = query;
            using (var mySqlDataAdapter = new MySqlDataAdapter(_command))
            {
                mySqlDataAdapter.Fill(dataTable);
            }
            _command.CommandText = null;
            return dataTable;
        }

        public DataRow ReadDataRow(string query, int timeout = 30)
        {
            _command.CommandTimeout = timeout;
            var dataTable = ReadDataTable(query);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0];
            }
            return null;
        }

        public string ReadString(string query, int timeout = 30)
        {
            _command.CommandTimeout = timeout;
            _command.CommandText = query;
            var result = _command.ExecuteScalar().ToString();
            _command.CommandText = null;
            return result;
        }

        public int ReadInt32(string query, int timeout = 30)
        {
            _command.CommandTimeout = timeout;
            _command.CommandText = query;
            var result = int.Parse(_command.ExecuteScalar().ToString());
            _command.CommandText = null;
            return result;
        }
    }
}