﻿using System;

namespace Pylenor.Storage
{
    public class DatabaseException : Exception
    {
        public DatabaseException(string sMessage) : base(sMessage)
        {
        }
    }
}