﻿using MySql.Data.MySqlClient;

namespace Pylenor.Storage
{
    internal class DatabaseManager
    {
        public Database Database;
        public DatabaseServer Server;

        public DatabaseManager(DatabaseServer server, Database database)
        {
            Server = server;
            Database = database;
        }

        public string ConnectionString
        {
            get
            {
                var connString = new MySqlConnectionStringBuilder
                {
                    Server = Server.Hostname,
                    Port = Server.Port,
                    UserID = Server.Username,
                    Password = Server.Password,
                    Database = Database.DatabaseName,
                    MinimumPoolSize = Database.PoolMinSize,
                    MaximumPoolSize = Database.PoolMaxSize,
                    Pooling = true
                };
                return connString.ToString();
            }
        }

        public DatabaseClient GetClient()
        {
            return new DatabaseClient(this);
        }
    }
}