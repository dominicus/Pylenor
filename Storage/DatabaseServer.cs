﻿using System;

namespace Pylenor.Storage
{
    public sealed class DatabaseServer
    {
        public DatabaseServer(string hostname, uint port, string username, string password)
        {
            if (string.IsNullOrEmpty(hostname))
            {
                throw new ArgumentException("sHost");
            }
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("sUser");
            }
            Hostname = hostname;
            Port = port;
            Username = username;
            Password = (password ?? "");
        }

        public string Hostname { get; }

        public uint Port { get; }

        public string Username { get; }

        public string Password { get; }

        public override string ToString()
        {
            return Username + "@" + Hostname;
        }
    }
}