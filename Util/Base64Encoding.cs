﻿using System;

namespace Pylenor.Util
{
    public class Base64Encoding
    {
        public const byte Negative = 64;
        public const byte Positive = 65;

        public static byte[] EncodeInt32(int i, int numBytes)
        {
            var bzRes = new byte[numBytes];
            for (var j = 1; j <= numBytes; j++)
            {
                var k = ((numBytes - j)*6);
                bzRes[j - 1] = (byte) (0x40 + ((i >> k) & 0x3f));
            }

            return bzRes;
        }

        public static byte[] EncodeUInt32(uint i, int numBytes)
        {
            return EncodeInt32((int) i, numBytes);
        }

        public static int DecodeInt32(byte[] bzData)
        {
            var i = 0;
            var j = 0;
            for (var k = bzData.Length - 1; k >= 0; k--)
            {
                var x = bzData[k] - 0x40;
                if (j > 0)
                    x *= (int) Math.Pow(64.0, j);

                i += x;
                j++;
            }

            return i;
        }

        public static uint DecodeUInt32(byte[] bzData)
        {
            return (uint) DecodeInt32(bzData);
        }
    }
}