﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pylenor.Util
{
    internal class DictionaryAdapter<TKey, TValue> : IDisposable
    {
        /// <summary>
        ///     Start the streaming.
        /// </summary>
        /// <param name="dictionary"></param>
        public DictionaryAdapter(IDictionary<TKey, TValue> dictionary)
        {
            Dictionary = dictionary;
        }

        /// <summary>
        ///     Directory we work on.
        /// </summary>
        public IDictionary<TKey, TValue> Dictionary { get; private set; }

        /// <summary>
        ///     End of stream
        /// </summary>
        public void Dispose()
        {
            Dictionary = null;
        }

        /// <summary>
        ///     Tries to return the right value.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue TryPopValue(TKey key)
        {
            TValue output;
            Dictionary.TryGetValue(key, out output);
            return output;
        }

        /// <summary>
        ///     Tries to return the right Key.
        /// </summary>
        /// <returns></returns>
        public TKey TryPopKey(TValue value)
        {
            foreach (var kvp in Dictionary.Where(kvp => kvp.Value.Equals(value)))
            {
                return kvp.Key;
            }

            return default(TKey);
        }
    }
}