﻿using System;
using System.Linq;
using System.Text;

namespace Pylenor.Util
{
    public class HabboHexRc4
    {
        /* Class written by Nillus based on V8 class from Sulake
         * V7 key extracted by Mike
         * Temporarily 'init' fix by Mike
         * <3
         */

        private static readonly char[] HexalphabetChars =
        {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'
        };

        private byte _i;
        private byte _j;
        private byte[] _mTable;

        public HabboHexRc4(string sPublicKey)
        {
            var iKeyHash = CalculateKeyHash(sPublicKey);
            Initialize(iKeyHash);
        }

        public void Initialize(int iKeyHash)
        {
            // Reset indexes and table
            _i = 0;
            _j = 0;
            _mTable = new byte[256];

            _mTable[0] = 210;
            _mTable[1] = 13;
            _mTable[2] = 16;
            _mTable[3] = 171;
            _mTable[4] = 150;
            _mTable[5] = 141;
            _mTable[6] = 29;
            _mTable[7] = 249;
            _mTable[8] = 134;
            _mTable[9] = 233;
            _mTable[10] = 237;
            _mTable[11] = 56;
            _mTable[12] = 236;
            _mTable[13] = 129;
            _mTable[14] = 136;
            _mTable[15] = 54;
            _mTable[16] = 41;
            _mTable[17] = 64;
            _mTable[18] = 73;
            _mTable[19] = 240;
            _mTable[20] = 187;
            _mTable[21] = 113;
            _mTable[22] = 170;
            _mTable[23] = 35;
            _mTable[24] = 196;
            _mTable[25] = 38;
            _mTable[26] = 93;
            _mTable[27] = 60;
            _mTable[28] = 30;
            _mTable[29] = 46;
            _mTable[30] = 189;
            _mTable[31] = 8;
            _mTable[32] = 192;
            _mTable[33] = 94;
            _mTable[34] = 160;
            _mTable[35] = 133;
            _mTable[36] = 231;
            _mTable[37] = 44;
            _mTable[38] = 167;
            _mTable[39] = 39;
            _mTable[40] = 177;
            _mTable[41] = 70;
            _mTable[42] = 88;
            _mTable[43] = 152;
            _mTable[44] = 162;
            _mTable[45] = 62;
            _mTable[46] = 247;
            _mTable[47] = 53;
            _mTable[48] = 72;
            _mTable[49] = 111;
            _mTable[50] = 235;
            _mTable[51] = 77;
            _mTable[52] = 204;
            _mTable[53] = 103;
            _mTable[54] = 74;
            _mTable[55] = 191;
            _mTable[56] = 78;
            _mTable[57] = 118;
            _mTable[58] = 241;
            _mTable[59] = 6;
            _mTable[60] = 71;
            _mTable[61] = 203;
            _mTable[62] = 224;
            _mTable[63] = 207;
            _mTable[64] = 9;
            _mTable[65] = 232;
            _mTable[66] = 99;
            _mTable[67] = 175;
            _mTable[68] = 219;
            _mTable[69] = 107;
            _mTable[70] = 142;
            _mTable[71] = 185;
            _mTable[72] = 79;
            _mTable[73] = 43;
            _mTable[74] = 40;
            _mTable[75] = 148;
            _mTable[76] = 181;
            _mTable[77] = 125;
            _mTable[78] = 172;
            _mTable[79] = 21;
            _mTable[80] = 137;
            _mTable[81] = 200;
            _mTable[82] = 27;
            _mTable[83] = 174;
            _mTable[84] = 109;
            _mTable[85] = 126;
            _mTable[86] = 85;
            _mTable[87] = 59;
            _mTable[88] = 102;
            _mTable[89] = 108;
            _mTable[90] = 28;
            _mTable[91] = 97;
            _mTable[92] = 75;
            _mTable[93] = 0;
            _mTable[94] = 4;
            _mTable[95] = 120;
            _mTable[96] = 163;
            _mTable[97] = 153;
            _mTable[98] = 157;
            _mTable[99] = 229;
            _mTable[100] = 246;
            _mTable[101] = 124;
            _mTable[102] = 145;
            _mTable[103] = 190;
            _mTable[104] = 63;
            _mTable[105] = 164;
            _mTable[106] = 154;
            _mTable[107] = 96;
            _mTable[108] = 52;
            _mTable[109] = 222;
            _mTable[110] = 24;
            _mTable[111] = 131;
            _mTable[112] = 92;
            _mTable[113] = 47;
            _mTable[114] = 214;
            _mTable[115] = 86;
            _mTable[116] = 143;
            _mTable[117] = 138;
            _mTable[118] = 188;
            _mTable[119] = 206;
            _mTable[120] = 161;
            _mTable[121] = 14;
            _mTable[122] = 115;
            _mTable[123] = 132;
            _mTable[124] = 117;
            _mTable[125] = 144;
            _mTable[126] = 158;
            _mTable[127] = 104;
            _mTable[128] = 69;
            _mTable[129] = 135;
            _mTable[130] = 18;
            _mTable[131] = 165;
            _mTable[132] = 228;
            _mTable[133] = 1;
            _mTable[134] = 61;
            _mTable[135] = 50;
            _mTable[136] = 166;
            _mTable[137] = 87;
            _mTable[138] = 83;
            _mTable[139] = 182;
            _mTable[140] = 234;
            _mTable[141] = 23;
            _mTable[142] = 105;
            _mTable[143] = 49;
            _mTable[144] = 225;
            _mTable[145] = 230;
            _mTable[146] = 2;
            _mTable[147] = 213;
            _mTable[148] = 159;
            _mTable[149] = 238;
            _mTable[150] = 10;
            _mTable[151] = 57;
            _mTable[152] = 51;
            _mTable[153] = 195;
            _mTable[154] = 227;
            _mTable[155] = 37;
            _mTable[156] = 155;
            _mTable[157] = 95;
            _mTable[158] = 253;
            _mTable[159] = 173;
            _mTable[160] = 205;
            _mTable[161] = 15;
            _mTable[162] = 89;
            _mTable[163] = 116;
            _mTable[164] = 98;
            _mTable[165] = 7;
            _mTable[166] = 146;
            _mTable[167] = 194;
            _mTable[168] = 19;
            _mTable[169] = 33;
            _mTable[170] = 201;
            _mTable[171] = 180;
            _mTable[172] = 3;
            _mTable[173] = 55;
            _mTable[174] = 168;
            _mTable[175] = 42;
            _mTable[176] = 184;
            _mTable[177] = 100;
            _mTable[178] = 84;
            _mTable[179] = 123;
            _mTable[180] = 90;
            _mTable[181] = 198;
            _mTable[182] = 67;
            _mTable[183] = 183;
            _mTable[184] = 149;
            _mTable[185] = 119;
            _mTable[186] = 110;
            _mTable[187] = 248;
            _mTable[188] = 186;
            _mTable[189] = 58;
            _mTable[190] = 34;
            _mTable[191] = 221;
            _mTable[192] = 81;
            _mTable[193] = 80;
            _mTable[194] = 20;
            _mTable[195] = 106;
            _mTable[196] = 91;
            _mTable[197] = 244;
            _mTable[198] = 216;
            _mTable[199] = 66;
            _mTable[200] = 197;
            _mTable[201] = 151;
            _mTable[202] = 179;
            _mTable[203] = 68;
            _mTable[204] = 112;
            _mTable[205] = 122;
            _mTable[206] = 255;
            _mTable[207] = 12;
            _mTable[208] = 114;
            _mTable[209] = 211;
            _mTable[210] = 147;
            _mTable[211] = 208;
            _mTable[212] = 209;
            _mTable[213] = 242;
            _mTable[214] = 199;
            _mTable[215] = 217;
            _mTable[216] = 82;
            _mTable[217] = 251;
            _mTable[218] = 243;
            _mTable[219] = 121;
            _mTable[220] = 223;
            _mTable[221] = 193;
            _mTable[222] = 45;
            _mTable[223] = 218;
            _mTable[224] = 128;
            _mTable[225] = 101;
            _mTable[226] = 76;
            _mTable[227] = 178;
            _mTable[228] = 22;
            _mTable[229] = 202;
            _mTable[230] = 130;
            _mTable[231] = 139;
            _mTable[232] = 245;
            _mTable[233] = 48;
            _mTable[234] = 127;
            _mTable[235] = 36;
            _mTable[236] = 17;
            _mTable[237] = 226;
            _mTable[238] = 140;
            _mTable[239] = 26;
            _mTable[240] = 156;
            _mTable[241] = 176;
            _mTable[242] = 25;
            _mTable[243] = 254;
            _mTable[244] = 239;
            _mTable[245] = 11;
            _mTable[246] = 31;
            _mTable[247] = 32;
            _mTable[248] = 250;
            _mTable[249] = 215;
            _mTable[250] = 169;
            _mTable[251] = 212;
            _mTable[252] = 5;
            _mTable[253] = 65;
            _mTable[254] = 252;
            _mTable[255] = 220;
        }

        public byte MixTable()
        {
            // Re-calculate table fields
            _i = (byte) ((_i + 1)%256);
            _j = (byte) ((_j + _mTable[_i])%256);

            // Swap table fields
            var bSwap = _mTable[_i];
            _mTable[_i] = _mTable[_j];
            _mTable[_j] = bSwap;

            return _mTable[(_mTable[_i] + _mTable[_j])%256];
        }

        public byte[] Encipher(byte[] data)
        {
            var result = new byte[data.Length*2];

            for (int a = 0, b = 0; a < data.Length; a++, b += 2)
            {
                var k = MixTable();
                var c = data[a] & 0xff ^ k;
                if (c > 0)
                {
                    result[b] = (byte) HexalphabetChars[c >> 4 & 0xf];
                    result[b + 1] = (byte) HexalphabetChars[c & 0xf];
                }
            }

            return result;
        }

        public string Encipher(string sData)
        {
            var sbResult = new StringBuilder(sData.Length*2);
            foreach (var c in from data in sData let k = MixTable() select data & 0xff ^ k)
            {
                if (c > 0)
                {
                    sbResult.Append(HexalphabetChars[c >> 4 & 0xf]);
                    sbResult.Append(HexalphabetChars[c & 0xf]);
                }
                else
                {
                    sbResult.Append("00");
                }
            }

            return sbResult.ToString();
        }

        public byte[] Decipher(byte[] data, int length)
        {
            if (length%2 != 0)
                throw new HabboRc4Exception("Invalid input data, input data is not hexadecimal.");

            var result = new byte[length/2];
            for (int a = 0, b = 0; a < length; a += 2, b++)
            {
                var c = ConvertTwoHexBytesToByte(data[a], data[a + 1]);

                result[b] = (byte) (c ^ MixTable());
            }

            return result;
        }

        public string Decipher(string sData)
        {
            if (sData.Length%2 != 0)
                throw new HabboRc4Exception("Invalid input data, input data is not hexadecimal.");

            var sbResult = new StringBuilder(sData.Length);
            for (int a = 0, b = 0; a < sData.Length; a += 2, b++)
            {
                var c = ConvertTwoHexBytesToByte((byte) sData[a], (byte) sData[a + 1]);

                sbResult.Append((char) (c ^ MixTable()));
            }

            return sbResult.ToString();
        }

        public static string GeneratePublicKeyString()
        {
            var keyLength = new Random(DateTime.Now.Millisecond).Next(52, 64);
            var v = new Random(DateTime.Now.Millisecond + DateTime.Now.Second + keyLength);
            var sb = new StringBuilder(keyLength);

            for (var i = 0; i < keyLength; i++)
            {
                var j = v.Next(0, 2) == 1 ? v.Next(97, 123) : v.Next(48, 58);
                sb.Append((char) j);
            }

            return sb.ToString();
        }

        public static int CalculateKeyHash(string sPublicKey)
        {
            var iHash = 0;
            var sTable = sPublicKey.Substring(0, sPublicKey.Length/2);
            var sKey = sPublicKey.Substring(sPublicKey.Length/2);

            for (var i = 0; i < sTable.Length; i++)
            {
                var iIndex = sTable.IndexOf(sKey[i]);
                if (iIndex%2 == 0)
                    iIndex *= 2;
                if (i%3 == 0)
                    iIndex *= 3;
                if (iIndex < 0)
                    iIndex = sTable.Length%2;

                iHash += iIndex;
                iHash ^= iIndex << (i%3)*8;
            }

            return iHash;
        }

        private static byte ConvertTwoHexBytesToByte(byte a, byte b)
        {
            var c = 0; // The output value
            var d = 0; // Counter used for determining hex value

            while (d < HexalphabetChars.Length)
            {
                if (HexalphabetChars[d] == (a & 0xff))
                {
                    c = (d << 4);
                    break;
                }
                d++;
            }

            d = 0;
            while (d < HexalphabetChars.Length)
            {
                if (HexalphabetChars[d] == (b & 0xff))
                {
                    c += d;
                    break;
                }
                d++;
            }

            return (byte) c;
        }
    }

    public class HabboRc4Exception : Exception
    {
        #region Constructor

        public HabboRc4Exception(string sMessage) : base(sMessage)
        {
        }

        #endregion
    }
}