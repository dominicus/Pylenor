﻿using System;
using System.Text;

namespace Pylenor.Util
{
    public static class OldEncoding
    {
        #region Base64

        public static string EncodeB64(int value, int length)
        {
            var stack = "";
            for (var x = 1; x <= length; x++)
            {
                var offset = 6*(length - x);
                var val = (byte) (64 + (value >> offset & 0x3f));
                stack += (char) val;
            }
            return stack;
        }

        public static string EncodeB64(string data)
        {
            var value = data.Length;
            const int length = 2;
            var stack = "";
            for (var x = 1; x <= length; x++)
            {
                var offset = 6*(length - x);
                var val = (byte) (64 + (value >> offset & 0x3f));
                stack += (char) val;
            }
            return stack;
        }

        public static int DecodeB64(string data)
        {
            var val = data.ToCharArray();
            var intTot = 0;
            var y = 0;
            for (var x = (val.Length - 1); x >= 0; x--)
            {
                int intTmp = (byte) ((val[x] - 64));
                if (y > 0)
                {
                    intTmp = intTmp*(int) (Math.Pow(64, y));
                }
                intTot += intTmp;
                y++;
            }
            return intTot;
        }

        #endregion

        #region VL64

        public static string EncodeVl64(int i)
        {
            var wf = new byte[6];
            var pos = 0;
            var startPos = pos;
            var bytes = 1;
            var negativeMask = i >= 0 ? 0 : 4;
            i = Math.Abs(i);
            wf[pos++] = (byte) (64 + (i & 3));
            for (i >>= 2; i != 0; i >>= 6)
            {
                bytes++;
                wf[pos++] = (byte) (64 + (i & 0x3f));
            }

            wf[startPos] = (byte) (wf[startPos] | bytes << 3 | negativeMask);

            var encoder = new ASCIIEncoding();
            var tmp = encoder.GetString(wf);
            return tmp.Replace("\0", "");
        }

        public static int DecodeVl64(string data)
        {
            return DecodeVl64(data.ToCharArray());
        }

        public static int DecodeVl64(char[] raw)
        {
            try
            {
                var pos = 0;
                var negative = (raw[pos] & 4) == 4;
                var totalBytes = raw[pos] >> 3 & 7;
                var v = raw[pos] & 3;
                pos++;
                var shiftAmount = 2;
                for (var b = 1; b < totalBytes; b++)
                {
                    v |= (raw[pos] & 0x3f) << shiftAmount;
                    shiftAmount = 2 + 6*b;
                    pos++;
                }

                if (negative)
                    v *= -1;

                return v;
            }
            catch
            {
                return 0;
            }
        }

        #endregion
    }
}